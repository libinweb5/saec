<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demoweba_saecdb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|X<0 1Uvb?qX@Z:Mg]AMa%K@lTo5%>`M[Zqgov|cn~RsX}S=Uf],?;a{q_C1bOmL' );
define( 'SECURE_AUTH_KEY',  '?`C=}i#HDcCyOkr&/s7M<9=Vpl5It.x(I&B!pJ*|$>okMz<yLOZyX%?g}55_1{ix' );
define( 'LOGGED_IN_KEY',    ')4elc+UBA]2_)G[4BqgJ5uA$J>T]7O#Gf)<u:uMKjR}7pJfJ2S.BVlp$q`?lN%a.' );
define( 'NONCE_KEY',        '7 FoIxr_gE0@KT/EF)+Q[^][gk99`x8(B316-rqhJ%`w,[E#*V8Af*P*oT2b#V|W' );
define( 'AUTH_SALT',        '`27V*VW y+E?15APi5&3qlMVhAY* Yvg!i:%Qm@EO8@gj)HYQ*aole|ow%Bd T|y' );
define( 'SECURE_AUTH_SALT', '9T}L0. [ u(!67b@N;<%5y*]vl*qWN5zq&V70W:8hq7bB:dh-2,ojTcOZ4xKZjXT' );
define( 'LOGGED_IN_SALT',   '/NYLtD82}-OQc*l4)I{_@!YNXv&e:J/_|IKZT[ofXIK-:k^H7({8>l$H`-M>;93E' );
define( 'NONCE_SALT',       'CCS(I&C^KKenc[T2U2;I}]5.3{xx4]%]9[sy[C0|i<Z1} o=noreg5m%0m|t8pHu' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'saec_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG_DISPLAY', false);


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
