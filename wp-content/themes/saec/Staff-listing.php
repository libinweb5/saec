<?php

/**
  Template Name: Staff-listing
*/


get_header();
?>

    <section>
        <div class="container content-only">
            <h1 class="title_line">NAAC SSR</h1>
            
            <h3><a href="pdf/C2-2014-2015.pdf" target="_blank">2014-2015</a></h3>
            <h3><a href="pdf/C2-2015-2016.pdf" target="_blank">2015-2016</a></h3>
            <h3><a href="pdf/2016-2017.pdf" target="_blank">2016-2017</a></h3>
            <h3><a href="pdf/2017-2018.pdf" target="_blank">2017-2018</a></h3>
            <h3><a href="pdf/2018-2019.pdf" target="_blank">2018-2019</a></h3>
        </div>
    </section>
<?php
get_footer();
