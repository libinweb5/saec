<?php

/**
  Template Name: about us
*/


get_header();
?>


<section class="about_main">
  <div class="about_title">
    <div class="container">
      <h3><?php the_field('main_title');?></h3>
      <span><?php the_field('main_content');?></span>
    </div>
  </div>
  <div class="about_rank">
    <div class="container">
      <div class="about_rank_left">
        <span><strong>27</strong><sup>th</sup>
          <label>RANK</label>
        </span>
        <p>Among Outstanding
          Engineering Colleges<br />
          in India.
          <span>Competition Success Reviewed</span>
        </p>
      </div>
    </div>
    <div class="about_bg_img">
      <?php the_post_thumbnail()?>
    </div>
  </div>

  <div class="about_main_sub">
    <div class="container">
      <h3 class="inner_sub_hd"><?php the_field('our_speciality_title');?></h3>
      <p><?php the_field('speciality_content');?> </p>
    </div>
  </div>

  <div class="about_main_sub">
    <div class="container">
      <h3 class="inner_sub_hd"><?php the_field('history_title');?></h3>
      <p><?php the_field('history_content');?></p>
    </div>
  </div>
</section>


<section class="about_clg">
  <div class="container">
    <div class="about_clg_lft">
      <img src="<?php the_field('college_logo');?>" class="img-responsive" />
    </div>
    <div class="about_clg_rgt">
      <h3><?php the_field('college_name');?></h3>
      <p><?php the_field('college_details');?></p>
    </div>
  </div>
</section>



<section class="our_standards">
  <div class="container">
    <h3 class="inner_sub_hd"><?php the_field('standards_title');?></h3>
    <p><?php the_field('standards_detaiils');?></p>
  </div>
</section>

  <section class="clg_range">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 clg_range_box">
          <div>
            <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 23);?>">0</h2>
            <span class="plus-symbol">+</span>
            <span><?php the_field('student_placed_count_label_gallery', 23);?></span>
          </div>
        </div>

        <div class="col-sm-4 clg_range_box">
          <div>
            <h2><label class="counter"><?php the_field('university_rank_gallery', 23);?></label></h2>
            <span><?php the_field('university_rank_label_gallery', 23);?></span>
          </div>
        </div>

        <div class="col-sm-4 clg_range_box">
          <div>
            <h2><label class="counter"><?php the_field('square_feet_count_gallery', 23);?></label>L</h2>
            <span><?php the_field('square_feet_label_gallery', 23);?></span>
          </div>
        </div>
      </div>
    </div>
  </section>

<section class="board_trustes" id="board-trustes">
  <div class="container">
    <h2 class="title_line"><?php the_field('board_of_trustees_title');?></h2>
    <div class="board_navslide"></div>
    <div class="board_trustes_inner">
      <div class="board_trustes_slder owl-carousel">

        <?php if( have_rows('board_of_trustees') ): ?>
        <?php while( have_rows('board_of_trustees') ): the_row(); ?>
        <div class="board_trustes_box">
          <div class="trustes_img">
            <img src="<?php the_sub_field('trustes_image'); ?>" alt="" class="img-responsive">
          </div>
          <span><?php the_sub_field('trustes_name'); ?></span>
          <p><?php the_sub_field('trustes_designation'); ?></p>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>

      </div>
      <div class="vision_mision">
        <div class="row">
          <div class="col-sm-4">
            <h2 class="title_line"><?php the_field('vision_mission_title');?></h2>
          </div>
          <div class="col-sm-8">
            <p><?php the_field('vision_mission_content');?></p>
          </div>
        </div>
      </div>

    </div>

  </div>
</section>


<section class="directr_msg">
  <div class="container">
    <div class="directr_msg_img">
      <div class="directr_msg_img_inr">
        <img src="<?php the_field('director_image');?>" alt="" class="img-responsive">
        <h4><?php the_field('director_name');?></h4>
        <span><?php the_field('director_designation');?> </span>
      </div>
    </div>
    <div class="directr_msg_content">
      <h2 class="title_line"><?php the_field('block_head');?> </h2>
        
      <span></span>
      <p><?php the_field('message_from_director');?></p>
    </div>
  </div>
</section>
<?php
get_footer();
