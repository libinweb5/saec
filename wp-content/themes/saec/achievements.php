<?php

/**
  Template Name: achievements
*/

get_header();
?>

<section class="inner dept_intro">
  <div class="container">
    <h1><?php the_title(); ?></h1>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
    the_content();
    endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
  </div>
</section>

<section class="ach-wrap-content">
  <div class="container">
    <div class="grid-layout row">
      <?php $loop = new WP_Query( array( 'post_type' => 'achievements') ); ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="grid-item col-md-4 col-xs-6">
        <div>
          <div class="img-contain">
            <img src="<?php the_field('achievements_icon'); ?>">
          </div>
          <h3><?php the_title(); ?></h3>
          <p>
            <?php the_content(); ?>
          </p>
        </div>
      </div>
      <?php endwhile; wp_reset_query(); ?>
    </div>
  </div>
</section>


<?php
get_footer();
