<?php

/**
  Template Name: admission
*/

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<section class="admis_intro inner">
  <div class="container">
    <div class="row">
      <div class="col-xs-6 img-contain">
        <img src="<?php echo $image[0]; ?>" alt="SAEC - Admissions" />
      </div>
      <div class="col-xs-6 text-contain">
        <h1><?php the_field('admission_banner_title');?> <?php //echo date("Y"); ?></h1>
        <p><?php the_field('admission_banner_content');?></p>
        <a href="online-application" class="btn_web">apply now</a>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>

<section class="admis_steps_wrap inner">
  <div class="container">
    <div class="admis_steps_intro">
      <h2><?php the_field('steps_intro_title');?></h2>
      <p><?php the_field('steps_intro_content');?></p>
    </div>

    <div class="admis_steps step01">
      <span>01</span>
      <?php the_field('admission_procedure_steps_01');?>
    </div>

    <div class="admis_steps step02">
      <span>02</span>
      <?php the_field('admission_procedure_steps_2');?>
    </div>

      <div class="external_links admis_extlinks">
        <a href="online-application" class="btn_web">Admission - 2018 Apply Now</a>
        <label>Also check: </label>
        <ul class="share_icons clearfix">
          <li>
            <a href="placed-students-details">
              <i>
                <svg>
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#share-icon"></use>
                </svg>
              </i>
              <span>Placed Students</span>
            </a>
          </li>
          <li><a href="companies-visited">
              <i>
                <svg>
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#share-icon"></use>
                </svg>
              </i>
              <span>Companies Visited</span>
            </a>
          </li>
        </ul>
      </div>

  </div>
</section>

<section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 596);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 596);?></span>
        </div>
      </div>

      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 596);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 596);?></span>
        </div>
      </div>

      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 596);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 596);?></span>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
