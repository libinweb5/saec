<?php

/**
  Template Name: alumni-registration
*/

get_header();
?>

<section class="inner">
  <div class="container content-only alumini-reg">
    <div class="row">
      <div class="col-md-8 col-sm-10">
        <h2 class="title_line">Alumni Registration Form</h2>
        <div class="alumni-registration_form">
          <?php echo do_shortcode( '[contact-form-7 id="2641" title="Alumni-registration"]' ) ?>
        </div>
      </div>
    </div>
  </div>
</section>



<?php
get_footer();
