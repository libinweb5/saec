<?php

/**
  Template Name: audit-statements
*/


get_header();
?>

<section>
  <div class="container content-only">
    <h1 class="title_line">Audit Statements</h1>
    <?php if( have_rows('audit-statements') ): ?>
    <?php while( have_rows('audit-statements') ): the_row(); ?>
    <h3>
      <a href="<?php the_sub_field ('audit-statements_pdf');?>" target="_blank">
        <?php the_sub_field ('audit-statements_ttitle');?>
      </a></h3>
    <?php endwhile; ?>
    <?php endif; ?>
  </div>
</section>
<?php
get_footer();
