<?php

function add_theme_scripts() {
  wp_enqueue_style( 'style', get_stylesheet_uri() );
  wp_enqueue_style( 'plugins', get_template_directory_uri() . '/css/plugins.css', array(), '1.1', 'all');
  wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), '1.1', 'all');
  wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/style.css', array(), '1.1', 'all');
  wp_enqueue_style( 'new-style', get_template_directory_uri() . '/css/scss/new-style.css', array(), '1.1', 'all');
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


add_action( 'wp_enqueue_scripts', 'my_script_holder' );
function my_script_holder() {
  wp_register_script( 'head_script', get_template_directory_uri() . '/js/head.min.js', array('jquery'), 
       '', true );
  wp_enqueue_script( 'head_script');
}



function wpb_hook_javascript() {
  if (is_page ('about-us')) { 
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        fadeinup('.about_title', '.about_main');
        fadeinleft('.about_main .about_rank .about_rank_left', '.about_rank .container');
        fadeinright('.about_main .about_rank .about_bg_img', '.about_rank .container');

        var about_data = TweenMax.staggerFromTo('.about_main .about_rank .about_rank_left > *', 1, {
          y: 20
        }, {
          y: 0,
          opacity: 1,
          delay: 0.5
        }, 0.2);
        new ScrollMagic.Scene({
          triggerElement: '.about_rank .container',
          reverse: false
        }).setTween(about_data).addTo(controller);

        fadeinleft('.about_clg .about_clg_lft', 'section.about_clg');
        fadeinright('.about_clg .about_clg_rgt', 'section.about_clg');
        fadeinup('.our_standards .container > *', '.our_standards');

        var about_slider = TweenMax.staggerFromTo('.board_trustes_slder .board_trustes_box', 1, {
          x: 20
        }, {
          x: 0,
          opacity: 1
        }, 0.2);
        new ScrollMagic.Scene({
          triggerElement: '.clg_range',
          reverse: false
        }).setTween(about_slider).addTo(controller);


        fadeinleft('.directr_msg_img', '.directr_msg');
        fadeinright('.directr_msg_content', '.directr_msg');
        fadeinright('.board_trustes_inner .vision_mision .row > div', 'section.board_trustes');
      });

    </script>
    <?php
      }
  
    if (is_page ('departments')) { 
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        fadeinup('.dept_intro .container > *', '#main');
        fadeinleft('.dept_number .dept_number-img .overbox > div', '.dept_number');
        fadeinright('.dept_number .dept_number-img .underimg', '.dept_number');

        var dept_data = TweenMax.staggerFromTo('.dept_number .dept_number-img .overbox > div > *', 1, {
          y: 20
        }, {
          y: 0,
          opacity: 1,
          delay: 0.5
        }, 0.2);
        new ScrollMagic.Scene({
          triggerElement: '.dept_number',
          reverse: false
        }).setTween(dept_data).addTo(controller);

        var dept_ug = TweenMax.staggerFromTo('.dept_courses div.dept_ugcourses .course-box > div', 1, {
          y: 20
        }, {
          y: 0,
          opacity: 1
        }, 0.3);
        new ScrollMagic.Scene({
          triggerElement: '.dept_courses > div > div.dept_ugcourses',
          reverse: false
        }).setTween(dept_ug).addTo(controller);

        var dept_pg = TweenMax.staggerFromTo('.dept_courses div.dept_pgcourses .course-box > div', 1, {
          y: 20
        }, {
          y: 0,
          opacity: 1
        }, 0.3);
        new ScrollMagic.Scene({
          triggerElement: '.dept_courses > div > div.dept_pgcourses',
          reverse: false
        }).setTween(dept_pg).addTo(controller);
      });

    </script>
    <?php
      }
  
  
    if (is_page ('placements')) {
    ?>
    <script type="text/javascript">
      jQuery(window).load(function() {
        fadeinleft('.placements_training .container > div:not(.placement_bg_img)', '#main');
        fadeinup('.our_recruiters .our_recruiters_sldr .our_recruiters_slide', '.our_recruiters');
        fadeinup('.placements_training .placements_training_lft .placed_count div > *', '#main');
        fadeinup('.major_fnctin .tic_list li', '.major_fnctin');
        fadeinup('.strngth_plcmnt_ofc > .container > div:not(.external_links) > *', '.strngth_plcmnt_ofc');
      });

    </script>
    <?php
      }
  
      if (is_page ('campus-life')) {
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        fadeinup('.campuslife-intro .container > *:not(.text-wrapper)', '#main');
        fadein('.camp-virt-tour', '.camp-virt-tour');
        fadeinup('.camp-virt-tour .campvirt-text span', '.camp-virt-tour');
        fadeinup('.camp-virt-tour .campvirt-text:last-of-type > *', '.camp-virt-tour');
        fadeinup('.campus_activtis .col-sm-4', '.campus_activtis');
      });

    </script>
    <?php
      }
  
  if (is_page ('contact-us')) {
    ?>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      fadeinleft('.contact_sec .container .row > div:first-of-type', '.contact_sec');
      fadeinright('.contact_sec .container .row > div:last-of-type', '.contact_sec');
    });

  </script>
  <?php
    }

    if (is_page ('admission')) {
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        var divlength = $('.admis_steps_wrap .container > div').length;
        fadeinup('.admis_intro .text-contain > *', '#main');
        fadeinright('.admis_intro .img-contain', '#main');
        for (var i = 1; i < divlength; i++) {
          fadeinup('.admis_steps_wrap .container > div:nth-of-type(' + i + ') > *', '.admis_steps_wrap  .container > div:nth-of-type(' + i + ')');
        }
      });

    </script>
    <?php
      }

  
}

add_action('wp_head', 'wpb_hook_javascript');

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
      'top-head-menu' => __( 'Top head Menu' ),
      'footer-menu' => __( 'Footer Menu' ),
      'footer-bottom-menu' => __( 'Footer Bottom Menu' )
    )
  );
}

add_action( 'init', 'register_my_menus' );

//google map


//function my_acf_init() {
//	
//	acf_update_setting('google_api_key', 'AIzaSyAHWM3sMRD05PQk_wVExFjxRwXEPJsDLoo');
//}

add_action('acf/init', 'my_acf_init');

//Enqueue the Dashicons script
add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
wp_enqueue_style( 'dashicons' );
}


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'SAEC options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

	
}

/**
* Adding extended support for the uploads format
*/

add_filter('upload_mimes', 'enable_extended_upload');
if(!function_exists("enable_extended_upload")) {
 function enable_extended_upload ( $mime_types =array() ) {
   $mime_types['webp']  = 'image/webp';
   $mime_types['svg']  = 'image/svg+xml';
   return $mime_types;
 }
}


function cptui_register_my_cpts_news_events() {

	/**
	 * Post Type: News-Events.
	 */

	$labels = array(
		"name" => __( "News-Events", "saectheme" ),
		"singular_name" => __( "News-events", "saectheme" ),
	);

	$args = array(
		"label" => __( "News-Events", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "news_events", "with_front" => true ),
		"query_var" => true,
		'menu_icon' => 'dashicons-visibility',
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "news_events", $args );
}

add_action( 'init', 'cptui_register_my_cpts_news_events' );



function cptui_register_my_cpts_achievements() {

	/**
	 * Post Type: Achievements.
	 */

	$labels = array(
		"name" => __( "Achievements", "saectheme" ),
		"singular_name" => __( "Achievements", "saectheme" ),
	);

	$args = array(
		"label" => __( "Achievements", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "achievements", "with_front" => true ),
		"query_var" => true,
    'menu_icon' => 'dashicons-businessman',
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "achievements", $args );
}

add_action( 'init', 'cptui_register_my_cpts_achievements' );

function cptui_register_my_cpts_testimonials() {

	/**
	 * Post Type: Testimonials.
	 */

	$labels = array(
		"name" => __( "Testimonials", "saectheme" ),
		"singular_name" => __( "testimonials", "saectheme" ),
	);

	$args = array(
		"label" => __( "Testimonials", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
    'menu_icon' => 'dashicons-format-quote',
		"rewrite" => array( "slug" => "testimonials", "with_front" => true ),
		"query_var" => true,
    
	);
  
  add_theme_support( 'post-thumbnails' );
	register_post_type( "testimonials", $args );
}

add_action( 'init', 'cptui_register_my_cpts_testimonials' );

function cptui_register_my_cpts_gallery() {

	/**
	 * Post Type: Gallery.
	 */

	$labels = array(
		"name" => __( "Gallery", "saectheme" ),
		"singular_name" => __( "Gallery", "saectheme" ),
	);

	$args = array(
		"label" => __( "Gallery", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "gallery", "with_front" => true ),
		"query_var" => true,
    'menu_icon' => 'dashicons-format-gallery',
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "gallery", $args );
}

add_action( 'init', 'cptui_register_my_cpts_gallery' );

function cptui_register_my_cpts_ug_programmes() {

	/**
	 * Post Type: UG Programmes.
	 */

	$labels = array(
		"name" => __( "UG Programmes", "saectheme" ),
		"singular_name" => __( "UG Programmes", "saectheme" ),
	);

	$args = array(
		"label" => __( "UG Programmes", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "ug_programmes", "with_front" => true ),
		"query_var" => true,
    'menu_icon' => 'dashicons-groups',
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "ug_programmes", $args );
}

add_action( 'init', 'cptui_register_my_cpts_ug_programmes' );

function cptui_register_my_cpts_pg_programmes() {

	/**
	 * Post Type: PG Programmes.
	 */

	$labels = array(
		"name" => __( "PG Programmes", "saectheme" ),
		"singular_name" => __( "PG Programmes", "saectheme" ),
	);

	$args = array(
		"label" => __( "PG Programmes", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "pg_programmes", "with_front" => true ),
		"query_var" => true,
    'menu_icon' => 'dashicons-buddicons-buddypress-logo',
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "pg_programmes", $args );
}

add_action( 'init', 'cptui_register_my_cpts_pg_programmes' );




function cptui_register_my_cpts_resource() {

	/**
	 * Post Type: Resource.
	 */

	$labels = array(
		"name" => __( "Resource", "saectheme" ),
		"singular_name" => __( "Resource", "saectheme" ),
	);

	$args = array(
		"label" => __( "Resource", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
    'menu_icon' => 'dashicons-visibility',
		"rewrite" => array( "slug" => "resource", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "resource", $args );
}

add_action( 'init', 'cptui_register_my_cpts_resource' );

function my_login_logo_one() { 
?>
<style type="text/css">
  body.login div#login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri();
    ?>/images/full-logo-wp.png);
    height: 45px !important;
    background-size: contain !important;
    width: 100% !important;
  }

</style>
<?php 
} add_action( 'login_enqueue_scripts', 'my_login_logo_one' );



?>
