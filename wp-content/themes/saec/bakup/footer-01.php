<?php include('config.php'); ?>
<footer>
  <span class="backtotop">
    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/scroll-top-icon.svg"></a>
  </span>
  <div class="footer_sec">
    <div class="container">
      <div class="row">
        <div class="footer_sec_top">
          <div class="col-sm-9 clearfix">
            <ul>
              <li><a href="pdf/saec.pdf" target="_blank">
                  <i><svg>
                      <use xlink:href="#dwnload_icon"></use>
                    </svg>
                  </i>
                  DOWNLOAD <br />
                  COLLEGE PROSPECTUS</a></li>
              <li><a href="online-application"><i><svg>
                      <use xlink:href="#bcome_icon"></use>
                    </svg></i>BECOME A <br />
                  STUDENT AT SAEC</a></li>
            </ul>
          </div>

          <div class="col-sm-3 clearfix">
            <div class="subscriber_box">
              <p><?php _e('Subscribe Newsletter') ?></p>
              <?php echo do_shortcode( '[mc4wp_form id="379"]' ); ?>

              <!--  <?php echo do_shortcode(get_field('newsletter_short_code')); ?>  -->
            </div>
          </div>
        </div>


        <div class="footer_sec_btm">
          <div class="col-sm-9 clearfix">
            <div class="footer_sec_btm_box footer-menu-wrap">
              <?php
              wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => false ) );
            ?>
            </div>

          </div>
          <div class="col-sm-3 clearfix">
            <!-- Address-->
            <p><?php the_field('address', 'option'); ?></p>

            <!-- social media-->
            <?php if( have_rows('social_media', 'option') ): ?>
            <ul class="footr_social">
              <label> <?php _e('Follow:') ?></label>
              <?php while( have_rows('social_media', 'option') ): the_row(); ?>
              <li>
                <a href="<?php the_sub_field('social_media_link'); ?>" target="_blank">
                  <img src="<?php the_sub_field('social_media_image'); ?>" alt="<?php the_sub_field('alt'); ?>" />
                </a>
              </li>
              <?php endwhile; ?>
            </ul>
            <?php endif; ?>

          </div>

        </div>

      </div>

    </div>
  </div>

  <div class="btm_footr">
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <?php
              wp_nav_menu( array( 'theme_location' => 'footer-bottom-menu', 'container' => false ) );
            ?>
        </div>
        <div class="col-sm-5">
          <p> <?php _e('Designed by:') ?><a href="http://webandcrafts.com/" target="_blank"><?php _e('webandcrafts.com:') ?> </a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
</div>
<div class="web-modal fade" id="mediamodal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <video autoplay="true" controls></video>
      </div>
    </div>

  </div>
</div>

<div class="org_modal web-modal fade" id="car_modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <!-- <h3 class="inner_sub_hd">Apply Now</h3> -->
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/organizational-chart.jpg" class="img-responsive">
      </div>
    </div>
  </div>
</div>

<!-- load libraries -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.hoverdir.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js'></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.selectric.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/masonry.pkgd.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/global.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/common.js"></script>
<!--animation plugins -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/TweenMax.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/ScrollMagic.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/animation.gsap.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/animation.js"></script>
<!--<script type="text/javascript" src="https://maps.google.com/maps/api/jskey=AIzaSyAHWM3sMRD05PQk_wVExFjxRwXEPJsDLoo"></script>-->

<script>
  $('.counter').each(function() {
    var $this = $(this),
      countTo = $this.attr('data-count');

    $({
      countNum: $this.text()
    }).animate({
        countNum: countTo
      },

      {

        duration: 8000,
        easing: 'linear',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
          //alert('finished');
        }
      });
  });

  $(document).ready(function() {
    fadeinup('.gallery_page_title > *', '#main');
    fadeinup('.gallery_page .gallery_sec ul li', '#main');
  });


  $(document).ready(function() {
    var divlength = $('.res_course-wrap > div').length;
    for (var i = 1; i <= divlength; i++) {
      if (i % 2 == 1) {
        fadeinleft('.res_course-wrap > div:nth-of-type(' + i + ') .container > .img-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
        fadeinright('.res_course-wrap > div:nth-of-type(' + i + ') .container > .text-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
      } else if (i % 2 == 0) {
        fadeinright('.res_course-wrap > div:nth-of-type(' + i + ') .container > .img-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
        fadeinleft('.res_course-wrap > div:nth-of-type(' + i + ') .container > .text-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
      }
    }
  });

//  $(document).ready(function() {
//    function init_map() {
//      var myOptions = {
//        zoom: 14,
//        center: new google.maps.LatLng(13.065630, 80.110942),
//
//        mapTypeId: google.maps.MapTypeId.ROADMAP
//      };
//
//      map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
//      marker = new google.maps.Marker({
//        map: map,
//        position: new google.maps.LatLng(13.065630, 80.110942)
//
//      });
//      infowindow = new google.maps.InfoWindow({
//        // content:"<b>Megamind</b><br>No 70, 1st Cross,<br>HAL 3rd stage, New Thippasandra,<br>Indiranagar, Bengaluru - 560075"
//      });
//      /*google.maps.event.addListener(marker, "click", function(){
//       infowindow.open(map,marker);
//       });
//       infowindow.open(map,marker);*/
//    }
//    google.maps.event.addDomListener(window, 'load', init_map);
//  });

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  /* Validation Events for changing response CSS classes */
  document.addEventListener('wpcf7invalid', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-danger');
  }, false);
  document.addEventListener('wpcf7spam', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-warning');
  }, false);
  document.addEventListener('wpcf7mailfailed', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-warning');
  }, false);
  document.addEventListener('wpcf7mailsent', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-success');
  }, false);


  //Inner page slider
  $(document).ready(function() {
    $(document).on('click', '#listmenu6', function() {

      setTimeout(function() {
        $('.board_trustes_slder_hh1').owlCarousel({
          loop: true,
          items: 6,
          margin: 30,
          nav: true,
          dots: false,
          autoplay: true,
          autoplayTimeout: 3000,
          smartSpeed: 900,
          autoplaySpeed: 900,
          autoplayHoverPause: true,
          navContainer: '.board_navslide',
          responsive: {
            0: {
              items: 1,
              mouseDrag: true,
              autoplay: true,
              loop: true,
              dots: true
            },
            700: {
              items: 2,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1200: {
              items: 3,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1440: {
              items: 4,
              mouseDrag: true,
              autoplay: true,
              loop: true
            }
          }
        });
      }, 150);
    });

  });
  $(document).ready(function() {
    $(document).on('click', '#listmenu5', function() {
      setTimeout(function() {
        $('.board_trustes_slder_hh1').owlCarousel({
          loop: true,
          items: 6,
          margin: 30,
          nav: true,
          dots: false,
          autoplay: true,
          autoplayTimeout: 3000,
          smartSpeed: 900,
          autoplaySpeed: 900,
          autoplayHoverPause: true,
          navContainer: '.board_navslide',
          responsive: {
            0: {
              items: 1,
              mouseDrag: true,
              autoplay: true,
              loop: true,
              dots: true
            },
            700: {
              items: 2,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1200: {
              items: 3,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1440: {
              items: 4,
              mouseDrag: true,
              autoplay: true,
              loop: true
            }
          }
        });
      }, 150);
    });

  });

  //  fadeinup('.dept_intro .container > *', '.dept_intro');

  $(document).ready(function() {
    $(document).ajaxStart(function() {
      $('.svg-loader').show();
    }).ajaxStop(function() {
      $('.svg-loader').hide();
    });

    $(document).ready(function() {
      $(document).ajaxStart(function() {
        $('.svg-loader').show();
      }).ajaxStop(function() {
        $('.svg-loader').hide();
      });

      // Get the modal
      var modal = document.getElementById('gen_car_modal');

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          $('.apply-form-error-msg').removeClass('error');
          $('#gen_career_form')[0].reset();
        }
      }

      // General Career Form

      var gen_career_form_data = {};

      $(document).on('click', '#gen_career_form input[type = "submit"]', function(e) {

        var count = 0;
        gen_career_form_data.name = $('#gen_name').val();
        gen_career_form_data.email = $('#gen_email').val();
        // gen_career_form_data.phone = $('#gen_phone').val();
        gen_career_form_data.fileselect = $('#gen_fileselect_hidden').val();

        for (var key in gen_career_form_data) {
          if (gen_career_form_data[key] == null || gen_career_form_data[key] == '' || gen_career_form_data[key] == 'undefined') {
            $('.gen_' + key + ' .apply-form-error-msg').addClass('error');
          }
        }
        for (var key in gen_career_form_data) {
          if (!(gen_career_form_data[key] == null || gen_career_form_data[key] == '' || gen_career_form_data[key] == 'undefined')) {
            count = count + 1;
            $('.gen_' + key + ' .apply-form-error-msg').removeClass('error');
          }
        }
        if (!validateEmail(gen_career_form_data.email)) {
          $('.gen_email .apply-form-error-msg').addClass('error');
        }
        var phone = $('#gen_phone').val();
        var phone_length = phone.length;
        if (phone_length < 10) {
          $('.gen_phone .apply-form-error-msg').addClass('error');
        }
        if (phone_length == 10) {
          $('.gen_phone .apply-form-error-msg').removeClass('error');
          count = count + 1;
        }

        if (count == 4 && validateEmail(gen_career_form_data.email)) {

          $('.gen_svg-loader').show();
          $('.apply-form-error-msg').removeClass('error');
          $.ajax({
            type: 'POST',
            url: 'sendmail.php',
            async: true,
            data: $('#gen_career_form').serialize(),
            success: function() {
              $('.gen_thank-you').addClass('active');
              $('#gen_career_form')[0].reset();
              $('.gen_fileselect p').html('No file choosen');
              $('.gen_svg-loader').hide();
            },
            error: function(res) {
              console.log(res);
            }

          });
        }
      });


      $("#gen_career_form").submit(function(e) {
        return false;
      });

      function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
      }

      $(document).on('change', '#gen_fileselect', function(e) {
        var file_data = $('#gen_fileselect').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);


        $.ajax({
          url: 'document-upload.php',
          type: 'post',
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,

        }).done(function(data) {

          if (data !== 'error') {

            $('#gen_fileselect_hidden').val(data);

          }
        }).fail(function(data) {

        });

      });

    });



    //Each Career Form 

    $(document).on('click', '.recr_tab .tab-content-wrap .tab-content .recr_box .col-sm-4 .btn_web', function() {
      var post = $(this).attr('data-url');
      $('#car_modal .job-post input').attr('placeholder', post);
      $('#car_modal .job-post input').attr('value', post);

    });

    var career_form_data = {};

    $(document).on('click', '#career_form input[type = "submit"]', function(e) {

      var count = 0;
      career_form_data.name = $('#name').val();
      career_form_data.email = $('#email').val();
      career_form_data.phone = $('#phone').val();
      career_form_data.fileselect = $('#fileselect').val();

      for (var key in career_form_data) {
        if (career_form_data[key] == null || career_form_data[key] == '' || career_form_data[key] == 'undefined') {
          $('.' + key + ' .apply-form-error-msg').addClass('error');
        }
      }
      for (var key in career_form_data) {
        if (!(career_form_data[key] == null || career_form_data[key] == '' || career_form_data[key] == 'undefined')) {
          count = count + 1;
        }
      }
      if (!validateEmail(career_form_data.email)) {
        $('.email .apply-form-error-msg').addClass('error');
      }


      if (count == 4 && validateEmail(career_form_data.email)) {

        $('.svg-loader').show();
        $('.apply-form-error-msg').removeClass('error');
        $.ajax({
          type: 'POST',
          url: 'sendmail.php',
          async: true,
          data: $('#career_form').serialize(),
          success: function() {
            $('.thank-you').addClass('active');
            $('#career_form')[0].reset();
            $('.fileselect p').html('No file choosen');
          },
          error: function(res) {
            console.log(res);
          }

        });
      }
    });


    $("#career_form").submit(function(e) {
      return false;
    });

    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test($email);
    }

    $(document).on('change', '#fileselect', function(e) {
      var file_data = $('#fileselect').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);


      $.ajax({
        url: 'document-upload.php',
        type: 'post',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,

      }).done(function(data) {

        if (data !== 'error') {

          $('#fileselect_hidden').val(data);

        }
      }).fail(function(data) {});
    });

  });


  //Number validate

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  function getPostTitle(title) {
    $("#post_ct").val(title);
  }

</script>
<script>
  $(document).ready(function() {
    var totselector = $("form.webform .form-group input[required='required'],form.webform .form-group select[required='required'], form.webform .form-group textarea[required='required']");

    totselector.parents('.form-group').addClass('requiredfield');

    totselector.change(function() {
      $(this).parents('.form-group').addClass('valchanged');
      if ($(this).val() == "") {
        $(this).parents('.form-group').removeClass('valchanged');
      }
      var tabindex = $(this).parents('.tab-content').index();
      var changelen = $(this).parents('.tab-content').find('.valchanged').length;
      var totallen = $(this).parents('.tab-content').find('.requiredfield').length;

      var totpercent = ((changelen / totallen) * 100) + "%";
      $('.applynow-wrap .tab-menu li').eq(tabindex).find('.progress').css('width', totpercent);

    });

    //  Setting Branch and Course using url

    var url = document.URL;
    var urlcourse = url.split('?').pop();
    var urldept = urlcourse.split('=');

    $('#ugselect option').each(function() {

      var attrValue = $(this).val();
      if (attrValue == urldept[0]) {
        $('#ugselect option').removeAttr('selected');
        $(this).attr("selected", "selected");
        var msg = $(this).html();
        $('.select_degree .selectric .label').html(msg);
      }
    });


    jQuery(function($) {
      $.ajax({

        url: 'getbranch.php',
        type: 'POST',
        data: {
          branch: urldept[0]
        },
        success: function(msg) {
          $('#course_select').html(msg);
          $('#course_select').selectric('refresh');
          $('#course_select option').each(function() {

            var attrValue = $(this).attr('data-url');
            if (attrValue == urldept[1]) {
              $('#course_select option').removeAttr('selected');
              $(this).attr("selected", "selected");
              var msg = $(this).html();
              $('.select_branch .selectric .label').html(msg);

            }
          });
        }
      });
    });
  });

  //Number check

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  //Setting Branch and Course

  function getbranch(s_branch) {
    jQuery(function($) {
      $.ajax({
        url: 'getbranch.php',
        type: 'POST',
        data: {
          branch: s_branch
        },
        success: function(msg) {
          $('#course_select').html(msg);
          $('#course_select').selectric('refresh');
        }
      });
    });
  }

  $(document).on('change', '#fileselect', function(e) {
    var file_data = $('#fileselect').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);


    $.ajax({
      url: 'image-upload.php',
      type: 'post',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,

    }).done(function(data) {

      if (data !== 'error') {

        $('#fileselect_hidden').val(data);

      }
    }).fail(function(data) {

    });

  });



  //Form Data Collection

  var form_data = {};
  var form_data_notreq = {};


  //Form Data Menu 1

  $(document).on("click", "#menu1-next-button", function() {
    var menu1_count = 0;
    form_data.select_degree = $('#ugselect').val();
    // form_data.select_file = $('#fileselect_hidden').val();
    form_data.select_course_type = $('.select_course_type input:checked').val();
    form_data.select_year = $('#year-select').val();
    form_data.select_branch = $('#course_select').val();

    for (var key in form_data) {
      if (form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined' || form_data[key] == 'off') {
        $('.' + key + ' .apply-form-error-msg').addClass('error');
      }
    }
    for (var key in form_data) {
      if (!(form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined')) {
        menu1_count = menu1_count + 1;
        $('.' + key + ' .apply-form-error-msg').removeClass('error');
      }
    }
    if (menu1_count >= 4) {
      $('.applynow-wrap li').removeClass('active');
      $('.applynow-wrap li:nth-child(2)').addClass('active');
      $('.tab-content').removeClass('in active');
      $('#menu2').addClass('in active');

    }

  });

  //Menu 2 Prev click


  $(document).on('click', '#menu2-prev-button', function() {
    $('.applynow-wrap li').removeClass('active');
    $('.applynow-wrap li:nth-child(1)').addClass('active');
    $('.tab-content').removeClass('in active');
    $('#menu1').addClass('in active');
  });



  //Form Data Menu 2

  $(document).on('click', '#menu2-next-button', function() {
    var menu2_count = 0;
    form_data.aplicants_name = $('#nametext').val();
    form_data.dob_date = $('#dob_date').val();
    form_data.dob_month = $('#dob_month').val();
    form_data.dob_year = $('#dob_year').val();
    form_data.blood_group = $('#bloodgselect').val();
    form_data.select_mother_toung = $('#mothtongselect').val();
    form_data.select_religion = $('#relegselect').val();
    form_data.select_cast = $('#castselect').val();
    form_data.select_comminity = $('#communityselect').val();
    form_data.father_name = $('#fathsnametext').val();
    form_data.father_occupation = $('#fathsoccuptext').val();
    form_data.mother_name = $('#mothsnametext').val();
    form_data.mother_occupation = $('#mothsoccuptext').val();
    form_data_notreq.age = $('#agetext').val();
    form_data_notreq.select_reserved_category = $('#rescatselect').val();
    form_data_notreq.select_handicapped = $('.physically_handicapped input:checked').val();
    form_data_notreq.father_income = $('#fathsinctext').val();
    form_data_notreq.mother_income = $('#mothsinctext').val();
    form_data_notreq.guarian_name = $('#guardnamtext').val();
    form_data_notreq.guarian_occupation = $('#guardoccuptext').val();
    form_data_notreq.guarian_income = $('#guardinctext').val();
    var dob = form_data.dob_date + '-' + form_data.dob_month + '-' + form_data.dob_year;

    for (var key in form_data) {
      if (form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined' || form_data[key] == 'off') {
        $('.' + key + ' .apply-form-error-msg').addClass('error');
      }
    }

    for (var key in form_data) {
      if (!(form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined')) {
        menu2_count = menu2_count + 1;
        $('.' + key + ' .apply-form-error-msg').removeClass('error');
      }
    }

    if (menu2_count >= 18) {
      $('.applynow-wrap li').removeClass('active');
      $('.applynow-wrap li:nth-child(3)').addClass('active');
      $('.tab-content').removeClass('in active');
      $('#menu3').addClass('in active');
    }

  });


  //Menu 3 Prev click 

  $(document).on('click', '#menu3-prev-button', function() {
    $('.applynow-wrap li').removeClass('active');
    $('.applynow-wrap li:nth-child(2)').addClass('active');
    $('.tab-content').removeClass('in active');
    $('#menu2').addClass('in active');
    alert('menu2');
  });


  // Form Data Menu 3

  $(document).on('click', '#menu3-next-button', function() {

    var menu3_count = 0;
    form_data.nationtext = $('#nationtext').val();
    form_data.native_place = $('#native_place').val();
    form_data.civicstattext = $('#civicstattext').val();
    form_data.stateselect = $('#stateselect').val();
    form_data.phonetext = $('#phonetext').val();
    form_data.emailtext = $('#emailtext').val();
    form_data.perma_addrline1text = $('.permanent-address #addrline1text').val();
    form_data.perma_addrline2text = $('.permanent-address #addrline2text').val();
    form_data.perma_citytext = $('.permanent-address #citytext').val();
    form_data.perma_pincodetext = $('.permanent-address #pincodetext').val();
    form_data.perma_addressphonetext = $('.permanent-address #addressphonetext').val();
    form_data.diff_addrline1text = $('.different-address #addrline1text').val();
    form_data.diff_addrline2text = $('.different-address #addrline2text').val();
    form_data.diff_citytext = $('.different-address #citytext').val();
    form_data.diff_pincodetext = $('.different-address #pincodetext').val();
    form_data.diff_addressphonetext = $('.different-address #addressphonetext').val();

    for (var key in form_data) {
      if (form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined' || form_data[key] == 'off') {
        $('.' + key + ' .apply-form-error-msg').addClass('error');
      }
    }
    var comm_add = $('.communication_adress input:checked').val();

    for (var key in form_data) {
      if (!(form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined')) {
        menu3_count = menu3_count + 1;
        $('.' + key + ' .apply-form-error-msg').removeClass('error');
      }
    }
    if (comm_add == 'no') {
      menu3_count = menu3_count + 1;
    }
    if ((menu3_count >= 29 || menu3_count >= 35) && validateEmail(form_data.emailtext)) {
      $('.applynow-wrap li').removeClass('active');
      $('.applynow-wrap li:nth-child(4)').addClass('active');
      $('.tab-content').removeClass('in active');
      $('#menu4').addClass('in active');
    }
    if (!validateEmail(form_data.emailtext)) {
      $('.email .apply-form-error-msg').addClass('error');
    }

    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test($email);
    }

  });


  //Menu 4 Prev click

  $(document).on('click', '#menu4-prev-button', function() {
    $('.applynow-wrap li').removeClass('active');
    $('.applynow-wrap li:nth-child(3)').addClass('active');
    $('.tab-content').removeClass('in active');
    $('#menu3').addClass('in active');
  });



  //Form Data Menu 4

  $(document).on('click', '#form-submit-button', function() {
    var menu4_count = 0;
    form_data.availconcess = $('.availconcess input:checked').val();
    form_data.tcnumtext = $('#tcnumtext').val();
    form_data.tc_date = $('#tc_date').val();
    form_data.tc_month = $('#tc_month').val();
    form_data.tc_year = $('#tc_year').val();
    form_data.qualifyexamtext = $('#qualifyexamtext').val();
    form_data.instrmedtext = $('#instrmedtext').val();
    for (var key in form_data) {
      if (form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined' || form_data[key] == 'off') {
        $('.' + key + ' .apply-form-error-msg').addClass('error');
      }
    }
    for (var key in form_data) {
      if (!(form_data[key] == null || form_data[key] == '' || form_data[key] == 'undefined')) {
        menu4_count = menu4_count + 1;
        $('.' + key + ' .apply-form-error-msg').removeClass('error');
      }
    }

    form_data_notreq.xm11 = $('#xm11').val();
    form_data_notreq.xm12 = $('#xm12').val();
    form_data_notreq.xm13 = $('#xm13').val();
    form_data_notreq.xm14 = $('#xm14').val();
    form_data_notreq.xm21 = $('#xm21').val();
    form_data_notreq.xm22 = $('#xm22').val();
    form_data_notreq.xm23 = $('#xm23').val();
    form_data_notreq.xm24 = $('#xm24').val();
    form_data_notreq.xm31 = $('#xm31').val();
    form_data_notreq.xm32 = $('#xm32').val();
    form_data_notreq.xm33 = $('#xm33').val();
    form_data_notreq.xm34 = $('#xm34').val();
    form_data_notreq.xm41 = $('#xm41').val();
    form_data_notreq.xm42 = $('#xm42').val();
    form_data_notreq.xm43 = $('#xm43').val();
    form_data_notreq.xm44 = $('#xm44').val();
    form_data_notreq.xm51 = $('#xm51').val();
    form_data_notreq.xm52 = $('#xm52').val();
    form_data_notreq.xm53 = $('#xm53').val();
    form_data_notreq.xm54 = $('#xm54').val();
    form_data_notreq.xm61 = $('#xm61').val();
    form_data_notreq.xm62 = $('#xm62').val();
    form_data_notreq.xm63 = $('#xm63').val();
    form_data_notreq.xm64 = $('#xm64').val();
    form_data_notreq.xm71 = $('#xm71').val();
    form_data_notreq.xm72 = $('#xm72').val();
    form_data_notreq.xm73 = $('#xm73').val();
    form_data_notreq.xm74 = $('#xm74').val();
    form_data_notreq.xm81 = $('#xm81').val();
    form_data_notreq.xm82 = $('#xm82').val();
    form_data_notreq.xm83 = $('#xm83').val();
    form_data_notreq.xm84 = $('#xm84').val();
    form_data_notreq.boardnametext = $('#boardnametext').val();
    form_data_notreq.regnumtext = $('#regnumtext').val();
    form_data_notreq.eligible_percentage = $('#eligible_percentage').val();

    //  Store data in DB

    var dob = form_data.dob_date + '-' + form_data.dob_month + '-' + form_data.dob_year;
    var tcdate = form_data.tc_date + '-' + form_data.tc_month + '-' + form_data.tc_year;
    if (menu4_count == 36 || menu4_count == 41) {
      $('.gen_svg-loader').addClass('active');
      jQuery(function($) {

        $.ajax({

          url: 'store-data.php',
          type: 'POST',
          data: {
            dob: dob,
            tcdate: tcdate,
            form_data: JSON.stringify(form_data),
            form_data_notreq: JSON.stringify(form_data_notreq),
          },
          success: function(msg) {

          }
        });

      });


      // generate pdf

      if (form_data.select_degree == 'ug') {
        jQuery(function($) {

          $.ajax({

            url: 'generate-pdf.php',
            type: 'POST',
            data: {
              image_name: form_data.select_file,
              form_data: JSON.stringify(form_data),
              form_data_notreq: JSON.stringify(form_data_notreq),
            },
            success: function(msg) {
              // window.open("about:blank", "newPage");
              //  window.open("generate-pdf.php", "newPage");
              window.location.href = "generate-pdf.php";
              $('#online-application-form')[0].reset();
              $('.applynow-wrap li').removeClass('active');
              $('.applynow-wrap li:nth-child(1)').addClass('active');
              $('.tab-content').removeClass('in active');
              $('#menu1').addClass('in active');
              $('.gen_svg-loader').removeClass('active');

            }

          }).done(function() {
            setTimeout(function() {
              location.reload();
            }, 2500);
          });
        });
      } else if (form_data.select_degree == 'pg') {
        jQuery(function($) {

          $.ajax({

            url: 'generate-pdf-pg.php',
            type: 'POST',
            data: {
              form_data: JSON.stringify(form_data),
              form_data_notreq: JSON.stringify(form_data_notreq),
            },
            success: function(msg) {
              window.location.href = "generate-pdf-pg.php";
              $('#online-application-form')[0].reset();
              $('.applynow-wrap li').removeClass('active');
              $('.applynow-wrap li:nth-child(1)').addClass('active');
              $('.tab-content').removeClass('in active');
              $('#menu1').addClass('in active');
              $('.gen_svg-loader').removeClass('active');

            }

          }).done(function() {
            setTimeout(function() {
              location.reload();
            }, 2500);
          });
        });
      }
    }
  });


  //  virtual campus tour
//
//  var repeathtml = "<div id='flashContent'><p><a href='http://www.adobe.com/go/getflashplayer'><img src='http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player'/></a></p></div>";
//
//  var flashvars = {};
//  var params = {};
//  params.quality = "high";
//  params.bgcolor = "#ffffff";
//  params.allowscriptaccess = "sameDomain";
//  params.allowfullscreen = "true";
//  params.base = ".";
//  var attributes = {};
//  attributes.id = "pano";
//  attributes.name = "pano";
//  attributes.align = "middle";
//
//  function embedswf(x) {
//    var num = x;
//    var filename = "virtual-tour/Files/" + num + ".swf";
//    swfobject.embedSWF(
//      filename, "flashContent",
//      "100%", "100%",
//      "9.0.0", "expressInstall.swf",
//      flashvars, params, attributes);
//  }
//  $('.videowrap').append(repeathtml);
//  embedswf(1);
//
//  $(document).on('click', '.vtour-wrap a', function(e) {
//    e.preventDefault();
//    $("html, body").animate({
//      scrollTop: 0
//    }, 500);
//    var index = $(this).parent().parent().index() + 1;
//    $('.videowrap').empty().append(repeathtml);
//    embedswf(index);
//  });
//  swfobject.registerObject("FlashID");
//


</script>
</body>

<?php wp_footer(); ?>

</html>
