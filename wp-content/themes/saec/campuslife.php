<?php

/**
  Template Name: campuslife
*/


get_header();
?>


<section class="campuslife-intro inner">
  <div class="container">
    <div class="text-wrapper">
      <video autoplay muted loop id="myVideo">
        <source src="<?php the_field('background_video');?>" type="video/mp4">
      </video>
      <ul class="camp-txtslider">
        <?php if( have_rows('campus_life_text_slider') ): ?>
        <?php while( have_rows('campus_life_text_slider') ): the_row(); ?>
        <li>
          <?php the_sub_field('campus_life_text_slider_content');?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <h1><?php the_field('campus_life_main_title');?> <span class="heart-icon"></span> with!</h1>
    <p><?php the_field('campus_life_contents');?>
    </p>
  </div>
</section>



<div class="camp-virt-tour">
  <div class="campvirt-video" style="background-image: url('<?php the_field('college_virtual_tour_bg');?>')">
    <div class="video_play_wrap media-video">
      <a href="#" class="full_cont-link modal-link" data-target="#mediamodal" data-url="<?php the_field('virtual_tour_video_url');?>"></a>
      <div class="ply-btn">
        <svg width="80" height="80" class="play-icon" viewBox="0 0 100 100">
          <rect x="4" y="4" width="92" height="92" class="box" rx="50" ry="50"></rect>
          <polygon points="44,42 62,52 44,62" class="play"></polygon>
        </svg>
      </div>
      <h3>Watch our video</h3>
    </div>
    <div>
      <div class="campvirt-text campvirt-span">
        <span><?php the_field('college_virtual_tour_count');?><sup>&deg;</sup></span>
      </div>
      <div class="campvirt-text">
        <h3><?php the_field('campvirt-text');?></h3>
        <label><?php the_field('college_virtual_tour_label');?></label>
        <a href="virtual-campus-tour" class="btn_web"><?php the_field('virtual_tour_button_text');?></a>
      </div>
    </div>
  </div>
</div>


<section class="campus_activtis">
  <div class="container">
    <div class="row">
      <?php if( have_rows('campus_activtis') ): ?>
      <?php while( have_rows('campus_activtis') ): the_row(); ?>
      <div class="col-sm-4 col-xs-4">
        <span>
           <img src="<?php the_sub_field('activtis_icon');?>" alt="">
        </span>
        <h2><?php the_sub_field('activtis_title');?></h2>
        <p><?php the_sub_field('activtis_detail');?></p>
        <a href="sports-center" class="full_cont-link"></a>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</section>

<!--infra -->
<section class="infra_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 infra_sec_left">
        <div class="review_bx">
          <div class="review_slider owl-carousel">

            <?php 
              query_posts(array( 
                  'post_type' => 'testimonials',
                  'showposts' => 10 
              ) );  
            ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="review_innr">

              <?php
                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail( 'full', array( 'class'  => 'img-circle' ) ); // show featured image
                } 
            ?>

              <h3><?php the_title(); ?></h3>
              <span><?php the_field('testimonials_designation'); ?><br> <?php the_field('batch'); ?></span>
              <div><?php the_content(); ?></div>
            </div>
            <?php endwhile;?>

          </div>
          <div class="quot_div"></div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="great_infra">
          <div class="col-sm-12">
            <h3 class="title_line">Great <br>
              Infrastructure</h3>
          </div>

          <?php if( have_rows('infrastructure', 9) ): ?>
          <?php while( have_rows('infrastructure', 9) ): the_row(); ?>

          <div class="col-sm-4">
            <div>
              <span>
                <img src="<?php the_sub_field('icon', 9); ?>" alt="" class="img-responsive">
              </span>
              <p><?php the_sub_field('facility'); ?></p>
              <div class="hov_overlay"></div>
              <a href="health" class="full_cont-link"></a>
            </div>
          </div>

          <?php endwhile; ?>
          <?php endif; ?>

          <div class="col-sm-4">
            <div>
              <p>Explore Facilities</p>
              <p class="learn-more"><span></span> </p>
              <div class="hov_overlay"></div>
              <a href="resources" class="full_cont-link"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 596);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 596);?></span>
        </div>
      </div>

      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 596);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 596);?></span>
        </div>
      </div>

      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 596);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 596);?></span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
