<?php

/**
  Template Name: careers
*/


get_header();
?>

<section class="about_main career_page">
  <div class="about_title">
    <div class="container">

      <h3><?php the_title(); ?></h3>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
      ?>
      <span><?php the_content();?></span>
      <?php
      endwhile; else: ?>
      <p>Sorry, no posts matched your criteria.</p>
      <?php endif; ?>
    </div>
  </div>
  <div class="about_rank clearfix">
    <div class="container">
      <div class="about_rank_left">
        <span><?php the_field('career_title');?></span>
        <p><?php the_field('career_sub_title');?>
          <span><?php the_field('career_content');?></span>
          <a href="#" class="btn_web modal-link" data-target="#gen_car_modal"><?php the_field('career_button_title');?></a>
        </p>

      </div>
    </div>

    <div class="about_bg_img">
    </div>
  </div>

  <div class="about_main_sub clearfix">
    <div class="container">
      <h3 class="title_line">Why Choose Us</h3>
      <div class="why_chos_us">
        <div class="row">

          <?php if( have_rows('why_choose_us') ): ?>
          <?php while( have_rows('why_choose_us') ): the_row(); ?>
          <div class="col-sm-4">
            <div>
              <span>
                <img src="<?php the_sub_field('why_choose_icon');?>" alt="">
              </span>
              <h4><?php the_sub_field('why_choose_title');?></h4>
              <p><?php the_sub_field('why_choose_content');?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>

        </div>
      </div>

    </div>
  </div>
</section>

<section class="recr_tab" id="recr_tab">
  <div class="course-tab-contain">
    <div class="container">
      <div class="tab-menu">
        <ul>
          <li class="active"><a data-toggle="tab" href="#menu1">Faculty Recruitment</a></li>
          <li><a data-toggle="tab" href="#menu3">Other Recruitment</a></li>
        </ul>
      </div>

      <div class="tab-content-wrap clearfix">
        <div class="tab-content fade active in" id="menu1">
          <?php if( have_rows('faculty_recruitment') ): ?>
          <?php while( have_rows('faculty_recruitment') ): the_row(); ?>
          <div class="recr_box">
            <div class="row">
              <div class="col-sm-8">
                <h3><?php the_sub_field('faculty_recruitment_title');?></h3>

              </div>
              <div class="col-sm-4">
                <a href="#"  onclick="getPostTitle('<?php the_sub_field('faculty_recruitment_title');?>')" class="btn_web  modal-link" data-target="#car_modal" data-url="Professor/EEE Dept.">Apply now</a>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>

        </div>

        <div class="tab-content fade" id="menu3">
          <?php if( have_rows('other_recruitment') ): ?>
          <?php while( have_rows('other_recruitment') ): the_row(); ?>
          <div class="recr_box">
            <div class="row">
              <div class="col-sm-8">
                <h3><?php the_sub_field('faculty_recruitment_title');?></h3>
              </div>
              <div class="col-sm-4">
                <a href="#ddd" class="btn_web  modal-link" data-target="#car_modal" data-url="Controller of Examinations (CoE)" onclick="getPostTitle('<?php the_sub_field('faculty_recruitment_title');?>')">apply now</a>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
</section>

<div class="car_modal web-modal fade" id="car_modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h3 class="inner_sub_hd">Apply Now</h3>
        <div class="contact_form careers-apply-now-wrap">
          <?php echo do_shortcode( '[contact-form-7 id="3706" title="Recruitment  Apply now"]' ) ?>
        </div>


      </div>
    </div>
  </div>

</div>

<div class="car_modal web-modal fade" id="gen_car_modal" role="dialog">
  <div class="modal-dialog">

    Modal content
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h3 class="inner_sub_hd">Apply Now</h3>
        <div class="contact_form careers-apply-now-wrap">
          <?php echo do_shortcode( '[contact-form-7 id="3705" title="Careers  Apply now"]' ) ?>
        </div>
      </div>
    </div>
  </div>

</div>
<?php
get_footer();
