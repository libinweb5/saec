<?php

/**
  Template Name: cms-login
*/


get_header();
?>

<section>
  <div class="container content-only">
    <div>
      <table border="0" cellpadding="50">
        <tbody>
          <tr>
            <td>
              <dl class="wp-caption  aligncenter">
                <dt class="wp-caption-dt">
                  <a href="<?php the_field('student_login_link');?>" target="_blank">
                    <img class=" wp-image-1575" src="<?php the_field('student_login_icon');?>" alt="Student Login">
                  </a>
                </dt>
                <dd class="wp-caption-dd"><strong><label><?php the_field('student_login_title');?></label></strong></dd>
              </dl>
            </td>
            <td>
              <dl class="wp-caption  aligncenter">
                <dt class="wp-caption-dt">
                  <a href="<?php the_field('staff_login_page_link');?>" target="_blank">
                    <img class="wp-image-1579 " src="<?php the_field('staff_login_icon');?>" alt="Staff Login">
                  </a>
                </dt>
                <dd class="wp-caption-dd"><strong><label><?php the_field('staff_login_title');?></label></strong></dd>
              </dl>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</section>
<?php
get_footer();
