<?php

/**
  Template Name: companies-visited
*/


get_header();
?>


<section>
  <div class="container content-only">
    <div>
      <h2 class="title_line"><?php the_field('list_of_companies_visited_title_2018-19');?></h2>
      <div>
       <?php the_field('list_of_companies_visited_2018-19');?>
      </div>

    </div>
  </div>

  <div class="container content-only">
    <div>
      <h2 class="title_line"><?php the_field('list_of_companies_visited_title_2017-18');?></h2>
      <div>
       <?php the_field('list_of_companies_visited_2017-18');?>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
