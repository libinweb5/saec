<?php

/**
  Template Name: contact-us
*/


get_header();
?>

<section class="contact_banner">
  <div class="banner-wrap" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/contact-banner.jpg')"></div>
  <div class="map_pop">
    <div id="gmap_canvas"></div>
  </div>
</section>

<div class="map_pin_box">
  <div class="container">
    <div class="map_pin">
      <svg>
        <use xlink:href="#map_pin"></use>
      </svg>
      <span class="close-search">
        <svg>
          <use xlink:href="#map_close"></use>
        </svg>
      </span>
    </div>
  </div>
</div>
<section class="contact_sec clearfix">
  <div class="container">
    <h3 class="inner_sub_hd">Get in touch with us</h3>
    <div class="row">
      <div class="col-sm-7">
        <div class="contact_form contact_form-wrap">
          <?php echo do_shortcode( '[contact-form-7 id="1580" ajax="true" title="Contact form"]' ) ?>
        </div>
      </div>

      <div class="col-sm-5">
        <div>
          <span>Address</span>
          <?php the_field('address');?>
        </div>

        <div>
          <span>Phone</span>
          <?php if( have_rows('phone') ): ?>
          <?php while( have_rows('phone') ): the_row(); ?>
          <p><a href="tel:<?php the_sub_field('phone_numbers');?>"> <?php the_sub_field('phone_numbers');?></a> </p>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <span>Email</span>
          <?php if( have_rows('email') ): ?>
          <?php while( have_rows('email') ): the_row(); ?>
          <p><a href="mailto:<?php the_sub_field('email_id');?>"> <?php the_sub_field('email_id');?></a> </p>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <span>Fax</span>
          <?php if( have_rows('fax') ): ?>
          <?php while( have_rows('fax') ): the_row(); ?>
          <p><?php the_sub_field('fax_numbers');?> </p>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

      </div>
    </div>
  </div>
</section>

<?php
get_footer();
