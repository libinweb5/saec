<section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery');?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery');?></span>
        </div>
      </div>
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery');?></label></h2>
          <span><?php the_field('university_rank_label_gallery');?>  </span>
        </div>
      </div>
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery');?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery');?></span>
        </div>
      </div>
    </div>
  </div>
</section>
