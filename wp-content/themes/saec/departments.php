<?php

/**
  Template Name: departments
*/


get_header();
?>


<section class="inner dept_intro">
  <div class="container">
    <h1><?php the_title();?></h1>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
    the_content();
    endwhile; else: ?>
    <p>Sorry, Content is empty.</p>
    <?php endif; ?>
  </div>
</section>

<div class="dept_number">
  <div class="container">
    <div class="dept_number-img">
      <div class="overbox">
        <div>
          <span>13<sup>+</sup></span>
          <h2>Under Graduate and Post Graduate Programs</h2>
          <label>Excellent Learning Environment</label>
        </div>
      </div>
      <div class="underimg">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/dept_intro.jpg" alt="Departments - SAEC" />
      </div>
    </div>
  </div>
</div>


<section class="dept_courses">
  <div class="container">
    <div class="dept_ugcourses clearfix">
      <h2 class="inner_sub_hd"><?php _e('UG Programmes') ?></h2>
      <?php $loop = new WP_Query( array( 'post_type' => 'ug_programmes') ); ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

      <div class="course-box">
        <div>
          <span>
            <img src="<?php the_field('programmes_icon'); ?>" alt="">
          </span>
          <p><?php the_title(); ?></p>
          <p class="learn-more"><span></span></p>
          <a href="<?php the_permalink(); ?>" class="full_cont-link"></a>
        </div>
      </div>
      <?php endwhile; wp_reset_query(); ?>
    </div>

    <div class="dept_pgcourses clearfix">
      <h2 class="inner_sub_hd"><?php _e('PG Programmes') ?></h2>

      <?php $loop = new WP_Query( array( 'post_type' => 'pg_programmes') ); ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

      <div class="course-box">
        <div>
          <span>
            <img src="<?php the_field('programmes_icon'); ?>" alt="">
          </span>
          <p><?php the_title(); ?></p>
          <p class="learn-more"><span></span></p>

          <?php 
           $pId= get_the_ID();
          if($pId==663) { 
              $plink="https://saec.ac.in/mca";
              }
              else { $plink = get_permalink();
            }
          ?>
          <a href="<?php echo $plink; ?>" class="full_cont-link"></a>
        </div>
      </div>
      <?php endwhile; wp_reset_query(); ?>

    </div>
  </div>
</section>



<?php include('virtual-tour-strip.php');?>

  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
