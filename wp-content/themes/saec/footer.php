<?php include('config.php'); ?>
<footer>
  <!--
  <span class="backtotop">
    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/scroll-top-icon.svg"></a>
  </span>
-->
  <a href="javascript:" id="return-to-top">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/scroll-top-icon.svg">
  </a>

  <div class="footer_sec">
    <div class="container">
      <div class="row">
        <div class="footer_sec_top">
          <div class="col-sm-9 clearfix">
            <ul>
              <li><a href="pdf/saec.pdf" target="_blank">
                  <i><svg>
                      <use xlink:href="#dwnload_icon"></use>
                    </svg>
                  </i>
                  DOWNLOAD <br />
                  COLLEGE PROSPECTUS</a></li>
              <li><a href="online-application"><i><svg>
                      <use xlink:href="#bcome_icon"></use>
                    </svg></i>BECOME A <br />
                  STUDENT AT SAEC</a></li>
            </ul>
          </div>

          <div class="col-sm-3 clearfix">
            <div class="subscriber_box">
              <p><?php _e('Subscribe Newsletter') ?></p>
              <?php echo do_shortcode( '[mc4wp_form id="379"]' ); ?>

              <!--  <?php echo do_shortcode(get_field('newsletter_short_code')); ?>  -->
            </div>
          </div>
        </div>


        <div class="footer_sec_btm">
          <div class="col-sm-9 clearfix">
            <div class="footer_sec_btm_box footer-menu-wrap">
              <?php
              wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => false ) );
            ?>
            </div>

          </div>
          <div class="col-sm-3 clearfix">
            <!-- Address-->
            <p><?php the_field('address', 'option'); ?></p>

            <!-- social media-->
            <?php if( have_rows('social_media', 'option') ): ?>
            <ul class="footr_social">
              <label> <?php _e('Follow:') ?></label>
              <?php while( have_rows('social_media', 'option') ): the_row(); ?>
              <li>
                <a href="<?php the_sub_field('social_media_link'); ?>" target="_blank">
                  <img src="<?php the_sub_field('social_media_image'); ?>" alt="<?php the_sub_field('alt'); ?>" />
                </a>
              </li>
              <?php endwhile; ?>
            </ul>
            <?php endif; ?>

          </div>

        </div>

      </div>

    </div>
  </div>

  <div class="btm_footr">
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <?php
              wp_nav_menu( array( 'theme_location' => 'footer-bottom-menu', 'container' => false ) );
            ?>
        </div>
        <div class="col-sm-5">
          <p> <?php _e('Designed by:') ?><a href="http://webandcrafts.com/" target="_blank"><?php _e('webandcrafts.com:') ?> </a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
</div>
<div class="web-modal fade" id="mediamodal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <video autoplay="true" controls></video>
      </div>
    </div>

  </div>
</div>

<div class="org_modal web-modal fade" id="car_modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <!-- <h3 class="inner_sub_hd">Apply Now</h3> -->
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/organizational-chart.jpg" class="img-responsive">
      </div>
    </div>
  </div>
</div>

<!-- load libraries -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.hoverdir.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js'></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.selectric.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/masonry.pkgd.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/global.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/common.js"></script>
<!--animation plugins -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/TweenMax.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/ScrollMagic.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/animation.gsap.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/animation.js"></script>

<script>
  $('.counter').each(function() {
    var $this = $(this),
      countTo = $this.attr('data-count');

    $({
      countNum: $this.text()
    }).animate({
        countNum: countTo
      },

      {

        duration: 8000,
        easing: 'linear',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
          //alert('finished');
        }
      });
  });

  $(document).ready(function() {
    fadeinup('.gallery_page_title > *', '#main');
    fadeinup('.gallery_page .gallery_sec ul li', '#main');
  });


  $(document).ready(function() {
    var divlength = $('.res_course-wrap > div').length;
    for (var i = 1; i <= divlength; i++) {
      if (i % 2 == 1) {
        fadeinleft('.res_course-wrap > div:nth-of-type(' + i + ') .container > .img-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
        fadeinright('.res_course-wrap > div:nth-of-type(' + i + ') .container > .text-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
      } else if (i % 2 == 0) {
        fadeinright('.res_course-wrap > div:nth-of-type(' + i + ') .container > .img-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
        fadeinleft('.res_course-wrap > div:nth-of-type(' + i + ') .container > .text-contain', '.res_course-wrap > div:nth-of-type(' + i + ')');
      }
    }
  });


  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  /* Validation Events for changing response CSS classes */
  document.addEventListener('wpcf7invalid', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-danger');
  }, false);
  document.addEventListener('wpcf7spam', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-warning');
  }, false);
  document.addEventListener('wpcf7mailfailed', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-warning');
  }, false);
  document.addEventListener('wpcf7mailsent', function(event) {
    $('.wpcf7-response-output').addClass('alert alert-success');
  }, false);


  //Inner page slider
  $(document).ready(function() {
    $(document).on('click', '#listmenu6', function() {

      setTimeout(function() {
        $('.board_trustes_slder_hh1').owlCarousel({
          loop: true,
          items: 6,
          margin: 30,
          nav: true,
          dots: false,
          autoplay: true,
          autoplayTimeout: 3000,
          smartSpeed: 900,
          autoplaySpeed: 900,
          autoplayHoverPause: true,
          navContainer: '.board_navslide',
          responsive: {
            0: {
              items: 1,
              mouseDrag: true,
              autoplay: true,
              loop: true,
              dots: true
            },
            700: {
              items: 2,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1200: {
              items: 3,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1440: {
              items: 4,
              mouseDrag: true,
              autoplay: true,
              loop: true
            }
          }
        });
      }, 150);
    });

  });
  $(document).ready(function() {
    $(document).on('click', '#listmenu5', function() {
      setTimeout(function() {
        $('.board_trustes_slder_hh1').owlCarousel({
          loop: true,
          items: 6,
          margin: 30,
          nav: true,
          dots: false,
          autoplay: true,
          autoplayTimeout: 3000,
          smartSpeed: 900,
          autoplaySpeed: 900,
          autoplayHoverPause: true,
          navContainer: '.board_navslide',
          responsive: {
            0: {
              items: 1,
              mouseDrag: true,
              autoplay: true,
              loop: true,
              dots: true
            },
            700: {
              items: 2,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1200: {
              items: 3,
              mouseDrag: true,
              autoplay: true,
              loop: true
            },
            1440: {
              items: 4,
              mouseDrag: true,
              autoplay: true,
              loop: true
            }
          }
        });
      }, 150);
    });

  });

  $(document).ready(function() {
    $(document).ajaxStart(function() {
      $('.svg-loader').show();
    }).ajaxStop(function() {
      $('.svg-loader').hide();
    });

    $(document).ready(function() {
      $(document).ajaxStart(function() {
        $('.svg-loader').show();
      }).ajaxStop(function() {
        $('.svg-loader').hide();
      });

      // Get the modal
      var modal = document.getElementById('gen_car_modal');

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          $('.apply-form-error-msg').removeClass('error');
          $('#gen_career_form')[0].reset();
        }
      }

      // General Career Form

      var gen_career_form_data = {};

      $(document).on('click', '#gen_career_form input[type = "submit"]', function(e) {

        var count = 0;
        gen_career_form_data.name = $('#gen_name').val();
        gen_career_form_data.email = $('#gen_email').val();
        // gen_career_form_data.phone = $('#gen_phone').val();
        gen_career_form_data.fileselect = $('#gen_fileselect_hidden').val();

        for (var key in gen_career_form_data) {
          if (gen_career_form_data[key] == null || gen_career_form_data[key] == '' || gen_career_form_data[key] == 'undefined') {
            $('.gen_' + key + ' .apply-form-error-msg').addClass('error');
          }
        }
        for (var key in gen_career_form_data) {
          if (!(gen_career_form_data[key] == null || gen_career_form_data[key] == '' || gen_career_form_data[key] == 'undefined')) {
            count = count + 1;
            $('.gen_' + key + ' .apply-form-error-msg').removeClass('error');
          }
        }
        if (!validateEmail(gen_career_form_data.email)) {
          $('.gen_email .apply-form-error-msg').addClass('error');
        }
        var phone = $('#gen_phone').val();
        var phone_length = phone.length;
        if (phone_length < 10) {
          $('.gen_phone .apply-form-error-msg').addClass('error');
        }
        if (phone_length == 10) {
          $('.gen_phone .apply-form-error-msg').removeClass('error');
          count = count + 1;
        }

        if (count == 4 && validateEmail(gen_career_form_data.email)) {

          $('.gen_svg-loader').show();
          $('.apply-form-error-msg').removeClass('error');
          $.ajax({
            type: 'POST',
            url: 'sendmail.php',
            async: true,
            data: $('#gen_career_form').serialize(),
            success: function() {
              $('.gen_thank-you').addClass('active');
              $('#gen_career_form')[0].reset();
              $('.gen_fileselect p').html('No file choosen');
              $('.gen_svg-loader').hide();
            },
            error: function(res) {
              console.log(res);
            }

          });
        }
      });


      $("#gen_career_form").submit(function(e) {
        return false;
      });

      function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
      }

      $(document).on('change', '#gen_fileselect', function(e) {
        var file_data = $('#gen_fileselect').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);


        $.ajax({
          url: 'document-upload.php',
          type: 'post',
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,

        }).done(function(data) {

          if (data !== 'error') {

            $('#gen_fileselect_hidden').val(data);

          }
        }).fail(function(data) {

        });

      });

    });



    //Each Career Form 

    $(document).on('click', '.recr_tab .tab-content-wrap .tab-content .recr_box .col-sm-4 .btn_web', function() {
      var post = $(this).attr('data-url');
      $('#car_modal .job-post input').attr('placeholder', post);
      $('#car_modal .job-post input').attr('value', post);

    });

    var career_form_data = {};

    $(document).on('click', '#career_form input[type = "submit"]', function(e) {

      var count = 0;
      career_form_data.name = $('#name').val();
      career_form_data.email = $('#email').val();
      career_form_data.phone = $('#phone').val();
      career_form_data.fileselect = $('#fileselect').val();

      for (var key in career_form_data) {
        if (career_form_data[key] == null || career_form_data[key] == '' || career_form_data[key] == 'undefined') {
          $('.' + key + ' .apply-form-error-msg').addClass('error');
        }
      }
      for (var key in career_form_data) {
        if (!(career_form_data[key] == null || career_form_data[key] == '' || career_form_data[key] == 'undefined')) {
          count = count + 1;
        }
      }
      if (!validateEmail(career_form_data.email)) {
        $('.email .apply-form-error-msg').addClass('error');
      }


      if (count == 4 && validateEmail(career_form_data.email)) {

        $('.svg-loader').show();
        $('.apply-form-error-msg').removeClass('error');
        $.ajax({
          type: 'POST',
          url: 'sendmail.php',
          async: true,
          data: $('#career_form').serialize(),
          success: function() {
            $('.thank-you').addClass('active');
            $('#career_form')[0].reset();
            $('.fileselect p').html('No file choosen');
          },
          error: function(res) {
            console.log(res);
          }

        });
      }
    });


    $("#career_form").submit(function(e) {
      return false;
    });

    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test($email);
    }

    $(document).on('change', '#fileselect', function(e) {
      var file_data = $('#fileselect').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);


      $.ajax({
        url: 'document-upload.php',
        type: 'post',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,

      }).done(function(data) {

        if (data !== 'error') {

          $('#fileselect_hidden').val(data);

        }
      }).fail(function(data) {});
    });

  });


  //Number validate

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  function getPostTitle(title) {
    $("#post_ct").val(title);
  }

</script>
<?php //  '.........................................................................................................................................................'; ?>
<script>
  // ===== Scroll to Top ==== 

  $(window).scroll(function() {
    if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
      $('#return-to-top').fadeIn(200); // Fade in the arrow
    } else {
      $('#return-to-top').fadeOut(200); // Else fade out the arrow
    }
  });
  $('#return-to-top').click(function() { // When arrow is clicked
    $('body,html').animate({
      scrollTop: 0 // Scroll to top of body
    }, 500);
  });

</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAHWM3sMRD05PQk_wVExFjxRwXEPJsDLoo"></script>
<script>
  $(document).ready(function() {
    function init_map() {
      var myOptions = {
        zoom: 14,
        center: new google.maps.LatLng(13.065630, 80.110942),
        // styles:   [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "administrative.country", "elementType": "geometry.fill", "stylers": [{"saturation": "96"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#bfbfbf"}, {"visibility": "on"}]}],
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
      marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(13.065630, 80.110942)

      });
      infowindow = new google.maps.InfoWindow({
        // content:"<b>Megamind</b><br>No 70, 1st Cross,<br>HAL 3rd stage, New Thippasandra,<br>Indiranagar, Bengaluru - 560075"
      });
      /*google.maps.event.addListener(marker, "click", function(){
       infowindow.open(map,marker);
       });
       infowindow.open(map,marker);*/
    }
    google.maps.event.addDomListener(window, 'load', init_map);
  });


  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  var contact_form_data = {};
  $(document).on('click', 'input[type = "submit"]', function(e) {


    var count = 0;
    contact_form_data.name = $('#name').val();
    contact_form_data.email = $('#email').val();
    // contact_form_data.phone = $('#phone').val();
    for (var key in contact_form_data) {
      if (contact_form_data[key] == null || contact_form_data[key] == '' || contact_form_data[key] == 'undefined') {
        $('.' + key + ' .apply-form-error-msg').addClass('error');
      }
    }
    for (var key in contact_form_data) {
      if (!(contact_form_data[key] == null || contact_form_data[key] == '' || contact_form_data[key] == 'undefined')) {
        count = count + 1;
        $('.' + key + ' .apply-form-error-msg').removeClass('error');
      }
    }
    if (!validateEmail(contact_form_data.email)) {
      $('.email .apply-form-error-msg').addClass('error');
    }


    var phone = $('#phone').val();
    var phone_length = phone.length;
    if (phone_length < 10) {
      $('.phone .apply-form-error-msg').addClass('error');
    }
    if (phone_length == 10) {
      $('.phone .apply-form-error-msg').removeClass('error');
      count = count + 1;
    }

    if (count == 3 && validateEmail(contact_form_data.email)) {
      $('.apply-form-error-msg').removeClass('error');
      $.ajax({
        type: 'POST', //hide url
        url: 'sendmail.php', //your form validation url
        data: $('form#contact_form').serialize(),
        success: function() {
          $('.thank-you').addClass('active');
          $('#contact_form')[0].reset();
        }
      });
    }
  });



  $("#contact_form").submit(function(e) {
    return false;
  });

  function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
  }



//  $(document).ready(function() {
//    fadeinleft('.contact_sec .container .row > div:first-of-type', '.contact_sec');
//    fadeinright('.contact_sec .container .row > div:last-of-type', '.contact_sec');
//  });

</script>

</body>

<?php wp_footer(); ?>

</html>
