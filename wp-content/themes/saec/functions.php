<?php

function add_theme_scripts() {
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('plugins', get_template_directory_uri() . '/css/plugins.css', array(), '1.1', 'all');
    wp_enqueue_style('fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), '1.1', 'all');
    wp_enqueue_style('main-style', get_template_directory_uri() . '/css/style.css', array(), '1.1', 'all');
    wp_enqueue_style('new-style', get_template_directory_uri() . '/css/scss/new-style.css', array(), '1.1', 'all');
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');


add_action('wp_enqueue_scripts', 'my_script_holder');

function my_script_holder() {
    wp_register_script('head_script', get_template_directory_uri() . '/js/head.min.js', array('jquery'), '', true);
    wp_enqueue_script('head_script');
}

function wpb_hook_javascript() {
    if (is_page('about-us')) {
        ?>
<script type="text/javascript">
  jQuery(document).ready(function() {
    fadeinup('.about_title', '.about_main');
    fadeinleft('.about_main .about_rank .about_rank_left', '.about_rank .container');
    fadeinright('.about_main .about_rank .about_bg_img', '.about_rank .container');

    var about_data = TweenMax.staggerFromTo('.about_main .about_rank .about_rank_left > *', 1, {
      y: 20
    }, {
      y: 0,
      opacity: 1,
      delay: 0.5
    }, 0.2);
    new ScrollMagic.Scene({
      triggerElement: '.about_rank .container',
      reverse: false
    }).setTween(about_data).addTo(controller);

    fadeinleft('.about_clg .about_clg_lft', 'section.about_clg');
    fadeinright('.about_clg .about_clg_rgt', 'section.about_clg');
    fadeinup('.our_standards .container > *', '.our_standards');

    var about_slider = TweenMax.staggerFromTo('.board_trustes_slder .board_trustes_box', 1, {
      x: 20
    }, {
      x: 0,
      opacity: 1
    }, 0.2);
    new ScrollMagic.Scene({
      triggerElement: '.clg_range',
      reverse: false
    }).setTween(about_slider).addTo(controller);


    fadeinleft('.directr_msg_img', '.directr_msg');
    fadeinright('.directr_msg_content', '.directr_msg');
    fadeinright('.board_trustes_inner .vision_mision .row > div', 'section.board_trustes');
  });

</script>
<?php
    }

    if (is_page('departments')) {
        ?>
<script type="text/javascript">
  jQuery(document).ready(function() {
    fadeinup('.dept_intro .container > *', '#main');
    fadeinleft('.dept_number .dept_number-img .overbox > div', '.dept_number');
    fadeinright('.dept_number .dept_number-img .underimg', '.dept_number');

    var dept_data = TweenMax.staggerFromTo('.dept_number .dept_number-img .overbox > div > *', 1, {
      y: 20
    }, {
      y: 0,
      opacity: 1,
      delay: 0.5
    }, 0.2);
    new ScrollMagic.Scene({
      triggerElement: '.dept_number',
      reverse: false
    }).setTween(dept_data).addTo(controller);

    var dept_ug = TweenMax.staggerFromTo('.dept_courses div.dept_ugcourses .course-box > div', 1, {
      y: 20
    }, {
      y: 0,
      opacity: 1
    }, 0.3);
    new ScrollMagic.Scene({
      triggerElement: '.dept_courses > div > div.dept_ugcourses',
      reverse: false
    }).setTween(dept_ug).addTo(controller);

    var dept_pg = TweenMax.staggerFromTo('.dept_courses div.dept_pgcourses .course-box > div', 1, {
      y: 20
    }, {
      y: 0,
      opacity: 1
    }, 0.3);
    new ScrollMagic.Scene({
      triggerElement: '.dept_courses > div > div.dept_pgcourses',
      reverse: false
    }).setTween(dept_pg).addTo(controller);
  });

</script>
<?php
    }


    if (is_page('placements')) {
        ?>
<script type="text/javascript">
  jQuery(window).load(function() {
    fadeinleft('.placements_training .container > div:not(.placement_bg_img)', '#main');
    fadeinup('.our_recruiters .our_recruiters_sldr .our_recruiters_slide', '.our_recruiters');
    fadeinup('.placements_training .placements_training_lft .placed_count div > *', '#main');
    fadeinup('.major_fnctin .tic_list li', '.major_fnctin');
    fadeinup('.strngth_plcmnt_ofc > .container > div:not(.external_links) > *', '.strngth_plcmnt_ofc');
  });

</script>
<?php
    }

    if (is_page('campus-life')) {
        ?>
<script type="text/javascript">
  jQuery(document).ready(function() {
    fadeinup('.campuslife-intro .container > *:not(.text-wrapper)', '#main');
    fadein('.camp-virt-tour', '.camp-virt-tour');
    fadeinup('.camp-virt-tour .campvirt-text span', '.camp-virt-tour');
    fadeinup('.camp-virt-tour .campvirt-text:last-of-type > *', '.camp-virt-tour');
    fadeinup('.campus_activtis .col-sm-4', '.campus_activtis');
  });

</script>
<?php
    }

    if (is_page('contact-us')) {
        ?>
<script type="text/javascript">
  jQuery(document).ready(function() {
    fadeinleft('.contact_sec .container .row > div:first-of-type', '.contact_sec');
    fadeinright('.contact_sec .container .row > div:last-of-type', '.contact_sec');
  });

</script>

<?php
        }
      if (is_page('achievements')) {
        ?>
<script type="text/javascript">
  jQuery(document).ready(function() {
    fadeinup('.dept_intro .container > *', '.dept_intro');
  });

</script>
<?php
        }

    if (is_page('admission')) {
        ?>
<script type="text/javascript">
  jQuery(document).ready(function() {
    var divlength = $('.admis_steps_wrap .container > div').length;
    fadeinup('.admis_intro .text-contain > *', '#main');
    fadeinright('.admis_intro .img-contain', '#main');
    for (var i = 1; i < divlength; i++) {
      fadeinup('.admis_steps_wrap .container > div:nth-of-type(' + i + ') > *', '.admis_steps_wrap  .container > div:nth-of-type(' + i + ')');
    }
  });

</script>
<?php
    }
        if (is_page('contact-us')) {
        ?>
      <script type="text/javascript">
        jQuery(document).ready(function() {
          fadeinleft('.contact_sec .container .row > div:first-of-type', '.contact_sec');
          fadeinright('.contact_sec .container .row > div:last-of-type', '.contact_sec');
        });

      </script>
<?php
        }
  
}

add_action('wp_head', 'wpb_hook_javascript');

function register_my_menus() {
    register_nav_menus(
            array(
                'main-menu' => __('Main Menu'),
                'top-head-menu' => __('Top head Menu'),
                'footer-menu' => __('Footer Menu'),
                'footer-bottom-menu' => __('Footer Bottom Menu')
            )
    );
}

add_action('init', 'register_my_menus');

//google map
//function my_acf_init() {
//	
//	acf_update_setting('google_api_key', 'AIzaSyAHWM3sMRD05PQk_wVExFjxRwXEPJsDLoo');
//}

add_action('acf/init', 'my_acf_init');

//Enqueue the Dashicons script
add_action('wp_enqueue_scripts', 'load_dashicons_front_end');

function load_dashicons_front_end() {
    wp_enqueue_style('dashicons');
}

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'SAEC options',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Header Settings',
        'menu_title' => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Footer Settings',
        'menu_title' => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));
}

/**
 * Adding extended support for the uploads format
 */
add_filter('upload_mimes', 'enable_extended_upload');
if (!function_exists("enable_extended_upload")) {

    function enable_extended_upload($mime_types = array()) {
        $mime_types['webp'] = 'image/webp';
        $mime_types['svg'] = 'image/svg+xml';
        return $mime_types;
    }

}
function cptui_register_my_cpts_news_events() {

	/**
	 * Post Type: News-Events.
	 */

	$labels = array(
		"name" => __( "News-Events", "saectheme" ),
		"singular_name" => __( "News-events", "saectheme" ),
	);

	$args = array(
		"label" => __( "News-Events", "saectheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "news_events", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "news_events", $args );
}

add_action( 'init', 'cptui_register_my_cpts_news_events' );


function cptui_register_my_cpts_achievements() {

    /**
     * Post Type: Achievements.
     */
    $labels = array(
        "name" => __("Achievements", "saectheme"),
        "singular_name" => __("Achievements", "saectheme"),
    );

    $args = array(
        "label" => __("Achievements", "saectheme"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "achievements", "with_front" => true),
        "query_var" => true,
        'menu_icon' => 'dashicons-businessman',
        "supports" => array("title", "editor", "thumbnail"),
    );

    register_post_type("achievements", $args);
}

add_action('init', 'cptui_register_my_cpts_achievements');

function cptui_register_my_cpts_testimonials() {

    /**
     * Post Type: Testimonials.
     */
    $labels = array(
        "name" => __("Testimonials", "saectheme"),
        "singular_name" => __("testimonials", "saectheme"),
    );

    $args = array(
        "label" => __("Testimonials", "saectheme"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        'menu_icon' => 'dashicons-format-quote',
        "rewrite" => array("slug" => "testimonials", "with_front" => true),
        "query_var" => true,
    );

    add_theme_support('post-thumbnails');
    register_post_type("testimonials", $args);
}

add_action('init', 'cptui_register_my_cpts_testimonials');

function cptui_register_my_cpts_gallery() {

    /**
     * Post Type: Gallery.
     */
    $labels = array(
        "name" => __("Gallery", "saectheme"),
        "singular_name" => __("Gallery", "saectheme"),
    );

    $args = array(
        "label" => __("Gallery", "saectheme"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "gallery", "with_front" => true),
        "query_var" => true,
        'menu_icon' => 'dashicons-format-gallery',
        "supports" => array("title", "editor", "thumbnail"),
    );

    register_post_type("gallery", $args);
}

add_action('init', 'cptui_register_my_cpts_gallery');

function cptui_register_my_cpts_ug_programmes() {

    /**
     * Post Type: UG Programmes.
     */
    $labels = array(
        "name" => __("UG Programmes", "saectheme"),
        "singular_name" => __("UG Programmes", "saectheme"),
    );

    $args = array(
        "label" => __("UG Programmes", "saectheme"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "ug_programmes", "with_front" => true),
        "query_var" => true,
        'menu_icon' => 'dashicons-groups',
        "supports" => array("title", "editor", "thumbnail"),
    );

    register_post_type("ug_programmes", $args);
}

add_action('init', 'cptui_register_my_cpts_ug_programmes');

function cptui_register_my_cpts_pg_programmes() {

    /**
     * Post Type: PG Programmes.
     */
    $labels = array(
        "name" => __("PG Programmes", "saectheme"),
        "singular_name" => __("PG Programmes", "saectheme"),
    );

    $args = array(
        "label" => __("PG Programmes", "saectheme"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "pg_programmes", "with_front" => true),
        "query_var" => true,
        'menu_icon' => 'dashicons-buddicons-buddypress-logo',
        "supports" => array("title", "editor", "thumbnail"),
    );

    register_post_type("pg_programmes", $args);
}

add_action('init', 'cptui_register_my_cpts_pg_programmes');

function cptui_register_my_cpts_resource() {

    /**
     * Post Type: Resource.
     */
    $labels = array(
        "name" => __("Resource", "saectheme"),
        "singular_name" => __("Resource", "saectheme"),
    );

    $args = array(
        "label" => __("Resource", "saectheme"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        'menu_icon' => 'dashicons-visibility',
        "rewrite" => array("slug" => "resource", "with_front" => true),
        "query_var" => true,
        "supports" => array("title", "editor", "thumbnail"),
    );

    register_post_type("resource", $args);
}

add_action('init', 'cptui_register_my_cpts_resource');

function my_login_logo_one() {
    ?>
<style type="text/css">
  body.login div#login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri();
    ?>/images/full-logo-wp.png);
    height: 45px !important;
    background-size: contain !important;
    width: 100% !important;
  }

</style>
<?php
}

add_action('login_enqueue_scripts', 'my_login_logo_one');


//ajax code for application form branch display
add_action('wp_ajax_populate_branch', 'populate_branch'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_populate_branch', 'populate_branch'); // wp_ajax_nopriv_{action}

function populate_branch() {
    $admit = $_POST['branch'];

    if ($admit == 'ug') {
        ?>

<option disabled selected>Select</option>
<option data-url="CE" value="B.E. Civil Engineering">B.E. Civil Engineering</option>
<option data-url="CS" value="B.E. Computer Science and Engineering">B.E. Computer Science and Engineering</option>
<option data-url="EEE" value="B.E. Electrical and Electronics Engineering">B.E. Electrical and Electronics Engineering </option>
<option data-url="EC" value="B.E. Electronics and Communication Engineering">B.E. Electronics and Communication Engineering</option>
<option data-url="IT" value="B.Tech Information Technology">B.Tech Information Technology</option>
<option data-url="ME" value="B.E. Mechanical Engineering">B.E. Mechanical Engineering</option>
<option data-url="HS" value="Humanities and Science">Humanities and Science</option>

<? } else {
        ?>

<option disabled selected>Select</option>
<option data-url="MCA" value="Master of Computer Applications">Master of Computer Applications</option>
<option data-url="MBA" value="Master of Business Administration">Master of Business Administration</option>
<option data-url="MCE" value="ME Computer Science and Engineering">ME Computer Science and Engineering</option>
<option data-url="MCS" value="ME Communication Systems">ME Communication Systems</option>
<option data-url="MES" value="ME Embedded System Technologies">ME Embedded System Technologies</option>
<option data-url="MCAD" value="ME CAD/CAM">ME CAD/CAM</option>

<?
    }
}

//ajax code for application form image upload
function wp_upload_dir_url() {
    $upload_dir = wp_upload_dir();
    $upload_dir = $upload_dir['baseurl'];
    return preg_replace('/^https?:/', '', $upload_dir);
}

add_action('wp_ajax_image_uploads', 'image_uploads'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_image_uploads', 'image_uploads'); // wp_ajax_nopriv_{action}

function image_uploads() {
    ini_set('post_max_size', '8M');
    ini_set('upload_max_filesize', '8M');
    $upload_dir = wp_upload_dir();
    if (isset($_FILES['file']['tmp_name']) && $_FILES['file']['name'] != "") {


        $file_tmp = $_FILES['file']['tmp_name'];
        $actual_image_name = $_FILES['file']['name'];
        $xy = uniqid();
        $filename = $xy . '.jpg';
//    try{
        $upload_file = move_uploaded_file($file_tmp, $upload_dir['basedir'] . '/applications_docs/' . $filename);

        if ($upload_file) {

            echo 'updated';
        } else {
            echo 'error';
        }
    }
}

add_action('wp_ajax_store_ap_data', 'store_ap_data'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_store_ap_data', 'store_ap_data'); // wp_ajax_nopriv_{action}

function store_ap_data() {
    global $wpdb;

    $store_data = $_POST['form_data'];    
    $data_form = stripslashes($store_data);
    $array = json_decode($data_form, true);
    
    
    $store_data_notreq = stripslashes($_POST['form_data_notreq']);
    $array_notreq = json_decode($store_data_notreq, true);
    $course = $array['select_degree'];
    $dob = $_POST['dob'];
    $tcdate = $_POST['tcdate'];
    //print_r($store_data);
   // print_r($array);
    if ($course == 'ug') {
        $table = 'ug_app_reg';
        $sql1 = "select id from ug_app_reg order by id desc limit 1";
        $lstid_obj = $wpdb->get_row($sql1);
        $lstid = $lstid_obj->id;
        $no = 1000 + $lstid;
        $app_no = "UG" . $no;
    } else if ($course == 'pg') {
        $table = 'pg_app_reg';
        $sql1 = "select id from pg_app_reg order by id desc limit 1";
        $lstid_obj = $wpdb->get_row($sql1);
        $lstid = $lstid_obj->id;
        $no = 1000 + $lstid;
        $app_no = "PG" . $no;
        
    }

    $variable_sql = "INSERT INTO $table (`id`, `app_no`, `applicant_name`, `dob`, `age`, `gender`, `blood`, `mobile`, `mail`, `tongue`, `nation`, `religion`, `caste`, `community`, `handi`, `reserve`, `native`, `civic`, `state`, `per_add1`, `per_add2`, `per_city`, `per_pin`, `per_phone`, `same_add`, `com_add1`, `com_add2`, `com_city`, `com_phone`, `com_pin`, `father`, `finc`, `focc`, `mother`, `minc`, `mocc`, `guardian`, `ginc`, `gocc`, `quota`, `tc`, `tcdate`, `qual`, `medium`, `xyear`, `xmark`, `xclass`, `xschool`, `xiiyear`, `xiimark`, `xiiclass`, `xiischool`, `dipyear`, `dipmark`, `dipclass`, `dipschool`, `hsc_board`, `hsc_reg`, `hsc_lamark`, `hsc_lamax`, `hsc_layear`, `hsc_laper`, `hsc_enmark`, `hsc_enmax`, `hsc_enyear`, `hsc_enper`, `hsc_phmark`, `hsv_phmax`, `hsc_phyear`, `hsc_phper`, `hsc_chmark`, `hsc_chmax`, `hsc_chyear`, `hsc_chper`, `hsc_mamark`, `hsc_mamax`, `hsc_mayear`, `hsc_maper`, `hsc_eli`, `vboard`, `vreg`, `v_lamark`, `v_lamax`, `v_layear`, `v_laper`, `v_enmark`, `v_enmax`, `v_enyear`, `v_enper`, `v_rsmark`, `v_rsmax`, `v_rsyear`, `v_rsper`, `v_tmark`, `v_tmax`, `v_tyear`, `v_tper`, `v_pmark`, `v_pmax`, `v_pyear`, `v_pper`, `v_prmark`, `v_prmax`, `v_pryear`, `v_prper`, `v_eli`, `be_qual`, `be_sub`, `be_univ`, `be_reg`, `be_fmark`, `be_fmax`, `be_fyear`, `be_fper`, `be_smark`, `be_smax`, `be_syear`, `be_sper`, `be_tmark`, `be_tmax`, `be_tyear`, `be_tper`, `be_fomark`, `be_fomax`, `be_foyear`, `be_foper`, `be_fimark`, `be_fimax`, `be_fiyear`, `be_fiper`, `be_simark`, `be_simax`, `be_siyear`, `be_siper`, `be_eli`, `di_board`, `di_reg`, `di_fmark`, `di_fmax`, `di_fyear`, `di_fper`, `di_smark`, `di_smax`, `di_syear`, `di_sper`, `di_eli`, `choose`, `hostel`, `branch`, `year`, `image`, `status`, `date`, `ug_year`, `ug_mark`, `ug_class`, `ug_school`) 

	VALUES ('', '$app_no', '$array[aplicants_name]', '$dob' , '$array_notreq[age]', 'NULL', '$array[blood_group]', '$array[phonetext]', '$array[emailtext]', '$array[select_mother_toung]', '$array[nationtext]', '$array[select_religion]', '$array[select_cast]', '$array[select_comminity]', '$array_notreq[select_handicapped]', '$array_notreq[select_reserved_category]', '$array[native_place]', '$array[civicstattext]', '$array[stateselect]', '$array[perma_addrline1text]', '$array[perma_addrline2text]', '$array[perma_citytext]', '$array[perma_pincodetext]', '$array[perma_addressphonetext]', '$array[diff_addrline1text]', '$array[diff_addrline2text]', '$array[diff_citytext]', 'NULL', '$array[diff_addressphonetext]', '$array[diff_pincodetext]', '$array[father_name]', '$array_notreq[father_income]', '$array[father_occupation]', '$array[mother_name]', '$array_notreq[mother_income]', '$array[mother_occupation]', '$array_notreq[guarian_name]', '$array_notreq[guarian_income]', '$array_notreq[guarian_occupation]','$array[availconcess]', '$array[tcnumtext]', '', '$array[qualifyexamtext]', '$array[instrmedtext]', '$array_notreq[xm11]', '$array_notreq[xm12]', '$array_notreq[xm13]', '$array_notreq[xm14]', '$array_notreq[xm21]', '$array_notreq[xm22]', '$array_notreq[xm23]', '$array_notreq[xm24]', '$array_notreq[xm31]', '$array_notreq[xm32]', '$array_notreq[xm33]', '$array_notreq[xm34]', '$array_notreq[boardnametext]', '$array_notreq[regnumtext]', '$array_notreq[xm42]', '$array_notreq[xm43]', '$array_notreq[xm41]', '$array_notreq[xm44]', '$array_notreq[xm52]', '$array_notreq[xm53]', '$array_notreq[xm51]', '$array_notreq[xm54]', '$array_notreq[xm62]', '$array_notreq[xm63]', '$array_notreq[xm61]', '$array_notreq[xm64]', '$array_notreq[xm72]', '$array_notreq[xm73]', '$array_notreq[xm71]', '$array_notreq[xm74]', '$array_notreq[xm82]', '$array_notreq[xm83]', '$array_notreq[xm81]', '$array_notreq[xm84]', '$array_notreq[eligible_percentage]', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', 'NUll', '$array[select_branch]', '$array[select_year]', '$array[select_file]', '0', 'NUll', 'NUll', 'NUll', 'NUll','NUll')";

    if ($wpdb->query($variable_sql)) {
        $lastid = $wpdb->insert_id;
         generate_pdf($table,$lastid);
    } else {
        echo $variable_sql;
    }
}

function generate_pdf($table,$lastid) {
    global $wpdb;
    $sql = "select * from ".$table." where id = '".$lastid."'";
   // $sql1 = "select * from pg_app_reg order by id desc limit 1";
    $query = $wpdb->get_results($sql, ARRAY_A);
    foreach($query as $row)
{
  print_r($row);
    $no = 999 + $row['id'];
    $app_no = "PG" . $no;
    $applicant_name = $row['applicant_name'];
    $fileselect = $row['image'];
    $imageurl = "applications-docs/" . $fileselect;
    $course = $row['branch'];
    $year = $row['year'];
    $dob = $row['dob'];
    $age = $row['age'];
    $gender = $row['gender'];
    $blood = $row['blood'];
    $mobile = $row['mobile'];
    $mail = $row['mail'];
    $tongue = $row['tongue'];
    $nation = $row['nation'];
    $religion = $row['religion'];
    $caste = $row['caste'];
    $community = $row['community'];
    $handi = $row['handi'];
    $reserve = $row['reserve'];
    $native = $row['native'];
    $civic = $row['civic'];
    $state = $row['state'];
    $per_add1 = $row['per_add1'];
    $per_add2 = $row['per_add2'];
    $per_city = $row['per_city'];
    $per_pin = $row['per_pin'];
    $per_phone = $row['per_phone'];
    $same_add = $row['same_add'];
    $com_add1 = $row['com_add1'];
    $com_add2 = $row['com_add2'];
    $com_city = $row['com_city'];
    $com_city = $row['com_phone'];
    $com_pin = $row['com_pin'];
    $father = $row['father'];
    $finc = $row['finc'];
    $focc = $row['focc'];
    $mother = $row['mother'];
    $minc = $row['minc'];
    $mocc = $row['mocc'];
    $guardian = $row['guardian'];
    $ginc = $row['ginc'];
    $gocc = $row['gocc'];
    $quota = $row['quota'];
    $tc = $row['tc'];
    $tcdate = $row['tcdate'];
    $qual = $row['qual'];
    $medium = $row['medium'];
    $xyear = $row['xyear'];
    $xmark = $row['xmark'];
    $xclass = $row['xclass'];
    $xschool = $row['xschool'];
    $xiiyear = $row['xiiyear'];
    $xiimark = $row['xiimark'];
    $xiiclass = $row['xiiclass'];
    $xiischool = $row['xiischool'];
    $dipyear = $row['dipyear'];
    $dipmark = $row['dipmark'];
    $dipclass = $row['dipclass'];
    $dipschool = $row['dipschool'];
    $hsc_board = $row['hsc_board'];
    $hsc_reg = $row['hsc_reg'];
    $hsc_lamark = $row['hsc_lamark'];
    $hsc_lamax = $row['hsc_lamax'];
    $hsc_layear = $row['hsc_layear'];
    $hsc_laper = $row['hsc_laper'];
    $hsc_enmark = $row['hsc_enmark'];
    $hsc_enmax = $row['hsc_enmax'];
    $hsc_enyear = $row['hsc_enyear'];
    $hsc_enper = $row['hsc_enper'];
    $hsc_phmark = $row['hsc_phmark'];
    $hsc_phmax = $row['hsv_phmax'];
    $hsc_phyear = $row['hsc_phyear'];
    $hsc_phper = $row['hsc_phper'];
    $hsc_chmark = $row['hsc_chmark'];
    $hsc_chmax = $row['hsc_chmax'];
    $hsc_chyear = $row['hsc_chyear'];
    $hsc_chper = $row['hsc_chper'];
    $hsc_mamark = $row['hsc_mamark'];
    $hsc_mamax = $row['hsc_mamax'];
    $hsc_mayear = $row['hsc_mayear'];
    $hsc_maper = $row['hsc_maper'];
    $hsc_eli = $row['hsc_eli'];
    $vboard = $row['vboard'];
    $vreg = $row['vreg'];
    $v_lamark = $row['v_lamark'];
    $v_lamax = $row['v_lamax'];
    $v_layear = $row['v_layear'];
    $v_laper = $row['v_laper'];
    $v_enmark = $row['v_enmark'];
    $v_enmax = $row['v_enmax'];
    $v_enyear = $row['v_enyear'];
    $v_enper = $row['v_enper'];
    $v_rsmark = $row['v_rsmark'];
    $v_rsmax = $row['v_rsmax'];
    $v_rsyear = $row['v_rsyear'];
    $v_rsper = $row['v_rsper'];
    $v_tmark = $row['v_tmark'];
    $v_tmax = $row['v_tmax'];
    $v_tyear = $row['v_tyear'];
    $v_tper = $row['v_tper'];
    $v_pmark = $row['v_pmark'];
    $v_pmax = $row['v_pmax'];
    $v_pyear = $row['v_pyear'];
    $v_pper = $row['v_pper'];
    $v_prmark = $row['v_prmark'];
    $v_prmax = $row['v_prmax'];
    $v_pryear = $row['v_pryear'];
    $v_prper = $row['v_prper'];
    $v_eli = $row['v_eli'];
    $be_qual = $row['be_qual'];
    $be_sub = $row['be_sub'];
    $be_univ = $row['be_univ'];
    $be_reg = $row['be_reg'];
    $be_fmark = $row['be_fmark'];
    $be_fmax = $row['be_fmax'];
    $be_fyear = $row['be_fyear'];
    $be_fper = $row['be_fper'];
    $be_smark = $row['be_smark'];
    $be_smax = $row['be_smax'];
    $be_syear = $row['be_syear'];
    $be_sper = $row['be_sper'];
    $be_tmark = $row['be_tmark'];
    $be_tmax = $row['be_tmax'];
    $be_tyear = $row['be_tyear'];
    $be_tper = $row['be_tper'];
    $be_fomark = $row['be_fomark'];
    $be_fomax = $row['be_fomax'];
    $be_foyear = $row['be_foyear'];
    $be_foper = $row['be_foper'];
    $be_fimark = $row['be_fimark'];
    $be_fimax = $row['be_fimax'];
    $be_fiyear = $row['be_fiyear'];
    $be_fiper = $row['be_fiper'];
    $be_simark = $row['be_simark'];
    $be_simax = $row['be_simax'];
    $be_siyear = $row['be_siyear'];
    $be_siper = $row['be_siper'];
    $be_eli = $row['be_eli'];
    $di_board = $row['di_board'];
    $di_reg = $row['di_reg'];
    $di_fmark = $row['di_fmark'];
    $di_fmax = $row['di_fmax'];
    $di_fyear = $row['di_fyear'];
    $di_fper = $row['di_fper'];
    $di_smark = $row['di_smark'];
    $di_smax = $row['di_smax'];
    $di_syear = $row['di_syear'];
    $di_sper = $row['di_sper'];
    $di_eli = $row['di_eli'];
    $choose = $row['choose'];
    $hostel = $row['hostel'];
    $year = $row['year'];
    $image = $row['image'];
    $date = $row['date'];
    $ug_year = $row['ug_year'];
    $ug_mark = $row['ug_mark'];
    $ug_class = $row['ug_class'];
    $ug_school = $row['ug_school'];
// $be_sevmark = $row['be_sevmark'];
// $be_sevmax = $row['be_sevmax'];
// $be_sevyear = $row['be_sevyear'];
// $be_sevper = $row['be_sevper'];
// $be_emark = $row['be_emark'];
// $be_emax = $row['be_emax'];
// $be_eyear = $row['be_eyear'];
// $be_eper = $row['be_eper'];
}

  //  require_once get_template_directory().'/dompdf/autoload.inc.php';
// reference the Dompdf namespace
  //  use Dompdf\Dompdf;
    $html = '<!DOCTYPE html>
<html>
<body>
<TITLE>Admission Application</TITLE>
<STYLE type="text/css">



body {margin:0 auto; width:100%;}

#page_1 {position:relative; overflow: hidden;margin: 8px 0px 115px 0px;padding: 0px;border: none;;}

#page_1 #p1dimg1 {position:absolute;top:0px;left:0px;z-index:-1;height:314px;}
#page_1 #p1dimg1 #p1img1 {height:314px;}


 #p2dimg1 {position:absolute;top:133px;left:19px;z-index:-1;height:322px;}
 #p2dimg1 #p2img1 {height:322px;}





 #id_1 {border:none;margin: 0px 0px 0px 23px;padding: 0px;border:none;;overflow: hidden;}
 #id_2 {border:none;margin: 81px 0px 0px 23px;padding: 0px;border:none;;overflow: hidden;}

#p4dimg1 {position:absolute;top:7px;left:0px;z-index:-1;height:681px;}
#p4dimg1 #p4img1 {height:681px;}




.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: 11px ;line-height: 14px;}
.ft1{font: 11px ;line-height: 13px;}
.ft2{font: bold 11px ;line-height: 14px;}
.ft3{font: 12px ;line-height: 15px;}
.ft4{font: 1px ;line-height: 10px;}
.ft5{font: 1px ;line-height: 11px;}
.ft6{font: italic 12px ;line-height: 15px;}
.ft7{font: 1px ;line-height: 8px;}
.ft8{font: 1px ;line-height: 1px;}
.ft9{font: 1px ;line-height: 5px;}
.ft10{font: italic 11px ;line-height: 14px;}
.ft11{font: 1px ;line-height: 7px;}
.ft12{font: 1px ;line-height: 6px;}
.ft13{font: 1px ;line-height: 9px;}
.ft14{font: italic 12px ;text-decoration: underline;line-height: 15px;}
.ft15{font: 1px ;line-height: 13px;}
.ft16{font: 1px ;line-height: 12px;}
.ft17{font: italic bold 13px ;text-decoration: underline;line-height: 16px;}
.ft18{font: 12px; margin-left: 4px;line-height: 15px;}
.ft19{font: bold 13px ;text-decoration: underline;line-height: 16px;}

.p0{text-align: left;padding-left: 533px;margin-top: 5px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 533px;margin-top: 0px;margin-bottom: 0px;}
.p2{text-align: left;padding-left: 149px;margin-top: 12px;margin-bottom: 0px;}
.p3{text-align: left;padding-left: 319px;margin-top: 5px;margin-bottom: 0px;}
.p4{text-align: left;margin-top: 16px;margin-bottom: 0px;white-space: nowrap; padding-left: 0px;}
.p5{text-align: right;padding-right: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-left: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 23px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 18px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 40px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: left;padding-left: 16px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: right;padding-right: 309px;margin-top: 33px;margin-bottom: 0px;}
.p14{text-align: right;padding-right: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;padding-left: 34px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 98px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 42px;margin-top: 33px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 33px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: left;padding-left: 11px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: left;padding-left: 21px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap; display: inline-block;}
.p21{text-align: left;padding-left: 25px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap; display: inline-block;}
.p22{text-align: left;padding-left: 25px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap; display: inline-block;}
.p23{text-align: left;padding-left: 26px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: left;padding-left: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p25{text-align: right;padding-right: 128px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p26{text-align: right;padding-right: 36px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p27{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p28{text-align: left;padding-left: 59px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p29{text-align: left;padding-left: 40px;margin-top: 60px;margin-bottom: 0px;}
.p30{text-align: left;padding-left: 50px;margin-top: 23px;margin-bottom: 0px;}
.p31{text-align: left;padding-left: 50px;margin-top: 13px;margin-bottom: 0px;}
.p32{text-align: left;padding-left: 50px;margin-top: 14px;margin-bottom: 0px;}
.p33{text-align: left;padding-left: 273px;margin-top: 0px;margin-bottom: 0px;}
.p34{text-align: left;margin-top: 21px;margin-bottom: 0px;}
.p35{text-align: left;margin-top: 23px;margin-bottom: 0px;}
.p36{text-align: left;padding-left: 245px;margin-top: 31px;margin-bottom: 0px;}
.p37{text-align: left;margin-top: 22px;margin-bottom: 0px;}
.p38{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p39{text-align: right;padding-right: 7px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p40{text-align: left;padding-left: 151px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}

.td0{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td1{padding: 0px;margin: 0px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td3{border-bottom: #095180 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td4{border-bottom: #095180 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td5{padding: 0px;margin: 0px;vertical-align: bottom;}
.td6{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td7{border-bottom: #095180 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td8{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td9{padding: 0px;margin: 0px;vertical-align: bottom;}
.td10{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td11{padding: 0px;margin: 0px;vertical-align: bottom;}
.td12{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td13{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td14{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td15{padding: 0px;margin: 0px;vertical-align: bottom;}
.td16{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td18{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td19{padding: 0px;margin: 0px;vertical-align: bottom;}
.td20{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td21{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td22{border-bottom: #095180 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td23{border-bottom: #095180 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td24{border-bottom: #095180 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td25{padding: 0px;margin: 0px;vertical-align: bottom;}
.td26{border-left: #0c0c0c 1px solid;border-right: #0c0c0c 1px solid;border-top: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td27{border-right: #0c0c0c 1px solid;border-top: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td28{border-right: #0c0c0c 1px solid;border-top: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td29{border-right: #0c0c0c 1px solid;border-top: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td30{border-right: #0c0c0c 1px solid;border-top: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td31{border-right: #0c0c0c 1px solid;border-top: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td32{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td33{border-left: #0c0c0c 1px solid;border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td34{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td35{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td36{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td37{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td38{border-left: #0c0c0c 1px solid;border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td39{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td40{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td41{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td42{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td43{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td44{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td45{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td46{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td47{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td48{padding: 0px;margin: 0px;vertical-align: bottom;}
.td49{border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td50{border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td51{border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;vertical-align: bottom;}
.td52{border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td53{border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td54{border-left: #0c0c0c 1px solid;border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td55{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td56{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td57{border-right: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td58{border-left: #0c0c0c 1px solid;border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td59{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td60{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td61{border-right: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td62{border-left: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td63{border-left: #0c0c0c 1px solid;border-bottom: #0c0c0c 1px solid;padding: 0px;margin: 0px;;vertical-align: bottom;}
.td64{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td65{padding: 0px;margin: 0px;vertical-align: bottom;}
.td66{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td67{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td68{padding: 0px;margin: 0px;vertical-align: bottom;}
.td69{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td70{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td71{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td72{padding: 0px;margin: 0px;vertical-align: bottom;}
.td73{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td74{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td75{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td76{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td77{padding: 0px;margin: 0px;;vertical-align: bottom;}
.td78{padding: 0px;margin: 0px;;vertical-align: bottom;}

.tr0{height: 19px;}
.tr1{height: 26px;}
.tr2{height: 28px;}
.tr3{height: 29px;}
.tr4{height: 10px;}
.tr5{height: 11px;}
.tr6{height: 8px;}
.tr7{height: 45px;}
.tr8{height: 51px;}
.tr9{height: 57px;}
.tr10{height: 5px;}
.tr11{height: 38px;}
.tr12{height: 22px;}
.tr13{height: 27px;}
.tr14{height: 25px;}
.tr15{height: 39px;}
.tr16{height: 47px;}
.tr17{height: 7px;}
.tr18{height: 34px;}
.tr19{height: 15px;}
.tr20{height: 6px;}
.tr21{height: 9px;}
.tr22{height: 17px;}
.tr23{height: 20px;}
.tr24{height: 16px;}
.tr25{height: 13px;}
.tr26{height: 24px;}
.tr27{height: 12px;}
.tr28{height: 21px;}
.tr29{height: 18px;}

.t0{;margin-left: 42px;margin-top: 77px;font: 14px;}
.t1{;margin-left: 19px;margin-top: 30px;font: italic 14px;}
.t2{;margin-left: 42px;font: 14px;}
.t3{;margin-left: 19px;margin-top: 30px;font: 14px;}
.t4{;margin-left: 19px;margin-top: 20px;font: italic 14px;}
.t5{;font: 14px;}
.t6{;margin-left: 40px;margin-top: 20px;font: 14px;}
.t7{;margin-top: 41px;font: 14px;}
.t8{;margin-top: 13px;font: 14px;}
.t9{;margin-top: 42px;font: 14px;}
.t10{;margin-top: 14px;font: 14px;}
.t11{;margin-left: 19px;margin-top:0px;font: 14px;}
.t12{;font: 1px;}
.headings {font-size: 16px;
    font-weight: 600;
    margin-top: 30px;
    margin-left: 20px;
    text-decoration: underline;
    float: left;
}
.clear{
	clear: both;
	margin: 20px 0;
}
	td.tdclass {
    border: 1px solid;
    padding:5px;
}
td.tdclass:last-of-type {
    border-right: 1px solid;
}
table.table {
    margin-left: 40px;
    margin-top: 25px;
}
p{
	font-size:14px;
	text-align:center;
}
div#id_1 {
    margin-left: 40px;
    margin-top: 30px;
}
img.photo {
    width: 150px;
    height: 150px;
    position:absolute;
    top:230px;
    right:0;
}
</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">
<DIV id="p1dimg1">
<img src="img/sa_logo.png" style="width:520px;">
</DIV>


<DIV class="dclr"></DIV>
<P class="p0 ft0">Poonamallee - Avadi Road, Veeraraghavapuram,</P>
<P class="p1 ft0">Thiruverkadu P.O., <NOBR>Chennai-600</NOBR> 077.</P>
<P class="p1 ft0">Phone : 2680 1499, 2680 1999</P>
<P class="p1 ft1">Telefax : 2680 1899</P>
<P class="p1 ft1">Website : www.saec.ac.in</P>
<P class="p1 ft0">Email : saec@saec.ac.in</P>
<p class="clear"></p>
<P class="p2 ft2" style="margin-top: 40px; text-align: center; width: 400px;">APPLICATION FORM FOR ADMISSION TO ' . $course . ' ACADEMIC YEAR : 2018 - 2019</P>
<TABLE cellpadding=0 cellspacing=0 class="t0" style= "position:relative;" >
<img class= "photo" src="' . $imageurl . '">
<TR>
	<TD class="tr0 td0"><P class="p4 ft3">Application Number</P></TD>
	<TD class="tr0 td1"><P class="p5 ft3">:</P></TD>
	<TD class="tr0 td2"><P class="p6 ft3">' . $app_no . '</P></TD>
</TR>
<TR>
	<TD class="tr1 td0"><P class="p4 ft3">Applicant Name</P></TD>
	<TD class="tr1 td1"><P class="p5 ft3">:</P></TD>
	<TD class="tr1 td2"><P class="p6 ft3">' . $applicant_name . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td0"><P class="p4 ft3">Admission Sought for</P></TD>
	<TD class="tr2 td1"><P class="p5 ft3">:</P></TD>
	<TD class="tr2 td2"><P class="p6 ft3">' . $year . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td0"><P class="p4 ft3">Name Of Branch</P></TD>
	<TD class="tr3 td1"><P class="p5 ft3">:</P></TD>
	<TD class="tr3 td2"><P class="p6 ft3">' . $course . '</P></TD>
</TR>

</TABLE>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>

	<TD><P class= "headings">PERSONNEL DETAILS</P></TD>

</TR>
<tr>
<td><p class="clear"></p></td>
</tr>

<TR>
	<TD class="tr7 td8"><P class="p8 ft3">Name of Applicant (As in HSC Marksheet)</P></TD>
	<TD colspan=2 class="tr7 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr7 td12"><P class="p10 ft3">' . $applicant_name . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Date of Birth / Age</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3"><NOBR> ' . $dob . ' /</NOBR> YEARS</P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p8 ft3">Gender</P></TD>
	<TD colspan=2 class="tr3 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr3 td12"><P class="p10 ft3">' . $gender . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Blood Group</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3">' . $blood . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Mobile Number</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD class="tr2 td6"><P class="p10 ft3">' . $mobile . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p8 ft3">Email Id</P></TD>
	<TD colspan=2 class="tr3 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr3 td12"><P class="p10 ft3">' . $mail . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Mother Tongue</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3">' . $tongue . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Nationality</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3">' . $nation . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p8 ft3">Religion</P></TD>
	<TD colspan=2 class="tr3 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr3 td12"><P class="p10 ft3">' . $religion . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Caste</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3">' . $caste . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p8 ft3">Community</P></TD>
	<TD colspan=2 class="tr3 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr3 td12"><P class="p10 ft3">' . $community . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Is Physically Handicapped ?</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD class="tr2 td6"><P class="p10 ft3">' . $handi . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Reserved Cateogory</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD class="tr2 td6"><P class="p10 ft3">' . $reserve . '</P></TD>

</TR>
<TR>
	<td><P class="headings">COMMUNICATION DETAILS</P></td>

</TR>
<tr>
<td><p class="clear"></p></td>
</tr>
<TR>
	<TD class="tr10 td8"><P class="p4 ft9">&nbsp;</P></TD>
	<TD class="tr10 td10"><P class="p4 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr11 td8"><P class="p8 ft3">Native Place</P></TD>
	<TD colspan=2 class="tr11 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr11 td12"><P class="p10 ft3">' . $native . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Civic Status of Native Place</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3">' . $civic . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td8"><P class="p8 ft3">Name of the State</P></TD>
	<TD colspan=2 class="tr2 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr2 td12"><P class="p10 ft3">' . $state . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p8 ft3">Permanent Address</P></TD>
	<TD colspan=2 class="tr3 td11"><P class="p9 ft3">:</P></TD>
	<TD colspan=3 class="tr3 td12"><P class="p10 ft3">' . $per_add1 . '</P></TD>
</TR>
<TR>
	<TD class="tr12 td8"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr12 td9"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr12 td5"><P class="p4 ft8">&nbsp;</P></TD>
	<TD colspan=3 class="tr12 td12"><P class="p10 ft3">' . $per_add2 . '</P></TD>
</TR>
<TR>
	<TD class="tr13 td8"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr13 td9"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr13 td5"><P class="p4 ft8">&nbsp;</P></TD>
	<TD colspan=3 class="tr13 td12"><P class="p10 ft3">' . $per_pin . '</P></TD>
</TR>
<TR>
	<TD class="tr1 td8"><P class="p8 ft3">Res. Phone No:</P></TD>
	<TD colspan=2 class="tr1 td11"><P class="p9 ft3">:</P></TD>
	<TD class="tr1 td6"><P class="p10 ft3">' . $per_phone . '</P></TD>

</TR>

<TR>
	<TD class="tr0 td14"><P class="p8 ft3">Address for Communication</P></TD>
	<TD colspan=2 class="tr1 td11"><P class="p9 ft3">:</P></TD>
	<TD class="tr0 td16"><P class="p10 ft3">' . $com_add . '</P></TD>
</TR>
<TR>
	<TD class="tr13 td8"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr13 td9"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr13 td5"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr12 td16"><P class="p10 ft3">' . $com_add2 . '</P></TD>
</TR>
<TR>
	<TD class="tr13 td8"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr13 td9"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr13 td5"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr14 td16"><P class="p10 ft0"><NOBR>' . $com_pin . '</NOBR></P></TD>
</TR>
<TR>
	<TD class="tr2 td14"><P class="p8 ft3">Res. Phone No:</P></TD>
	<TD colspan=2 class="tr1 td11"><P class="p9 ft3">:</P></TD>
	<TD class="tr2 td16"><P class="p10 ft3">' . $com_phone . '</P></TD>
</TR>
</TABLE>




<TABLE cellpadding=0 cellspacing=0 class="t2">


</TABLE>
<p class="clear"></p>
<TABLE cellpadding=0 cellspacing=0 class="t3">
<TR>
	<td colspan="2" ><P class="headings">PARENT / GUARDIAN DETAILS</P></td>
	<p class="clear"></p>

</TR>
<tr>
<td><p class="clear"></p></td>
</tr>
<TR>
	<TD class="tr0 td17"><P class="p8 ft3">Fathers Name</P></TD>
	<TD class="tr0 td18"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr0 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr0 td20"><P class="p15 ft3">' . $father . '</P></TD>
</TR>
<TR>
	<TD class="tr1 td17"><P class="p8 ft3">Fathers Occupation</P></TD>
	<TD class="tr1 td18"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr1 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr1 td20"><P class="p15 ft3">' . $focc . '</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p8 ft3">Fathers Annual Income</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3">' . $finc . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td17"><P class="p8 ft3">Mothers Name</P></TD>
	<TD class="tr2 td18"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3">' . $mother . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td17"><P class="p8 ft3">Mothers Occupation</P></TD>
	<TD class="tr3 td18"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr3 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr3 td20"><P class="p15 ft3">' . $mocc . '</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p8 ft3">Mothers Annual Income</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3">' . $minc . '</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p8 ft3">Name of the Guardian and relationship :</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3">' . $guardian . '</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr3 td21"><P class="p8 ft3">Guardians Occupation</P></TD>
	<TD class="tr3 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr3 td20"><P class="p15 ft3">' . $gocc . '</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p8 ft3">Annual Income of Guardian</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3">' . $ginc . '</P></TD>
</TR>

<TR>
	<TD class="tr17 td17"><P class="p4 ft11">&nbsp;</P></TD>
	<TD class="tr17 td18"><P class="p4 ft11">&nbsp;</P></TD>
	<TD class="tr17 td19"><P class="p4 ft11">&nbsp;</P></TD>
</TR>
<TR >

<td colspan="2"><P class="headings" style="margin-top:50px;">ACADEMIC DETAILS</P></td>
</TR>
<tr>
<td><p class="clear"></p></td>
</tr>
<TR>
	<TD colspan=2 class="tr11 td21"><P class="p8 ft3">Whether availing First Graduate Concession.</P></TD>
	<TD class="tr11 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr11 td20"><P class="p15 ft3">' . $quota . '</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr0 td21"><P class="p8 ft3">If yes, certificate from Revenue Department</P></TD>
	<TD class="tr0 td19"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr0 td20"><P class="p4 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr0 td17"><P class="p8 ft3">is to be enclosed.</P></TD>
	<TD class="tr0 td18"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr0 td19"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr0 td20"><P class="p4 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr0 td21"><P class="p8 ft6">(Goverment Quota Candidates only)</P></TD>
	<TD class="tr0 td19"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr0 td20"><P class="p4 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr11 td21"><P class="p8 ft6">Transfer Certificate (TC) Details</P></TD>
	<TD class="tr11 td19"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr11 td20"><P class="p4 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td17"><P class="p16 ft3">(i)</P></TD>
	<TD class="tr2 td18"><P class="p4 ft3">T.C. No.</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3">' . $tc . '</P></TD>
</TR>
<TR>
	<TD class="tr2 td17"><P class="p16 ft3">(ii)</P></TD>
	<TD class="tr2 td18"><P class="p4 ft3">T.C. Date.</P></TD>
	<TD class="tr2 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr2 td20"><P class="p15 ft3"><NOBR>' . $tcdate . '</NOBR></P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr11 td21"><P class="p8 ft3">Qualifying Examination</P></TD>
	<TD class="tr11 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr11 td20"><P class="p15 ft3">' . $qual . '</P></TD>
</TR>
<TR>
	<TD class="tr3 td17"><P class="p8 ft3">Medium of Instruction</P></TD>
	<TD class="tr3 td18"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr3 td19"><P class="p14 ft3">:</P></TD>
	<TD class="tr3 td20"><P class="p15 ft3">' . $medium . '</P></TD>
</TR>
</TABLE>

<P class="p17 ft6">Academic details of (X Std & HSC / Equivalent)</P>


<TABLE class="table" >
<TR>
	<TD class = "tdclass" style="height: : 40px;"><P>S.no</P></TD>
	<TD class = "tdclass" style="height: : 40px;"><P>Class</P></TD>
	<TD class = "tdclass" style="height: : 40px;"><P>Year Of Passing</P></TD>
	<TD class = "tdclass" style="height: : 40px;"><P>% of Mark</P></TD>
	<TD class = "tdclass" style="height: : 40px;"><P>Class</P></TD>
	<TD class = "tdclass" style="width: 200px;"><P>Name and address of the School / Polytechnic</P></TD>
</TR>

<TR>

	<TD class = "tdclass" ><P >(i)</P></TD>
	<TD class = "tdclass" ><P >X Std. / Matric</P></TD>
	<TD class = "tdclass" ><P >' . $xyear . '</P></TD>
	<TD class = "tdclass" ><P >' . $xmark . '</P></TD>
	<TD class = "tdclass" ><P >' . $xclass . '</P></TD>
	<TD class = "tdclass" ><P >' . $xschool . '</P></TD>
</TR>

<TR>

	<TD class = "tdclass" ><P >(ii)</P></TD>
	<TD class = "tdclass" ><P >XII Std./Intermediate</P></TD>
	<TD class = "tdclass" ><P >' . $xiiyear . '</P></TD>
	<TD class = "tdclass" ><P >' . $xiimark . '</P></TD>
	<TD class = "tdclass" ><P >' . $xiiclass . '</P></TD>
	<TD class = "tdclass" ><P >' . $xiischool . '</P></TD>
</TR>

<TR>

	<TD class = "tdclass" ><P >(iii)</P></TD>
	<TD class = "tdclass" ><P >Diploma</P></TD>
	<TD class = "tdclass" ><P >' . $dipyear . '</P></TD>
	<TD class = "tdclass" ><P >' . $dipmark . '</P></TD>
	<TD class = "tdclass" ><P >' . $dipclass . '</P></TD>
	<TD class = "tdclass" ><P >' . $dipschool . '</P></TD>
</TR>

<TR>

	<TD class = "tdclass"><P >(iv)</P></TD>
	<TD class = "tdclass"><P >UG</P></TD>
	<TD class = "tdclass"><P ></P></TD>
	<TD class = "tdclass"><P ></P></TD>
	<TD class = "tdclass"><P ></P></TD>
	<TD class = "tdclass"><P ></P></TD>
</TR>

</TABLE>

<table class="table "  style= "margin-left: 22px; border: 0px;">
<tr>
<td><p class="clear"></p></td>
</tr>
<tr>
<td><p class="clear"></p></td>
</tr>
<TR>
	<TD ><P style= "margin-left: 22px; margin-top:40px; border: 0px;">HSC (Academic or Equivalent)</P></TD>
</TR>
<TR>
	<TD ><P >(i) Name of the Board: </P></TD>

	<TD  ><P >' . $hsc_board . '</P></TD>

</TR>
<TR>
	<TD ><P >(ii) Register Number:</P></TD>
	<TD><P >' . $hsc_reg . '</P></TD>

</TR>
</table>

<TABLE class="table" style= "margin-left: 40px;">


<TR>
	<TD class = "tdclass" ><P >Subject</P></TD>
	<TD class = "tdclass" ><P >Marks Obtained</P></TD>
	<TD class = "tdclass" ><P >Maximum Marks</P></TD>
	<TD class = "tdclass" ><P >Month and Year Of Passing</P></TD>
	<TD class = "tdclass" ><P >Percentage</P></TD>
</TR>

<TR>
	<TD class = "tdclass" ><P >Language</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_lamark . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_lamax . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_layear . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_laper . '</P></TD>
</TR>

<TR>
	<TD class = "tdclass" ><P >English</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_enmark . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_enmax . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_enyear . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_enper . '</P></TD>
</TR>

<TR>
	<TD class = "tdclass" ><P >Physics</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_phmark . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_phmax . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_phyear . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_phper . '</P></TD>
</TR>

<TR>
	<TD class = "tdclass" ><P >Chemistry</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_chmark . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_chmax . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_chyear . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_chper . '</P></TD>
</TR>

<TR>
	<TD class = "tdclass" ><P >Mathematics</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_mamark . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_mamax . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_mayear . '</P></TD>
	<TD class = "tdclass" ><P >' . $hsc_maper . '</P></TD>

</TR>



</TABLE>

<table>
	<TR>
	<TD ><P style="margin:10px 0 0 40px;">Eligible Percentage (Maths+Physics+Chemistry) = </P></TD>

	<TD ><P style="margin:10px 0 0 40px;">' . $hsc_eli . '</P></TD>
</TR>
</table>
<TABLE cellpadding=0 cellspacing=0 class="t6">
<TR>
	<TD class="tr24 td64"><P class="">Whether hostel accomodation needed ?</P></TD>
</TR>
</TABLE>
<P class="p29 ft17">Enclose attested copies of</P>
<P class="p30 ft3"><SPAN class="ft0">(1)</SPAN><SPAN class="ft18">Mark Sheets (X / Matric, HSC / Intermediate, Diploma (I to VI Sem. for Lateral Entry)</SPAN></P>
<P class="p31 ft3"><SPAN class="ft0">(2)</SPAN><SPAN class="ft18">Conduct Certificate</SPAN></P>
<P class="p31 ft3"><SPAN class="ft0">(3)</SPAN><SPAN class="ft18">Community Certificate (in the case of BC / MBC / SC / ST / SCA)</SPAN></P>
<P class="p32 ft3"><SPAN class="ft0">(4)</SPAN><SPAN class="ft18">Transfer Certificate</SPAN></P>
<P class="p31 ft3"><SPAN class="ft0">(5)</SPAN><SPAN class="ft18">Provisional Certificate (For Lateral Entry only)</SPAN></P>
<P class="p31 ft3"><SPAN class="ft0">(6)</SPAN><SPAN class="ft18">First Graduate Certificate & Joint Declaration and bring Colour Photos (6 PP, 2 Stamp Size)</SPAN></P>




<DIV id="id_1">
<P class="p33 ft6">DECLARATION BY THE APPLICANT</P>
<TABLE cellpadding=0 cellspacing=0 class="t7">
<TR>
	<TD class="tr22 td66"><P class="p4 ft3">I ........................................................................</P></TD>
	<TD class="tr22 td67"><P class="p4 ft3">(Name in full), Son / Daughter of..................................................................</P></TD>
	<TD class="tr22 td68"><P class="p4 ft0">hereby solemnly</P></TD>
</TR>
</TABLE>
<P class="p34 ft3">declare that the informations as above furnished by me are true and correct. I further declare if the information furnished is incorrect, I shall</P>
<P class="p35 ft3">liable to forfeit the admission besides facing appropriate legal action thereof.</P>
<P class="p35 ft3">Place :</P>
<TABLE cellpadding=0 cellspacing=0 class="t8">
<TR>
	<TD class="tr24 td69"><P class="p4 ft3">Date :</P></TD>
	<TD class="tr24 td70"><P class="p4 ft0">Signature of the Applicant</P></TD>
</TR>
</TABLE>
<P class="p36 ft6">DECLARATION BY THE PARENT / GUARDIAN</P>
<TABLE cellpadding=0 cellspacing=0 class="t9">
<TR>
	<TD class="tr24 td66"><P class="p4 ft3">I ........................................................................</P></TD>
	<TD class="tr24 td71"><P class="p4 ft3">(Name in full), Son / Daughter / Wife of ......................................................................</P></TD>
	<TD class="tr24 td72"><P class="p4 ft3">hereby</P></TD>
</TR>
</TABLE>
<P class="p37 ft3">solemnly declare that I am fully aware of the declaration made by my Son / Daughter / Ward as above and I declare that I will stand by the</P>
<P class="p37 ft3">above declaration.</P>
<P class="p35 ft3">Place :</P>
<TABLE cellpadding=0 cellspacing=0 class="t10">
<TR>
	<TD class="tr24 td73"><P class="p4 ft3">Date :</P></TD>
	<TD class="tr24 td74"><P class="p4 ft0">Signature of the Parent / Guardian</P></TD>
</TR>
</TABLE>
<TABLE cellpadding=0 cellspacing=0 class="t11">
<tr>
<td><p class="clear" style = "margin-top:40px;"></p></td>
</tr>
<TR>
	<TD class="tr28 td75"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr28 td48"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr28 td66"><P class="p38 ft19">OFFICE USE ONLY</P></TD>
</TR>
<TR>
	<TD class="tr18 td75"><P class="p4 ft3">Whether hostel accomodation needed ?</P></TD>
	<TD class="tr18 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr18 td66"><P class="p18 ft3">YES / NO</P></TD>
</TR>
<TR>
	<TD class="tr3 td75"><P class="p4 ft3">If the candidate is selected,</P></TD>
	<TD class="tr3 td48"><P class="p4 ft8">&nbsp;</P></TD>
	<TD class="tr3 td66"><P class="p4 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr0 td75"><P class="p40 ft3">Programme</P></TD>
	<TD class="tr0 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr0 td66"><P class="p18 ft3">B.E. / B.Tech.</P></TD>
</TR>
<TR>
	<TD class="tr29 td75"><P class="p40 ft3">Branch</P></TD>
	<TD class="tr29 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr29 td66"><P class="p18 ft3">MCA / MBA / ME CSE / ME CS / ME ES</P></TD>
</TR>
<TR>
	<TD class="tr0 td75"><P class="p40 ft3">Category</P></TD>
	<TD class="tr0 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr0 td66"><P class="p18 ft3">OC / BC / BCM / MBC / SC / ST / SCA</P></TD>
</TR>
<TR>
	<TD class="tr0 td75"><P class="p40 ft3">Admission No</P></TD>
	<TD class="tr0 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr0 td66"><P class="p18 ft3">' . $app_no . '</P></TD>
</TR>
<TR>
	<TD class="tr0 td75"><P class="p40 ft3">Date of Admission</P></TD>
	<TD class="tr0 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr0 td66"><P class="p4 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr0 td75"><P class="p40 ft3">Regular or Lateral</P></TD>
	<TD class="tr0 td48"><P class="p39 ft3">:</P></TD>
	<TD class="tr0 td66"><P class="p18 ft3">Regular / Lateral</P></TD>
</TR>
</TABLE>
</DIV>
<DIV id="id_2">
<TABLE cellpadding=0 cellspacing=0 class="t12" style="margin-left: 40px; margin-top: 0px;">
<TR>
	<TD class="tr24 td76"><P class="p4 ft3">Enclosures Verified by ................................</P></TD>
	<TD class="tr24 td77"><P class="p4 ft3">Sign. of A.O.</P></TD>
	<TD class="tr24 td78"><P class="p4 ft0">Signature of the Director</P></TD>
</TR>
</TABLE>
</DIV>
</DIV>
';
echo $html;
$header = "From:" . $applicant_name.'<'.$mail.'>'."\r\n";
$header.= "Reply-To: ". $applicant_name .'<'.$mail.'>'." \r\n";
$header.= "MIME-Version: 1.0\r\n";
$header.= "Content-Type: text/html; charset=ISO-8859-1"; 

 $to = "bibisha.webandcrafts@gmail.com, saec@saec.ac.in";
$subject = "Contact Request from $applicant_name";
 if(wp_mail($to, $subject, $html, $header)){

    $yacht = "libin.webandcrafts@gmail.com, saec@saec.ac.in";
    $replyMail = '-f'. $yacht;

    $header1= "From: S. A. Engineering Collge<" . strip_tags( $yacht) .">\r\n";
    $header1.= "Reply-To: S. A. Engineering Collge<" . strip_tags( $yacht) .">\r\n";
    $header1.= "MIME-Version: 1.0\r\n";
    $header1.= "Content-Type: text/html; charset=ISO-8859-1";

    $to1 = $mail;
    $subject1 = 'Acknowledgement for  Online Application from S. A. Engineering Collge';
    $message1 = '<div style="background: #e2e2e2; font-family: arial,sans-serif; font-size: 14px;">
                <table border="0" width="600" cellspacing="0" cellpadding="5" align="center">
                <tbody>
                <tr>
                <td height="60">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
                <tr bgcolor="#fff">
                <td height="60">&nbsp;</td>
                <td colspan="3">
                <div style="text-align: center; margin-bottom: 40px;     border-bottom: 1px solid #ddd;
                    padding-bottom: 25px;"><a href="http://saec.ac.in" target="_blank"><img src="http://saec.ac.in/img/response-mail-logo.png"></a></div>
                </td>
                <td>&nbsp;</td>
                </tr>
                <tr>
                <td bgcolor="#FFFFFF" height="32">&nbsp;</td>
                <td colspan="3" bgcolor="#FFFFFF">
                <div>
                <h2 style="color: #000; font-weight: normal; font-size: 16px;">Hello '.$name.',</h2>
                </div>
                </td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                <td bgcolor="#FFFFFF" height="26">&nbsp;</td>
                <td colspan="3" bgcolor="#FFFFFF">
                <div style="color: #333; font-size: 15px;">Thank you for your Application. We will contact you soon. We look forward to welcoming you.</div>
                </td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                <td bgcolor="#FFFFFF" height="71">&nbsp;</td>
                <td colspan="3" bgcolor="#FFFFFF">
                <div>
                <div style="color: #333; font-size: 15px;">Regards,</div>
                <div style="color: #333; font-size: 15px; ">Team SAEC.</div>
                </div>
                </td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                <td bgcolor="#FFFFFF" height="26">&nbsp;</td>
                <td colspan="3" bgcolor="#FFFFFF">
                <div style="text-align: center; font-size: 10px; color: #990000; margin-bottom: 40px;">[ This is an automated message. Please do not reply to this <span class="il">mail</span> ]</div>
                </td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                <td height="60">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </div>';
echo $message1;

    $mail = wp_mail($to1, $subject1, $message1, $header1);
 }
// instantiate and use the dompdf class
 //   $dompdf = new Dompdf();
 //   $dompdf->loadHtml($html);

// // (Optional) Setup the paper size and orientation
// $dompdf->setPaper('A4', 'portrait');
 //   $dompdf->set_option('isHtml5ParserEnabled', true);


   // $pdf = $dompdf->render();

   // $invnoabc = 'doc.pdf';

   // ob_end_clean();

  //  $dompdf->stream($invnoabc);
  //  exit;
}
?>
