<?php

/**
  Template Name: gallery
*/


get_header();
?>

<section class="gallery_page clearfix">
  <div class="container container2">
    <div class="gallery_page_title">
      <h1><?php the_title()?></h1>
      <p></p>
    </div>
    <div class="gallery_sec clearfix">
      <?php $loop = new WP_Query( array( 'post_type' => 'gallery') ); ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <ul>
        <li>
          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail(); ?>
            <div class="gallery_sec_content">
              <div class="real_icon test">
                <span>
                  <img src="<?php the_field('gallery_icon'); ?>" alt="">
                </span>
                <div class="hvr_icon">
                  <span>
                    <img src="<?php the_field('gallery_icon_on_hover'); ?>" alt="">
                  </span>
                </div>
              </div>
              <p><?php the_title(); ?></p>
            </div>
            <div class="hov_overlay"></div>
          </a>
        </li>
      </ul>
      <?php endwhile; wp_reset_query(); ?>
    </div>
  </div>
</section>



<?php
get_footer();
