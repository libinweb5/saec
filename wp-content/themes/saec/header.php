<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta charset="UTF-8">
  <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="HandheldFriendly" content="true">
  <meta http-equiv="x-rim-auto-match" content="none">
  <meta name="google-site-verification" content="BovoULUMTMwR8AQLQPVEZw0AhZ6_dZDSNX7YPB7b3SY" />
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" type="image/gif">
  <title>Best Engineering College In Chennai, Tamil Nadu | SAEC</title>
  <?php include('config.php'); ?>

  <?php wp_head(); ?>
</head>


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12460194-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();

</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122164601-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'UA-122164601-1');

</script>

<body>
  <div class="dummy-svg" style="height: 0;overflow: hidden;">
    <?php include 'img/sprite.txt';?>
  </div>
  <div id="wrapper">
    <!-- mobile menu -->
    <div class="mobile-menu"></div>
    <div id="main">
      <header>
        <div class="menu-top">
          <div class="container">
            <p class="counselting_btn">
              <?php the_field('top_head_button_title', 'option'); ?></p>
            <div class="top-menu-inner">
              <?php
                wp_nav_menu( array( 'theme_location' => 'top-head-menu', 'container' => false ) );
              ?>
            </div>

          </div>
        </div>
        <!--- menu-top -->
        <div class="menu-logo">
          <div class="container">
            <a href="<?php echo home_url();?>" class="header-logo">
              <img src="<?php the_field('logo_image', 'option'); ?>" alt="<?php the_field ('alt_text' , 'option'); ?>">
            </a>

            <a href="<?php echo home_url();?>" class="mobile-header-logo">
              <img src="<?php the_field('mobile_logo', 'option'); ?>" alt="<?php the_field ('alt_text_mobile' , 'option'); ?>">
            </a>
            <div>
              <a href="tel:044-26801999" class="header-contact">
                <label> <?php _e('Contact Us') ?></label>
                <span><?php the_field('contact_number', 'option'); ?></span>

              </a>

              <a href="tel:9941497973" class="header-contact">
                <label> <?php _e('For Admission 2019') ?></label>
                <span><?php the_field('for_admission', 'option'); ?></span>
              </a>
              
              <a href="<?php echo $siteurl ?>/online-application" class="btn_web">
                <?php the_field('apply_button_title', 'option'); ?>
              </a>
            </div>
            <div class="hammenu-wrap">
              <p class="code"> <?php the_field('top_head_button_title', 'option'); ?></p>
            </div>
          </div>
        </div>
        <div class="main-menu-nav">
          <div class="container">
            <?php
              wp_nav_menu( array( 'theme_location' => 'main-menu', 'container' => false ) );
            ?>
          </div>
        </div>
      </header>
      <!-- header -->
