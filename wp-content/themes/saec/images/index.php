<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="author" content="Abin Jose Tom, webandcrafts"/> 
  <meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
  <meta name="robots" content="index, follow" /> 
  <meta name="keywords" content="SA ENGINEERING COLLEGE,sa,s.a,college,engineering,chennai,sa chennai,saec,saec.co.in,saec.ac.in,Computer Science,Electronics Communication Engineering,Top Engineering Colleges,Engineering Colleges chennai,Top Engineering College chennai,Engineering College near to chennai, Electrical & Electronics Engineering,Civil Engineering,Professional Colleges,Mechanical Engineering,Information Technology,Engineering,syllabus,B.Tech,M.Tech,Undergraduate courses,Vaccancies,Manager,Principal,m.e," /> 
  <meta name="description" content="S.A.ENGINEERING COLLEGE" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>S.A.ENGINEERING COLLEGE</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css"/>
<link href="common.css" rel="stylesheet" type="text/css"/>
<link href="css/dropdown.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/default.advanced.css" media="all" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script type="text/javascript" src="js/swfobject.js"></script>
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>
	<body >

		<div id="container">

<?php

//menu starts here

INCLUDE 'header.php';

//menu ends here

?>

  <div class="header">

  

  <div id="banner">

    <div id="flash">

      <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="940" height="160">

        <param name="movie" value="banner.swf" />

        <param name="quality" value="high" />

        <param name="wmode" value="opaque" />

        <param name="swfversion" value="8.0.35.0" />

        <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

        <param name="expressinstall" value="Scripts/expressInstall.swf" />

        <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

        <!--[if !IE]>-->

        <object type="application/x-shockwave-flash" data="banner.swf" width="940" height="160">

          <!--<![endif]-->

          <param name="quality" value="high" />

          <param name="wmode" value="opaque" />

          <param name="swfversion" value="8.0.35.0" />

          <param name="expressinstall" value="Scripts/expressInstall.swf" />

          <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

          <div>

            <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>

          </div>

          <!--[if !IE]>-->

        </object>

        <!--<![endif]-->

      </object>

    </div>

  </div>

  </div>

<div class="cleardivf"></div>

	<div id="menubar">

		<div class="mainmenu">





<?php

//menu starts here

INCLUDE 'menu.php';

//menu ends here

?>



</div>

		<div class="search"><a href="search.php" class="searchdata"><strong>Search Here</strong></a></div>

	  <div  class="mainsubcontainer" b>

 <div class="whatnews"><div class="whatbutton"><img src="images/what'snews.png" /></div><div class="whtnwscontent"><div class="marquee"><marquee onmouseover="stop()" onmouseout="start()">
   <a href=images/logo/GL.htm class="marquee" target="newwindow">1.GUEST LECTURES</a> <a href="images/logo/IV.htm" target="newwindow" class="marquee">2. INDUSTRIAL VISITS . . .</a>
 </marquee>
       </div>

 </div>

 </div>

 <div class="sidecontainer">

 <?php

//menu starts here

INCLUDE 'rightbar.php';

//menu ends here

?>

</div>

   	<div class="maintext">

   	  <div class="maintextcontainer">

   	    <div class="contenttextonly">

   	      <div class="textheader">

   	        <div class="textheader_bg">About Us</div>

   	        <div class="insideheader2"></div>

          </div>

   	      <div class="aboutusdata">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The  Dharma Naidu Educational and Charitable Trust started functioning under the  leadership of late <strong>Thiru.D.Sudharssanam, M.L.A</strong>. The S.A. Engineering College  got established in the Academic Year 1998-99 with the approval of the AICTE and  affiliation with the University of Madras and the general policy of the Govt. of TamilNadu to give high priority to Technical Education.   <br /> <br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Impressed by the excellent record of performance of the institute academically and in the matters of financial discipline and environment inside the campus, six U.G Programmes of B.E / B.Tech with an intake of 480 P.G. Programmes of M.E, M.C.A and M.B.A. each with an intake of 18, where approved by AICTE and affiliated to Anna University.<br /> 
          <br />

   	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   The campus is spread over a vast area of about 42 acres and has 2.95 lakh sq.ft. of constructed area which includes class rooms, drawing halls, laboratories, workshops and associated facilities.   	        </div>

   	    </div>

   	    <div class="contenttextonly">

   	      <div class="textheader">

   	        <div class="textheader_bg">Vision and Mission</div>

   	        <div class="insideheader2"></div>

          </div><div class="visiondata"><span class=" maintextdata">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>To transform our institution into quality technical education center imparting updated technical knowledge with character building.<br ><br >
          
          

<span class=" maintextdata">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>To create an excellent teaching and learning environment for our staff ad students to realize their full potential thus enabling them to contribute positively to the community.To significantly enhance the self-confidence level for developing creative skills of staff and students. </div>

   	    </div>
   	    <div class="contenttextonly">
   	      <div class="textheader">
   	        <div class="textheader_bg">Quality Policy</div>
   	        <div class="insideheader2"></div>
 	        </div>
   	      <div class="visiondata"><span class=" maintextdata">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>We are committed to provide Quality Education to students to acquire life skills enabling them to achieve  academic and professional excellence.<br />
   	        <br />
   	        <span class=" maintextdata">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> We continually motivate all our educators and support service providers for enhancing professional competencies for effective implementation of Quality Management System.</div>
 	      </div>
   	    <div class="contenttextonly">
   	      <div class="logobox">
          <div class="logo" align="center">
            <marquee direction="up" scrollamount="3"  onmouseover="stop()" onmouseout="start()" height="100px" >
            <a href="images/logo/IEEE.htm" target="newwindow"><img src="images/logo/ieeelarge.jpg" alt="" width="185" height="78" /></a>
            <a href="images/logo/SME.htm" target="newwindow"><img src="images/logo/sae_lrg.jpg" alt="" width="184" height="71"  /></a>
            <a href="images/logo/CSI.htm" target="newwindow"><img src="images/logo/csi.jpg" width="101" height="98" /></a>
            </marquee>
          </div>
          <div class="video"> <a href="/video/index.html" target="newwindow"><img src="images/logo/video-gallery-icon.jpg" alt="" width="193" height="92" /></a>
          </div>
          </div>
          </div>
      </div>

   	  <div class="rightcontainer">

   	    <div class="latestnews">

   	      <div class="insideheader">

   	        <div class="insideheader1">Latest News &amp; Events</div>

   	        <div class="insideheader2" ></div>

          </div><div class="latestnewscontent" >

             <?php

//menu starts here

INCLUDE 'latestnews.php';

//menu ends here

?>

          </div>

        </div>
   	    <div class="quoteoftheday">
   	      <div class="insideheader">
   	        <div class="insideheader1"> Image Gallery</div>
   	        <div class="insideheader2"></div>
 	        </div>
   	     <script language="JavaScript1.2">

/*
Up down slideshow Script
By Dynamic Drive (www.dynamicdrive.com)
For full source code, terms of use, and 100's more scripts, visit http://www.dynamicdrive.com
*/

///////configure the below four variables to change the style of the slider///////
//set the scrollerwidth and scrollerheight to the width/height of the LARGEST image in your slideshow!
var scrollerwidth='227px'
var scrollerheight='146px'
//3000 miliseconds=3 seconds
var pausebetweenimages=3000

//configure the below variable to change the images used in the slideshow. If you wish the images to be clickable, simply wrap the images with the appropriate <a> tag
var slideimages=new Array()
slideimages[0]='<a href="photogallery.php"><img src="images/home/01.jpg" border="0"></a>'
slideimages[1]='<a href="photogallery.php"><img src="images/home/02.jpg" border="0"></a>'
slideimages[2]='<a href="photogallery.php"><img src="images/home/03.jpg" border="0"></a>'
slideimages[3]='<a href="photogallery.php"><img src="images/home/04.jpg" border="0"></a>'
slideimages[4]='<a href="photogallery.php"><img src="images/home/05.jpg" border="0"></a>'
slideimages[5]='<a href="photogallery.php"><img src="images/home/06.jpg" border="0"></a>'
slideimages[6]='<a href="photogallery.php"><img src="images/home/07.jpg" border="0"></a>'
slideimages[7]='<a href="photogallery.php"><img src="images/home/08.jpg" border="0"></a>'
slideimages[8]='<a href="photogallery.php"><img src="images/home/09.jpg" border="0"></a>'
//extend this list

///////Do not edit pass this line///////////////////////

var ie=document.all
var dom=document.getElementById

if (slideimages.length>2)
i=2
else
i=0

function move1(whichlayer){
tlayer=eval(whichlayer)
if (tlayer.top>0&&tlayer.top<=5){
tlayer.top=0
setTimeout("move1(tlayer)",pausebetweenimages)
setTimeout("move2(document.main.document.second)",pausebetweenimages)
return
}
if (tlayer.top>=tlayer.document.height*-1){
tlayer.top-=5
setTimeout("move1(tlayer)",50)
}
else{
tlayer.top=parseInt(scrollerheight)
tlayer.document.write(slideimages[i])
tlayer.document.close()
if (i==slideimages.length-1)
i=0
else
i++
}
}

function move2(whichlayer){
tlayer2=eval(whichlayer)
if (tlayer2.top>0&&tlayer2.top<=5){
tlayer2.top=0
setTimeout("move2(tlayer2)",pausebetweenimages)
setTimeout("move1(document.main.document.first)",pausebetweenimages)
return
}
if (tlayer2.top>=tlayer2.document.height*-1){
tlayer2.top-=5
setTimeout("move2(tlayer2)",50)
}
else{
tlayer2.top=parseInt(scrollerheight)
tlayer2.document.write(slideimages[i])
tlayer2.document.close()
if (i==slideimages.length-1)
i=0
else
i++
}
}

function move3(whichdiv){
tdiv=eval(whichdiv)
if (parseInt(tdiv.style.top)>0&&parseInt(tdiv.style.top)<=5){
tdiv.style.top=0+"px"
setTimeout("move3(tdiv)",pausebetweenimages)
setTimeout("move4(second2_obj)",pausebetweenimages)
return
}
if (parseInt(tdiv.style.top)>=tdiv.offsetHeight*-1){
tdiv.style.top=parseInt(tdiv.style.top)-5+"px"
setTimeout("move3(tdiv)",50)
}
else{
tdiv.style.top=scrollerheight
tdiv.innerHTML=slideimages[i]
if (i==slideimages.length-1)
i=0
else
i++
}
}

function move4(whichdiv){
tdiv2=eval(whichdiv)
if (parseInt(tdiv2.style.top)>0&&parseInt(tdiv2.style.top)<=5){
tdiv2.style.top=0+"px"
setTimeout("move4(tdiv2)",pausebetweenimages)
setTimeout("move3(first2_obj)",pausebetweenimages)
return
}
if (parseInt(tdiv2.style.top)>=tdiv2.offsetHeight*-1){
tdiv2.style.top=parseInt(tdiv2.style.top)-5+"px"
setTimeout("move4(second2_obj)",50)
}
else{
tdiv2.style.top=scrollerheight
tdiv2.innerHTML=slideimages[i]
if (i==slideimages.length-1)
i=0
else
i++
}
}

function startscroll(){
if (ie||dom){
first2_obj=ie? first2 : document.getElementById("first2")
second2_obj=ie? second2 : document.getElementById("second2")
move3(first2_obj)
second2_obj.style.top=scrollerheight
second2_obj.style.visibility='visible'
}
else if (document.layers){
document.main.visibility='show'
move1(document.main.document.first)
document.main.document.second.top=parseInt(scrollerheight)+5
document.main.document.second.visibility='show'
}
}

window.onload=startscroll

                                          </script>
                                            <ilayer id="main" width=&{scrollerwidth}; height=&{scrollerheight}; visibility=hide> <layer id="first" left=0 top=1 width=&{scrollerwidth};>
                                              <script language="JavaScript1.2">
if (document.layers)
document.write(slideimages[0])
                                            </script>

                                              </layer> <layer id="second" left=0 top=0 width=&{scrollerwidth}; visibility=hide>
                                                <script language="JavaScript1.2">
if (document.layers)
document.write(slideimages[dyndetermine=(slideimages.length==1)? 0 : 1])
                                            </script>
                                                </layer> </ilayer>
                                            <script language="JavaScript1.2">
if (ie||dom){
document.writeln('<div id="main2" style="position:relative;width:'+scrollerwidth+';height:'+scrollerheight+';overflow:hidden;">')
document.writeln('<div style="position:absolute;width:'+scrollerwidth+';height:'+scrollerheight+';clip:rect(0 '+scrollerwidth+' '+scrollerheight+' 0);left:0px;top:0px">')
document.writeln('<div id="first2" style="position:absolute;width:'+scrollerwidth+';left:0px;top:1px;">')
document.write(slideimages[0])
document.writeln('</div>')
document.writeln('<div id="second2" style="position:absolute;width:'+scrollerwidth+';left:0px;top:0px;visibility:hidden">')
document.write(slideimages[dyndetermine=(slideimages.length==1)? 0 : 1])
document.writeln('</div>')
document.writeln('</div>')
document.writeln('</div>')
}
                                            </script>   	   
 	      </div>
   	    <div class="quoteoftheday">
   	      <div class="insideheader">
   	        <div class="insideheader1"> College Magazine</div>
   	        <div class="insideheader2"></div>
 	        </div>
          <a href="gallerydata/saec_magazine_2010.pdf" "><img src="images/college_mg.jpg" width="227" height="146" /></a>
   	    </div>
      </div>

    </div>

    <div class="cleardivf"></div>   

    

             

		<?php

//menu starts here

INCLUDE 'footer.php';

//menu ends here

?>

    

  </div></div>

              



</div>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

</body>

</html>

