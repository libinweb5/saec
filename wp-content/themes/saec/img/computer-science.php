<!DOCTYPE html>

<style>
#electrical-electronics-engineering-page div#menu4 .col-sm-4 {
    min-height: 361px;
}
.owl-prev {
    display: block !important;
}
.owl-next {
    display: block !important;
}
.board_trustes_box .img-responsive{
    max-height:217px;
	height:217px;
}
div#menu6 .board_navslide {
    top: unset !important;
     /* right: 250px !important;  */
}
.course-tab-contain .tab-content-wrap .tab-content > div {
     margin-bottom: 0px !important;
}
.course-tab-contain .tab-content-wrap .tab-content table {
   
    margin-bottom: 35px !important;
}
.course-tab-contain .tab-content-wrap .tab-content > div table tr td{
    padding: 10px !important;
}
.course-tab-contain .tab-content-wrap .tab-content > div span{
    margin-bottom: 0px !important;
}
.infra_table tr td{
    border: none !important;
} 
.infra_table tr:first-of-type td{
    font-weight: unset !important;
}
.eee-fac-desc p {
    line-height: 20px;
    margin-top: 10px;
}
.eee-fac-desc {
    display: inline-block;
    width: calc(100% - 150px);
}
.eee-photo-col {
    width: 150px;
    height: 160px !important;
    display: inline-block;
    float: left;
    padding-right: 5%;
}
.eee-photo-col a.fancybox {
    height: 100%;
}
.eee-photo-col .img-responsive {
    max-width: 100%;
    display: block;
    height: 100%;
}
.eee-fac-desc a img {
    width: 35px;
    height: auto;
    padding-top: 18px;
    padding-right: 6px;
    filter: grayscale(100%);
}

.eee-fac-desc a img:hover {
    filter: unset;
}
.single-eee-fac.col-sm-4 {
    margin: 25px 0px;
    min-height: 210px; 
}
.row .text1{
    padding-top: 10px !important;
    padding-bottom: 10px !important;
}
.text{
    font-weight: 400 !important;
    text-transform: unset !important;
    font-size: 14px !important;
    padding-bottom: 70px !important;
    line-height: 1.5;
    padding-top: 10px !important;
}
.course-tab-contain .tab-content-wrap #menu1 > div p:first-of-type{
    /*text-indent: 80px;*/
    text-align: justify;
}
.course-tab-contain .tab-content-wrap .tab-content > div h4{
    width: 70%;
    display: inline-block;
}
.course-tab-contain .tab-content-wrap .tab-content > div .ach_sec{
    margin: 0px 
}

.course-tab-contain .tab-content-wrap .tab-content > div .ach_sec .col-sm-4 div{
    border: 1px solid #ccc;
    padding: 20px;
    margin-bottom: 20px;
}
.col-sm-4 .ach_sec.clearfix {
    margin: 0px !important;
    padding: 15px;
    border: 1px solid #ddd;
    height: 200px;
    text-align: center;

}
.col-sm-4 .ach_sec.clearfix a, .col-sm-4 .ach_sec.clearfix a img{
    height: 100%;
}
.tab-content .row{
    margin-top: 25px;
}

.course-tab-contain .tab-content-wrap .tab-content .col-sm-4 span{
    text-align: center;
    margin-top: 10px; 
    height: 30px;
}
div#menu5 .board_navslide {
    top: unset !important;
/*    right: 250px !important;*/
}
.owl-stage .col-sm-4 {
    width: 555px;
}
.board {
  background: #f8f8f8;
}
.board .container {
  position: relative;
}
.board h2 {
  font-family: 'Montserrat', sans-serif;
  font-size: 30px;
  line-height: 1.3;
  letter-spacing: 0.05em;
  font-weight: 600;
}
 .board_trustes_inner {
  margin-top: 80px;
}
 .board_trustes_inner .board_trustes_slder_hh {
  text-align: center;
}
 .board_trustes_inner .board_trustes_slder_hh .trustes_img {
  background: #fff;
  padding: 20px;
  border-radius: 14px;
  margin-bottom: 60px;
}
 .board_trustes_inner .board_trustes_slder_hh span {
  font-family: 'Montserrat', sans-serif;
  margin-bottom: 20px;
  font-weight: 600;
}
 .board_trustes_inner .board_trustes_slder_hh p {
  font-weight: 500;
}
.board_trustes_inner .vision_mision {
  margin-top: 220px;
}
 .board_trustes_inner .vision_mision .col-sm-8 {
  padding-left: 100px;
}
.board_trustes_inner .vision_mision .col-sm-8 p {
  font-weight: 400;
}
.board_trustes_slder_hh1.owl-carousel.owl-theme.owl-loaded .owl-controls {
    text-align: center !important;
}

@media (max-width: 1200px){
   
    .single-eee-fac.col-sm-4{
        width: 50% !important;
    }
    .eee-fac-desc p{
        font-size: 14px;
    }
    .course-tab-contain .tab-content-wrap .tab-content .col-sm-4{
        width: 48% !important;
        float: left;
    }
}
@media (max-width: 991px){
.course-tab-contain .tab-content-wrap .tab-content .col-sm-4 span {
   
    margin-bottom: 35px !important;
    }
}
@media (max-width: 768px){
    .single-eee-fac.col-sm-4{
        width: 100% !important;
    }
    .course-tab-contain .tab-content-wrap .tab-content .col-sm-4{
        width: 100% !important;
        display:block;
        margin:auto;
        float:none;
        max-width:400px;
        word-break:break-word;
    }
    .course-tab-contain .tab-content-wrap .tab-content table {
    min-width: 170px !important;
   
    }
    .course-tab-contain .tab-content-wrap .tab-content > div table tr td{
        font-size: 12px !important;
    }
    .text{
        font-size: 12!important;
    }
    span.text1{
        font-size: 12!important;
    }
}

@media (max-width: 640px){
    .col-sm-4 .ach_sec.clearfix {
        height: 200px;
    }
    .board .board_trustes_inner {
    margin-top: 60px;
    }

}

@media (max-width: 400px){
    .course-tab-contain .tab-content-wrap .tab-content .col-sm-4{
        width: 100% !important;
    }


}
@media(max-width: 600px){
.course-tab-contain .tab-content-wrap .tab-content > div {
    overflow-x: hidden !important;
    overflow-y: hidden;
    }
    .text{
        padding-bottom: 30px !important;
    }
}

</style>


<html>
<head lang="en">
    <title>Best Computer Science Engineering College In Chennai, Tamil Nadu | SAEC</title>


<meta name="description" content="SAEC has expertise in Computer Science Engineering. Join one of the Best Computer Science Engineering Colleges In Chennai, Tamil Nadu. ">
<meta name="keywords" content="best computer science engineering colleges in chennai">
<link rel="canonical" href="http://saec.ac.in/computer-science" />
    <?php include('header.php'); ?>

    <div class="courses_banner" style="background-image: url('img/banner-ug-cs.jpg')">
        <div class="container">
            <div class="course_title">
                <div>
                    <span>
                        <svg>
                            <use xlink:href="#ug-cse"></use>
                        </svg>
                    </span>
                    <h1>Computer Science <br/>and Engineering</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="course-tab-contain">
        <div class="container">
            <div class="tab-menu">
            <!--  <a href="<?php echo $siteurl ?>/online-application?ug=CS" class="btn_web">Apply Now</a> -->
                <ul>
                    <li class="active"><a data-toggle="tab" href="#menu1">About Course</a></li>
                    <li><a data-toggle="tab" href="#menu2">Department</a></li>
                    <li><a data-toggle="tab" href="#menu3">Faculty</a></li>
                    <li><a data-toggle="tab" href="#menu4">Gallery</a></li>
                    <li><a data-toggle="tab" href="#menu5">Activities </a></li>
					<li><a data-toggle="tab" id="listmenu6" href="#menu6">Department Activities</a></li>
                    <li><a data-toggle="tab" href="#menu7"> Infrastructure</a></li>
                </ul>
            </div>
            <div class="tab-content-wrap clearfix">
				<div class="tab-content fade in active" id="menu1">
					<p>The Department of Computer Science and Engineering was established in the year 1998-1999. 
						It is affiliated to Anna University , accredited by National Board of Accreditation,National. 
						The Post Graduate programme viz. M.E- Computer Science and Engineering was commissioned in the year 2006-07.
						The Department has been recognized as a research center by Anna University to the year 2015.
						The Department has modernized and well-equipped laboratories in the fields of Big Data, Cloud computing, Networking,
							Internet of Things and Mobile Application Development.
							Internet of Things Lab has been supported by Intel Intelligent systems.
					</p>

					<div>
						<h3><strong>Genesis :</strong></h3>
						<ul class="tic_list">
							<li>Affiliated to Anna University Chennai</li>
							<li><strong>Accredited by NBA in 2016 for 3 years</strong></li>
							<li>UG Started with <u>4<strong>0 </strong></u>seats in <strong><u>1998-1999</u></strong></li>
							<li>Intake increased to <strong><u>60</u></strong> in <strong><u>1999-2000</u></strong></li>
							<li>Intake increased to <strong><u>90</u></strong> in <strong><u>2002-2003</u></strong></li>
							<li>Intake increased to <strong><u>120</u></strong> in <strong><u>2005-2006</u></strong></li>
							<li>Intake increased to <strong><u>180</u></strong> in <strong><u>2012-2013</u></strong></li>
							<li>PG Started in the year <strong><u>2006-07 </u></strong>with <strong><u>18</u></strong> seats</li>
						</ul>
					</div>
					<div>
						<h3><strong>Highlights :</strong></h3>
						<ul class="tic_list">
							<li><strong>Intel Intelligent systems supported Internet of Things (IoT) Lab.</strong></li>
							<li><strong>Accredited by National Board of Accreditation.</strong></li>
							<li><strong>Awarded as THE BEST DEPARTMENT 2017-2018.</strong></li>
							<li><strong>Approved research centre of Anna University, Chennai</strong></li>
							<li><strong>Six Doctorates and 70% faculty members pursuing research.</strong></li>
							<li><strong>Received funds worth Rs.35 Lakhs from various funding agencies.</strong></li>
							<li><strong><q>Cisco Networking Academy</q>for training students and faculty members for international CCNA certification.</strong></li>
							<li><strong>Oracle academy that offers Oracle certification training and licensed oracle software usage at the college premises.</strong></li>
							<li><strong>Research and consultancy Projects by students.</strong></li>
							<li><strong>100% faculty members published research papers in reputed journals.</strong></li>
							<li><strong>23 faculty members out of 35 have produced 100% results.</strong></li>
							<li><strong>Consistent 80% results in End Semester Anna University Exams.</strong></li>
							<li><strong>Students attend internship/in-plant trainings in every semester.</strong></li>
							<li><strong>Excellent student mentoring system.</strong></li>
							<li><strong>Organizing IEEE International Conference in every year.</strong></li>
							<li><strong>Offers additional courses on recent topics in IT apart from University curriculum.</strong></li>
							<li><strong>Exclusive Placement training right from first year by external experts.</strong></li>
							<li><strong>Consistent 90% placement in every year.</strong></li>
						</ul>
					</div>

						<div>                    
							<div class="row">

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-1.JPG" data-fancybox-group="gallery20" title="">
										<img src="<?php echo $siteurl ?>/img/cse-about-1.JPG" class="img-responsive">
										</a>
									</div>
									<span>Best Department Award 2017-2018</span>                        
								</div>
								
							</div>

							<div class="row">
								
								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-2a.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
										<img src="<?php echo $siteurl ?>/img/cse-about-2a.jpg" class="img-responsive">
										</a>
									</div>
									<span>Best Accredited Student Branch award from CSI , Academic year 2016-2017</span>                        
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-2b.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
										<img src="<?php echo $siteurl ?>/img/cse-about-2b.jpg" class="img-responsive">
										</a>
									</div>
									<span>Best Accredited Student Branch award from CSI , Academic year 2015-2016</span>                        
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-2c.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2015-2016)">
										<img src="<?php echo $siteurl ?>/img/cse-about-2c.jpg" class="img-responsive">
										</a>
									</div>
									<span>Best Accredited Student Branch award from CSI , Academic year 2013-2014</span>                        
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-2d1.JPG" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
										<img src="<?php echo $siteurl ?>/img/cse-about-2d1.JPG" class="img-responsive">
										</a>
									</div>
									<span>Best Accredited Student Branch award from CSI , Academic year 2012-2013</span>                        
								</div>
								
								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-2d2.JPG" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
										<img src="<?php echo $siteurl ?>/img/cse-about-2d2.JPG" class="img-responsive">
										</a>
									</div>
									<span>Best Accredited Student Branch award from CSI , Academic year 2012-2013</span>                        
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-2e.JPG" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
										<img src="<?php echo $siteurl ?>/img/cse-about-2e.JPG" class="img-responsive">
										</a>
									</div>
									<span></span>                        
								</div>

						</div>
						
						<div>
							<div class="row">

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
									<a class="fancybox" href="img/cse-about-3a.jpg" data-fancybox-group="gallery20" title="SAEC sponsored Rs.15,000/- ECE final year Students to present their project on Development of Robo Hydro Jetter Cum Cutter for Replacement of manual Scavenging at Colombo,Srilanka">
										<img src="<?php echo $siteurl ?>/img/cse-about-3a.jpg" class="img-responsive">
										</a>
									</div>            
									<span>International Conference (ICICES-18)</span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-3b.png" data-fancybox-group="gallery20" title="SAEC sponsored Rs.22,500/- to ECE  Student Mr.Vignesh of ECE student to play for Indian Rural Hockey Team at Butan and Won 1st place">
										<img src="<?php echo $siteurl ?>/img/cse-about-3b.png" class="img-responsive">
										</a>
									</div>            
									<span>International Conference (ICICES-17)</span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-3c.jpg" data-fancybox-group="gallery20" title="SAEC Sponsored Rs.3,000/- to Ms.Janani of ECE Third year student to attend Conference at Goa organized by IEEE Students Chapter">
										<img src="<?php echo $siteurl ?>/img/cse-about-3c.jpg" class="img-responsive">
										</a>
									</div>            
									<span>International Conference (ICICES-16)</span>            
								</div>
								
							</div>
						</div>

						<div>
							<div class="row">

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
									<a class="fancybox" href="img/cse-about-4.jpg" data-fancybox-group="gallery20" title="SAEC sponsored Rs.15,000/- ECE final year Students to present their project on Development of Robo Hydro Jetter Cum Cutter for Replacement of manual Scavenging at Colombo,Srilanka">
										<img src="<?php echo $siteurl ?>/img/cse-about-4.jpg" class="img-responsive">
										</a>
									</div>            
									<span>ACM-Local Hack Day</span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-5.jpg" data-fancybox-group="gallery20" title="SAEC sponsored Rs.22,500/- to ECE  Student Mr.Vignesh of ECE student to play for Indian Rural Hockey Team at Butan and Won 1st place">
										<img src="<?php echo $siteurl ?>/img/cse-about-5.jpg" class="img-responsive">
										</a>
									</div>            
									<span>CSI State Level Convention</span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-6.jpg" data-fancybox-group="gallery20" title="SAEC Sponsored Rs.3,000/- to Ms.Janani of ECE Third year student to attend Conference at Goa organized by IEEE Students Chapter">
										<img src="<?php echo $siteurl ?>/img/cse-about-6.jpg" class="img-responsive">
										</a>
									</div>            
									<span>Third international mentoring summit empowering young Grampreneurs to create jobs, 
										V.Vivek  2018 batch won cash Prize of worth Rs.20,000</span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
									<a class="fancybox" href="img/cse-about-7a.jpg" data-fancybox-group="gallery20" title="SAEC sponsored Rs.15,000/- ECE final year Students to present their project on Development of Robo Hydro Jetter Cum Cutter for Replacement of manual Scavenging at Colombo,Srilanka">
										<img src="<?php echo $siteurl ?>/img/cse-about-7a.jpg" class="img-responsive">
										</a>
									</div>            
									<span>Sushmitha Shao won, State Level </span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-7b.jpg" data-fancybox-group="gallery20" title="SAEC sponsored Rs.22,500/- to ECE  Student Mr.Vignesh of ECE student to play for Indian Rural Hockey Team at Butan and Won 1st place">
										<img src="<?php echo $siteurl ?>/img/cse-about-7b.jpg" class="img-responsive">
										</a>
									</div>            
									<span>Sushmitha Shao won, District Level</span>            
								</div>

								<div class="col-sm-4 custom-h">
									<div class="ach_sec clearfix">
										<a class="fancybox" href="img/cse-about-8.jpg" data-fancybox-group="gallery20" title="SAEC Sponsored Rs.3,000/- to Ms.Janani of ECE Third year student to attend Conference at Goa organized by IEEE Students Chapter">
										<img src="<?php echo $siteurl ?>/img/cse-about-8.jpg" class="img-responsive">
										</a>
									</div>            
									<span>Best Alumni Award </span>            
								</div>
								
							</div>
						</div>

						<div class="course_feature">
							<div class="feat-box">
								<i>
									<svg>
										<use xlink:href="#icon-program"></use>
									</svg>
								</i>
								<div>
									<label>Program</label>
									<p>Under Graduate Degree</p>
								</div>
							</div>
							<div class="feat-box">
								<i>
									<svg>
										<use xlink:href="#icon-duration"></use>
									</svg>
								</i>
								<div>
									<label>Duration</label>
									<p>04 Years</p>
								</div>
							</div>
							<div class="feat-box">
								<i>
									<svg>
										<use xlink:href="#icon-language"></use>
									</svg>
								</i>
								<div>
									<label>Language</label>
									<p>English</p>
								</div>
							</div>
							<div class="feat-box">
								<a class="full_cont-link" href="pdf/saec.pdf" target="_blank"></a>
								<i>
									<svg>
										<use xlink:href="#icon-download"></use>
									</svg>
								</i>
								<div>
									<label>Download</label>
									<p>Brochure</p>
								</div>
							</div>
						</div>
						<div>
							<h3><strong>Courses Offered</strong></h3>
							<p><strong>B.E. – Computer Science and Engineering</strong></p>
							<p><strong>M.E. – Computer Science and Engineering</strong></p>
							<p><strong>Ph.D.</strong></p>
						</div>
						
						<div>
							<h3>Programme Educational Objectives</h3>
							<p>PEO I: Our graduates will have professional competency in the field of Computer Science and Engineering to investigate, analyze and demonstrate problem solving skills across broad range of application areas with sound technical expertise.</p><br/>
							<p>PEO II: Our graduates will have ethical standards, leadership qualities, communication, presentation and team work skills necessary to function effectively and professionally.</p><br/>
							<p>PEO III: Our graduates will adapt to new technologies, tools and methodologies, to assess and respond to the challenges of the changing environment and needs of the society by providing sustainable innovative solutions to upgrade the society forever.</p>
						</div>
						<div>
							<h3><strong>Programme Outcomes</strong></h3>
							<p><strong>Engineering Graduates will be able to:</strong></p>
							<ul class="tic_list">
								<li><strong>Engineering knowledge:</strong> Apply the knowledge of mathematics, science, engineering fundamentals, and an engineering specialization to the solution of complex engineering problems.</li>
								<li><strong>Problem analysis:</strong> Identify, formulate, review research literature, and analyze complex engineering problems reaching substantiated conclusions using first principles of mathematics, natural sciences, and engineering sciences.</li>
								<li><strong>Design/development of solutions:</strong> Design solutions for complex engineering problems and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety, and the cultural, societal, and environmental considerations.</li>
								<li><strong>Conduct investigations of complex problems:</strong> Use research-based knowledge and research methods including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions.</li>
								<li><strong>Modern tool usage:</strong> Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations.</li>
								<li><strong>The engineer and society:</strong> Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal and cultural issues and the consequent responsibilities relevant to the professional engineering practice.</li>
								<li><strong>Environment and sustainability:</strong> Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development.</li>
								<li><strong>Ethics:</strong> Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice.</li>
								<li><strong>Individual and team work</strong>: Function effectively as an individual, and as a member or leader in diverse teams, and in multidisciplinary settings.</li>
								<li><strong>Communication:</strong> Communicate effectively on complex engineering activities with the engineering community and with society at large, such as, being able to comprehend and write effective reports and design documentation, make effective presentations, and give and receive clear instructions.</li>
								<li><strong>Project management and finance:</strong> Demonstrate knowledge and understanding of the engineering and management principles and apply these to one’s own work, as a member and leader in a team, to manage projects and in multidisciplinary environments.</li>
								<li><strong>Life-long learning:</strong> Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change.</li>
							</ul>
					</div>
					<div>
						<h3><strong>Program specific outcomes</strong></h3>
						<ul class="tic_list">
							<li>To adapt to the emerging and rapidly changing Information and Communication Technologies by learning and employing new programming skills and technologies.</li>
						</ul>
						<ul class="tic_list">
							<li>To use the acquired technical knowledge across the domains with inter-personal skills in developing scalable software solutions for the industry and society needs.</li>
						</ul>
					</div>
					
				</div>
			</div>
				<div class="tab-content fade" id="menu2">
                    <div><p>The Department of Computer Science Engineering was established in the year 1998-99. The department is accredited by National Board of Accreditation. The Department has been continuously making progress in teaching-learning process. The Department is committed to create a culture that encourages technological innovation and exploration of the latest advances in the rapidly changing field of Computer Science. The Post Graduate programme viz. M.E. – Computer Science Engineering was introduced in the year 2006-07. The visiting and guest faculty comprising of eminent professionals, career diplomats and academicians with rich and varied experiences and expertise’s from the industry / government sector to enrich the potence of our students. The department has modernized and well-equipped laboratory in the fields of Computer Software, Information Technology, Image processing, Multimedia, Computer Hardware, &nbsp;Mobile &nbsp;Application Development, Grid &amp; Cloud Computing, &nbsp;Big Data, &nbsp;Internet of Things.</p></div>
					<div>
						<h3>Awards:</h3>
						<span>CSI AWARDS</span>
						<div class="ach_sec clearfix">
							<div class="col-sm-3">
								<div>
									<a class="fancybox" href="img/awards1.jpg" data-fancybox-group="gallery1" title="Best accredited student branch award & largest student branch award">
									<img src="<?php echo $siteurl ?>/img/awards1.jpg" class="img-responsive">
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div>
									<a class="fancybox" href="img/award2.jpg" data-fancybox-group="gallery1" title="Best active student branch award">
									<img src="<?php echo $siteurl ?>/img/award2.jpg" class="img-responsive">
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div>
									<a class="fancybox" href="img/award3.jpg" data-fancybox-group="gallery1" title="Best accredited student branch award">
									<img src="<?php echo $siteurl ?>/img/award3.jpg" class="img-responsive">
									</a>
								</div>
							</div>
						</div>
						<span>FACULTY AWARDS</span>
						<div class="ach_sec clearfix">
									<div class="col-sm-3">
									<div>
										<a class="fancybox" href="img/award6.jpg" data-fancybox-group="gallery2" title="Mr.S.Prabhu, received “Smart Deployment Award” by Google Apps, 2015">
										<img src="<?php echo $siteurl ?>/img/award6.jpg" class="img-responsive">
										</a>
									</div>
									</div>
						<div class="col-sm-3">
									<div>
										<a class="fancybox" href="img/award7.jpg" data-fancybox-group="gallery2" title="Mr.S.Prabhu had received an“Outstanding Contribution in Free Education Award” in the Presentation of Award and Launch of Dr. A.P.J Students Federation of India on 15-10-2015">
										<img src="<?php echo $siteurl ?>/img/award7.jpg" class="img-responsive">
										</a>
									</div>
									</div>
						
						<div class="col-sm-3">
									<div>
										<a class="fancybox" href="img/award4.jpg" data-fancybox-group="gallery2" title="Mr.S.Prabhu received “Instructor Excellence Award – Expert Level”for the Year 2015 By Cisco Networking Academy">
										<img src="<?php echo $siteurl ?>/img/award4.jpg" class="img-responsive">
										</a>
									</div>
									
						</div>			
						<div class="col-sm-3">
									<div>
										<a class="fancybox" href="img/award5.jpg" data-fancybox-group="gallery2" title="Dr.E.A.Mary Anita received “Paper Presenter Award at International Conference” from CSI during CSI Annual Convention December 2015.">
										<img src="<?php echo $siteurl ?>/img/award5.jpg" class="img-responsive">
										</a>
									</div>
									
						</div>
				</div>
			<ul class="tic_list">
			<li><strong>Dr.E.A.Mary Anita</strong> has been selected for the <strong>“Best Academic Researcher Award”</strong> by ASDF Global Awards, December 2016.</li>
			<li><strong>Dr.E.A.Mary Anita</strong> has been selected for the <strong>“National Citizenship Gold Medal Award”</strong> by Global Economic Progress &amp; Research Association GEPRA, March 2016.</li>
			<li><strong>Mr.Anwar Basha H</strong> had received <strong>“Dr.S.M. Shaik Nurddin Alumni Achiever award in Academic Research &amp; Training”</strong> on 27-03-2016 from Aalim Muhammed Salegh College of Engineering.</li>
			<li><strong>Mr.Anwar Basha H</strong> received “Alumni Achiever award in Interdisciplinary &amp; Societal Development” on 26/01/2014 from Dr. MGR Educational &amp; Research Institute University, Chennai.</li>
			</ul>
			
			</div>
			<div>
			<h3>Student's Achievements:</h3>
            
			<h3>Other Events</h3>
			<table width="667">
				<tbody>
				<tr>
				<td rowspan="3" width="88">
				<p><strong>YEAR</strong></p>
				</td>
				<td colspan="4" width="471">
				<p><strong>SYMPOSIUM</strong></p>
				</td>
				<td rowspan="3" width="108">
				<p><strong>WORKSHOP</strong></p>
				</td>
				</tr>
				<tr>
				<td colspan="2" width="249">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td colspan="2" width="222">
				<p><strong>EVENTS</strong></p>
				</td>
				</tr>
				<tr>
				<td width="147">
				<p><strong>PARTICIPATED</strong></p>
				</td>
				<td width="102">
				<p><strong>WON</strong></p>
				</td>
				<td width="132">
				<p><strong>PARTICIPATED</strong></p>
				</td>
				<td width="90">
				<p><strong>WON</strong></p>
				</td>
				</tr>
				<tr>
				<td>
				<p>2017-2018</p>
				</td>
				<td width="147">
				<p>129</p>
				</td>
				<td width="102">
				<p>10</p>
				</td>
				<td width="132">
				<p>182</p>
				</td>
				<td width="90">
				<p>34</p>
				</td>
				<td>
				<p>32</p>
				</td>
				</tr>
				<tr>
				<td width="88">
				<p>2016-2017</p>
				</td>
				<td width="147">
				<p>212</p>
				</td>
				<td width="102">
				<p>16</p>
				</td>
				<td width="132">
				<p>105</p>
				</td>
				<td width="90">
				<p>40</p>
				</td>
				<td width="108">
				<p>23</p>
				</td>
				</tr>
				<tr>
				<td width="88">
				<p>2015-2016</p>
				</td>
				<td width="147">
				<p>47</p>
				</td>
				<td width="102">
				<p>10</p>
				</td>
				<td width="132">
				<p>65</p>
				</td>
				<td width="90">
				<p>10</p>
				</td>
				<td width="108">
				<p>20</p>
				</td>
				</tr>
				<tr>
				<td width="88">
				<p>2014-2015</p>
				</td>
				<td width="147">
				<p>01</p>
				</td>
				<td width="102">
				<p>NIL</p>
				</td>
				<td width="132">
				<p>75</p>
				</td>
				<td width="90">
				<p>10</p>
				</td>
				<td width="108">
				<p>21</p>
				</td>
				</tr>
				<tr>
				<td width="88">
				<p>2013-2014</p>
				</td>
				<td width="147">
				<p>12</p>
				</td>
				<td width="102">
				<p>3</p>
				</td>
				<td width="132">
				<p>59</p>
				</td>
				<td width="90">
				<p>12</p>
				</td>
				<td width="108">
				<p>17</p>
				</td>
				</tr>
				<tr>
				<td width="88">
				<p>2012-2013</p>
				</td>
				<td width="147">
				<p>NIL</p>
				</td>
				<td width="102">
				<p>NIL</p>
				</td>
				<td width="132">
				<p>2</p>
				</td>
				<td width="90">
				<p>NIL</p>
				</td>
				<td width="108">
				<p>21</p>
				</td>
				</tr>
				</tbody>
			</table>
			</div>
			<div>
			<span>2017-2018</span>
			<table width="907">
				<tbody>
				<tr>
				<td width="51">
				<p><strong>&nbsp;</strong></p>
				<p><strong>&nbsp;</strong></p>
				<p><strong>S.NO</strong></p>
				</td>
				<td width="90">
				<p><strong>&nbsp;</strong></p>
				<p><strong>DATE OF THE EVENT</strong></p>
				<p><strong>&nbsp;</strong></p>
				</td>
				<td width="142">
				<p><strong>&nbsp;</strong></p>
				<p><strong>STUDENT NAME</strong></p>
				</td>
				<td width="57">
				<p><strong>&nbsp;</strong></p>
				<p><strong>YEAR/</strong></p>
				<p><strong>SEM</strong></p>
				</td>
				<td width="142">
				<p><strong>&nbsp;</strong></p>
				<p><strong>NATURE OF PROGRAMME</strong></p>
				</td>
				<td width="180">
				<p><strong>&nbsp;</strong></p>
				<p><strong>NAME OF THE INSTITUTION</strong></p>
				</td>
				<td width="132">
				<p><strong>&nbsp;</strong></p>
				<p><strong>EVENT NAME</strong></p>
				</td>
				<td width="113">
				<p><strong>&nbsp;</strong></p>
				<p><strong>PRIZE WINNING STATUS</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<ol>
				<li>&nbsp;</li>
				</ol>
				</td>
				<td width="90">
				<p><strong>26.7.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MUTHALAGAN .V</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-</strong></p>
				<p><strong>DREADNOUGHT’ 17</strong></p>
				</td>
				<td width="180">
				<p><strong>SRI SAIRAM INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUEST QUENCH</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>2.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>26.7.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SADHAM HUSSAIN.M</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-</strong></p>
				<p><strong>DREADNOUGHT’ 17</strong></p>
				</td>
				<td width="180">
				<p><strong>SRI SAIRAM INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUEST QUENCH</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>3.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>26.7.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>NANDEESWARAN.M</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-</strong></p>
				<p><strong>DREADNOUGHT’ 17</strong></p>
				</td>
				<td width="180">
				<p><strong>SRI SAIRAM INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>ASGARDIAN SQUAD</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				<p><strong>&nbsp;</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>4.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>26.7.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>NANDEESWARAN.M</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-</strong></p>
				<p><strong>DREADNOUGHT’ 17</strong></p>
				</td>
				<td width="180">
				<p><strong>SRI SAIRAM INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUEST QUENCH</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>5.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>01.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>POOJA.P</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PANCHSHEEL-2017</strong></p>
				</td>
				<td width="180">
				<p><strong>PRINCE SHRI VENKATESHWARA PADMAVATHY ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BRAIN BUSTERS</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>6.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>12.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VIGNESH.P</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-COGNIT</strong></p>
				</td>
				<td width="180">
				<p><strong>MNM JAIN ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>CONNECTIONS</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>7.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>12.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>M.V.NERISAI</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-COGNIT</strong></p>
				</td>
				<td width="180">
				<p><strong>MNM JAIN ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>CONNECTIONS</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>8.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>17.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SHAMLA.Y</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-SHREYAS’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SHA SUN JAIN COLLEGE FOR WOMEN</strong></p>
				</td>
				<td width="132">
				<p><strong>LINKERS</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>9.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>17.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DHARANI.A</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-SHREYAS’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SHA SUN JAIN COLLEGE FOR WOMEN</strong></p>
				</td>
				<td width="132">
				<p><strong>LINKERS</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>10.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>17.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DHARSHINI.N</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-SHREYAS’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SHA SUN JAIN COLLEGE FOR WOMEN</strong></p>
				</td>
				<td width="132">
				<p><strong>MASTER CHEF</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>11.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>17.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SAGARIKA.C</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-SHREYAS’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SHA SUN JAIN COLLEGE FOR WOMEN</strong></p>
				</td>
				<td width="132">
				<p><strong>MASTER CHEF</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>12.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VIGNESH.S</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>GAMING</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>13.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VIGNESH.S</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>14.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VIGNESH.S</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>15.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>RAJADURAI</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION, DEBUG</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>16.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MAHESH VARMA</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>17.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>RAVI SAI.G</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>&nbsp;</strong></p>
				<p><strong>FOTO-DIT</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>18.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MAHESH VARMA</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>BUG FINDER</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>19.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SARIKA YADAV.R</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>20.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>NELSON MICHAEL</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>21.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>E.NIVEDHA</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>22.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>18.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>A.MONISHA</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHSEA17</strong></p>
				</td>
				<td width="180">
				<p><strong>JNN INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>23.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>19.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>ILAKKIYA.M</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-AGNIFEST 2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>AGNI COLLEGE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>MEHANDI</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>24.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>19.08.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>KARTHIGHA.P</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-AGNIFEST 2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>AGNI COLLEGE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>MEHANDI</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>25.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>31.08.17</strong></p>
				</td>
				<td width="142">
				<p><strong>VISHNURAJAN.R</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM- KNIGHTRON’17</strong></p>
				</td>
				<td width="180">
				<p><strong>LOYOLA INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUIZ TECHNICA</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>26.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>31.08.17</strong></p>
				</td>
				<td width="142">
				<p><strong>VIJAI SHANKAR.R</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM- KNIGHTRON’17</strong></p>
				</td>
				<td width="180">
				<p><strong>LOYOLA INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUIZ TECHNICA</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>27.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>31.08.17</strong></p>
				</td>
				<td width="142">
				<p><strong>SHANMUGAM.M</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM- KNIGHTRON’17</strong></p>
				</td>
				<td width="180">
				<p><strong>LOYOLA INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUIZ TECHNICA</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>28.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>31.08.17</strong></p>
				</td>
				<td width="142">
				<p><strong>SARAVAN PERUMAL</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM- KNIGHTRON’17</strong></p>
				</td>
				<td width="180">
				<p><strong>LOYOLA INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>DEBUG TRASH</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>29.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>31.08.17</strong></p>
				</td>
				<td width="142">
				<p><strong>SARAVANAN.M.R</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM- KNIGHTRON’17</strong></p>
				</td>
				<td width="180">
				<p><strong>LOYOLA INSTITUTE OF TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>QUIZ TECHNICA</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>30</p>
				<p>&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MURALIDHARAN</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>31.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>G.B.NAGAYUGESH</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>32.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>C.R.KOUSHIK</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>33.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>NAVEENRAJ.K</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>34.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>RANJITHKUMAR</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>35.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>RAGHURAM.R</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>36.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DEEPAK KESAVAN</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>37.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>NIRMALA.A</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>QIUZ,SHERLOCK</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>38.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>ENIYAN KUMAR</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>39.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>JISHNU.R</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>BLENDER</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>40.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>H.BHUVANESH</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>CODINQUIDDITCH</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>41.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>LAKSHWANTH PRASAD</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>CODINQUIDDITCH</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>42.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DHARSHINI.N</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION,</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>43.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SAGARIKA.C</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PECTRON</strong></p>
				</td>
				<td width="180">
				<p><strong>PRATHYUSHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION,</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>44.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>08.9.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SAMJAISINGH.K</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>WORKSHOP-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>IOT</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>45.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MAHALAKSHMI.D</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MATLAB</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>46.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>KRUPALI.R</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MATLAB</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>47.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>PRASANTH.B</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MATLAB</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>48.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VENKATESH M.K</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MATLAB</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>49.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MOHAMED HUSSAIN</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MATLAB</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>50.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MONICA.K</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MATLAB</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>51.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SHAFRIN.N</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-ARTIFICIAL INTELLIGENCE &amp; IOT</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>52.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SACHIN KARTHICK.R</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- ARTIFICIAL INTELLIGENCE &amp; IOT</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>53.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>KARTHIK.B</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- ARTIFICIAL INTELLIGENCE &amp; IOT</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>54.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SRIDHAR.M</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>55.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MONIKA.A</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INVENTE2.0</strong></p>
				</td>
				<td width="180">
				<p><strong>SSN COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>56.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>UDHAYAKUMAR.S</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-CRYPTRIX(KERNEL)</strong></p>
				</td>
				<td width="180">
				<p><strong>ST JOSEPH’S COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>TECHVSINE</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>57.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>09.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>UDHAYAKUMAR.S</strong></p>
				</td>
				<td width="57">
				<p><strong>II/III</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-CRYPTRIX(KERNEL)</strong></p>
				</td>
				<td width="180">
				<p><strong>ST JOSEPH’S COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WEBSTER</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>58.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>15.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>ASHWITHA.R</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM- INSPIRA 2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>ALPHA COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>WEBIND</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>59.</p>
				</td>
				<td width="90">
				<p><strong>22.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SATHISHKUMAR.S</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PENTESTER 2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>JERUSALEM COLLEGE OF ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>TECHQ</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>60.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>22.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VIGNESH.N</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-WISTERIA2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>JERUSALEM COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>CONNECTION</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>61.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>22.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VISHNURAJAN.R</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-PENTESTER2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>JERUSALEM COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>DEBUGGING BUG HUNT</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				<p><strong>&nbsp;</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>62.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>23.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>ANANDHAA LAKSHMI</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-KINITRIYAS2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>SRI VENKATESWARA COLEEGE OF ENGINEERING AND TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>63.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>23.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>JEEVITHA.S</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-INTERRUPT2K17</strong></p>
				</td>
				<td width="180">
				<p><strong>SRI VENKATESWARA COLEEGE OF ENGINEERING AND TECHNOLOGY</strong></p>
				</td>
				<td width="132">
				<p><strong>PAPER PRESENTATION</strong></p>
				</td>
				<td width="113">
				<p><strong>THIRD PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>64.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>23.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SATYA PRIYA</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>SYMPOSIUM-TECHNOFES</strong></p>
				</td>
				<td width="180">
				<p><strong>DMI COLLEGE OF ENGINEERING</strong></p>
				</td>
				<td width="132">
				<p><strong>BRAIN TWISTER</strong></p>
				</td>
				<td width="113">
				<p><strong>SECOND PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>65.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>26.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>NANDEESWARAN.M</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>MIND SWEEPER</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>66&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>26.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DIVAGAR.T</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>MIND SWEEPER</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>67.</p>
				</td>
				<td width="90">
				<p><strong>26.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>PRASAD.R</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>MIND SWEEPER</strong></p>
				</td>
				<td width="113">
				<p><strong>FIRST PRIZE</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>68 .</p>
				</td>
				<td width="90">
				<p><strong>26.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SINDHU BALA.B</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MANGO D</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>69.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>26.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SWETHA.M</strong></p>
				</td>
				<td width="57">
				<p><strong>IV/VII</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-MANGO D</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>70.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>VISHAL.M</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP-</strong></p>
				<p><strong>WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>71.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DIVYA SREE</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>72.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>DHAARANI NAMBI</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>73 .</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SIVA KUMAR.P</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>74.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>SUNIL</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>75.&nbsp;</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>THENMOZHI.B</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				<tr>
				<td width="51">
				<p>76.</p>
				</td>
				<td width="90">
				<p><strong>27.09.2017</strong></p>
				</td>
				<td width="142">
				<p><strong>MOHAMMAD JOHNVAP</strong></p>
				</td>
				<td width="57">
				<p><strong>III/V</strong></p>
				</td>
				<td width="142">
				<p><strong>DRESTIEN’17</strong></p>
				</td>
				<td width="180">
				<p><strong>SAVEETHA ENGINEERING COLLEGE</strong></p>
				</td>
				<td width="132">
				<p><strong>WORKSHOP- WEB DEVELOPMENT( ANGULAR JS)</strong></p>
				</td>
				<td width="113">
				<p><strong>PARTICIPATION</strong></p>
				</td>
				</tr>
				</tbody>
				</table>
			</div>
			
			<div>
			<span>Student's Publications</span>
				<table width="667">
				<tbody>
				<tr>
				<td width="61"><strong>S.No</strong></td>
				<td width="91"><strong>Academic Year</strong></td>
				<td width="102"><strong>National Conference</strong></td>
				<td width="108"><strong>International Conference</strong></td>
				<td width="114"><strong>International Journal</strong></td>
				<td width="96"><strong>National Journal</strong></td>
				<td width="96"><strong>Total</strong></td>
				</tr>
				<tr>
				<td width="61">1</td>
				<td width="91">2015-2016</td>
				<td width="102">55</td>
				<td width="108">15</td>
				<td width="114">14</td>
				<td width="96">–</td>
				<td width="96">84</td>
				</tr>
				<tr>
				<td width="61">2</td>
				<td width="91">2014-2015</td>
				<td width="102">48</td>
				<td width="108">25</td>
				<td width="114">21</td>
				<td width="96">1</td>
				<td width="96">95</td>
				</tr>
				<tr>
				<td width="61">3</td>
				<td width="91">2013-2014</td>
				<td width="102">53</td>
				<td width="108">05</td>
				<td width="114">15</td>
				<td width="96">–</td>
				<td width="96">73</td>
				</tr>
				<tr>
				<td width="61">4</td>
				<td width="91">2012-2013</td>
				<td width="102">07</td>
				<td width="108">–</td>
				<td width="114">22</td>
				<td width="96">08</td>
				<td width="96">37</td>
				</tr>
				</tbody>
				</table>
			</div>
			
			<div>
			<h4>Sports</h4>
				<table width="617">
				<tbody>
				<tr>
				<td width="113">
				<p><strong>SL.NO</strong></p>
				</td>
				<td width="262">
				<p><strong>ACADEMIC YEAR</strong></p>
				</td>
				<td width="242">
				<p><strong>NO OF STUDENTS PARTICIPATED</strong></p>
				</td>
				</tr>
				<tr>
				<td width="113">
				<p><strong>1.</strong></p>
				</td>
				<td width="262">
				<p><strong>2017-2018</strong></p>
				</td>
				<td width="242">
				<p><strong>19</strong></p>
				</td>
				</tr>
				<tr>
				<td width="113">
				<p><strong>2.</strong></p>
				</td>
				<td width="262">
				<p><strong>2016-2017</strong></p>
				</td>
				<td width="242">
				<p><strong>81</strong></p>
				</td>
				</tr>
				<tr>
				<td width="113">
				<p><strong>3.</strong></p>
				</td>
				<td width="262">
				<p><strong>2015-2016</strong></p>
				</td>
				<td width="242">
				<p><strong>47</strong></p>
				</td>
				</tr>
				<tr>
				<td width="113">
				<p><strong>4.</strong></p>
				</td>
				<td width="262">
				<p><strong>2014-2015</strong></p>
				</td>
				<td width="242">
				<p><strong>51</strong></p>
				</td>
				</tr>
				<tr>
				<td width="113">
				<p><strong>5.</strong></p>
				</td>
				<td width="262">
				<p><strong>2013-2014</strong></p>
				</td>
				<td width="242">
				<p><strong>41</strong></p>
				</td>
				</tr>
				<tr>
				<td width="113">
				<p><strong>6.</strong></p>
				</td>
				<td width="262">
				<p><strong>2012-2013</strong></p>
				</td>
				<td width="242">
				<p><strong>31</strong></p>
				</td>
				</tr>
				</tbody>
				</table>
			</div>
			
			<div>
			<span>2017-2018</span>
					<table>
					<tbody>
					<tr>
					<td width="70">
					<p><strong>S.NO</strong></p>
					</td>
					<td width="102">
					<p><strong>DATE</strong></p>
					</td>
					<td width="129">
					<p><strong>NAME OF STUDENT</strong></p>
					</td>
					<td width="88">
					<p><strong>YEAR/SEM</strong></p>
					</td>
					<td width="119">
					<p><strong>NAME OF INSTITUTION</strong></p>
					</td>
					<td width="109">
					<p><strong>NAME OF EVENT</strong></p>
					</td>
					<td width="98">
					<p><strong>PRIZE WINNING STATUS</strong></p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>21.09.2017</p>
					</td>
					<td width="129">
					<p>M.DIVYA LAKSHMI</p>
					</td>
					<td width="88">
					<p>III/V</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>VOLLEY BALL</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>21.09.2017</p>
					</td>
					<td width="129">
					<p>A.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;A.MONIKA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>VOLLEY BALL</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>21.09.2017</p>
					</td>
					<td width="129">
					<p>SUSHIMITHA SAHU</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>VOLLEY BALL</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>21.09.2017</p>
					</td>
					<td width="129">
					<p>PREETHI. J</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>VOLLEY BALL</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>20.09.2017</p>
					</td>
					<td width="129">
					<p>S. SAKTHIVEL</p>
					</td>
					<td width="88">
					<p>IV/VII</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>KHO-KHO</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>14/09/2017 TO 15/09/2017</p>
					</td>
					<td width="129">
					<p>S. INDUMATHY</p>
					</td>
					<td width="88">
					<p>III/V</p>
					</td>
					<td width="119">
					<p>S. A&nbsp; ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BASKETBALL</p>
					</td>
					<td width="98">
					<p>SECOND ROUND</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>14/09/2017 TO 15/09/2017</p>
					</td>
					<td width="129">
					<p>M. DIVYA LAKSHMI</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>S. A&nbsp; ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BASKETBALL</p>
					</td>
					<td width="98">
					<p>SECOND ROUND</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>31.08.2017</p>
					</td>
					<td width="129">
					<p>PRADHOTHA RAO</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BADMINTON</p>
					</td>
					<td width="98">
					<p>THIRD PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>31.08.2017</p>
					</td>
					<td width="129">
					<p>SURYA NARAYANA RAO</p>
					</td>
					<td width="88">
					<p>I</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BADMINTON</p>
					</td>
					<td width="98">
					<p>THIRD PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>10.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>31.08.2017</p>
					</td>
					<td width="129">
					<p>S SANDHYA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BADMINTON</p>
					</td>
					<td width="98">
					<p>LOSED IN SECOND ROUND</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>11.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>31.08.2017</p>
					</td>
					<td width="129">
					<p>A MONIKA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>VELAMMAL ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BADMINTON</p>
					</td>
					<td width="98">
					<p>LOSED IN SECOND ROUND</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>12.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>11.08.2017</p>
					</td>
					<td width="129">
					<p>S SANDHYA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>PRATHYUSHA ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BADMINTON</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>13.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>11.08.2017</p>
					</td>
					<td width="129">
					<p>A MONIKA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>PRATHYUSHA ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>BADMINTON</p>
					</td>
					<td width="98">
					<p>QUARTER FINALS</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>14.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>7.08.2017</p>
					</td>
					<td width="129">
					<p>S SANDHYA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>S. A&nbsp; ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>TABLE TENNIS</p>
					</td>
					<td width="98">
					<p>THIRD PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>15.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>7.08.2017</p>
					</td>
					<td width="129">
					<p>A MONIKA</p>
					</td>
					<td width="88">
					<p>II/III</p>
					</td>
					<td width="119">
					<p>S. A&nbsp; ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>TABLE TENNIS</p>
					</td>
					<td width="98">
					<p>THIRD PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>16.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>7.08.2017</p>
					</td>
					<td width="129">
					<p>H VINUSHA</p>
					</td>
					<td width="88">
					<p>IV/VII</p>
					</td>
					<td width="119">
					<p>S. A&nbsp; ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>TABLE TENNIS</p>
					</td>
					<td width="98">
					<p>SECOND PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>17.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>7.08.2017</p>
					</td>
					<td width="129">
					<p>S INDUMATHY</p>
					</td>
					<td width="88">
					<p>III/V</p>
					</td>
					<td width="119">
					<p>S. A&nbsp; ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>TABLE TENNIS</p>
					</td>
					<td width="98">
					<p>SECOND PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>18.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>03/08/2017</p>
					</td>
					<td width="129">
					<p>G SENTHIL RAGHAVAN</p>
					</td>
					<td width="88">
					<p>III/V</p>
					</td>
					<td width="119">
					<p>JAYA ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>CHESS</p>
					</td>
					<td width="98">
					<p>THIRD PLACE</p>
					</td>
					</tr>
					<tr>
					<td width="70">
					<p>19.&nbsp; &nbsp;</p>
					</td>
					<td width="102">
					<p>03/08/2017</p>
					</td>
					<td width="129">
					<p>H VINUSHA</p>
					</td>
					<td width="88">
					<p>IV/VII</p>
					</td>
					<td width="119">
					<p>JAYA ENGINEERING COLLEGE</p>
					</td>
					<td width="109">
					<p>CHESS</p>
					</td>
					<td width="98">
					<p>THIRD PLACE</p>
					</td>
					</tr>
					</tbody>
					</table>
			</div>
			<div>
			<h3>Research and Development</h3>
			<h4>BEST PROJECTS</h4>
			<span><a href="pdf/best-projects-16-17.pdf" target="_blank">List of best projects</a></span>
			<span>
			</div>
			<div>
			<h4>BOOK PUBLICATIONS</h4>
			<table width="705">
			<tbody>
			<tr>
			<td width="53">
			<p><strong>S.No.</strong></p>
			</td>
			<td width="174">
			<p><strong>Name of the Faculty</strong></p>
			</td>
			<td width="228">
			<p><strong>Title of the Book</strong></p>
			</td>
			<td width="114">
			<p><strong>Name of the Publisher</strong></p>
			</td>
			<td width="137">
			<p><strong>Year of</strong></p>
			<p><strong>&nbsp;Publication</strong></p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>1.</p>
			</td>
			<td width="174">
			<p>Dr.S.Veena</p>
			</td>
			<td width="228">
			<p>Human Computer Interaction</p>
			</td>
			<td width="114">
			<p>Dhanam Publications</p>
			</td>
			<td width="137">
			<p>March 2017</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>2.</p>
			</td>
			<td width="174">
			<p>Dr.G.Umarani Srikanth.</p>
			<p>Mrs.R.Geetha,</p>
			<p>Mrs.M.Preetha</p>
			</td>
			<td width="228">
			<p>Multi Core Architecture and Programming</p>
			</td>
			<td width="114">
			<p>Sams Publications</p>
			</td>
			<td width="137">
			<p>February 2017</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>3.</p>
			</td>
			<td width="174">
			<p>Dr.E.A.Mary Anita</p>
			</td>
			<td width="228">
			<p>Bharat Ratna Sir M.Visvesvaraya</p>
			</td>
			<td width="114">
			<p>VSRD Academic Publishing</p>
			</td>
			<td width="137">
			<p>November 2016</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>4.</p>
			</td>
			<td width="174">
			<p>Dr.G.Umarani srikanth</p>
			</td>
			<td width="228">
			<p>A Novel Task Scheduling Model using Ant Colony Optimization, Advances In Computer Networks And Information Technology Volume I, (Book Chapter 6)</p>
			</td>
			<td width="114">
			<p>United Scholars Publications</p>
			</td>
			<td width="137">
			<p>January 2016</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>5.</p>
			</td>
			<td width="174">
			<p>Dr.E.A.Mary Anita</p>
			</td>
			<td width="228">
			<p>Securing Multicast Routing Protocols for Mobile Adhoc Networks</p>
			<p>&nbsp;</p>
			</td>
			<td width="114">
			<p>Lambert Academic Publishing, Germany</p>
			</td>
			<td width="137">
			<p>2013</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>6.</p>
			</td>
			<td width="174">
			<p>Dr.E.A.Mary Anita</p>
			</td>
			<td width="228">
			<p>Soft Computing for Intelligent Transportation, pg 78-89.(Book Chapter)</p>
			<p>&nbsp;</p>
			</td>
			<td width="114">
			<p>Charulatha Publications</p>
			</td>
			<td width="137">
			<p>2012</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>7.</p>
			</td>
			<td width="174">
			<p>R.Geetha</p>
			</td>
			<td width="228">
			<p>Object Oriented Programming</p>
			</td>
			<td width="114">
			<p>Charulatha Publications</p>
			</td>
			<td width="137">
			<p>2002</p>
			</td>
			</tr>
			<tr>
			<td width="53">
			<p>8.</p>
			</td>
			<td width="174">
			<p>R.Geetha</p>
			</td>
			<td width="228">
			<p>System Software</p>
			</td>
			<td width="114">
			<p>Charulatha Publications</p>
			</td>
			<td width="137">
			<p>2002</p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		
		
		<div>
		<h3>Sponsored Research – Completed</h3>
		<table width="687">
		<tbody>
		<tr>
		<td width="175">
		<p><strong>Project Title</strong></p>
		</td>
		<td width="86">
		<p><strong>Funding Agency</strong></p>
		</td>
		<td width="161">
		<p><strong>Project Coordinator</strong></p>
		</td>
		<td width="114">
		<p><strong>Sanction Details </strong></p>
		</td>
		<td width="139">
		<p><strong>Duration &amp; Status</strong></p>
		</td>
		</tr>
		<tr>
		<td width="175">
		<p>Design and Development of interactive Off Line software for Analysis of Automotive Trial Data</p>
		</td>
		<td width="86">
		<p>CVRDE</p>
		</td>
		<td width="161">
		<p>Dr.E.A.Mary Anita</p>
		<p>&nbsp;</p>
		</td>
		<td width="114">
		<p>Rs.9,80,223 sanctioned on 3/7/2016</p>
		</td>
		<td width="139">
		<p>Submitted on 01/12/2016</p>
		</td>
		</tr>
		<tr>
		<td width="175">
		<p>CISCO Network LAB under MODROBS Scheme</p>
		</td>
		<td width="86">
		<p>AICTE</p>
		</td>
		<td width="161">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="114">
		<p>Rs 8,90,000 sanctioned on 07/02/2014</p>
		</td>
		<td width="139">
		<p>1 year (2014-2015)Submitted UC on19/3/2015</p>
		</td>
		</tr>
		</tbody>
		</table>
	</div>
	

	<div>
		<h3>Grants Received</h3>
	<h4><u>2017-2018</u></h4>
	<table width="681">
	<tbody>
	<tr>
	<td width="173">
	<p><strong>Title of the Proposal</strong></p>
	</td>
	<td width="82">
	<p><strong>Funding Agency</strong></p>
	</td>
	<td width="163">
	<p><strong>Coordinators</strong></p>
	</td>
	<td width="113">
	<p><strong>Sanction Details</strong></p>
	</td>
	<td width="138">
	<p><strong>Status</strong></p>
	</td>
	</tr>
	<tr>
	<td width="173">
	<p>Travel Grant</p>
	</td>
	<td width="82">
	<p>ITS-SERB</p>
	</td>
	<td width="163">
	<p>Dr.E.A.Mary Anita</p>
	<p>&nbsp;</p>
	</td>
	<td width="113">
	<p>Rs.20,000 sanctioned on</p>
	<p>3.11.17</p>
	</td>
	<td width="138">
	<p>&nbsp;</p>
	</td>
	</tr>
	<tr>
	<td width="173">
	<p>National Workshop on Quantum Computation and Cryptography</p>
	</td>
	<td width="82">
	<p>SERB-DST</p>
	</td>
	<td width="163">
	<p>Dr.E.A.Mary Anita</p>
	<p>Dr.S.Veena</p>
	</td>
	<td width="113">
	<p>Rs.50000 sanctioned on</p>
	<p>30.10.17</p>
	</td>
	<td width="138">
	<p>To be conducted on 23/01/2018</p>
	</td>
	</tr>
	<tr>
	<td width="173">
	<p>State Student convention</p>
	</td>
	<td width="82">
	<p>Computer Society of India</p>
	</td>
	<td width="163">
	<p>Dr.R.Geetha, CSE</p>
	<p>Dr.N.Partheeban, IT</p>
	<p>Ms.V.Sujatha, MCA</p>
	</td>
	<td width="113">
	<p>Rs.25,000 sanctioned on</p>
	<p>24.8.17</p>
	</td>
	<td width="138">
	<p>&nbsp;</p>
	</td>
	</tr>
	<tr>
	<td width="173">
	<p>Workshop on Phython Programming</p>
	</td>
	<td width="82">
	<p>ISTE</p>
	</td>
	<td width="163">
	<p>Dr. R.Geetha</p>
	<p>&nbsp;</p>
	</td>
	<td width="113">
	<p>Rs.5000 sanctioned on</p>
	<p>03.08.17</p>
	</td>
	<td width="138">
	<p>Conducted Workshop on 22/09/2017</p>
	</td>
	</tr>
	</tbody>
	</table>
	</div>
	
	
	<div>
	<h4><u>2016-2017</u></h4>
			<table width="681">
			<tbody>
			<tr>
			<td width="173">
			<p><strong>Title of the Proposal</strong></p>
			</td>
			<td width="82">
			<p><strong>Funding Agency</strong></p>
			</td>
			<td width="163">
			<p><strong>Coordinators</strong></p>
			</td>
			<td width="113">
			<p><strong>Sanction Details</strong></p>
			</td>
			<td width="138">
			<p><strong>Status</strong></p>
			</td>
			</tr>
			<tr>
			<td width="173">
			<p>Travel Grant</p>
			</td>
			<td width="82">
			<p>ITS-SERB</p>
			</td>
			<td width="163">
			<p>Dr.E.A.Mary Anita</p>
			<p>&nbsp;</p>
			</td>
			<td width="113">
			<p>Rs.20,000 sanctioned on</p>
			<p>3.11.17</p>
			</td>
			<td width="138">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="173">
			<p>National Workshop on Quantum Computation and Cryptography</p>
			</td>
			<td width="82">
			<p>SERB-DST</p>
			</td>
			<td width="163">
			<p>Dr.E.A.Mary Anita</p>
			<p>Dr.S.Veena</p>
			</td>
			<td width="113">
			<p>Rs.50000 sanctioned on</p>
			<p>30.10.17</p>
			</td>
			<td width="138">
			<p>To be conducted on 23/01/2018</p>
			</td>
			</tr>
			<tr>
			<td width="173">
			<p>State Student convention</p>
			</td>
			<td width="82">
			<p>Computer Society of India</p>
			</td>
			<td width="163">
			<p>Dr.R.Geetha, CSE</p>
			<p>Dr.N.Partheeban, IT</p>
			<p>Ms.V.Sujatha, MCA</p>
			</td>
			<td width="113">
			<p>Rs.25,000 sanctioned on</p>
			<p>24.8.17</p>
			</td>
			<td width="138">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="173">
			<p>Workshop on Phython Programming</p>
			</td>
			<td width="82">
			<p>ISTE</p>
			</td>
			<td width="163">
			<p>Dr. R.Geetha</p>
			<p>&nbsp;</p>
			</td>
			<td width="113">
			<p>Rs.5000 sanctioned on</p>
			<p>03.08.17</p>
			</td>
			<td width="138">
			<p>Conducted Workshop on 22/09/2017</p>
			</td>
			</tr>
			</tbody>
			</table>
			</div>
			<div>
			<h4><u>2015-2016</u></h4>
			<table width="625">
			<tbody>
			<tr>
			<td width="163"><strong>Title of the Proposal</strong></td>
			<td width="76"><strong>Funding Agency</strong></td>
			<td width="152"><strong>Coordinators</strong></td>
			<td width="105"><strong>Sanction Details</strong></td>
			<td width="129"><strong>Status</strong></td>
			</tr>
			<tr>
			<td width="163">Workshop on Data Science Research</td>
			<td width="76">DST- ICPS</td>
			<td width="152">Dr.E.A.Mary Anita, &amp; Dr.G.Umarani Srikanth</td>
			<td width="105">Rs. 6,00,000 sanctioned on 06/04/2016</td>
			<td width="129">To be conducted during 15/12/2016 to 17/12/2016</td>
			</tr>
			<tr>
			<td width="163">Workshop on Mathematical Modelling of WSN</td>
			<td width="76">IEEE Madras Section</td>
			<td width="152">Dr.E.A.Mary Anita, &amp; Dr.G.Umarani Srikanth</td>
			<td width="105">Rs. 5000 sanctioned on 31/05/2016</td>
			<td width="129">Conducted on 28/07/2016 Submitted UC</td>
			</tr>
			<tr>
			<td width="163">ACM-W Networking Grant</td>
			<td width="76">ACM</td>
			<td width="152">Dr.E.A.Mary Anita &amp; Dr.G.Umarani Srikanth</td>
			<td width="105">$300 (Rs.19800) sanctioned on 4 /02/ 2016</td>
			<td width="129">Conducted on 23/3/2016 &amp; 24/3/2016</td>
			</tr>
			<tr>
			<td width="163">National Level Workshop on “MATLAB and its Applications in Computational Intelligence”</td>
			<td width="76">IEEE Madras Section</td>
			<td width="152">Dr.E.A.Mary Anita, &amp; Dr.G.Umarani Srikanth</td>
			<td width="105">Rs. 5000 sanctioned on 7/10/2015</td>
			<td width="129">Conducted on 27/8/2015 Submitted UC</td>
			</tr>
			</tbody>
			</table>
			
		</div>
		<div>
		<h3><strong><u>2014-2015</u></strong></h3>
		<table width="625">
		<tbody>
		<tr>
		<td width="163"><strong>&nbsp;Title of the Proposal</strong></td>
		<td width="76"><strong>Funding Agency</strong></td>
		<td width="152"><strong>&nbsp;Coordinator</strong></td>
		<td width="105"><strong>Sanction Details </strong></td>
		<td width="129"><strong>Status</strong></td>
		</tr>
		<tr>
		<td width="163">
		<p style="text-align: left;">Science Expo 2014</p>
		<p>Seminar Grant under Popularization of Science</p></td>
		<td width="76">TNSCST</td>
		<td width="152">Dr.E.A.Mary Anita</td>
		<td width="105">Rs.20,000 sanctioned on 7/4/2015</td>
		<td width="129">Conducted&nbsp; Science Expo on 28/11/2014 &amp; 29/11/2014 Submitted UC</td>
		</tr>
		<tr>
		<td width="163">Tech Bridge Programme</td>
		<td width="76">CSI</td>
		<td width="152">Mrs. R.Geetha</td>
		<td width="105">Rs.5000 sanctioned&nbsp; on 28/4/2015</td>
		<td width="129">Conducted&nbsp; Workshop on 24/09/2014</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h3><strong><u>2013-2014</u></strong></h3>
		<table width="625">
		<tbody>
		<tr>
		<td width="157"><strong>&nbsp;Title of the Proposal</strong></td>
		<td width="82"><strong>Funding Agency</strong></td>
		<td width="152"><strong>&nbsp;Coordinator</strong></td>
		<td width="105"><strong>Sanction Details </strong></td>
		<td width="129"><strong>Status</strong></td>
		</tr>
		<tr>
		<td width="157">CISCO Network LAB under MODROBS Scheme</td>
		<td width="82">&nbsp; AICTE</td>
		<td width="152">Dr.E.A.Mary Anita</td>
		<td width="105">Rs.8,90,000 sanctioned on 07/02/2014</td>
		<td width="129">Submitted UC on 19/03/2015</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h3><strong><u>2012-2013</u></strong></h3>
		<table style="height: 188px;" width="676">
		<tbody>
		<tr>
		<td width="157"><strong>&nbsp;Title of the Proposal</strong></td>
		<td width="82"><strong>Funding Agency</strong></td>
		<td width="152"><strong>&nbsp;Coordinators</strong></td>
		<td width="105"><strong>Sanction Details </strong></td>
		<td width="129"><strong>Status</strong></td>
		</tr>
		<tr>
		<td width="157">National Conference on Cyber Security</td>
		<td width="82">DRDO</td>
		<td width="152">Dr.E.A.Mary Anita &amp; Ms. H.Mercy</td>
		<td width="105">Rs.30,000 sanctioned on 16/01/2013</td>
		<td width="129">Conducted on 27/03/2013 &amp; 28/03/2013 Submitted UC<strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong></td>
		</tr>
		</tbody>
		</table>
		</div>
		
		
		<div>
		<h3>CONSULTANCY PROJECTS</h3>
		<h4><strong><u>2016–2017</u></strong></h4>
		<table width="652">
		<tbody>
		<tr>
		<td width="56">
		<p><strong>S.No</strong></p>
		</td>
		<td width="151">
		<p><strong>Name of the Faculty (Chief Consultant)</strong></p>
		</td>
		<td width="208">
		<p><strong>Client Organization</strong></p>
		<p><strong>&nbsp;</strong></p>
		</td>
		<td width="143">
		<p><strong>Title of Consultancy Project</strong></p>
		</td>
		<td width="93">
		<p><strong>Sanctioned Amount (in Rupees)</strong></p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11</p>
		</td>
		<td rowspan="2" width="151">
		<p>Dr. G.Umarani</p>
		<p>srikanth</p>
		</td>
		<td width="208">
		<p>Yashwanth Automobiles, Pattalam, Chennai</p>
		</td>
		<td width="143">
		<p>Website Creation for &nbsp;Yashwanth Automobiles</p>
		</td>
		<td width="93">
		<p>&nbsp;</p>
		<p>4000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Wahr Enterprises, No.5,Cholan nagar, Maduravoyal,chennai-95</p>
		</td>
		<td width="143">
		<p>Website designing for Wahr Enterprises</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="3" width="151">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>Dr. E.A. Mary Anita</p>
		</td>
		<td width="208">
		<p>SAEC CISCO NET Academy</p>
		</td>
		<td width="143">
		<p>CISCO Courses</p>
		</td>
		<td width="93">
		<p>26,000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Chennai Ford</p>
		<p>423,Poonamalle High Road, Arumbakkam, Chennai-106</p>
		</td>
		<td width="143">
		<p>Biometric Automatic Attendance Tracking</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Sri Sai Gramiya Millets,C12,Sri Sai Nivas,Anna st., Poompozil nagar,Avadi,Chennai-62.</p>
		</td>
		<td width="143">
		<p>Stock Management System</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Dr.V.Manjula</p>
		</td>
		<td width="208">
		<p>Harish Computers, No.133,Kaladipet,Rajakadai,Thiruvottiyur, Chennai-19.</p>
		</td>
		<td width="143">
		<p>Webdesign</p>
		<p>Project</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>V.M Enterprises, No.82,S.P.H.road,Manavalanagar, Thiruvallur-602002.</p>
		</td>
		<td width="143">
		<p>Mobile Anti-theft System</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Dr. V. Ramesh</p>
		</td>
		<td width="208">
		<p>Malar Pharmaceuticals,</p>
		<p>8,Palliarasan Street, Anna Nagar East,Chennai-102</p>
		</td>
		<td width="143">
		<p>Website and Database&nbsp; Creation for a Pharmacy</p>
		</td>
		<td width="93">
		<p>4000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Dr.T.Ravi</p>
		</td>
		<td width="208">
		<p>Naidu Borewell Services 4,Allapakkam,Ramaiah Avenue,Valasaravakkam, Chennai-87.</p>
		</td>
		<td width="143">
		<p>Website Design for Naidu Borewell services.</p>
		</td>
		<td width="93">
		<p>5000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>10.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Dr.S.Veena</p>
		</td>
		<td width="208">
		<p>GKM Hospital</p>
		</td>
		<td width="143">
		<p>Hospital Management System</p>
		</td>
		<td width="93">
		<p>4000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>11.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Sri Renugambal Transport,17,Ganapathy street,Vasantham Nagar,</p>
		<p>Avadi,Chennai-71</p>
		</td>
		<td width="143">
		<p>Digital Marketing for a transport company</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>12.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms.Geetha</p>
		</td>
		<td width="208">
		<p>Ratna Nursery And Primary School,Muthukrishnapuram,</p>
		<p>Kadayanallur.</p>
		</td>
		<td width="143">
		<p>Website Creation for Ratna school</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>13.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Crystal Pesticides Industry,181,1<sup>st</sup> main road,Kottivakkam,Chennai</p>
		</td>
		<td width="143">
		<p>Website design for crystal pesticides company</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>14.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mr.C.Balakrishnan</p>
		</td>
		<td width="208">
		<p>G.R Material Supply,</p>
		<p>#3/33,Pillayar Koil street,Mettupalayam,Chennai</p>
		</td>
		<td width="143">
		<p>Inventory Management System</p>
		</td>
		<td width="93">
		<p>5000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>15.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mr. R. Prasanna</p>
		<p>kumar</p>
		</td>
		<td width="208">
		<p>Sri Venkateswara Mobile Shop,38,Karumariyamman Kovil Street, Kamaraj Nagar,Avadi, Chennai-71</p>
		</td>
		<td width="143">
		<p>Website creation</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>16.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Motherland Press, No.119,Thambu chetty street,Mannady,Chennai-01.</p>
		</td>
		<td width="143">
		<p>Static web design for printing press</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>17.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms.N.S.Usha</p>
		</td>
		<td width="208">
		<p>Karunyam Prayer House,</p>
		<p>Rajiv Gandhi nagar, Kozhumanivakkam, Mangadu,Chennai-101.</p>
		</td>
		<td width="143">
		<p>Web design Project</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>18.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Chennai Higher Secondary School, Subbarayan street, Shenoy nagar,Chennai-30.</p>
		</td>
		<td width="143">
		<p>&nbsp;E-Admission System</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>19.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mr.A.Mani</p>
		</td>
		<td width="208">
		<p>Hari Electricals,</p>
		<p>No. 50,Amman Kovil street,Mannady,Chennai-01</p>
		</td>
		<td width="143">
		<p>Stock Description System</p>
		</td>
		<td width="93">
		<p>500</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>20.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Bhandari Electricals, No.16,Krishna Iyer Street, Surya complex,Chennai-79</p>
		</td>
		<td width="143">
		<p>Employee</p>
		<p>Management</p>
		<p>System</p>
		</td>
		<td width="93">
		<p>4000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>21.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mrs. D. Vinodha</p>
		</td>
		<td width="208">
		<p>Orbit College of Animation,Chennai.</p>
		</td>
		<td width="143">
		<p>Grace Exam Management System(GEMS)</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>22.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mr.S.Prabhu</p>
		</td>
		<td width="208">
		<p>Jeevan Trust</p>
		<p>Sri harihara apartment,ground floor,2,2<sup>nd</sup> street, k.k.nagar, Thirumullaivoyal,Chennai 62.</p>
		</td>
		<td width="143">
		<p>File Protection</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>23.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mr.S.Muthu</p>
		<p>kumarasamy</p>
		</td>
		<td width="208">
		<p>Apostolic Christian Assembly, 340,School street,Manali,Chennai-68.</p>
		</td>
		<td width="143">
		<p>Website creation for Church &amp; School.</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>24.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>HTC Global services</p>
		</td>
		<td width="143">
		<p>Website for Satheesh balaji school</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>25.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms.M.Renuka Devi</p>
		</td>
		<td width="208">
		<p>Tanesha Enterprises,</p>
		<p>No.191,Sridevi Nagar, Thiruverkadu, Chennai-77</p>
		</td>
		<td width="143">
		<p>Website for Tanesha Enterprises</p>
		</td>
		<td width="93">
		<p>&nbsp;</p>
		<p>3000</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>26.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Arcus Automation Private Limited,</p>
		<p>No.175, Kanniamman nagar mainroad, Vanagaram, Chennai-95.</p>
		</td>
		<td width="143">
		<p>Employee Management System</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>27.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Ms.J.Sangeetha</p>
		</td>
		<td width="208">
		<p>KLN Motor Agencies Pvt Ltd, 232,Poonamalle HighRoad,Kilpauk,Chennai</p>
		</td>
		<td width="143">
		<p>Information System</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>28.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mr. H.AnwarBasha</p>
		</td>
		<td width="208">
		<p>R.R Agencies</p>
		<p>62,Kanniyamman Koil Street, Ambathur&nbsp; Industrial Estate,Chennai</p>
		<p>Ph:9841689491</p>
		</td>
		<td width="143">
		<p>Company Software Pack</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>29.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Sudha Stones</p>
		<p>36/25 kumaranstreet, Lakshmipuram,Chennai-99</p>
		</td>
		<td width="143">
		<p>Website Development</p>
		</td>
		<td width="93">
		<p>500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>30.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms.S.Sajini</p>
		</td>
		<td width="208">
		<p>Ivesta Technologies</p>
		<p>28 A,D-Mundy Street,Vellore-632004</p>
		<p>Ph:9840041478</p>
		</td>
		<td width="143">
		<p>Website Development</p>
		</td>
		<td width="93">
		<p>4000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>31.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Auto Consultancy</p>
		<p>Mr.S.N.Sugumar,</p>
		<p>147,Bells Road,</p>
		<p>Cheapauk, Chennai-05</p>
		</td>
		<td width="143">
		<p>Website Creation for a auto consultancy</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>32.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms. M. Preetha</p>
		</td>
		<td width="208">
		<p>Arul Sweets, Nagpur, Maharashtra</p>
		</td>
		<td width="143">
		<p>Website Creation for New Arul Sweets</p>
		</td>
		<td width="93">
		<p>500</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>33.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>St.John’s Matriculation School, No.15, Anbalagan Street, Manali, Chennai-68</p>
		</td>
		<td width="143">
		<p>Website Creation for St.John’s educational trust</p>
		</td>
		<td width="93">
		<p>2000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>34.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mrs.V.Sureka</p>
		</td>
		<td width="208">
		<p>NCUBE Beacons, 100,2<sup>nd</sup> floor,Pai layout, Mahadevapura,Bengaluru, Karnataka.</p>
		</td>
		<td width="143">
		<p>Website Creation</p>
		</td>
		<td width="93">
		<p>3000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>35.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mrs.S.Jonisha</p>
		</td>
		<td width="208">
		<p>S. R. Lingam Dental Clinic,No.131,Main road,Mangadu,Chennai-122.</p>
		</td>
		<td width="143">
		<p>Mini Project on Modern Dental Care</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>36.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Prem Enterprises</p>
		<p>Avadi,No.87/4,P.H road,Avadi,Chennai-54.</p>
		</td>
		<td width="143">
		<p>Billing software creation for &nbsp;Prem Enterprises</p>
		</td>
		<td width="93">
		<p>500</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>37.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mrs. K.B.Aruna</p>
		</td>
		<td width="208">
		<p>G.G.Engineering ,60 A,Poonamalle road,Achuthan nagar,ekkatuthangal,Chechen</p>
		</td>
		<td width="143">
		<p>Website Creation for a GG engineering company</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>38.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mr.M.P.Karthikeyan</p>
		</td>
		<td width="208">
		<p>Success Training Academy, No.21-B,Ekambarapuram street,Kancheepuram-631502.</p>
		</td>
		<td width="143">
		<p>Online Quiz Application</p>
		</td>
		<td width="93">
		<p>1500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>39.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Perambur Taluk Office,</p>
		<p>No.2,Perambur High Road,Perambur,Chennai-11.</p>
		</td>
		<td width="143">
		<p>Implementation of Online Billing System</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>40.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mrs.K.Jayanthi</p>
		</td>
		<td width="208">
		<p>Shri Raayan Bharath Gas Agency &amp; S.R Enterprises, No.32/16 ,&nbsp; Egmore High Road, Egmore, Chennai-8</p>
		</td>
		<td width="143">
		<p>Gas agency Management systems</p>
		</td>
		<td width="93">
		<p>&nbsp;</p>
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>41.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>&nbsp;</p>
		</td>
		<td width="208">
		<p>Angel Beauty &amp; Care, No.38/17 Gandhi nagar,Tondiarpet,</p>
		<p>Chennai – 81</p>
		</td>
		<td width="143">
		<p>Online Booking For Spa and Hair Salons</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>42.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms.K.Ramyadevi</p>
		</td>
		<td width="208">
		<p>Nettlinx Billing Ltd.</p>
		</td>
		<td width="143">
		<p>Telephone Billing System</p>
		</td>
		<td width="93">
		<p>1000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>43.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Vivegam Institute</p>
		<p>52, Nehru Street,</p>
		<p>Kanagam TaraMani Post,Chennai-600113</p>
		</td>
		<td width="143">
		<p>Student Database Management system</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>44.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mrs.L.Sudha</p>
		</td>
		<td width="208">
		<p>Step-In Funschool, No.32, 2<sup>nd</sup> Cross Street, Senthil Nagar, Kolathur, Chennai-99</p>
		</td>
		<td width="143">
		<p>Bulk message Sender for Step0in fun school</p>
		</td>
		<td width="93">
		<p>&nbsp;</p>
		<p>2000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>45.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>R.C.M Hr.Sec.School,</p>
		<p>Kamaraj Nagar,</p>
		<p>Avadi,Chennai-71.</p>
		</td>
		<td width="143">
		<p>SMS Broadcasting</p>
		</td>
		<td width="93">
		<p>1500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>46.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mr. T.Vignesh</p>
		</td>
		<td width="208">
		<p>The Salvation Army Haven Matriculation Higher Secondary School,21,Thiru Narayana Guru road,Choolai,Chennai-112.</p>
		</td>
		<td width="143">
		<p>Student Information System</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>47.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Sona Publishing House, #15, Thiruvallur Puram, 2<sup>nd</sup> Street, Choolaimedu, Chennai-94</p>
		</td>
		<td width="143">
		<p>Client</p>
		<p>Management System</p>
		</td>
		<td width="93">
		<p>2000</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>48.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mrs.N.R.G.Sreevani</p>
		</td>
		<td width="208">
		<p>Vivekananda Engineering Company. No.147, Linghi Chetty Street, Chennai-1</p>
		</td>
		<td width="143">
		<p>Web site Creation for&nbsp; Vivekananda Engineering Company</p>
		</td>
		<td width="93">
		<p>2000</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>49.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Premier Controls And Drives,No.15/8,</p>
		<p>G.A road,Old Washermanpet,Chennai-21.</p>
		</td>
		<td width="143">
		<p>The Electronic Service Shop</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>50.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mrs.P.Rubasudha</p>
		</td>
		<td width="208">
		<p>My Business Visual PrivateLimited,No.1/3,Perumal Koil Garden 2<sup>nd</sup> street, Arumbakkam,Chechen-106.</p>
		</td>
		<td width="143">
		<p>Employee Leave Management System</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>51.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mrs.K.Elavarasi</p>
		</td>
		<td width="208">
		<p>Dynamic Systems,No.48,Park street,Jayalakshmi nagar, Moulivakkam,Chennai-125.</p>
		</td>
		<td width="143">
		<p>Web Service System</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>52.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>B.L Impex,</p>
		<p>15,First Floor,Godown Strret,Chennai-01</p>
		<p>Ph:9444270175</p>
		</td>
		<td width="143">
		<p>Webpage Design for Textile shop</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>53.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mrs.V. Saraswathi</p>
		</td>
		<td width="208">
		<p>Murugan Stores</p>
		</td>
		<td width="143">
		<p>Web Based Provision Application</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms. C. Saranya</p>
		</td>
		<td width="208">
		<p>Reefer care services pvt ltd</p>
		</td>
		<td width="143">
		<p>Website creation for Reefer Care Service Pvt Ltd.</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>54.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Shri Kanaiyalal Agarwal Bal – Niketan Nursery and Primary school,</p>
		<p>6,chinna naickaran street,sowcarpet,Chennai-79</p>
		</td>
		<td width="143">
		<p>Web design Project</p>
		</td>
		<td width="93">
		<p>500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>55.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms. A. Lallithashri</p>
		</td>
		<td width="208">
		<p>V.G.R.Travels No.14,Thiruvalluvar Theatre complex,South Rampart,Thanjavur-1</p>
		</td>
		<td width="143">
		<p>Android Application for Online Bus Ticket Booking</p>
		</td>
		<td width="93">
		<p>3000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>56.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Jagadees Printers,</p>
		<p>6,First Floor,Solai Amman Koil Lane, Purasaiwakkam,</p>
		<p>Chennai-07</p>
		</td>
		<td width="143">
		<p>Website Creation for a printing press</p>
		</td>
		<td width="93">
		<p>500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>57.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Ms. P. Dharani</p>
		<p>&nbsp;</p>
		</td>
		<td width="208">
		<p>Voltas AC Service</p>
		</td>
		<td width="143">
		<p>Data Entry Software</p>
		<p>&nbsp;</p>
		</td>
		<td width="93">
		<p>500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>58.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>LTR Motors,</p>
		<p>Sengathur Nagar,</p>
		<p>By Pass Road,</p>
		<p>Thiruthani-631209</p>
		</td>
		<td width="143">
		<p>Website Creation</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>59.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Ms. G. Gowri</p>
		</td>
		<td width="208">
		<p>Nagarjuna Herbal Concentrates Private Ltd.Kodamballam,Chennai..</p>
		</td>
		<td width="143">
		<p>Database application for Nagarjuna Herbal Concentrates Ltd,</p>
		</td>
		<td width="93">
		<p>1000</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>60.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mrs. E. Abirami</p>
		</td>
		<td width="208">
		<p>Jayam Cable TV Pvt Ltd</p>
		</td>
		<td width="143">
		<p>Cable Operator Management System</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>61.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>Rakshan&nbsp; Pharma, 1/13,N M SComplex 1<sup>st</sup> floor,Railway feeder road, Aranthangi-16</p>
		</td>
		<td width="143">
		<p>Website Creation for Rakshan Pharmacy.</p>
		</td>
		<td width="93">
		<p>&nbsp;</p>
		<p>2500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>62.&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="151">
		<p>Mrs. T. Padmavathy</p>
		</td>
		<td width="208">
		<p>Dell Showroom</p>
		</td>
		<td width="143">
		<p>Stock Database</p>
		<p>&nbsp;</p>
		</td>
		<td width="93">
		<p>500</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>63.&nbsp; &nbsp;</p>
		</td>
		<td width="208">
		<p>KKM Nursery And Primary Schiool, Thiruvannamalai</p>
		</td>
		<td width="143">
		<p>Website Creation fro K.K.M school</p>
		</td>
		<td width="93">
		<p>1000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>64.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>Mr.M.Balasubramanian</p>
		</td>
		<td width="208">
		<p>Dream Flower Beauty Parlour,No.1 Murugan Kovil street, Kundrathur,chennai.&nbsp;</p>
		</td>
		<td width="143">
		<p>Website Design for&nbsp; Dreams Flower Beauty Parlor</p>
		</td>
		<td width="93">
		<p>2000</p>
		</td>
		</tr>
		<tr>
		<td width="56">
		<p>65.&nbsp; &nbsp;</p>
		</td>
		<td width="151">
		<p>&nbsp;</p>
		</td>
		<td width="208">
		<p>SMP Auto Consultant No.3,Sri sastri nagar,1<sup>st</sup>street,,Villivakkam, chennai-49.</p>
		</td>
		<td width="143">
		<p>Automobile Dealership Project</p>
		</td>
		<td width="93">
		<p>4000</p>
		</td>
		</tr>
		<tr>
		<td colspan="4" width="559">
		<p>&nbsp;</p>
		<p>Total Amount</p>
		</td>
		<td width="93">
		<p>1,57,500</p>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h4>2015 – 2016</h4>
		<table width="631">
		<tbody>
		<tr>
		<td width="57"><strong>Sl.no.</strong></td>
		<td width="300"><strong>Name of the Organization</strong></td>
		<td width="180"><strong>Project Details</strong></td>
		<td width="95"><strong>Amount</strong> <strong>&nbsp;(in Rs.)</strong></td>
		</tr>
		<tr>
		<td width="57">1.</td>
		<td width="300">Eyeopen Technologies, Rajamanickam Enclave, 18th Main Road, Anna Nagar, Chennai- 600 040</td>
		<td width="180">&nbsp; Institution Management System</td>
		<td width="95">&nbsp; Rs.20,000</td>
		</tr>
		<tr>
		<td width="57">2.</td>
		<td width="300">Chennai Logistics Corporation, 1/1 Hinduja towers, Ist Floor, Sriperumbadur, Chennai- 602 105</td>
		<td width="180">Smart Booking and Billing System</td>
		<td width="95">&nbsp; Rs.10,000</td>
		</tr>
		<tr>
		<td width="57">3.</td>
		<td width="300">SAEC CISCO NET Academy</td>
		<td width="180">CISCO Courses</td>
		<td width="95">Rs.75,000</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h4><strong>2014 – 2015</strong></h4>
		<table width="643">
		<tbody>
		<tr>
		<td width="57"><strong>Sl.no.</strong></td>
		<td width="312"><strong>Name of the Organization</strong></td>
		<td width="170"><strong>Project Details</strong></td>
		<td width="104"><strong>Amount</strong> <strong>&nbsp;(in Rs.)</strong></td>
		</tr>
		<tr>
		<td width="57">1.</td>
		<td width="312">Soft Square Solution Pvt Ltd 4/397, Radio Colony, Palavakkam, Chennai 600041.</td>
		<td width="170">Health Care Information System on FORCE.COM</td>
		<td width="104">Rs.50,000</td>
		</tr>
		<tr>
		<td width="57">2.</td>
		<td width="312">VGN Homes Private Ltd. 333, Poonamallee High Road, Amaindakarai, Chennai – 600 029.</td>
		<td width="170">Interactive Web Designing</td>
		<td width="104">Rs.50,000</td>
		</tr>
		<tr>
		<td width="57">3.</td>
		<td width="312">Vijayanta Model Higher Secondary School, HVF , Avadi, Chennai -54.</td>
		<td width="170">School Management Software</td>
		<td width="104">Rs.10,000</td>
		</tr>
		<tr>
		<td width="57">4.</td>
		<td width="312">Shri Gomathi Vidyalaya Nursery &amp; Primary School, Perumalpattu, Thrivalluvar Dist., Pin -602 024</td>
		<td width="170">Student Information Portal</td>
		<td width="104">Rs.10,000</td>
		</tr>
		<tr>
		<td width="57">5.</td>
		<td width="312">German Leprosy &amp; TB Relief Association India(GLRA India) Old No.4, New No. 94, Gajapathy Street,Shenoy Nagar, Chennai – 600030.</td>
		<td width="170">Patient Information Portal</td>
		<td width="104">Nil</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h4><strong>2013 – 2014</strong></h4>
		<table width="640">
		<tbody>
		<tr>
		<td width="64"><strong>Sl.no.</strong></td>
		<td width="236"><strong>Name of the Organization</strong></td>
		<td width="236"><strong>Project Details</strong></td>
		<td width="104"><strong>Amount</strong> <strong>&nbsp;(in Rs.)</strong></td>
		</tr>
		<tr>
		<td width="64">1</td>
		<td width="236">Eyeopen Technologies Rajamanickam Enclave, 18th Main Road, Anna Nagar, Chennai- 600 040</td>
		<td width="236">Survey Game</td>
		<td width="104">Rs.20,000</td>
		</tr>
		<tr>
		<td width="64">2</td>
		<td width="236">Nokia Solutions and Networking India Pvt. Ltd.</td>
		<td width="236">Optical Inspection System For Measuring The Thickness Of Heat Sink Paste(Thermal Gel)</td>
		<td width="104">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; –</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h3><u></u><strong>STAFF PUBLICATIONS</strong></h3>
		<h4>2017-2018</h4>
		<table width="678">
		<tbody>
		<tr>
		<td rowspan="2" width="54">
		<p><strong>S.NO</strong></p>
		</td>
		<td rowspan="2" width="180">
		<p><strong>NAME OF THE FACULTY</strong></p>
		</td>
		<td colspan="2" width="444">
		<p><strong>PAPERS PUBLISHED</strong></p>
		</td>
		</tr>
		<tr>
		<td width="215">
		<p><strong>Title of the paper</strong></p>
		</td>
		<td width="229">
		<p><strong>Name of the Journal/Conference with vol no, issue no, page no and impact factor.</strong></p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>1.</p>
		</td>
		<td width="180">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="215">
		<p>Detection and prevention of Cooperative Black Hole attack in mobile Adhoc Network: A Review</p>
		</td>
		<td width="229">
		<p>International Journal of Engineering Research in Electronics Engineering and Communication</p>
		<p>(IJERECE)</p>
		<p>Vol 4,</p>
		<p>Issue 6,</p>
		<p>June 2017</p>
		<p>ISSN (Online)</p>
		<p>2394-6849</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>2.</p>
		</td>
		<td width="180">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="215">
		<p>Secure Vehicular Communication Using ID Based Signature Scheme</p>
		</td>
		<td width="229">
		<p>Wireless Personal Communication (2017).</p>
		<p>July 2017</p>
		<p><a href="https://doi.org/10.1007/s11277-017-4923-7">https://doi.org/10.1007/s11277-017-4923-7</a></p>
		<p>&nbsp;(Annexure -I)</p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>3.</p>
		</td>
		<td width="180">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="215">
		<p>Health Recommender System using Big data analytics</p>
		</td>
		<td width="229">
		<p>Journal of Management Science and Business Intelligence</p>
		<p>2017, 2–2</p>
		<p>Aug. 2017, DOI: 10.5281/zenodo.834918, ISBN 2472-9264 (Online), 2472-9256 (Print)</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>4.</p>
		</td>
		<td width="180">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="215">
		<p>Fuzzy Based Scheme For Detection Of Sybil Node In</p>
		<p>Wireless Sensor Networks</p>
		</td>
		<td width="229">
		<p>Journal Of Advanced Research In Dynamical And Control Systems</p>
		<p>Vol. 9. Sp– 6/ 2017</p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>5.</p>
		</td>
		<td width="180">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="215">
		<p>Gender Classification System Based on Wave ATOM Transform</p>
		</td>
		<td width="229">
		<p>Indian Journal of Public Health Research &amp; Development Year</p>
		<p>Volume 8, Issue 3s. 2017,</p>
		<p>pp 71-73</p>
		<p>ISSN: 0976-5506. DOI : 10.5958/0976-5506.2017.00241.8</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>6.</p>
		</td>
		<td width="180">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="215">
		<p>A Survey on Performance Based Security Analysis of Geographical Routing Protocols in MANET</p>
		</td>
		<td width="229">
		<p>International Journal of Multi-disciplinary Approach and Studies</p>
		<p>Vol 4, No.3 , June 2017, pp 131-138,</p>
		<p>ISSN: 2348-537X</p>
		</td>
		</tr>
		<tr>
		<td width="54">
		<p>7.</p>
		</td>
		<td width="180">
		<p>Mr.T.Vignesh</p>
		</td>
		<td width="215">
		<p>Soft Computing Techniques for Land Use and Land Cover Monitoring with Multispectral Remote Sensing Images: A Review</p>
		</td>
		<td width="229">
		<p>Archives of Computational Methods in Engineering</p>
		<p>July 2017,pp1-27 https://link.springer.com/article/10.1007/s11831-017-9239-y</p>
		<p>&nbsp;</p>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h4>2016-2017</h4>
		<table width="742">
		<tbody>
		<tr>
		<td width="61">
		<p>S.NO.</p>
		</td>
		<td width="122">
		<p>NAME OF THE FACULTY</p>
		</td>
		<td width="201">
		<p>TITLE OF THE PAPER</p>
		</td>
		<td width="358">
		<p>NAME OF THE JOURNAL/ISSUE NO./VOL. NO./DOI/DATE OF PUBLICATION</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>1.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="9" width="122">
		<p>Dr.G.Umarani Srikanth</p>
		</td>
		<td width="201">
		<p>Task Scheduling Using Probabilistic Ant Colony Heuristics</p>
		</td>
		<td width="358">
		<p>The International Arab Journal of Information Technology, Vol. 13, No. 4, pp- 375-379 , July 2016,ISSN:1683-3198 (print)ISSN:2309-4524 (Online)</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>2.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Android Applications for Personal Security</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,,&nbsp;Volume 04, Issue 2,Scientific Journal Impact Factor(SJIF):4.364,ISSN:2349-9745,February 2017,DOI :&nbsp;<a href="https://dx.doi.org/10.21884/IJMTER.2017.4043.SGBKP">10.21884/IJMTER.2017.4043.SGBKP</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>3.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p><a href="http://digital.ijre.org/index.php/int_j_res_eng/article/view/251">A Survey on Gateway Approach for Efficient Big-Data Broadcasting</a></p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol. 4, No. 2,pp 51-52, February 2017,ISSN 2348-7860.&nbsp; ISSN:2348-7860(O),DOI :&nbsp; 10.21276/IJRE</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>4.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Privacy Preserving Patient centric Clinical Decision Support System</p>
		</td>
		<td width="358">
		<p>International Journal of Current Engineering and Scientific Research,Vol 4, Issue 2,pp 4-39, February 2017,ISSN(P) : 2393-8374,ISSN(O) : 2394-0697, 2017,Impact Factor :3.058<strong>DOI : 10.21276/ijcesr</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>5.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Task Scheduling Model in Cloud Computing using Optimization Technique</p>
		</td>
		<td width="358">
		<p>International Journal Of Advanced Research(IJAR),ISSN:2320-5407,Volume 05, Issue 02,pp&nbsp; 345-348,February 2017,DOI: 10.21474/ijar01/3155.</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>6.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Load Balancing in Cloud Computing using Optimization Technique</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol. 04, No 01, pp 28-30, February 2017, ISSN:2348-7860(O), ISSN(P):2348-7852,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>7.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>SOTERIA –An Android&nbsp; application for personal security</p>
		</td>
		<td width="358">
		<p>National Conference on Computing Communication and Information Technology (N3CIT’17), T.J.S Engineering College</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>8.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Query Providing and report Generation of Cancer Prediction using Naïve Bayes Classifier</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, March 2017,Vol 4, No. 3,pp 82-85,ISSN : 2348-7960(O),ISSN : 2348-7852(P), DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>9.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Load Balancing in Cloud Computing using Optimization Technique</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College,10<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>10. &nbsp;</p>
		</td>
		<td rowspan="7" width="122">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="201">
		<p>A Survey on Trust Based Schemes for Sybil Attack in Wireless Networks</p>
		</td>
		<td width="358">
		<p>11<sup>th</sup> National Conference on Science, Engineering and Technology(NCSET16),7 &amp; 8<sup>th&nbsp; </sup>November 2016</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>11. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey on Compressive Data Collection Techniques For Wireless Sensor Networks</p>
		</td>
		<td width="358">
		<p>International conference on Information,Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070765">10.1109/ICICES.2017.8070765</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>12. &nbsp;</p>
		</td>
		<td width="201">
		<p>Sensing based&nbsp; Malicious Clone Detection in&nbsp; Wireless Sensor Networks</p>
		</td>
		<td width="358">
		<p>National conference on trends in computing technologies, NCTCT’17, Meenakshi Sundararajan Engineering College on 17<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>13. &nbsp;</p>
		</td>
		<td width="201">
		<p>A survey on Tongue Image Analysis for Diabetic Diagnosis using Image Segmentation</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Computer Technology, ISSN : 2320-0790, Special Issue, April 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>14. &nbsp;</p>
		</td>
		<td width="201">
		<p>A survey on privacy preserving data aggregation in wireless sensor networks</p>
		</td>
		<td width="358">
		<p>International conference on Information,Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070768">10.1109/ICICES.2017.8070768</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>15. &nbsp;</p>
		</td>
		<td width="201">
		<p>A novel zone based routing protocol for detection of replicas in static wireless sensor networks</p>
		</td>
		<td width="358">
		<p>International conference on Information,Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070790">10.1109/ICICES.2017.8070790</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>16. &nbsp;</p>
		</td>
		<td width="201">
		<p>An analysis of tongue shape to identify diseases by using supervised learning techniques</p>
		</td>
		<td width="358">
		<p>International conference on Information,Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070786">10.1109/ICICES.2017.8070786</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>17. &nbsp;</p>
		</td>
		<td rowspan="5" width="122">
		<p>Dr.V.Manjula</p>
		</td>
		<td width="201">
		<p>Resilient Protocols For Detection and Benign Node Recovery Of Node Replication Attack In WSN</p>
		</td>
		<td width="358">
		<p>Transylvanian Review ,Vol XXIV,No. 9,pp&nbsp; 1377 – 1391,ISSN 1221-1249,August 2016</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>18. &nbsp;</p>
		</td>
		<td width="201">
		<p>Resilient protocol for clone attack detection in IoT using Graph Theory</p>
		</td>
		<td width="358">
		<p>Transylvanian Review:Vol XXIV, No. 10,pp 1913 – 1923,ISSN 1221-1249,August 2016</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>19. &nbsp;</p>
		</td>
		<td width="201">
		<p>Prevention Protocol for Node Replication Attack</p>
		</td>
		<td width="358">
		<p>Asian Journal of Information Technology Volume: 15,Issue: 21,pp.: 4255-4268,2016,&nbsp;ISSN : 1682-3915,2016DOI: <a href="http://dx.doi.org/10.3923/ajit.2016.4255.4268">10.3923/ajit.2016.4255.4268</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>20. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Travel Blockage Sharing System</p>
		</td>
		<td width="358">
		<p>International Journal of AdvancedEngineering and Research Development, Volume 4, Issue 1,p-ISSN (P) :2348-6406, PP 339-343,January 2017, DOI:10.21090/IJAERD</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>21. &nbsp;</p>
		</td>
		<td width="201">
		<p>Prevention Mechanism of Collision Attack and Sybil Attack in Cloud Services</p>
		</td>
		<td width="358">
		<p>International Journal of AdvancedEngineering and Research Development, Volume 4, Issue 2,p-ISSN (P) :2348-6406, January 2017, , DOI:10.21090/IJAERD</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>22. &nbsp;</p>
		</td>
		<td rowspan="6" width="122">
		<p>Dr.S.Veena</p>
		</td>
		<td width="201">
		<p>A Survey On Secure Data Transmission Algorithms In VANETs</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>23. &nbsp;</p>
		</td>
		<td width="201">
		<p>De Duplication Scalable Secure File Sharing On Untrusted Storage In Big Data</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070763">10.1109/ICICES.2017.8070763</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>24. &nbsp;</p>
		</td>
		<td width="201">
		<p><a href="http://ijaers.com/detail/a-survey-on-mobile-phone-authentication-2/"><br> <strong>A Survey on Mobile Phone Authentication</strong></a></p>
		</td>
		<td width="358">
		<p><strong>International Journal of Advanced Engineering Research and Science (IJAERS), </strong></p>
		<p>ISSN :&nbsp;<strong>2349-6495(P) | 2456-1908(O),</strong><strong>&nbsp; </strong>&nbsp;</p>
		<p><a href="http://ijaers.com/call-for-papers/">Vol. 4, Issue-2</a><strong>,</strong></p>
		<p>Impact Factor:&nbsp;<strong>2.465,</strong> February 2017,<strong>DOI:</strong>&nbsp;<a href="https://dx.doi.org/10.22161/ijaers.4.2.7">10.22161/ijaers.4.2.7</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>25. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey on Storage and Security for De duplication</p>
		</td>
		<td width="358">
		<p>International Journal of Current Engineering and Scientific Research,Vol 4, Issue 2, pp 18-22,ISSN(P) : 2393-8374,ISSN(O) : 2394-0697, 2017,<strong>DOI : 10.21276/ijcesr</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>26. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Detection of minor Problems in the&nbsp; motor car using mobile applications</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol. 4, No. 3,ISSN(O):2348-7860,ISSN(P) : 2348-7852,March 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>27. &nbsp;</p>
		</td>
		<td width="201">
		<p>Smart Way To Authenticate The Social Networking Accounts Using Screen Brightness</p>
		</td>
		<td width="358">
		<p>4<sup>th</sup> National Conference on Signals, Computing and communication CommuniQuest’17 on 22<sup>nd</sup> March 2017 at Rajiv Gandhi College of Engineering</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>28. &nbsp;</p>
		</td>
		<td rowspan="7" width="122">
		<p>Mrs.R.Geetha</p>
		</td>
		<td width="201">
		<p>A Novel Resource Constraint Secure(RCS) Routing Protocol for Wireless Sensor Networks</p>
		</td>
		<td width="358">
		<p>Journal of Engineering Science and Technology, Vol 12, No.2,pp 518 – 529, ISSN&nbsp;&nbsp;: 1823-4690,February 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>29. &nbsp;</p>
		</td>
		<td width="201">
		<p>Providing Location Privacy Protection Using Dynamic Grid System For Mobile Location Based Services</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>&nbsp;&nbsp;&nbsp;&nbsp; DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070791">10.1109/ICICES.2017.8070791</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>30. &nbsp;</p>
		</td>
		<td width="201">
		<p><a href="http://www.ijmter.com/published-papers/volume-4/issue-2/a-survey-on-providing-location-privacy-protection/"><br> A Survey on Providing Location Privacy Protection</a></p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,&nbsp;ISSN:2349-9745, Volume 04, Issue 2, Scientific Journal Impact Factor(SJIF):4.364,February 2017,DOI:&nbsp;<a href="https://dx.doi.org/10.21884/IJMTER.2017.4053.K3HZH">10.21884/IJMTER.2017.4053.K3HZH</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>31. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Technologies used in Glucose Monitoring for Diabetic Patients</p>
		</td>
		<td width="358">
		<p>International Journal of Innovative Research in Computer Science &amp; Technology(IJIRCST), ISSN:2347-5552,Vol-5,Issue-2, March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>32. &nbsp;</p>
		</td>
		<td width="201">
		<p>IoT enabled life cycle monitoring and assistance system for diabetic patients</p>
		</td>
		<td width="358">
		<p>National Conference on Computing Communication and Information Technology (N3CIT’17), T.J.S Engineering College</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>33. &nbsp;</p>
		</td>
		<td width="201">
		<p>Secure Communication using Efficient key Management Scheme for Wireless Sensor Networks</p>
		</td>
		<td width="358">
		<p>4<sup>th</sup> International Conference on Engineering and Applied Sciences, 22-24<sup>th</sup> March 2017, at S.R.I College of Engineering and Technology</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>34. &nbsp;</p>
		</td>
		<td width="201">
		<p>Efficient Key Management Scheme in Wireless Sensor Networks : An Overview</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Engineering and ResearchDevelopment, Volume 4, Issue 2,P-ISSN (P): 2348-6406, (SJIF): 4.72, February 2017, <a href="http://www.ijaerd.com/papers/finished_papers/EFFICIENT%20KEY%20MANGEMENT%20SCHEME%20IN%20WIRELESS%20SENSOR%20NETWORKS-%20AN%20OVERVIEW-IJAERDV04I0225462.pdf" target="_blank">DOI:10.21090/IJAERD.25462</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>35. &nbsp;</p>
		</td>
		<td rowspan="6" width="122">
		<p>Mr.C.BalaKrishnan</p>
		</td>
		<td width="201">
		<p>A Survey On Detection Of Wormhole Attack</p>
		</td>
		<td width="358">
		<p>International Journal of Engineering Science and Computing, VOL 6, ISSUE 6,pp 6545-6549,June 2016,DOI:10.4010/2016.1572, ISSN 2321 3361</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>36. &nbsp;</p>
		</td>
		<td width="201">
		<p>Voice Recognition based Call and Notification Android App</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,&nbsp;ISSN:2393-8161, Volume 04, Issue 2, Scientific Journal Impact Factor(SJIF):4.364, February 2017, DOI:10.21884/IJMTER.2017.4058.WNI40</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>37. &nbsp;</p>
		</td>
		<td width="201">
		<p>Secure Transmission of Data over Internet using Elgammal Algorithm</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,&nbsp;ISSN:2393-8161, Volume 04, Issue 2, Scientific Journal Impact Factor(SJIF):4.364, February 2017, DOI:10.21884/IJMTER.2017.4044.LTJIB</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>38. &nbsp;</p>
		</td>
		<td width="201">
		<p>Personalized Portable Travelogue Guide on Multi-Source</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Engineering and Research Development,Volume 4, Issue 2,p-ISSN (P) :2348-6406, February 2017,e-ISSN (O): 2348-4470,Scientific Journal of Impact Factor (SJIF): 4.72,DOI: 10.21090/ijaerd</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>39. &nbsp;</p>
		</td>
		<td width="201">
		<p>Personalized Portable Travelogue Guide on Multi-Source Big Social Media</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College on 10<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>40. &nbsp;</p>
		</td>
		<td width="201">
		<p>An Effective Heterogeneous L:oad Distribution mechanism for Cluster based MANETs using Dynamic Channel Allocation</p>
		</td>
		<td width="358">
		<p>Second InterNational Conference on Innovative and Emerging Trends in Engineering and Technology(ICIETET ’17), Panimalar Institute of Technology on 15<sup>th</sup> May 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>41. &nbsp;</p>
		</td>
		<td rowspan="4" width="122">
		<p>Mr.R.Prasanna Kumar</p>
		</td>
		<td width="201">
		<p>A Survey on Real-Time Automated Grid Clock Control System</p>
		</td>
		<td width="358">
		<p>International Journal of Innovative Research in Engineering and ManagementVolume 4, Issue 1,ISSN&nbsp; :2350-0557, January 2017,DOI : <strong>10.21276/ijirem</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>42. &nbsp;</p>
		</td>
		<td width="201">
		<p>Hazardous Gas Detection and Alerting using Sensors</p>
		</td>
		<td width="358">
		<p>International Journal of Innovative Research in Engineering And Management(IJIREM), Vol. 4, Issue-1, January-2017,ISSN(O):2349-9745, ISSN(P):2393-8161,pp :597-600., January 2017,DOI : 10.21276/IJIREM</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>43. &nbsp;</p>
		</td>
		<td width="201">
		<p>Natural Tamil Language Processing</p>
		</td>
		<td width="358">
		<p>5 th national conference on computing communication &amp; information technology, N3CIT’17, T.J.S. Engineering college on 25<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>44. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey on Mining High-Utility Item-sets using Particle-Swarm Optimization-HUIM-BPSO Algorithm</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College, 10<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>45. &nbsp;</p>
		</td>
		<td rowspan="7" width="122">
		<p>Mrs.N.S.Usha</p>
		</td>
		<td width="201">
		<p>Military Reconnaissance Robot</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Engineering&nbsp; Research and Science(IJAERS),Vol. 4, pp 049-056, ISSN(O):2456-1908, ISSN(P):2349-6495, February 2017,<strong>DOI:</strong>&nbsp;<a href="https://dx.doi.org/10.22161/ijaers.4.2.7">10.22161/IJAERS</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>46. &nbsp;</p>
		</td>
		<td width="201">
		<p>Retrieval of Anomaly Details using Vehicle Number Plate Identification for Traffic Guards</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Advanced Engineering Research and Science (IJAERS), </strong>ISSN :&nbsp;<strong>2349-6495(P),&nbsp; 2456-1908(O),</strong></p>
		<p><a href="http://ijaers.com/call-for-papers/">Vol.-4, Issue-2</a><strong>, </strong>February 2017,</p>
		<p>Impact Factor:&nbsp;<strong>2.465, DOI:</strong>&nbsp;<a href="https://dx.doi.org/10.22161/ijaers.4.2.7">10.22161/ijaers.4.2.</a><strong>20</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>47. &nbsp;</p>
		</td>
		<td width="201">
		<p>Label Based Picture Finding Of Research and Engineering</p>
		</td>
		<td width="358">
		<p>International Journal ofResearch and Engineering,ISSN(O):2348-7860, ISSN(P):2348-7852,Vol. 04, pp:31-33, February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>48. &nbsp;</p>
		</td>
		<td width="201">
		<p>Detection of Misbehaving Clone Nodes in Static Wireless Networks using Sensing</p>
		</td>
		<td width="358">
		<p>National level conference on Emering Technologies un Engineering EMERGE-2017, Dr.M.G.R. Educational &amp; Research Institute University on 12<sup>th</sup>&amp; 13<sup>th</sup> 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>49. &nbsp;</p>
		</td>
		<td width="201">
		<p>Sensing based&nbsp; Malicious Clone Detection in&nbsp; Wireless Sensor Networks</p>
		</td>
		<td width="358">
		<p>National Conference on trends in computing technologies, NCTCT’17, Meenakshi Sundararajan Engineering College on 17<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>50. &nbsp;</p>
		</td>
		<td width="201">
		<p>A novel zone based routing protocol for detection of replicas in static wireless sensor networks</p>
		</td>
		<td width="358">
		<p>International conference on Information,Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070790">10.1109/ICICES.2017.8070790</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>51. &nbsp;</p>
		</td>
		<td width="201">
		<p>&nbsp; Studies on open source real time operating systems: For vehicle suspension control</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070787">10.1109/ICICES.2017.8070787</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>52. &nbsp;</p>
		</td>
		<td rowspan="5" width="122">
		<p>Mr.A.Mani</p>
		</td>
		<td width="201">
		<p>Survey on Pollution Monitoring System using Sensors</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol.4, No. 2, pp. 38-40,ISSN(O) 2348-7860.&nbsp;ISSN(P):2348-7852(O), February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>53. &nbsp;</p>
		</td>
		<td width="201">
		<p>Privacy preserving Mining on Vertically Partitioned Database</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Advanced Engineering Research and Science (IJAERS), </strong>ISSN :&nbsp;<strong>2349-6495(P) | 2456-1908(O),</strong><strong>&nbsp; </strong>&nbsp;</p>
		<p><a href="http://ijaers.com/call-for-papers/">Vol.4, Issue-2</a><strong>,</strong></p>
		<p>Impact Factor:&nbsp;<strong>2.465,</strong> February 2017DOI: 10.22161/ijaers.4.2.534</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>54. &nbsp;</p>
		</td>
		<td width="201">
		<p>An Overview on Identification of Fradulent and Genuine Users in Online Advertising Market</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,Vol. 4, Issue 2,ISSN(O) : 2349-9745,ISSN(P) :2393-8181,February 2017DOI : 10.21884/IJMTER.2017.4066.IIIYP</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>55. &nbsp;</p>
		</td>
		<td width="201">
		<p>Identification of Fradulent and Genuine Users in Online Advertising Market</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College, 10<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>56. &nbsp;</p>
		</td>
		<td width="201">
		<p>Air Pollution Monitoring System using Sensors</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College, 10<sup>th</sup> March 2017.</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>57. &nbsp;</p>
		</td>
		<td rowspan="5" width="122">
		<p>Mrs. D. Vinodha</p>
		</td>
		<td width="201">
		<p>A Survey on Proxy Re-encryption Method using Cloud Computing</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol. 4, No. 2,pp. 57-59, ISSN 2348-7860.&nbsp;ISSN:2348-7860(O),February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>58. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey on Secure Data Aggregation using Clustering Methods in WSN</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol.4, No 2, pp. 63-65,&nbsp;ISSN 2348-7860.&nbsp;ISSN:2348-7860(O),February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>59. &nbsp;</p>
		</td>
		<td width="201">
		<p>Novel framework for secure data aggregation in wireless sensor networks.</p>
		</td>
		<td width="358">
		<p>National Conference on Recent Trends &amp; Emerging Technologies (NCRTET’17), 24<sup>th</sup> March 2017 at Rajiv Gandhi Engineering college</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>60. &nbsp;</p>
		</td>
		<td width="201">
		<p>Analyzing Doctor and Patient Medical Details Retrieval by Proxy Re-Encryption Method using Cloud Computing</p>
		</td>
		<td width="358">
		<p>5<sup>th</sup>&nbsp; national conference on computing communication &amp; information technology, N3CIT’17, T.J.S. Engineering college on 25<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>61. &nbsp;</p>
		</td>
		<td width="201">
		<p>A survey on privacy preserving data aggregation in wireless sensor networks</p>
		</td>
		<td width="358">
		<p>International conference on Information,Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070768">10.1109/ICICES.2017.8070768</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>62. &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mr.S.Prabhu</p>
		</td>
		<td width="201">
		<p>Android Application for College Management System(M-Insproplus)</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,Volume 04, Issue 2,ISSN:2349-9745, Scientific Journal Impact Factor(SJIF):4.364, February 2017,DOI:&nbsp;<a href="https://dx.doi.org/10.21884/IJMTER.2017.4049.XFHWV">10.21884/IJMTER.2017.4049.XFHWV</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>63. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey on Cloud Based Video Suggestions with Categorization of users in Shared Systems</p>
		</td>
		<td width="358">
		<p>International Journal of Innovative Research in Engineering and Management,Volume 4, Issue 1, ISSN : 2350-0557,January 2017,DOI : 10.21276/ijirem</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>64. &nbsp;</p>
		</td>
		<td rowspan="3" width="122">
		<p>Mr.MuthuKumarasamy</p>
		</td>
		<td width="201">
		<p>Border Alert System And Emergency Contact For Fisherman Using RSSI</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017),23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017,</a><a href="mailto:2017@S.A.Engineering">S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070766">10.1109/ICICES.2017.8070766</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>65. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Data Control from Sender using Cloud Computing</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Advanced Engineering Research and Science (IJAERS), </strong>ISSN :&nbsp;<strong>2349-6495(P) | 2456-1908(O),</strong><strong>&nbsp; </strong>&nbsp;</p>
		<p><a href="http://ijaers.com/call-for-papers/">Vol.4, Issue-2</a><strong>,</strong></p>
		<p>Impact Factor:&nbsp;<strong>2.465,</strong> February 2017,DOI: <a href="https://dx.doi.org/10.22161/ijaers.4.2.5">10.22161/ijaers.4.2.5</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>66. &nbsp;</p>
		</td>
		<td width="201">
		<p>Data Control from Sender using Cloud Computing</p>
		</td>
		<td width="358">
		<p>National Conference on Computing Communication and Information Technology (N3CIT’17), T.J.S Engineering College</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>67. &nbsp;</p>
		</td>
		<td rowspan="5" width="122">
		<p>Mrs. M. Preetha</p>
		</td>
		<td width="201">
		<p>A Survey on Entry Restriction System for the Fake Server Scheme</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering , ISSN: 2348-7860 (O),Vol. 4, No. 2, PP. 41-44 ,February 2017, DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>68. &nbsp;</p>
		</td>
		<td width="201">
		<p>Flood Alert – without Tower</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,Vol. 4, Issue 1,ISSN(O) : 2349-9745,ISSN(P) :2393-8161,January 2017,DOI :&nbsp;<a href="https://doi.org/10.21884/IJMTER.2017.4040.DGKRI">10.21884/IJMTER.2017.4040.DGKRI</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>69. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Two Tier Security with Data Sharing and Data Integrity Inspection in&nbsp; Cloud</p>
		</td>
		<td width="358">
		<p>International Journal of Modern Trends in Engineering and Research,&nbsp; ISSN:2393-8161, Volume 04, Issue 2, Scientific Journal Impact Factor(SJIF):4.364,February 2017,DOI:10.21884/IJMTER.2017.4052.HIU2T</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>70. &nbsp;</p>
		</td>
		<td width="201">
		<p>An Intelligent Digital System for Visually ImpairedPerson (Vip’s)</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070769">10.1109/ICICES.2017.8070769</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>71. &nbsp;</p>
		</td>
		<td width="201">
		<p>Two Tier Security with Data Sharing and Data Integrity Inspection in&nbsp; Cloud</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College,10<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>72. &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.V.Sureka</p>
		</td>
		<td width="201">
		<p>Hybrid Data Security Mechanism for Cloud Storage System</p>
		</td>
		<td width="358">
		<p>International Journal of Current Engineering And Scientific Research ( IJCESR),Vol. 4,Issue 2,ISSN(PRINT):2393-8374,February 2017,<strong>DOI : 10.21276/ijcesr</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>73. &nbsp;</p>
		</td>
		<td width="201">
		<p>Leer detection with webcam in a desktop environment</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070785">10.1109/ICICES.2017.8070785</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>74. &nbsp;</p>
		</td>
		<td rowspan="3" width="122">
		<p>Mrs. K.B.Aruna</p>
		</td>
		<td width="201">
		<p>Cloud Based Vehicle Parking System for Anonymous Place using Internet of Things</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Advanced Engineering Research and Science (IJAERS), </strong>ISSN :&nbsp;<strong>2349-6495(P) | 2456-1908(O),</strong><a href="http://ijaers.com/call-for-papers/">Vol.-4, Issue-2</a><strong>, </strong>Impact Factor:&nbsp;<strong>2.465,</strong>February 2017, <strong>DOI:</strong>&nbsp;<a href="https://dx.doi.org/10.22161/ijaers.4.2.7">10.22161/ijaers.4.2.</a><strong>13</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>75. &nbsp;</p>
		</td>
		<td width="201">
		<p>Protection for Multi Owner Data Sharing Scheme</p>
		</td>
		<td width="358">
		<p>Bonfring International Journal of Advances in Image processing,Vol 7, No.1, March 2017, DOI : 10.9756/BIJAIP.10485</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>76. &nbsp;</p>
		</td>
		<td width="201">
		<p>Leer detection with webcam in a desktop environment</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070785">10.1109/ICICES.2017.8070785</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>77. &nbsp;</p>
		</td>
		<td rowspan="4" width="122">
		<p>Mr.M.P. Karthikeyan</p>
		</td>
		<td width="201">
		<p>A Survey on Effective Attendance Marking using Face Recozgnition Behavior Monitoring and RFID</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Engineering Research</strong>&nbsp;(IJER), ISSN (Online)&nbsp;: 2319-6890, Vol. 6 Issue 3 PP:136-139,March &nbsp;2017,DOI: 10.17950</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>78. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on Identification of Glaucoma, Exudates using Retinal Scan and Wavelet Filters</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Engineering Research</strong>&nbsp;(IJER), ISSN (Online)&nbsp;: 2319-6890, Vol. 6 Issue 3 PP:132-135,March 2017DOI: 10.17950</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>79. &nbsp;</p>
		</td>
		<td width="201">
		<p>Effective Attendance Marking using Face Recognition Behavior Monitoring and RFID</p>
		</td>
		<td width="358">
		<p><strong>National Conference on Data Science and Intelligent Information Technology (NCDSIIT’17), Rajalakshmi Institute of Technology </strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>80. &nbsp;</p>
		</td>
		<td width="201">
		<p>Authentication Through Identification of Glaucoma Exudates and Diabetics, using Retinal Scan and Wavelet Filters</p>
		</td>
		<td width="358">
		<p>National Conference on Recent Innovations in Software Engineering &amp; Computer Technologies NCRISECT 2017, SRM University, 23.03.2017 &amp; 24.03.2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>81. &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.K.Jayanthi</p>
		</td>
		<td width="201">
		<p>A Survey on Quick Response Code</p>
		</td>
		<td width="358">
		<p>International Journal of Innovative Research in Engineering and Management, Volume 4, Issue 1, ISSN : 2350-0557, January 2017, DOI:10.21276/IJIREM</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>82. &nbsp;</p>
		</td>
		<td width="201">
		<p>Leer detection with webcam in a desktop environment</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070785">10.1109/ICICES.2017.8070785</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>83. &nbsp;</p>
		</td>
		<td rowspan="4" width="122">
		<p>Ms.K.RamyaDevi</p>
		</td>
		<td width="201">
		<p>Detecting Victim System in Client and Client Networks</p>
		</td>
		<td width="358">
		<p>International Journal of Current Engineering and Scientific Research,Vol 4, Issue 2, pp 1-4,ISSN(P): 2393 8374,ISSN(O) : 2394 0697,2017,<strong>DOI : 10.21276/ijcesr</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>84. &nbsp;</p>
		</td>
		<td width="201">
		<p>Flou Images Decoloration via deep Learning</p>
		</td>
		<td width="358">
		<p>International Journal Of Advanced Research(IJAR),ISSN:2320-5407,Volume 05, Issue 02,pp 1947-1952, February 2017,DOI : <a href="http://dx.doi.org/10.21474/IJAR01">10.21474/IJAR01</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>85. &nbsp;</p>
		</td>
		<td width="201">
		<p>An Intelligent Digital System for Visually ImpairedPerson (Vip’s)</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070769">10.1109/ICICES.2017.8070769</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>86. &nbsp;</p>
		</td>
		<td width="201">
		<p>Fuzzy Images Deblurring</p>
		</td>
		<td width="358">
		<p>National Conference on Novel Computing, NCNC’17, SMK FOMRA Institute of Technology, 24.02.2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>87. &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.L.Sudha</p>
		</td>
		<td width="201">
		<p>Cloud Based Vehicle Parking System for Anonymous Place using Internet of Things</p>
		</td>
		<td width="358">
		<p><strong>International Journal of Advanced Engineering Research and Science (IJAERS), </strong>ISSN :&nbsp;<strong>2349-6495(P) | 2456-1908(O),</strong><strong>&nbsp; </strong>&nbsp;<a href="http://ijaers.com/call-for-papers/">Vol. 4, Issue-2</a><strong>, </strong>Impact Factor:&nbsp;<strong>2.465,</strong>Febraury 2017,<strong>DOI:</strong>&nbsp;<a href="https://dx.doi.org/10.22161/ijaers.4.2.7">10.22161/ijaers.4.2.</a><strong>13</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>88. &nbsp;</p>
		</td>
		<td width="201">
		<p>Leer detection with webcam in a desktop environment</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070785">10.1109/ICICES.2017.8070785</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>89. &nbsp;</p>
		</td>
		<td rowspan="3" width="122">
		<p>Mr.T.Vignesh</p>
		</td>
		<td width="201">
		<p>Analysis of Thresholding versus Image Fusion Techniques to change Detection using Remote Sensing Images</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Research in Computer Science and Management Studies, Vol. 4, Issue 4, August 2016,ISSN: 2321-7782(Online),Impact Factor : 6.047</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>90. &nbsp;</p>
		</td>
		<td width="201">
		<p>A Novel Multiple Unsupervised Algorithm for Land Use/Land Cover Classification</p>
		</td>
		<td width="358">
		<p>Indian Journal of Science and Technology,Vol 9(42), November 2016,ISSN(Print) : 0974-6846ISSN(Online) : 0974-5645,DOI : 10.17485/ijst/2016/ v9i42/99682</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>91. &nbsp;</p>
		</td>
		<td width="201">
		<p>Water Bodies Identification from Multispectral Images using Gabor filter, FCM and Canny Edge Detection methods</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070785">10.1109/ICICES.2017.8070785</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>92. &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.P.Rubasudha</p>
		</td>
		<td width="201">
		<p>Online Toll Payment System using Android Application</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol. 04 No. 02,ISSN: 2348-7860 (O), pp 48-50,February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>93. &nbsp;</p>
		</td>
		<td width="201">
		<p>Leer detection with webcam in a desktop environment</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070785">10.1109/ICICES.2017.8070785</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>94. &nbsp;</p>
		</td>
		<td rowspan="4" width="122">
		<p>Mrs.K.Elavarasi</p>
		</td>
		<td width="201">
		<p>A Survey on Quick Response Code</p>
		</td>
		<td width="358">
		<p>International Journal of Innovative Research in Engineering and Management, Volume 4, Issue 1, ISSN : 2350-0557, January 2017, DOI:10.21276/IJIREM</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>95. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey On Lugar Motion And Alert System</p>
		</td>
		<td width="358">
		<p>International Journal Of Advanced Engineering And&nbsp; Research Development (IJAERD), E-ISSN:2348-4470,P-ISSN:2348-6406,Vol. 04, Issue-01,pp :240-243, January-2017, DOI:10.21090/IJAERD</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>96. &nbsp;</p>
		</td>
		<td width="201">
		<p>An Intelligent Digital System for Visually ImpairedPerson (Vip’s)</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017, S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070769">10.1109/ICICES.2017.8070769</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>97. &nbsp;</p>
		</td>
		<td width="201">
		<p>Lugar Motion Detection and Alert Notification Through Mobile APP</p>
		</td>
		<td width="358">
		<p>National Conference on Novel Computing, NCNC’17, SMK FOMRA Institute of Technology, 24.02.2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>98. &nbsp;</p>
		</td>
		<td rowspan="3" width="122">
		<p>Mrs.V.Saraswathi</p>
		</td>
		<td width="201">
		<p>Implementation of Bidirectional Voice Communication Between Normal and Deaf &amp; Dumb Person</p>
		</td>
		<td width="358">
		<p>International Journal of Computer Science and Engineering, Volume 2,Issue 6, ISSN : 2456-1843,June 2016</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>99. &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey: Novel Framework for Smart Health Consulting Using Android Device</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Engineering and Research Development,Volume 4, Issue 2,ISSN (P): 2348-6406, (SJIF): 4.72,19.02.2017, DOI:10.21090/IJAERD.84872</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>100.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Smart Health Consulting System</p>
		</td>
		<td width="358">
		<p>National Conference on Emerging Trends &amp; Transformation in Engineering and Technology, NC2(ET), Sri Lakshmi Ammal Engineering College</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>101.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="122">
		<p>Mrs. C.Saranya</p>
		</td>
		<td width="201">
		<p>Implementation of Bidirectional Voice Communication Between Normal and Deaf &amp; Dumb Person</p>
		</td>
		<td width="358">
		<p>International Journal of Computer Science and Engineering,Volume 2, Issue 6,ISSN : 2456-1843,June 2016</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>102.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs. S.Sushmitha</p>
		</td>
		<td width="201">
		<p>Implementation of Bidirectional Voice Communication Between Normal and Deaf &amp; Dumb Person</p>
		</td>
		<td width="358">
		<p>International Journal of Computer Science and Engineering,Volume 2, Issue 6,ISSN : 2456-1843,June 2016</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>103.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Survey on Breakdown Vehicle Alert System</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering, Vol. 04 No. 02,ISSN: 2348-7860 (O), PP. 45-47,February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>104.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Ms. G. Gowri</p>
		</td>
		<td width="201">
		<p>A Survey on Entry Restriction System for the Fake Server Scheme</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering , ISSN: 2348-7860 (O),Vol. 4, No. 2, PP. 41-44 ,February 2017,DOI :&nbsp; 10.21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>105.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Trustworthy access control for wireless body area networks</p>
		<p>&nbsp;</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070744">10.1109/ICICES.2017.8070744</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>106.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.LalithaShri</p>
		</td>
		<td width="201">
		<p>Protection for Multi Owner Data Sharing Scheme</p>
		</td>
		<td width="358">
		<p>Bonfring International Journal of Advances in Image processing,Vol 7, No.1, March 2017,DOI : 10.9756/BIJAIP.10485</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>107.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey On Linear Network Coding And Multimedia Traffic Over Wireless Networks</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Research in Management, Architecture, Technology and Engineering (IJARMATE),(ISSN 2454-9762(Print), ISSN 2454-9762 (Online),Impact factor: 4.231, Volume 3,Special Issue 11, March 2017, DOI: <a href="http://www.ijarmate.com/index.php?option=com_login&amp;task=download_volume_doc&amp;fname=v3s11march2017srisubramanya&amp;foldertype=conference&amp;id=776">10.20256/IJARMATE.2017.03S1103002</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>108.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="122">
		<p>Ms.P.Dharani</p>
		</td>
		<td width="201">
		<p>A Survey On Linear Network Coding And Multimedia Traffic Over Wireless Networks</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Research in Management, Architecture, Technology and Engineering (IJARMATE),(ISSN 2454-9762(Print), ISSN 2454-9762 (Online),Impact factor: 4.231, Volume 3,Special Issue 11, March 2017, DOI: <a href="http://www.ijarmate.com/index.php?option=com_login&amp;task=download_volume_doc&amp;fname=v3s11march2017srisubramanya&amp;foldertype=conference&amp;id=776">10.20256/IJARMATE.2017.03S1103002</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>109.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.E.Abirami</p>
		</td>
		<td width="201">
		<p>Survey On 5G Technology-Evolution And Revolution</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Research in Management, Architecture, Technology and Engineering (IJARMATE),(ISSN 2454-9762(Print), ISSN 2454-9762 (Online),Impact factor: 4.231, Volume 3,Special Issue 11, March 2017, DOI: <a href="http://www.ijarmate.com/index.php?option=com_login&amp;task=download_volume_doc&amp;fname=v3s11march2017srisubramanya&amp;foldertype=conference&amp;id=777">10.20256/IJARMATE.2017.03S1103003</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>110.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Proficient key management scheme for multicast groups using group key agreement and broadcast encryption</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070789">10.1109/ICICES.2017.8070789</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>111.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Mrs.T.Padmavathy</p>
		</td>
		<td width="201">
		<p>Survey On 5G Technology-Evolution And Revolution</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Research in Management, Architecture, Technology and Engineering (IJARMATE),(ISSN 2454-9762(Print), ISSN 2454-9762 (Online),Impact factor: 4.231, Volume 3,Special Issue 11, March 2017, DOI: <a href="http://www.ijarmate.com/index.php?option=com_login&amp;task=download_volume_doc&amp;fname=v3s11march2017srisubramanya&amp;foldertype=conference&amp;id=777">10.20256/IJARMATE.2017.03S1103003</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>112.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Proficient key management scheme for multicast groups using group key agreement and broadcast encryption</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070789">10.1109/ICICES.2017.8070789</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>113.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="4" width="122">
		<p>Mr.M.Balasubramanian</p>
		</td>
		<td width="201">
		<p>Advance Metering Using Power Line Carrier Communication</p>
		</td>
		<td width="358">
		<p>International Journal of Advanced Engineering and Research Development,Volume 4, Issue 2,ISSN (O): 2348-4470, ISSN(P) : 2348-6406,(SJIF): 4.72, February 2017,DOI: 10.21474/ijar</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>114.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Adaptive Processing for Distributed Skyline Queries over Uncertain Data</p>
		</td>
		<td width="358">
		<p>International Research Journal of Engineering and Technology(IRJET),Volume 4, Issue 3,pp 1050-1053,e-ISSN : 2395-0056,p-ISSN : 2395-0072,Impact Factor – 5.181, March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>115.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>An Effective Heterogeneous L:oad Distribution mechanism for Cluster based MANETs using Dynamic Channel Allocation</p>
		</td>
		<td width="358">
		<p>Second InterNational Conference on Innovative and Emerging Trends in Engineering and Technology(ICIETET ’17), Panimalar Institute of Technology on 15<sup>th</sup> May 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>116.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>&nbsp; Studies on open source real time operating systems: For vehicle suspension control</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070787">10.1109/ICICES.2017.8070787</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>117.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="5" width="122">
		<p>Ms.Guna Nandhini</p>
		</td>
		<td width="201">
		<p>Privacy-Preserving Patient-Centric Clinical Decision Support System</p>
		</td>
		<td width="358">
		<p>International Journal of Current Engineering and Scientific Research(IJCESR), ISSN(P):2393-8374, ISSN(O):2394-0697,Vol. 4, Issue-2, 2017,<strong>DOI : 10.21276/ijcesr</strong></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>118.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Query Providing and report Generation of Cancer Prediction using Naïve Bayes Classifier</p>
		</td>
		<td width="358">
		<p>International Journal of Research and Engineering,Vol 4, No. 3,pp 82-85,ISSN : 2348-7960(O),ISSN : 2348-7852(P), March 2017,DOI :21276/ijre</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>119.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Authorizing Tool for Enhancing in Wed Base Learning Resources</p>
		</td>
		<td width="358">
		<p>National Conference on Cutting Edge Technologies in Computing, NCCETICB’17, Sri Ram Engineering College, 10<sup>th</sup> March 2017</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>120.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Integrated SPAM detection for multilingual emails</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070784">10.1109/ICICES.2017.8070784</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>121.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>Trustworthy access control for wireless body area networks</p>
		<p>&nbsp;</p>
		</td>
		<td width="358">
		<p>International conference on Information, Communication and Embedded Systems(ICICES2017), 23 &amp; 24 Feb <a href="mailto:2017@S.A.Engineering">2017@S.A.Engineering</a> College,<strong>DOI: </strong><a href="https://doi.org/10.1109/ICICES.2017.8070744">10.1109/ICICES.2017.8070744</a></p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>122.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td rowspan="2" width="122">
		<p>Dr.E.A.Mary Anita</p>
		</td>
		<td width="201">
		<p>A Survey on Mathematical Models for Wireless Sensor Network</p>
		</td>
		<td width="358">
		<p>Proceedings of International conference on Frontiers in Engineering, Applied Sciences and Technology(FIEAST’17), NIT Trichy, March 2017.ISBN : 978-81-908388-8-7</p>
		</td>
		</tr>
		<tr>
		<td width="61">
		<p>123.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>
		</td>
		<td width="201">
		<p>A Survey on data breach challenges in cloud computing security: Issues and Threats”</p>
		</td>
		<td width="358">
		<p>IEEE International Conference on Circuit, Power and Computing Technologies (ICCPT 2017), pp 1-8,&nbsp; ISBN : 978-15090-4967-7, DOI:10.1109/ICCPCT,2017.8074287.</p>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<span>List of staff publications in 2016-2017.</span>
		<h4>2015-2016</h4>
		<table style="height: 12603px;" width="697">
		<tbody>
		<tr>
		<td width="7%"><strong>S.No.</strong></td>
		<td width="21%"><strong>Name of the Faculty</strong></td>
		<td width="28%"><strong>Title of the Paper</strong></td>
		<td width="42%"><strong>Paper Details</strong></td>
		</tr>
		<tr>
		<td width="7%">1.</td>
		<td rowspan="5" width="21%">Dr. G. Umarani Srikanth</td>
		<td width="28%">E-Learning and its Impact on Indian Education</td>
		<td width="42%">National Seminar on Educational Technologies and E-Learning Technologies for Indian Class Rooms in the New Millennium, PRIST University, pp 10-14, June 2015</td>
		</tr>
		<tr>
		<td width="7%">2.</td>
		<td width="28%">Navigation Support for the Visually Impaired using Digitalized Pole</td>
		<td width="42%">National Conference on Recent Trends in Information Technology and Communication, March 2016</td>
		</tr>
		<tr>
		<td width="7%">3.</td>
		<td width="28%">Improving Network Life Time in Presence of Critical Node in the Path</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">4.</td>
		<td width="28%">Eliminating and Avoiding Critical Node in the Wireless Sensor Networks</td>
		<td width="42%">International Journal of Engineering Science and Computing, Vol 6, Issue 5, pp 5022-5025</td>
		</tr>
		<tr>
		<td width="7%">5.</td>
		<td width="28%">A survey on the Utilization of ANT Colony Optimization Algorithm in WSN</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016(ICICES2016)</td>
		</tr>
		<tr>
		<td width="7%">6.</td>
		<td rowspan="8" width="21%">Dr.E.A.Mary Anita</td>
		<td width="28%">Detection of Sybil Attack in Wireless Sensor Network</td>
		<td width="42%">Middle-East Journal of Scientific Research 23 (Sensing, Signal Processing and Security), pp.202-206, July 2015, ISSN 1990-9233, DOI:0.5829/Idosi.Mejsr.2015.23.Ssps.55</td>
		</tr>
		<tr>
		<td width="7%">7.</td>
		<td width="28%">A Signature based Secure Authentication Framework for Vehicular Adhoc Networks</td>
		<td width="42%">International Journal of Computer, Electrical, Automation, Control and Information Engineering Vol:10, No:2, 2016, pp. 369-374.</td>
		</tr>
		<tr>
		<td width="7%">8.</td>
		<td width="28%">Interactive Big Data Management in Healthcare Using Spark</td>
		<td width="42%">ISBCC’16, Volume 49 of the series Smart Innovation, Systems and Technologies, Springer International Publishing,&nbsp; pp 265-272. DOI: 10.1007/978-3-319-30348-2_21. ISBN 978-3-319-30347-5</td>
		</tr>
		<tr>
		<td width="7%">9.</td>
		<td width="28%">Secure and Efficient Distance Effect Routing Algorithm for Mobility (SE_DREAM) in MANETs</td>
		<td width="42%">ISBCC ‘16, Volume 49 of the series Smart Innovation, Systems and Technologies, Springer International Publishing, pp 65-80. DOI: 10.1007/978-3-319-30348-2_6. ISBN 978-3-319-30347-5</td>
		</tr>
		<tr>
		<td width="7%">10.</td>
		<td width="28%">A Survey on Authentication Schemes of VANETs</td>
		<td width="42%">International Conference on Information Communication and Embedded System (ICICES 2016). DOI: 10.1109/ICICES.2016.7518946. ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%">11.</td>
		<td width="28%">Tongue Image Analysis for Diabetic Diagnosis Detection Using Image segmentation</td>
		<td width="42%">Asian Journal of Information Technology, Volume 15, Issue 3, pp. 424-436. DOI : 10.3923/Ajit.2016.424.436</td>
		</tr>
		<tr>
		<td width="7%">12.</td>
		<td width="28%">Challenges and Security Issues in Vehicular Adhoc Networks</td>
		<td width="42%">3rd National Conference on Innovative Computing Techniques (NCICT’16), pp. 90-93.&nbsp; ISBN 978-1-941505-46-5</td>
		</tr>
		<tr>
		<td width="7%">13.</td>
		<td width="28%">A Novel Robust Routing protocol RAEED to Avoid DOS Attacks in WSN</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) DOI: 10.1109/ICICES.2016.7518946.&nbsp; ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>14.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="3" width="21%">Dr.V.Manjula</td>
		<td width="28%">Selfish Misbehaviour Detection and Prevention of Congestion Attacks With False Data Injection</td>
		<td width="42%">International Journal of Engineering Science and Computing, May 2016 DOI 10.4010/2016.1204, 6, Issue No.5, pp. 4848 -4851</td>
		</tr>
		<tr>
		<td width="7%"><strong>15.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Resilient Enroute Filtering Scheme for False Data Injection Attack in Cyber Physical Networked System(CPNS)</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>16.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Resilient Systems on Secure Sharing of Information Against False Data Injection</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>17.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="6" width="21%">Mrs.R.Geetha</td>
		<td width="28%">Effective Transmission of Aggregated Data for Wireless Sensor Networks</td>
		<td width="42%">International Journal of Advanced Research Trends in Engineering and Technology, Volume 3 Special Edition, 19<sup>th</sup> April 2016 pp549-554</td>
		</tr>
		<tr>
		<td width="7%"><strong>18.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">E-Learning and its Impact on Indian Education</td>
		<td width="42%">National Seminar on Educational Technologies and E-Learning Technologies for Indian Class Rooms in the New Millennium, PRIST University, pp 10-14,June 2015</td>
		</tr>
		<tr>
		<td width="7%"><strong>19.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A Hybrid Key Management Approach for Secure Communication in Wireless Sensor Networks</td>
		<td width="42%">Indian Journal of Science and Technology, Vol 8(15),57603, pp 1-8, ISSN(Print): 0974-6846, ISSN : 0974-5645, July 2015</td>
		</tr>
		<tr>
		<td width="7%"><strong>20.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Secure and Efficient Transmission of Aggregated Data (SETA) for Mobile Wireless Sensor Networks</td>
		<td width="42%">International Journal of Computer Electrical Automation Control and Information Engineering Vol:10, No:3, 2016, pp. 570-574. Waset.Org/Publication/10004879</td>
		</tr>
		<tr>
		<td width="7%"><strong>21.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Automatic Stress Detector and Compressor in Working Environment using Pressure Sensor</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>22.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Securing Data in Cloud Environment by Implementing Self Destructive Scheme.</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>23.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="5" width="21%">Mrs.S.Veena</td>
		<td width="28%">A survey on Privacy Preserving K-NN Classification over Encrypted Data</td>
		<td width="42%">International Journal of Engineering Science And Computing, Vol 6,Issue No.5, May 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>24.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Query Processing using Privacy Preserving K-NN Classification over Encrypted Data</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>25.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Semantically Secure Encryption with K-NN Classification in Relational Data</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>26.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Group User Revocation for Shared Dynamic Cloud Data in Cloud Storage</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>27.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Reputation based Task Scheduling for Database Systems by Electing Servers</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>28.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="5" width="21%">&nbsp; &nbsp; &nbsp; Mr.C.Bala Krishnan</td>
		<td width="28%">Secure Data Aggregation in WSN: Filtering out Collusion Attacker’s Impact</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>29.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Vehicle Communication based Driver Assistance and Immediate Payment for Tollgate</td>
		<td width="42%">International Conference on Innovations and Challenges in Engineering and Technology, Kings Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>30.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Prevention of Eves Dropping in Network Security using Quantum Key Generation Algorithm</td>
		<td width="42%">International&nbsp; Conference on Science and Innovative Engineering , Jawahar Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>31.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">An Enhanced Iterative Filtering Technique for Data Aggregation in WSN</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>32.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A survey on Data Aggregation Scheme in WSN</td>
		<td width="42%">International Journal of Engineering Science and Computing, DOI 10.4010/2016.1503</td>
		</tr>
		<tr>
		<td width="7%"><strong>33.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="21%">Mr.R.Prasanna Kumar</td>
		<td width="28%">Data Security in Cloud Environment using Multiple KMSP</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>34.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="5" width="21%">Mrs.N.S.Usha</td>
		<td width="28%">A Novel Robust Routing Protocol RAEED to Avoid DOS Attacks in WSN</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>35.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Public Auditing for Regenerating Code using Proxy in Cloud Storage</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>36.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Avoidance of DOS Attack in WSN using RAEED Protocol</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>37.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Secure Droid Restrict User Access Control based on GEO Location S/M for Mobile Devices</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>38.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A survey on Hello Flood Attack</td>
		<td width="42%">International Journal of Engineering Science and Computing, DOI 10.4010/2016.1099</td>
		</tr>
		<tr>
		<td width="7%"><strong>39.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="2" width="21%">Mr.A.Mani</td>
		<td width="28%">Crime Reporter COP’s Android APP</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>40.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">IOT Based Parking System using Cloud and Sensors.</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>41.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="3" width="21%">Mrs.D.Vinodha</td>
		<td width="28%">Energy Efficient Intrusion Detection System</td>
		<td width="42%">International Journal for Scientific Research &amp; Development IJSRD, April 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>42.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Cost-Effective Authentic and Anonymous Data Sharing with Forward Security</td>
		<td width="42%">International Conference on Advances in Engineering, Science &amp; Technology @ Chennai.</td>
		</tr>
		<tr>
		<td width="7%"><strong>43.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Data Security EOR Complex Data: Privacy and Data Mining</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>44.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="2" width="21%">Mr.S.Prabhu</td>
		<td width="28%">Enhancing Security using ECDH in VANET</td>
		<td width="42%">National Conference on Recent Trends in Information Technology and Communication 2016,&nbsp;March 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>45.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A Secured Architecture for Light Weight Integrity in Named Data Networking</td>
		<td width="42%">National Conference on Recent Trends in Information Technology and Communication</td>
		</tr>
		<tr>
		<td width="7%"><strong>46.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td rowspan="8" width="21%">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mr.S.Muthu Kumarasamy</td>
		<td width="28%">Stratified Report Assisted Reputation Administration (SRA) System for MANETS</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>47.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Balanced Clustering in Mobile Adhoc Networks using Route Cluster</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%"><strong>48.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A Survey on Reputation System and Price System based Cooperation Inducement Scheme in Mobile Ad-hoc Networks</td>
		<td width="42%">International Journal of Engineering Trends And Technology, Seventh Sense Research Group, Vol. 5,No. 1, Jan 2016, DOI : 10.14445/22315381/IJETT-V35P205</td>
		</tr>
		<tr>
		<td width="7%"><strong>49.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A Controlled Overhead and Throughput using Hybrid Wireless Networks Distributive Approach</td>
		<td width="42%">International Journal of&nbsp; Innovative Research in Computer and Communication Engineering</td>
		</tr>
		<tr>
		<td width="7%"><strong>50.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Cooperative Communication in MANETS using Extended and Weakly Connected Dominating Set</td>
		<td width="42%">International Journal of&nbsp; Emerging Technology in Computer Science and Electronics&nbsp; ISSN No. 0076-1353, Vol 19, Issue 2, Jan 2016</td>
		</tr>
		<tr>
		<td width="7%"><strong>51.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Efficient Channel Management non Uniform Load Distribution in Cluster based MANETS</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%"><strong>52.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">Certificateless Effective Group Key Management in Dynamic Wireless Sensor Network</td>
		<td width="42%">ICTACT-NCACT-16 @ DMI College Of Engineering, March 2016.</td>
		</tr>
		<tr>
		<td width="7%"><strong>53.&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;</strong></td>
		<td width="28%">A Hierarchical ARM System for MANET’s</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">54.</td>
		<td width="21%">Mrs.M.Renuka Devi</td>
		<td width="28%">Cloud Based Internal Discussion and Decision Portal</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%">55.</td>
		<td rowspan="2" width="21%">Ms.J.Sangeetha</td>
		<td width="28%">Phony Video Exposure</td>
		<td width="42%">National Conference on Computing Communication and Information Technology (N3CIT16) April 16</td>
		</tr>
		<tr>
		<td width="7%">56.</td>
		<td width="28%">Reranking Model for Web Image Search Using Attributes</td>
		<td width="42%">National Conference on Recent Trends in Information Technology and Communication 2016,&nbsp;March 2016</td>
		</tr>
		<tr>
		<td width="7%">57.</td>
		<td width="21%">Mr.Anwar Basha H</td>
		<td width="28%">Collaborative Attacks &nbsp;in MANETs and a Corrective SAIT Detection Approach</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%">58.</td>
		<td width="21%">Ms.S.Sajini</td>
		<td width="28%">Automatic Sentiment Analysis of User Reviews</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">59.</td>
		<td rowspan="6" width="21%">Mrs.M.Preetha</td>
		<td width="28%">An Energy Efficient Routing Protocol for Correlated Data using CL-LEACH in WSN</td>
		<td width="42%">The Journal of Mobile Communication, Computation and Information, ISSN: 1022-0038 (Print) 1572-8196 (Online),May 2016, Volume 22, <a href="http://link.springer.com/journal/11276/22/4/page/1">Issue&nbsp;4</a>,pp 1415–1423, DOI : 10.1007/s11276-015-1063-4</td>
		</tr>
		<tr>
		<td width="7%">60.</td>
		<td width="28%">Intrusion Detection System in Standalone and Cooperative Network</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">61.</td>
		<td width="28%">Identifying Data Integrity and Routing Path in Wireless Sensor Network</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, 02.04.2016</td>
		</tr>
		<tr>
		<td width="7%">62.</td>
		<td width="28%">Identifying Selfish Nodes Using Mutual Neighbor Based Watchdog Mechanism for DTN</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES 16) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%">63.</td>
		<td width="28%">A survey on Misbehaviour Report Authentication Scheme of Selfish Node Detection using Collaborative Approach in MANET</td>
		<td width="42%">IJESC, Vol 6 Issue No.5 May 2016</td>
		</tr>
		<tr>
		<td width="7%">64.</td>
		<td width="28%">Aggregation Technique in the Presence of Collusion Attack for Secure Data using Interactive Filtering Algorithm in Wireless Sensor Networks</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%">65.</td>
		<td rowspan="2" width="21%">Mrs.V.Sureka</td>
		<td width="28%">A Mobile Augmented&nbsp; Reality Framework in Marketing Purposes</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%">66.</td>
		<td width="28%">Specification based Intrusion Detection and Prevention using Advanced Metering&nbsp; Infrastructure</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">67.</td>
		<td rowspan="3" width="21%">Mrs.K.B.Aruna</td>
		<td width="28%"><a href="https://scholar.google.co.in/citations?view_op=view_citation&amp;hl=en&amp;user=0TfxORsAAAAJ&amp;citation_for_view=0TfxORsAAAAJ:u5HHmVD_uO8C">Query Specific Fusion for Image Retrieval System using Ontology</a></td>
		<td width="42%">International Journal of Computer and Organization Trends, Volume 30 Number 2 – March 2016 Issue. Impact Factor:1.190</td>
		</tr>
		<tr>
		<td width="7%">68.</td>
		<td width="28%">Recommendation System based on Clustering and Collaborative Filtering</td>
		<td width="42%">International Conference on Emerging and Recent Technology 2016, April 2016</td>
		</tr>
		<tr>
		<td width="7%">69.</td>
		<td width="28%">Geographical Influence and Points of Interest Based Service Recommendation</td>
		<td width="42%">National Conference on Recent Trends in Information Technology and Communication,&nbsp;March 2016</td>
		</tr>
		<tr>
		<td width="7%">70.</td>
		<td rowspan="2" width="21%">Mr.M.P.Karthikeyan</td>
		<td width="28%">Sensory Data Transmission Using Wireless Sensor Network on the IOT Environment in Health Care</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">71.</td>
		<td width="28%">Secure User Identity Verification using Context Aware Security Hierarchical by Multilevel Architecture(CASHMA)</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%">72.</td>
		<td rowspan="2" width="21%">Ms.K.Jayanthi</td>
		<td width="28%">A Tracking System using Location Prediction and Dynamic Threshold for Minimising SMS Delivery</td>
		<td width="42%">International Journal of Advanced Research Trends in Engineering and Technology (IJARTET), Vol. 3, Special Issue 19, April 2016 ICICET2016 @ Kings Engineering College</td>
		</tr>
		<tr>
		<td width="7%">73.</td>
		<td width="28%">Network Immunisation and Virus Propagation in E-mail Maleware</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">74.</td>
		<td rowspan="3" width="21%">Ms.K.Ramya Devi</td>
		<td width="28%">Cloud Intelligence in an Inter Cloud</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">75.</td>
		<td width="28%">A Novel Robust Routing Protocol RAEED to Avoid DOS Attack in WSN</td>
		<td width="42%">International Conference on Information Communication and Embedded System 2016 (ICICES2016) ISBN: 978-1-5090-2552-7</td>
		</tr>
		<tr>
		<td width="7%">76.</td>
		<td width="28%">PIN Entry Method -Resilient to Shoulder Surfing and Recording Attack</td>
		<td width="42%">National Conference on Computing Communication &amp; Information Technology, TJS College, April 2016</td>
		</tr>
		<tr>
		<td width="7%">77.</td>
		<td rowspan="3" width="21%">&nbsp; Mrs.L.Sudha</td>
		<td width="28%"><a href="https://scholar.google.co.in/citations?view_op=view_citation&amp;hl=en&amp;user=0TfxORsAAAAJ&amp;citation_for_view=0TfxORsAAAAJ:u5HHmVD_uO8C">Query Specific Fusion for Image Retrieval System using Ontology</a></td>
		<td width="42%">International Journal of Computer and Organization Trends, Volume 30 Number 2 – March 2016 Issue.Impact Factor:1.190</td>
		</tr>
		<tr>
		<td width="7%">78.</td>
		<td width="28%">Monitoring Greenhouse using IOT</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, 02.04.2016</td>
		</tr>
		<tr>
		<td width="7%">79.</td>
		<td width="28%">Profit Driven Hybrid Cloud Management for Multimedia Cloud Computing</td>
		<td width="42%">International Journal of Advanced Research Trends in Engineering and Technology(IJARET), Vol 3, Special Issue 19, April 2016.</td>
		</tr>
		<tr>
		<td width="7%">80.</td>
		<td rowspan="2" width="21%">Mr.T.Vignesh</td>
		<td width="28%">A Secure ERASURE Code-based Cloud Storage System using Proxy ReEncryption</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology, April 2016</td>
		</tr>
		<tr>
		<td width="7%">81.</td>
		<td width="28%">Efficient Classification Methodology for Change Detection using Satellite Imagery</td>
		<td width="42%">Australian Journal of Basic and Applied Sciences, 9(20) June 2015, Pages: 580-590, ISSN:1991-8178, June-2015</td>
		</tr>
		<tr>
		<td width="7%">82.</td>
		<td width="21%">Mrs.NRG.Sreevani</td>
		<td width="28%">Remote Data Acquisition for Combat Vehicle</td>
		<td width="42%">International Journal for Research in Applied Science and Engineering Technology, Vol 4, Issue V, May 2016, ISSN : 2321-9653, IC value : 13.98</td>
		</tr>
		<tr>
		<td width="7%">83.</td>
		<td width="21%">Ms.P.Rubasudha</td>
		<td width="28%">Query Specific Fusion for Image Retrieval System using Ontology</td>
		<td width="42%">International Journal of Computer and Organization Trends, Volume 30 Number 2 – March&nbsp;2016 Issue.Impact Factor:1.190</td>
		</tr>
		<tr>
		<td width="7%">84.</td>
		<td rowspan="2" width="21%">Mrs.K.Elavarasi</td>
		<td width="28%">Android based Secure Account Login and Capture the Hacker</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">85.</td>
		<td width="28%">A Smart Phone Application for False Detection and Sensing Accidents</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology</td>
		</tr>
		<tr>
		<td width="7%">86.</td>
		<td rowspan="2" width="21%">Mrs.V.Saraswathi</td>
		<td width="28%">Balancing Workload of a Cloud and Dynamic Request Redirection for Cloud based Video Services using CDN and Data Cube</td>
		<td width="42%">National Conference on Cutting Edge Technologies in Computing and Bioinformatics (NCCETICB’16) @ Sri Ram Engineering College</td>
		</tr>
		<tr>
		<td width="7%">87.</td>
		<td width="28%">Privacy Location Provenance for Mobile Devices Using a Witness and an Auditor</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology</td>
		</tr>
		<tr>
		<td width="7%">88.</td>
		<td rowspan="2" width="21%">Ms.C.Saranya</td>
		<td width="28%">Secure Transmission of Data using Random Linear Network Coding</td>
		<td width="42%">National Conference on Computing, Communication &amp; Information Technology (NCIT ), April 2016</td>
		</tr>
		<tr>
		<td width="7%">89.</td>
		<td width="28%">Remote Data Acquisition for Combat Vehicle</td>
		<td width="42%">International Journal for Research in Applied Science and Engineering Technology, Vol 4, Issue V, May 2016, ISSN : 2321-9653, IC Value : 13.98</td>
		</tr>
		<tr>
		<td width="7%">90.</td>
		<td rowspan="2" width="21%">Ms.S.Sushmitha</td>
		<td width="28%">Map Based Multicopy Dynamic Data Possession in Cloud Computing</td>
		<td width="42%">International Conference on Breakthrough in Engineering Science &amp; Technology-2016, March 2016&nbsp;@ Salem, India.</td>
		</tr>
		<tr>
		<td width="7%">91.</td>
		<td width="28%">Placement Cell Automation System</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology</td>
		</tr>
		<tr>
		<td width="7%">92.</td>
		<td width="21%">Ms.P.Dharani</td>
		<td width="28%">Extended (K,L,n)-Threshold Secret Sharing Scheme in Secure Deduplication.</td>
		<td width="42%">International Journal of Advances in Engineering Science and Technology (IJAEST), Volume 4, Number 3. Nov 2016.</td>
		</tr>
		<tr>
		<td width="7%">93.</td>
		<td rowspan="2" width="21%">Mr.M.Bala Subramanian</td>
		<td width="28%">Provably Secure Timed-Release Public Key Encryption</td>
		<td width="42%">National Conference on Computing, Communication and Information Technology, 07-04-2016</td>
		</tr>
		<tr>
		<td width="7%">94.</td>
		<td width="28%">Studies on Open Source Real Time Operating Systems for Vehicle Suspension Control</td>
		<td width="42%">National Conference on Innovative Trends in Engineering &amp; Technology (NCITET 2K16) @ Arignar Anna Institute of Science &amp; Technology</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h4><u></u><strong><u>2014-2015</u></strong></h4>
		<table style="height: 12317px;" width="691">
		<tbody>
		<tr>
		<td width="49"><strong>S.No</strong></td>
		<td width="157"><strong>Name of the Faculty</strong></td>
		<td width="199"><strong>Title of the Paper</strong></td>
		<td width="265"><strong>Paper Details</strong></td>
		</tr>
		<tr>
		<td width="49">1.</td>
		<td rowspan="6" width="157">Dr.G.Umarani Srikanth</td>
		<td width="199">Moving ATM Applications to Smart Phones with Secured PIN-Entry Methods</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE) e-ISSN:2278 0661, p-ISSN:&nbsp; 2278-8727 ,Volume 17, Issue 1, Ver.II, PP: 58-65 DOI: 10.9790/0661-17125865, Jan-Feb 2015</td>
		</tr>
		<tr>
		<td width="49">2.</td>
		<td width="199">Task Scheduling Model</td>
		<td width="265">Indian Journal of Science and Technology Volume 8(S7), 33-42, ISSN (Print): 0974-6846, ISSN(Online): 0974-5645, April 2015</td>
		</tr>
		<tr>
		<td width="49">3.</td>
		<td width="199">Effective Cancer Detection Using Soft Computing Technique</td>
		<td width="265">IOSR Journal of Computer Engineering(IOSR-JCE) e-ISSN 2278-0661, p-ISSN 2278-8727,Volume 17, Issue 1, Ver.IV, PP: 01-05DOI: 10.9790/0661-17140105, Jan-Feb 2015</td>
		</tr>
		<tr>
		<td width="49">4.</td>
		<td width="199">Secure SMS Communication using Cipher-SMS Protocol in Android Mobile Phone</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">5.</td>
		<td width="199">A Trinity Construction For Web Extraction Using Efficient Algorithm</td>
		<td width="265">IOSR Journal of Computer Engineering(IOSR-JCE) e-ISSN:2278-0661,p-ISSN:2278-8727 ,Volume 17, Issue 1, Ver.II, pp: 46-52,DOI: 10.9790/0661-17124652,Jan-Feb 2015</td>
		</tr>
		<tr>
		<td width="49">6.</td>
		<td width="199">An Efficient Data Security System for Group Data Sharing in Cloud System Environment</td>
		<td width="265">International Conference on Information Communication and Embedded Systems, ICICES 2014</td>
		</tr>
		<tr>
		<td width="49">7.</td>
		<td rowspan="10" width="157">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Dr.E.A.Mary Anita &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Dr. E.A.Mary Anita</td>
		<td width="199">A secure Authentication Framework for Mobile Adhoc Networks</td>
		<td width="265">Asian Journal of Information Technology, 13(2): 58-67 ISSN: 1682-3915, JULY 2014</td>
		</tr>
		<tr>
		<td width="49">8.</td>
		<td width="199">Heuristic Approach of Supervised Learning for Intrusion Detection</td>
		<td width="265">Indian Journal of Science and Technology, Vol 7(S6), 11–14, ISSN (Print) : 0974-6846 ISSN(Online):0974-5645, Oct-2014</td>
		</tr>
		<tr>
		<td width="49">9.</td>
		<td width="199">Performance Analysis of Black Hole Attacks in Geographical Routing MANET</td>
		<td width="265">International Journal of Engineering and Technology (IJET),ISSN : 0975-4024, Vol 6,No.5, Oct-Nov-2014</td>
		</tr>
		<tr>
		<td width="49">10.</td>
		<td width="199">A Survey of Big Data Analytics in Healthcare and Government</td>
		<td width="265">2<sup>nd</sup>International Symposium on Big Data and Cloud Computing(ISBCC’15) Procedia Computer science 50(2015) 408-413. DOI:10.1016/j.procs2015.04.021,April 2015</td>
		</tr>
		<tr>
		<td width="49">11.</td>
		<td width="199">Securing Multi Cloud using Secret Sharing Algorithm</td>
		<td width="265">2<sup>nd</sup>International Symposium on Big Data and Cloud Computing(ISBCC’15) Procedia Computer science 50(2015) 421-426. DOI:10.1016/j.procs2015.04.011,April – 2015</td>
		</tr>
		<tr>
		<td width="49">12.</td>
		<td width="199">Smart Bus- An intelligent Footboard Accident Prevention System</td>
		<td width="265">National Conference on Innovative Intelligence in Computer Technology(NCIICT), 26,27March- 2015</td>
		</tr>
		<tr>
		<td width="49">13.</td>
		<td width="199">Do Do Algorithm- Evaluation of&nbsp; Do Do with AES</td>
		<td width="265">International Conference on Recent Advances and Innovation in Engineering and Technology, 20<sup>th</sup>&amp;21<sup>st</sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">14.</td>
		<td width="199">A Novel Secure Framework for IP Multimedia Subsystem,</td>
		<td width="265">Open Journal of Information Security and Applications, Volume 1, Number 2, ISSN(Print): 2374-6262 ISSN: 2374-6289 DOI: 10.15764/Isa.2014.02004, Sep-2014</td>
		</tr>
		<tr>
		<td width="49">15.</td>
		<td width="199">An Efficient Location Based Anonymous Secure Routing for Mobile Ad hoc Network</td>
		<td width="265">Open Journal of Information Security and Applications Volume 1, Number 2,Issn(Print): 2374-6262 ISSN(Online): 2374-6289, DOI: 10.15764/Isa.2014.02005, Sep-2014</td>
		</tr>
		<tr>
		<td width="49">16.</td>
		<td width="199">A Novel Hybrid Key Management Scheme for Establishing Secure Communication in Wireless Sensor Networks</td>
		<td width="265">Wireless Personal Communication, DOI : 10.1007/s11277-015-2290-9, Jan- 2015</td>
		</tr>
		<tr>
		<td width="49">17.</td>
		<td rowspan="3" width="157">Dr.P.Bala Subramanian</td>
		<td width="199">FPGA-Based Synthesis of High-Speed Hybrid Carry Select Adders</td>
		<td width="265">Hindawi Publishing Corporation Advances in Electronics,Volume 2015,Article ID 713843,13 Pages,http://dx.doi.org/10.1155/2015/713843</td>
		</tr>
		<tr>
		<td width="49">18.</td>
		<td width="199">Mathematical Analysis of Logical Masking Capability of Logic Gates</td>
		<td width="265">Recent Advances in Electrical Engineering and Electronic Devices,ISBN:978-1-61804-266-8</td>
		</tr>
		<tr>
		<td width="49">19.</td>
		<td width="199">A Standard cell Based Voter for use in&nbsp; TMR Implementation</td>
		<td width="265">Recent Advances in Electrical Engineering and Electronic Devices,ISBN:978-1-61804-266-8</td>
		</tr>
		<tr>
		<td width="49">20.</td>
		<td width="157">Dr.V.Manjula</td>
		<td width="199">Effective Data Retrieval in Disruption Tolerant Networks Using Cipher Text&nbsp; Policy-Attribute Based Encryption</td>
		<td width="265">Indian Journal of Applied Research, Volume:5 Issue 6 June 2015,ISSN-2249-555x,DOI:10.15373/2249555x,June 2015</td>
		</tr>
		<tr>
		<td width="49">21.</td>
		<td rowspan="10" width="157">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mrs.R.Geetha</td>
		<td width="199">A Novel Secure Framework for IP Multimedia Subsystem,</td>
		<td width="265">Open Journal of Information Security and Applications, Volume 1, Number 2, ISSN(Print): 2374-6262,ISSN: 2374-6289 DOI: 10.15764/Isa.2014.02004, Sep-2014</td>
		</tr>
		<tr>
		<td width="49">22.</td>
		<td width="199">An Efficient Location Based Anonymous Secure Routing for Mobile Ad hoc Network</td>
		<td width="265">Open Journal of Information Security and Applications Volume 1, Number 2,ISSN(Print): 2374-6262 ISSN: 2374-6289, DOI: 10.15764/Isa.2014.02005, Sep-2014</td>
		</tr>
		<tr>
		<td width="49">23.</td>
		<td width="199">A Novel Hybrid Key Management Scheme for Establishing Secure Communication in Wireless Sensor Networks</td>
		<td width="265">Wireless Personal Communication, DOI : 10.1007/s11277-015-2290-9, Jan- 2015</td>
		</tr>
		<tr>
		<td width="49">24.</td>
		<td width="199">Secure Communication Against Framing Attack in Wireless Sensor Network</td>
		<td width="265">International Review on Computers and Software(I.RE.CO.S)&nbsp; Vol:10,No4 ISSN 1828-6003,DOI- 10.15866, April-2015</td>
		</tr>
		<tr>
		<td width="49">25.</td>
		<td width="199">Guaranteed&nbsp; Security in Wireless Network using Improved Key Management Scheme</td>
		<td width="265">International Journal of Science and Innovative Engineering &amp; Technology, ISBN:978-81-904760-6-5,ISSUE Vol 1, May -2015</td>
		</tr>
		<tr>
		<td width="49">26.</td>
		<td width="199">Fuzzy Logic based Technique using Trust Authentication for a Secure Data Exchange in Wireless Sensor Network</td>
		<td width="265">International Journal of P2P Network Trends and technology(IJPTT) volume 18,Number 1,ISSN:2249-2615, DOI-10.14445, May-2015</td>
		</tr>
		<tr>
		<td width="49">27.</td>
		<td width="199">Implementation of Relay based Key Generation for Data Security in Wireless Environment</td>
		<td width="265">National Conference on Novel Computing&nbsp; NCNC’15,13<sup>th</sup> March- 2015</td>
		</tr>
		<tr>
		<td width="49">28.</td>
		<td width="199">Resource Conscious Secure Routing (RCS) Protocol for Wireless Sensor Networks</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">29.</td>
		<td width="199">Effectiveness of Key Management Schemes in Wireless Sensor Networks</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">30.&nbsp;&nbsp;&nbsp;&nbsp; 33</td>
		<td width="199">Secure Routing Protocol based on Dynamic Trust Management for Wireless Sensor Networks</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">31.</td>
		<td rowspan="6" width="157">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mrs.S.Veena</td>
		<td width="199">Optimization of Active Apriori Algorithm using an Effective Genetic Algorithm for Identification of Top-l Elements in a Peer to Peer Network</td>
		<td width="265">Asian Journal Applied Sciences,ISSN:2321-0893 Vol 02, Issue 05, Oct-2014</td>
		</tr>
		<tr>
		<td width="49">32.</td>
		<td width="199">Optimal Knowledge Based Top L Element Identification using Adaptive Genetic Algorithm&nbsp; in Peer to Peer Network</td>
		<td width="265">International Review on Computers and Software(IRECOS),ISSN 1828-6003, Volume 9, No. 6,Page no. 1125 – 1133, June 2014</td>
		</tr>
		<tr>
		<td width="49">33.</td>
		<td width="199">An Efficient Association Rule Mining and Novel Genetic Algorithm for Identification of Top – l Elements</td>
		<td width="265">CiiT International Journal, Vol No.6,No.4, June -2014</td>
		</tr>
		<tr>
		<td width="49">34.</td>
		<td width="199">An Efficient Method to Identify the Most Frequent Item Sets using Database Count Algorithm and Effective ACO Algorithm in a Peer to Peer Network</td>
		<td width="265">International Journal of Computer Trends and Technology (IJCTT), Volume 17 Number 6 PP:300 – 303, DOI : 10.14445/22312803/IJCTT, Nov 2014</td>
		</tr>
		<tr>
		<td width="49">35.</td>
		<td width="199">Effective Data Retrieval in Disruption Tolerant Networks using Cipher Text&nbsp; policy-Attribute Based Encryption</td>
		<td width="265">Indian Journal of Applied Research, Volume:5 Issue 6 June 2015,ISSN-2249-555x, DOI:10.15373/2249555x,June 2015</td>
		</tr>
		<tr>
		<td width="49">36.</td>
		<td width="199">Sharing Encrypted Data in Cloud Using Key Aggregate Crypto System</td>
		<td width="265">National Conference on Novel Computing&nbsp; NCNC’15,13<sup>th</sup> March- 2015</td>
		</tr>
		<tr>
		<td width="49">37.</td>
		<td rowspan="8" width="157">&nbsp; &nbsp; Mr.C.Bala Krishnan</td>
		<td width="199">Peer to Peer Data Dissemination over Disconnected MANET based on Gossip</td>
		<td width="265">International Journal of Engineering Trends and Technology (IJETT), Volume 20 Number 2, 95-100&nbsp; ISSN:2231-5381, DOI: 10.14445/22315381/IJETT-V20P217, Feb 2015</td>
		</tr>
		<tr>
		<td width="49">38.</td>
		<td width="199">XSD Technique for Web Application to Overcome Denial of Service Attack</td>
		<td width="265">Indian Journal of Applied Research Volume 5,Issue:4,ISSN -2249-555X, April – 2015</td>
		</tr>
		<tr>
		<td width="49">39.</td>
		<td width="199">Cipher Text Policy-Attribute Based Encryption for Data Retrieval in Disruption Tolerant Networks</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015)</td>
		</tr>
		<tr>
		<td width="49">40.</td>
		<td width="199">“ On – Time” Proximity Estimation Using Smartphones</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">41.</td>
		<td width="199">Web Application Tracer Against Unsolicited Request using XSD Technique</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">42.</td>
		<td width="199">Tracing Unsolicited Request in Web Application using CPR Approach</td>
		<td width="265">International Journal of Applied Engineering Research, ISSN 0973-4562 Vol.10 No.2(2015) pp.1298-1302, March- 2015</td>
		</tr>
		<tr>
		<td width="49">43.</td>
		<td width="199">Emerging Role of Distance Bounding Protocol with RFID Technology in Aerospace System</td>
		<td width="265">IJSR- International Journal of Scientific Research, Volume:4 Issue:4,&nbsp; ISSN No:2277-8179, April 2015</td>
		</tr>
		<tr>
		<td width="49">44.</td>
		<td width="199">Authentication Logic Through Turn Around Time in WSN</td>
		<td width="265">International Journal of Applied Engineering Research, ISSN : 0973-4562, Vol 10,No.2, pp.1303-1307, April 2015</td>
		</tr>
		<tr>
		<td width="49">45.</td>
		<td width="157">Mrs.N.S.Usha</td>
		<td width="199">Implementation of Speedy Emergency Alert using Tweet Analysis</td>
		<td width="265">National Conference on Novel Computing&nbsp; NCNC’15,13<sup>th</sup> March- 2015</td>
		</tr>
		<tr>
		<td width="49">46.</td>
		<td width="157">Mrs.D.Vinodha</td>
		<td width="199">A Smart ATM for Smartphones with Secure Pin Entry Methods</td>
		<td width="265">International Journal of Computer and Organization Trends (IJCOT) v20(1):10-14, ISSN:2249-2593, May -2015</td>
		</tr>
		<tr>
		<td width="49">47.</td>
		<td rowspan="2" width="157">&nbsp; &nbsp; &nbsp; Mrs.M.Preetha</td>
		<td width="199">The Grouping of Files in Allocation of Job using Server Scheduling in Load Balancing</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE) e-ISSN: 2278-0661,p-ISSN:2278-8727,volume17,Issue 5,ver VI, pp 70-75,DOI: 10.9790/0661-17367075, June 2015</td>
		</tr>
		<tr>
		<td width="49">48.</td>
		<td width="199">Fortify data Regaining in Decentralized Military Networks</td>
		<td width="265">National Conference on “Leveraging Best Practices in Management”,19<sup>th </sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">49.</td>
		<td width="157">Mrs.Joice Jessie</td>
		<td width="199">Text Detection and localization in Images.</td>
		<td width="265">National Conference on “Leveraging Best Practices in Management “,19<sup>th </sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">50.</td>
		<td rowspan="7" width="157">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mr.S.Muthu Kumarasamy</td>
		<td width="199">Visual Authentication Using QR code to Prevent Keylogging</td>
		<td width="265">International Journal of Engineering Trends and Technology (IJETT), Volume 20 Number 3, 149-154. ISSN:2231-5381 DOI: 10.14445/22315381/IJETT-V20P227, Feb -2015</td>
		</tr>
		<tr>
		<td width="49">51.</td>
		<td width="199">Secured Authentication for Aadhaar Card Through Sensory Information Using Mobile Phone</td>
		<td width="265">International Journal of Scientific Research. (IJSR),Volume :4; Issue :4 March 2015; ISSN No 2277 – 8179 DOI: 10.15373/22778179, April- 2015</td>
		</tr>
		<tr>
		<td width="49">52.</td>
		<td width="199">Corporate Policy Governance in Secure MD5 Data Changes and Multihand Administration</td>
		<td width="265">IOSR Journal of Computer Engineering(IOSR-JCE) e-ISSN:2278-0661,p-ISSN 2278-8727 , Volume 17, Issue 1, Ver IV,PP 18-23 DOI: 10.9790/0661-17141823 ANED-DDL 11.0661/iosr-jce-D017141823, Jan-Feb 2015</td>
		</tr>
		<tr>
		<td width="49">53.</td>
		<td width="199">QR-Based TOTP Authentication in E-Banking</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">54.</td>
		<td width="199">Dual Authenticated Smart Card</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">55.</td>
		<td width="199">A Self Robust SAR Scheduling for Evaluating an Approach for File Sharing in Load Balancing</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">56.</td>
		<td width="199">Probabilistic Mischief Detection Scheme in Delay Tolerant Networks</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">57.</td>
		<td rowspan="2" width="157">&nbsp; Ms.J.Sangeetha</td>
		<td width="199">A Web Extraction Using Soft Algorithm for Trinity Structure</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE)e-ISSN: 2278-0661, p-ISSN: 2278-8727, Volume 17, Issue 3, Ver. III, PP 42-48, May-Jun 2015</td>
		</tr>
		<tr>
		<td width="49">58.</td>
		<td width="199">Shared Authority Based Privacy-Pre-Serving Authentication Protocol in Cloud Computing</td>
		<td width="265">National Conference on “Leveraging Best Practices in Management”,19<sup>th</sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">59.</td>
		<td rowspan="2" width="157">Mrs.Renuka Devi</td>
		<td width="199">Captcha as Graphical Passwords using Multiple Images</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">60.</td>
		<td width="199">A Web Extraction Using Soft Algorithm for Trinity Structure</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE)e-ISSN: 2278-0661, p-ISSN: 2278-8727, Volume 17, Issue 3, Ver. III, PP 42-48, May-Jun 2015</td>
		</tr>
		<tr>
		<td width="49">61.</td>
		<td rowspan="6" width="157">&nbsp; &nbsp; &nbsp; Mr.H.Anwar Basha</td>
		<td width="199">An Efficient Traffic Delivery in Adhoc Network – A Survey</td>
		<td width="265">International Conference on Innovative Engineering Technologies (ICIET’ 2014), Dec 28-29, 2014 (Thailand), Dec-2014</td>
		</tr>
		<tr>
		<td width="49">62.</td>
		<td width="199">An Efficient Traffic Delivery in Adhoc Network – A Survey</td>
		<td width="265">International Journal of Computing &amp; Instrumentation Engineering(IJCCIE) Vol 1,Issue 1 ISSN:2349-1469, e-ISSN:2349-1477 http://dx.doi.org/10.15242/IJCCIE.E1214005, Dec- 2014</td>
		</tr>
		<tr>
		<td width="49">63.</td>
		<td width="199">A New Approach towards Biometric Authentication System using Face Vein</td>
		<td width="265">National Conference on Computing Technologies Todays and&nbsp; Beyond (NCCTTB’15),ISBN 978-93-84743-54-3 2015 Bonfring,4<sup>th</sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">64.</td>
		<td width="199">Privacy Preserving Back Propagation Neural Network Learning using Cloud Computing</td>
		<td width="265">International Journal of Technical Search and Application, e-ISSN:2320-8163,Volume 3. Issue 2,&nbsp; PP-58-61, Mar-April 2015</td>
		</tr>
		<tr>
		<td width="49">65.</td>
		<td width="199">Back-Propagation Neural Network Learning with Preserved Privacy using Cloud Computing</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE) e-ISSN:2278-0661, p-ISSN:2278-8727, Volume 17 , Issue 2, Ver.1, PP-75-79, MAR-APR 2015</td>
		</tr>
		<tr>
		<td width="49">66.</td>
		<td width="199">Back Propagation Neural Network Learning Using Cloud Computing</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> Mar-2015</td>
		</tr>
		<tr>
		<td width="49">67.</td>
		<td rowspan="3" width="157">&nbsp; &nbsp; &nbsp; &nbsp; Mrs.S.Sajini</td>
		<td width="199">A Web Extraction Using Soft Algorithm for Trinity Structure</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE)e-ISSN: 2278-0661, p-ISSN: 2278-8727, Volume 17, Issue 3, Ver. III, PP 42-48, May-Jun 2015</td>
		</tr>
		<tr>
		<td width="49">68.</td>
		<td width="199">Bus Arrival Time Predication based on Cell Tower Sequence Information Shared by Participatory Users</td>
		<td width="265">National Conference on “Security Issues and Challenges in SMART Computing (SICSC 2015)”,20<sup>th</sup> May 2015</td>
		</tr>
		<tr>
		<td width="49">69.</td>
		<td width="199">Bus Arrival Time Predication based on Cell Tower Sequence Information Shared by Participatory Users</td>
		<td width="265">International Journal of Applied Engineering research,ISSN:0973-4562Vol 10 No56(2015), May 2015</td>
		</tr>
		<tr>
		<td width="49">70.</td>
		<td width="157">Mrs.V.Sureka</td>
		<td width="199">A Smart ATM for Smartphones with Secure Pin Entry Methods</td>
		<td width="265">International Journal of Computer and Organization Trends (IJCOT) v20(1):10-14 ISSN:2249-2593, May -2015</td>
		</tr>
		<tr>
		<td width="49">71.</td>
		<td width="157">&nbsp; Mrs.S.Jonisha</td>
		<td width="199">A Smart ATM for Smartphones with Secure Pin Entry Methods</td>
		<td width="265">International Journal of Computer and Organization Trends (IJCOT) v20(1):10-14 ISSN:2249-2593, May -2015</td>
		</tr>
		<tr>
		<td width="49">72.</td>
		<td width="157">Ms.K.Jayanthi</td>
		<td width="199">A Smart ATM for Smartphones with Secure Pin Entry Methods</td>
		<td width="265">International Journal of Computer and Organization Trends (IJCOT) v20(1):10-14 ISSN:2249-2593, May -2015</td>
		</tr>
		<tr>
		<td width="49">73.</td>
		<td rowspan="2" width="157">&nbsp; &nbsp; Mrs.K.Elavarasi</td>
		<td width="199">The Grouping of Files in Allocation of Job using Server Scheduling in Load Balancing</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE) e-ISSN: 2278-0661,p-ISSN:2278-8727,volume17,Issue 5,ver VI, pp 70-75,DOI: 10.9790/0661-17367075, June 2015</td>
		</tr>
		<tr>
		<td width="49">74.</td>
		<td width="199">Online Signature Verification for User Identification</td>
		<td width="265">National Conference on “Leveraging Best Practices in Management”,19<sup>th </sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">75.</td>
		<td width="157">&nbsp; Ms.NRG&nbsp; Sreevani</td>
		<td width="199">A Web Extraction Using Soft Algorithm for Trinity Structure</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE)e-ISSN: 2278-0661, p-ISSN: 2278-8727, Volume 17, Issue 3, Ver. III, PP 42-48, May-Jun 2015</td>
		</tr>
		<tr>
		<td width="49">76.</td>
		<td rowspan="2" width="157">Mr.T.Vignesh</td>
		<td width="199">Local Binary Pattern Texture Feature for Satellite Imagery Classification</td>
		<td width="265">International Conference on Science, Engineering and Management Research, (ICSEMER 2014) Nov 27-29, 2014, Nov-2014</td>
		</tr>
		<tr>
		<td width="49">77.&nbsp;&nbsp;&nbsp;&nbsp; 66</td>
		<td width="199">Personalized Search and Recommendation for Movies in Relational Database</td>
		<td width="265">National Conference on Novel Computing&nbsp; NCNC’15,13<sup>th </sup>March- 2015</td>
		</tr>
		<tr>
		<td width="49">78.</td>
		<td width="157">Mr.M.Balasubramanian</td>
		<td width="199">Personalized Q&amp;A System Based on Social Networks.</td>
		<td width="265">National Conference on Emerging Trends in Computing(ETIC 2015),6<sup>th</sup> March- 2015</td>
		</tr>
		<tr>
		<td width="49">79.</td>
		<td rowspan="2" width="157">Ms.Ramyadevi</td>
		<td width="199">The Grouping of Files in Allocation of Job using Server Scheduling in Load Balancing</td>
		<td width="265">IOSR Journal of Computer Engineering (IOSR-JCE) e-ISSN: 2278-0661,p-ISSN:2278-8727,volume17,Issue 5,ver VI, pp 70-75, DOI: 10.9790/0661-17367075, June 2015</td>
		</tr>
		<tr>
		<td width="49">80.</td>
		<td width="199">Cash Payment Voucher</td>
		<td width="265">National Conference on Novel Computing&nbsp; NCNC’15,13<sup>th </sup>March- 2015</td>
		</tr>
		</tbody>
		</table>
		<p><strong><u>2013-2014</u></strong></p>
		<table style="height: 4308px;" width="692">
		<tbody>
		<tr>
		<td width="45"><strong>Sl. No</strong></td>
		<td width="142"><strong>Name of the Faculty</strong></td>
		<td width="217"><strong>Title of the Paper</strong></td>
		<td width="246"><strong>Paper Details</strong></td>
		</tr>
		<tr>
		<td width="45">1.</td>
		<td rowspan="3" width="142">Dr.G.Umarani Srikanth</td>
		<td width="217">Load Balancing and Coordination of Web Services in Cloud</td>
		<td width="246">International Journal of Advanced Research in Computer Science &amp; Technology (IJARCST 2014), Vol 2, Issue Special Jan-March 2014</td>
		</tr>
		<tr>
		<td width="45">2.</td>
		<td width="217">Enhancing Data Privacy and Efficiency for Secure Cloud Storage Model</td>
		<td width="246">International Journal of Advanced Research in Compute Engineering &amp; Technology (IJARCET)</td>
		</tr>
		<tr>
		<td width="45">3.</td>
		<td width="217">A Partial Inference Control Based Data Disclosure</td>
		<td width="246">International Conference on Information Communication and Embedded Systems(ICICES 2014), Feb 2014</td>
		</tr>
		<tr>
		<td width="45">4.</td>
		<td rowspan="4" width="142">Dr.E.A.Mary Anita</td>
		<td width="217">Confronts and Applications in Marine Sensor Networks</td>
		<td width="246">International Journal of Computer Science and Mobile Computing,, May 2014, Vol.3 Issue.5, 251-256, May 2014</td>
		</tr>
		<tr>
		<td width="45">5.</td>
		<td width="217">A New Compact Micro strip Integrated E-Array Patch Antenna with High Gain and High Aperture Efficiency</td>
		<td width="246">International Journal of Wireless Personal Communication-Springer Science, April 2014, DOI 10.1007/s11277-014-1798-8</td>
		</tr>
		<tr>
		<td width="45">6.</td>
		<td width="217">Performance Analysis of Routing Protocols for Wireless Sensor Networks for Disaster Management,</td>
		<td width="246">International Journal of Computer Science and Engineering Communications, Vol 2, Issue.106-112, Feb 2014</td>
		</tr>
		<tr>
		<td width="45">7.</td>
		<td width="217">A Wireless Sensor Network Architecture for Forest Fire Detection</td>
		<td width="246">International Journal of Computer Applications in Engineering Sciences, Vol.III, Issue.III, 105-107, Sep 2013,</td>
		</tr>
		<tr>
		<td width="45">8.</td>
		<td rowspan="3" width="142">Dr.P.Bala Subramanian</td>
		<td width="217">FPGA based Synthesis of Hybrid Carry Select Adders</td>
		<td width="246">International Conference on Advances in Computer Science and Information Technology (ACSIT2K14), 28-29<sup>th</sup>March 2014</td>
		</tr>
		<tr>
		<td width="45">9.</td>
		<td width="217">FPGA&nbsp; Implementation of Synchronous Section-Carry based Look Ahead Adders</td>
		<td width="246">International Conference on Devices, Circuits and Systems(ICDCS,14), 6-8<sup>th</sup>March 2014,260-263</td>
		</tr>
		<tr>
		<td width="45">10.</td>
		<td width="217">Computation of Error Resiliency of Muller C-element</td>
		<td width="246">International Conference on Computational Science and Computational Intelligence (CSCI 2014), March&nbsp; 2014</td>
		</tr>
		<tr>
		<td width="45">11.</td>
		<td rowspan="2" width="142">Mrs.R.Geetha</td>
		<td width="217">Fuzzy Logic Based Compromised Node Detection and Revocation in Clustered Wireless Sensor Networks</td>
		<td width="246">4<sup>th</sup> IEEE International&nbsp; Conference on Information Communication and Embedded Systems(ICICES 2014), 27-28<sup>th</sup> Feb 2014</td>
		</tr>
		<tr>
		<td width="45">12.</td>
		<td width="217">SEBAR: Secure Energy Based Ant Routing Algorithm for Wireless Sensor Networks</td>
		<td width="246">16<sup>th</sup> International Conference on Computing, Networking and Communications(ICCNC 2014), 21-22<sup>nd</sup>Feb 2014</td>
		</tr>
		<tr>
		<td width="45">13.</td>
		<td width="142">Mrs.S.Veena</td>
		<td width="217">A Technique with Differentiated Sampling in Anomaly Detection System for Outlier Identification</td>
		<td width="246">International Journal of Research in Commerce, IT&amp; Management ,&nbsp; Nov 2013</td>
		</tr>
		<tr>
		<td width="45">14.</td>
		<td rowspan="9" width="142">&nbsp; &nbsp; &nbsp; &nbsp; Mr.C.Bala Krishnan</td>
		<td width="217">SDRP and SSO Mechanism for Impersonation Attack in Wireless Sensor Networks</td>
		<td width="246">International Journal of Advanced Research in Computer Science &amp; Technology (IJARSCT 2014), 2347-9817, Volume 2, Issue Special 1 Jan- March 2014</td>
		</tr>
		<tr>
		<td width="45">15.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">SDRP and SSO Mechanism for Impersonation Attack in Wireless Sensor Networks</td>
		<td width="246">AICTE Sponsored International Conference&nbsp; Hi-Tech Trends In Emerging Computational Technologies,20,21-Feb 2014</td>
		</tr>
		<tr>
		<td width="45">16.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Smart Tracking of Human Location&nbsp; and Events based on WPS using Android Technology</td>
		<td width="246">International Journal of Computer Science and Engineering (IJCSE) 2347-2693, Volume 2, Issue 1 31/01/2014, E-ISSN: 2347-2693,Jan 2014</td>
		</tr>
		<tr>
		<td width="45">17.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">An Efficient Tracking of Human Mobility&nbsp; and Events based on WPS using Android Technology</td>
		<td width="246">International Conference on Advanced Computer Science &amp; Information Technology(ICACSIT) held at Chennai on 20<sup>th</sup> Oct 2013 (ICACSIT -2013)</td>
		</tr>
		<tr>
		<td width="45">18.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Efficient in Revoking Certificates of Malicious Nodes in MANET</td>
		<td width="246">International Conference on Advanced Computer Science &amp; Information Technology(ICACSIT) held at Chennai on 20<sup>th</sup> Oct 2013 (ICACSIT -2013)</td>
		</tr>
		<tr>
		<td width="45">19.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Efficient in Revoking Certificates of Malicious nodes in MANET</td>
		<td width="246">International Journal of Advanced Computational Engineering and Networking, 2320-2106, Volume1,Issue 9,&nbsp; Nov -2013</td>
		</tr>
		<tr>
		<td width="45">20.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Improving Node Cooperation for Packet Delivery in Multi Hop Wireless Network</td>
		<td width="246">International Journal of Scientific in Computer Science Application and Management Studies (IJSRSAMS), 2319-1953,Volume 3, Issue 2, March 2014</td>
		</tr>
		<tr>
		<td width="45">21.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Cluster Based Certificate Revocation of Attacker’s Nodes in MANET</td>
		<td width="246">International Journal of Computer Science and Engineering Volume 2,Issue 1 E-ISSN: 2347-2693</td>
		</tr>
		<tr>
		<td width="45">22.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Power Draining Prevention in Ad-Hoc Sensor networks using Sensor Network Encryption Protocol</td>
		<td width="246">ICICES.2014.7033971 Print ISBN:978-1-4799-3835-3 INSPEC Accession Number:14915905 DOI:10.1109/Publisher : IEEE</td>
		</tr>
		<tr>
		<td width="45">23.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="142">Mr.S.Prabhu</td>
		<td width="217">Enhanced Techniques to strengthening DTN against Flood Attacks</td>
		<td width="246">International Conference on Information Communication and Embedded Systems(ICICES 2014)</td>
		</tr>
		<tr>
		<td width="45">24.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td rowspan="2" width="142">Mr.S.Muthu Kumarasamy</td>
		<td width="217">Efficient Video Transmission for Wireless Communication Networks using ZIGBEE Protocol</td>
		<td width="246">International Conference on Advances in Computer Science and Information Technology(ACSIT’14),28,29-Mar 2014</td>
		</tr>
		<tr>
		<td width="45">25.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Efficient Video Transmission for Wireless Communication Networks using ZIGBEE Protocol</td>
		<td width="246">International Journal of Research in Computer&nbsp; Application and Management&nbsp; (IJRCAM), ISSN 2231-1009, Volume No , (2013), Issue No. 11, November 2013</td>
		</tr>
		<tr>
		<td width="45">26.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="142">Ms.J.Sangeetha</td>
		<td width="217">E-Voting System using Android Application</td>
		<td width="246">International Journal of Research in Engineering &amp; Advanced Technology, Vol2 I 2, 2014, April-May 2014</td>
		</tr>
		<tr>
		<td width="45">27.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td rowspan="2" width="142">Mrs.K.Kowsalya Devi</td>
		<td width="217">Green Computing: Eco-Friendly Computing Resources</td>
		<td width="246">International Journal of Advanced Research in Computer Science,&nbsp; Aug 2013</td>
		</tr>
		<tr>
		<td width="45">28.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Secure Retrieval over Encrypted Cloud Data</td>
		<td width="246">National Conference on Challenges and Issues in Computing and Technology, 2013</td>
		</tr>
		<tr>
		<td width="45">29.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td rowspan="2" width="142">Mrs.M.Preetha</td>
		<td width="217">COA-LEACH-An Efficient Energy Postulate Based on Energy Cost Modeling in Wireless Sensor Networks</td>
		<td width="246">JOKULL Aug 2013</td>
		</tr>
		<tr>
		<td width="45">30.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">C-MAC: An Efficient Energy Postulate Based on Energy Cost Modeling in Wireless Sensor Network</td>
		<td width="246">Asian Journal of Information Technology 12(6):176-183,2013 ISSN:1682-3915, Dec 2013</td>
		</tr>
		<tr>
		<td width="45">31.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="142">Mr.H.Anwar Basha</td>
		<td width="217">Load Rebalancing in Cloud for Distributed File Transfer</td>
		<td width="246">National Conference on Advanced Computing Technology, 2014</td>
		</tr>
		<tr>
		<td width="45">32.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="142">Mrs.S.Sajini</td>
		<td width="217">A Priority and Fair Connection based Opt Packet Scheduling in IEEE 802.11 based Mesh Topology</td>
		<td width="246">International Journal of Research in Engineering and Advanced Technology (IJREAT), Dec 2013</td>
		</tr>
		<tr>
		<td width="45">33.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td rowspan="2" width="142">Mrs.V.Sureka</td>
		<td width="217">QKD Technology for High Performance Computing and Wireless Network</td>
		<td width="246">International Journal of Research and Advanced Technology, Vol 1 Issue 6, Jan 2014</td>
		</tr>
		<tr>
		<td width="45">34.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Non Path based Mutual Anonymity Protocol for Decentralized&nbsp; P2P Systems</td>
		<td width="246">International Journal of Research in Engineering and Advanced&nbsp; Technology,&nbsp; Vol 1,Issue 6, JAN 2014</td>
		</tr>
		<tr>
		<td width="45">35.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td rowspan="2" width="142">Ms.S.Jonisha</td>
		<td width="217">QKD Technology for High Performance Computing and Wireless Network</td>
		<td width="246">International Journal of Research and Advanced Technology, Vol 1 Issue 6, Jan 2014</td>
		</tr>
		<tr>
		<td width="45">36.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="217">Non Path based Mutual Anonymity Protocol for Decentralized&nbsp; P2P Systems</td>
		<td width="246">International Journal of Research in Engineering and Advanced&nbsp; Technology,&nbsp; Vol 1,Issue 6, Jan 2014</td>
		</tr>
		<tr>
		<td width="45">37.&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;</strong></td>
		<td width="142">Ms.K.Jayanthi</td>
		<td width="217">Data Consistency for Co-Operative Caching in Mobile Networks</td>
		<td width="246">National Conference on VLSI, Image Processing and Networking, April 2014</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div>
		<h4><strong><u>2012-2013</u></strong></h4>
		<table style="height: 3398px;" width="680">
		<tbody>
		<tr>
		<td><strong>S.No.</strong></td>
		<td><strong>Name of the Faculty</strong></td>
		<td><strong>Title of the Paper</strong></td>
		<td><strong>Paper Details</strong></td>
		</tr>
		<tr>
		<td>1.</td>
		<td>Mrs.Jebarani Sargunar</td>
		<td>An Efficient Processing of Regular Register Prone to Continuous Change</td>
		<td>IJREAT International Journal of Research in Engineering &amp; Advanced Technology, Volume 1, Issue 1, March, 13 ISSN: 2320 – 8791</td>
		</tr>
		<tr>
		<td>2.</td>
		<td>Dr.E.A.Mary Anita</td>
		<td>A Wireless Sensor Network Architecture for Forest Fire Detection</td>
		<td>International Journal of Computer Applications in Engineering Sciences, Volume III, Issue III, Sep 2013, ISSN:2231-4946.</td>
		</tr>
		<tr>
		<td>3.</td>
		<td rowspan="3">Mrs.G.Umarani Srikanth</td>
		<td>Scheduling of Real Time Task using Ant Colony Optimization</td>
		<td>International Journal of Soft Computing, Vol.8, Issue No.1, pp.50-55, 2013.</td>
		</tr>
		<tr>
		<td>4.</td>
		<td>Beehive Backbone Construction with Clone Attacks In Wireless Sensor Network</td>
		<td>International Journal of Societal Applications of Computer Science, , Vol. 2, Issue No.2, 2013, Feb 2013</td>
		</tr>
		<tr>
		<td>5.</td>
		<td>Ant Colony optimization in Diverse engineering application: An Overview</td>
		<td>International Journal of Computer Application, July 2012</td>
		</tr>
		<tr>
		<td>6.</td>
		<td rowspan="2">Mrs.R.Geetha</td>
		<td>Ant colony optimization in diverse engineering application: An overview</td>
		<td>International Journal of Computer Application, July 2012</td>
		</tr>
		<tr>
		<td>7.</td>
		<td>TARF-Prevention from Adversaries of Multi-hop Routing in WSN</td>
		<td>CiiT International Journal of Wireless Communication,&nbsp; 2012</td>
		</tr>
		<tr>
		<td>8.</td>
		<td>Mrs.E.Sujatha</td>
		<td>Detection of Visual Impairments using Back Propagation Neural Network</td>
		<td>International Journal of Computer Science and Engineering, , Vol. 4, No.3, Mar 2013</td>
		</tr>
		<tr>
		<td>9.</td>
		<td rowspan="5">&nbsp; &nbsp; &nbsp; &nbsp; Mr.C.Bala Krishnan</td>
		<td>Resisting Password based Systems from Online Guessing Attacks</td>
		<td>International Journal of Emerging Technology and Advanced Engineering ,&nbsp; Volume 3 Special Issue1, Jan 2013</td>
		</tr>
		<tr>
		<td>10.</td>
		<td>Embedding of Executable File in Encrypted Image using LSB Mechanism</td>
		<td>International Journal of Emerging Technology and Advanced Engineering, Volume 3 Special Issue1, Jan 2013</td>
		</tr>
		<tr>
		<td>11.</td>
		<td>CAPTCHA: A Defensive Mechanism against Attacks</td>
		<td>International Journal of Engineering Associates(IJEA) ,Vol. 1, Issue4, ISSN 2320-0804</td>
		</tr>
		<tr>
		<td>12.</td>
		<td>Misbehaving Node Detection and Renovation Scheme in Disruption Tolerant Network</td>
		<td>International Journal of Engineering Associates(IJEA) , Vol. 1 Issue 4, February 2013, ISSN: 2320-0804</td>
		</tr>
		<tr>
		<td>13.</td>
		<td>Detecting Selfish Routing and Misbehavior of Malicious Node in Disruption Tolerant Networks</td>
		<td>International Journal of Emerging Technology and Advanced Engineering, Volume 3,Special Issue 1, January 2013</td>
		</tr>
		<tr>
		<td>14.</td>
		<td>Mrs.N.S.Usha</td>
		<td>An Effective Geo cache collection in mobile networks using boomerang protocol</td>
		<td>International Journal of emerging technology and advanced engineering, 2012</td>
		</tr>
		<tr>
		<td>15.</td>
		<td>Mr.A.Mani</td>
		<td>Proactive Moderation System for Auction Fraud Detection using Linear Coding Function and Selective Labeling</td>
		<td>IT Research International Conference on Advanced Computer Science And Information Technology, ICACSIT, 10<sup>th</sup> March 2013</td>
		</tr>
		<tr>
		<td>16.</td>
		<td rowspan="7">Mr.S.Muthu Kumarasamy</td>
		<td>Stop Password Hacking Attacks by using Opass Prototype</td>
		<td>International Journal of Computer Science and Management Research in vol II, Issue II ISSN 2278-733x</td>
		</tr>
		<tr>
		<td>17.</td>
		<td>Identity Theft Detection &amp; Prevention via Data Mining and Threshold Transaction</td>
		<td>International Conference on ICCSCM/ICIIE/ICMMME-2013,, Feb 2013</td>
		</tr>
		<tr>
		<td>18.</td>
		<td>Increasing the Throughput of Wi-Fi using Sleep well</td>
		<td>2<sup>nd</sup> International Conference on innovative Research in Engineering and Technology 2013 ICRET 2013</td>
		</tr>
		<tr>
		<td>19.</td>
		<td>Detection of Misbehaving Packet Droppers and Modifies in Wireless networks Using an Adaptive Protocol in IJARCCE</td>
		<td>International Journal of Advanced Research in Computer and Communication Engineering, Vol2, Issue 2, Feb 2013</td>
		</tr>
		<tr>
		<td>20.</td>
		<td>To Alleviate Distributed DoS Attacks in Allied Applications with Clock Drifts using DPH</td>
		<td>International Journal of Engineering Association, Vol 1, Issue 3, Jan 2013</td>
		</tr>
		<tr>
		<td>21.</td>
		<td>Traffic Avoidance: A Wi-Fi Management using Beacons</td>
		<td>International Journal of Societal Applications of Computer Science, Vol II, Issue III , March 2013</td>
		</tr>
		<tr>
		<td>22.</td>
		<td>To Create a User Secure Environment by using Android Mobiles</td>
		<td>International Journal of Computational Linguistics and NLP, IJCLNLP March 2013, in Vol II and Issue III</td>
		</tr>
		<tr>
		<td>23.</td>
		<td>Mr.M.Radha</td>
		<td>Image Authentication Mechanism</td>
		<td>National Conference on Advanced Computing Technologies and Applications, 2012</td>
		</tr>
		<tr>
		<td>24.</td>
		<td>Mr.H.Anwar Basha</td>
		<td>Detection of Visual Impairments using Back Propagation Neural Network</td>
		<td>International Journal of Computer Science and Engineering, , Vol. 4, No.3, Mar 2013</td>
		</tr>
		<tr>
		<td>25.</td>
		<td rowspan="2">Mrs.K.Kowsalya Devi</td>
		<td>A Survey on Security and Privacy in Cloud Computing</td>
		<td>International Journal of Engineering Research and Technology, 2012</td>
		</tr>
		<tr>
		<td>26.</td>
		<td>Two Step Share Synthesized Image Stamper Algorithm for Secure Visual Sharing</td>
		<td>National Conference on Advances in Computing and Technology, 2012</td>
		</tr>
		<tr>
		<td>27.</td>
		<td>Mr.S.Udhaya Kumar</td>
		<td>Detection of Visual Impairments using Back Propagation Neural Network</td>
		<td>International Journal of Computer Science and Engineering, , Vol. 4, No.3, Mar 2013</td>
		</tr>
		</tbody>
		</table>
		</div>
		
		
		<div>
		<h3 class="mmtl-heading mmtl-space-medium">UNIVERSITY RANK HOLDERS</h3>
		<h4>2015-2016&nbsp; M.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/JENEFA.jpg" data-fancybox-group="univ rank holder1" title="JENEFA.J, RANK 23">
                                    <img src="<?php echo $siteurl ?>/img/JENEFA.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/CHARANYA.jpg" data-fancybox-group="univ rank holder1" title="T.CHARANYA, RANK 42">
                                    <img src="<?php echo $siteurl ?>/img/CHARANYA.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								
					</div>
					</div>
					
					<div>
					<h4>2015-2016&nbsp; M.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/MOUNICA.C.jpg" data-fancybox-group="univ rank holder2" title="MOUNICA.C, RANK 30">
                                    <img src="<?php echo $siteurl ?>/img/MOUNICA.C.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/ANJALI-PANDEY.jpg" data-fancybox-group="univ rank holder2" title="T.ANJALI-PANDEY, RANK 36">
                                    <img src="<?php echo $siteurl ?>/img/ANJALI-PANDEY.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
					</div>
					</div>
					<div>
					<h4>M.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/KAVITHA.C.jpg" data-fancybox-group="univ rank holder3" title="KAVITHA.C, RANK 32">
                                    <img src="<?php echo $siteurl ?>/img/KAVITHA.C.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/INDUJA-PRIYADHARSHINI.jpg" data-fancybox-group="univ rank holder3" title="INDUJA-PRIYADHARSHINI, RANK 48">
                                    <img src="<?php echo $siteurl ?>/img/INDUJA-PRIYADHARSHINI.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/VALLI.N.jpg" data-fancybox-group="univ rank holder3" title="VALLI.N, RANK 50">
                                    <img src="<?php echo $siteurl ?>/img/VALLI.N.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
					</div>
		</div>
		<div>
					<h4>2015-2016&nbsp; M.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/B.SARANYA.jpg" data-fancybox-group="univ rank holder4" title="B.SARANYA, RANK 9">
                                    <img src="<?php echo $siteurl ?>/img/B.SARANYA.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/V.B.SWETHA.jpg" data-fancybox-group="univ rank holder4" title="V.B.SWETHA, RANK 20">
                                    <img src="<?php echo $siteurl ?>/img/V.B.SWETHA.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SIMLA-MERCY.jpg" data-fancybox-group="univ rank holder4" title="SIMLA-MERCY, RANK 27">
                                    <img src="<?php echo $siteurl ?>/img/SIMLA-MERCY.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/C.SARANYA.jpg" data-fancybox-group="univ rank holder4" title="C.SARANYA, RANK 33">
                                    <img src="<?php echo $siteurl ?>/img/C.SARANYA.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/PRIYADHARSHINI.M.R.jpg" data-fancybox-group="univ rank holder4" title="PRIYADHARSHINI.M.R, RANK 34">
                                    <img src="<?php echo $siteurl ?>/img/PRIYADHARSHINI.M.R.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SUBHA PONMANI.jpg" data-fancybox-group="univ rank holder4" title="SUBHA PONMANI, RANK 36">
                                    <img src="<?php echo $siteurl ?>/img/SUBHA PONMANI.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/PAVITHRA.T.jpg" data-fancybox-group="univ rank holder4" title="SUBHA PONMANI, RANK 38">
                                    <img src="<?php echo $siteurl ?>/img/PAVITHRA.T.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/PADMAVATHY.P.jpg" data-fancybox-group="univ rank holder4" title="PADMAVATHY.P, RANK 41">
                                    <img src="<?php echo $siteurl ?>/img/PADMAVATHY.P.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/MEENAKSHI.jpg" data-fancybox-group="univ rank holder4" title="MEENAKSHI.S., RANK 45">
                                    <img src="<?php echo $siteurl ?>/img/MEENAKSHI.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/NEENA.E.K.jpg" data-fancybox-group="univ rank holder4" title="NEENA.E.K, RANK 50">
                                    <img src="<?php echo $siteurl ?>/img/NEENA.E.K.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
					</div>
		</div>
		<div>
		
					<h4>2012-2013-2016&nbsp; B.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SRIDEVI.R.jpg" data-fancybox-group="univ rank holder5" title="SRIDEVI.R, RANK 34">
                                    <img src="<?php echo $siteurl ?>/img/SRIDEVI.R.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/REKHA.V.jpg" data-fancybox-group="univ rank holder5" title="REKHA.V, RANK 46">
                                    <img src="<?php echo $siteurl ?>/img/REKHA.V.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
		</div>
	</div>	
	<div>
		<h4>M.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/ARSHEY.jpg" data-fancybox-group="univ rank holder6" title="ARSHEY M., RANK 5">
                                    <img src="<?php echo $siteurl ?>/img/ARSHEY.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SUDHA.L.jpg" data-fancybox-group="univ rank holder6" title="SUDHA.L, RANK 16">
                                    <img src="<?php echo $siteurl ?>/img/SUDHA.L.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/JEYAVASANTHI-MABEL.J.jpg" data-fancybox-group="univ rank holder6" title="JEYAVASANTHI MABEL J, RANK 26">
                                    <img src="<?php echo $siteurl ?>/img/JEYAVASANTHI-MABEL.J.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/GAYATHRI.jpg" data-fancybox-group="univ rank holder6" title="GAYATHRI S, RANK 28">
                                    <img src="<?php echo $siteurl ?>/img/GAYATHRI.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/PREETHY-JEMIMA-P.jpg" data-fancybox-group="univ rank holder6" title="PREETHY JEMIMA P, RANK 31">
                                    <img src="<?php echo $siteurl ?>/img/PREETHY-JEMIMA-P.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/AARTHY.D.K.jpg" data-fancybox-group="univ rank holder6" title="AARTHY.D.K, RANK 39">
                                    <img src="<?php echo $siteurl ?>/img/AARTHY.D.K.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/HEMA.M.jpg" data-fancybox-group="univ rank holder6" title="HEMA M.A., RANK 45">
                                    <img src="<?php echo $siteurl ?>/img/HEMA.M.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
					</div>
	</div>
	<div>
		<h4>2011-2012&nbsp;B.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SHARMILA.R.jpg" data-fancybox-group="univ rank holder7" title="SHARMILA.R., RANK 13">
                                    <img src="<?php echo $siteurl ?>/img/SHARMILA.R.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/LAKSHMI.P.jpg" data-fancybox-group="univ rank holder7" title="LAKSHMI.P, RANK 32">
                                    <img src="<?php echo $siteurl ?>/img/LAKSHMI.P.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/ANUPRIYA.U.jpg" data-fancybox-group="univ rank holder7" title="ANUPRIYA.U, RANK 42">
                                    <img src="<?php echo $siteurl ?>/img/ANUPRIYA.U.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/ABIRAMI.E.jpg" data-fancybox-group="univ rank holder7" title="ABIRAMI.E, RANK 43">
                                    <img src="<?php echo $siteurl ?>/img/ABIRAMI.E.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SHAILAJA.K.jpg" data-fancybox-group="univ rank holder7" title="SHAILAJA.K, RANK 45">
                                    <img src="<?php echo $siteurl ?>/img/SHAILAJA.K.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/DHIVYA.D.jpg" data-fancybox-group="univ rank holder7" title="DHIVYA.D, RANK 46">
                                    <img src="<?php echo $siteurl ?>/img/DHIVYA.D.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								
					</div>
	</div>
	<div>
		<h4>M.E (CSE)</h4>
					<div class="ach_sec clearfix">
                                <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/ANITHA.jpg" data-fancybox-group="univ rank holder8" title="ANITHA.S., RANK 2">
                                    <img src="<?php echo $siteurl ?>/img/ANITHA.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/VIJAYALKSHMI.R.jpg" data-fancybox-group="univ rank holder8" title="VIJAYALKSHMI.R, RANK 17">
                                    <img src="<?php echo $siteurl ?>/img/VIJAYALKSHMI.R.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/NITHYA.M.R.jpg" data-fancybox-group="univ rank holder8" title="NITHYA.M.R, RANK 22">
                                    <img src="<?php echo $siteurl ?>/img/NITHYA.M.R.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/AKILANDESWARI.M.jpg" data-fancybox-group="univ rank holder8" title="AKILANDESWARI.M, RANK 22">
                                    <img src="<?php echo $siteurl ?>/img/AKILANDESWARI.M.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/NITHYA.J.jpg" data-fancybox-group="univ rank holder8" title="NITHYA.J, RANK 24">
                                    <img src="<?php echo $siteurl ?>/img/NITHYA.J.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/SUDHA.M.jpg" data-fancybox-group="univ rank holder8" title="SUDHA.M, RANK 27">
                                    <img src="<?php echo $siteurl ?>/img/SUDHA.M.jpg" class="img-responsive">
                                    </a>
                                </div>
                                </div>
								
					</div>
			</div>
		</div>

    <div class="tab-content fade" id="menu3">
		

		<div class="clearfix"></div>            
		<div>
			<h4><strong>HEAD OF DEPARTMENT &amp; PROFESSOR</strong></h4>


			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/geetha.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/geetha.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Dr.R.Geetha</p>
					<p>Network Security, Wireless Sensor Networks, Data Mining</p>
					<p>geetha@saec.ac.in</p>

					<a href="https://orcid.org/my-orcid" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?hl=en&user=2ywSrSkAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Geetha_Rg" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
		</div>

		
		<div class="clearfix"></div>            
		<div>
			<h4><strong>PROFESSORS</strong></h4>


			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/mary-anitha.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/mary-anitha.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Dr.E.A. Mary Anita</p>
					<p>Network Security</p>
					<p>drmaryanita@saec.ac.in</p>

					<a href="https://orcid.org/0000-0003-1538-4222" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?user=FEZ_yZ8AAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Mary_Anita2" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="img/veena.jpg" data-fancybox-group="faculty1">
					<img src="img/veena.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Dr. S. Veena</p>
					<p>Data Mining, Network Security, Operating System</p>
					<p>veenas@saec.ac.in</p>

					<a href="https://orcid.org/0000-0003-0126-9119" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=-9CsUWMAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Veena_Sundareswaran" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/ahmed-mudassar-alli.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/ahmed-mudassar-alli.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Dr.Ahmed Mudassar Alli</p>
					<p>Web Mining, Data Mining, Computer Architecture</p>
					<p>ahmedmudassarali@saec.ac.in</p>
					
					<!-- since Dr.Ahmed Mudassar Alli's scholar details was not provided
					<a href="https://orcid.org/0000-0002-9498-9790" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?user=xEyVGGgAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Suresh_A2" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
					-->
				</div>
			</div>
		</div>


		<div class="clearfix"></div>
		<div>   
			<h4><strong>ASSOCIATE PROFESSORS</strong></h4>
		

		<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/preetha.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/preetha.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Dr. M. Preetha</p>
					<p>Computer Networks, Network Security, Wireless Sensor Networks</p>
					<p>preetha@saec.ac.in</p>

					<a href="https://orcid.org/0000-0001-8483-9871" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?view_op=new_articles&hl=en&imq=Preetha+Marappan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/signup.SignUpRequestVerificationSent.html?email=smpreetha14%40gmail.com" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/balakrishnan.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/balakrishnan.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr.C. Balakrishnan</p>
					<p>Network Security, Computer Networks, Operating Systems</p>
					<p>balakrishnan@saec.ac.in</p>

					<a href="https://orcid.org/my-orcid"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=pnisd4oAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/mani.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/mani.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr.A. Mani</p>
					<p>Web Technology, Java And J2EE, Mobile Computing</p>
					<p>mani@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-5258-3063" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?hl=en&user=-q0_bb8AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Mani_Athimoolam" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/vinotha.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/vinotha.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.D.Vinodha</p>
					<p>Theory of Computation, Compiler Design, Programming Languages</p>
					<p>vinodha@saec.ac.in</p>

					<a href="https://orcid.org/my-orcid"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="http://scholar.google.com/citations?user=iN8aAZUAAAAJ&hl=en"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Vinodha_Dhanus/stats" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/balasubramanian.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/balasubramanian.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr. M. Balasubramanian</p>
					<p>Data Base Management Systems, Data Warehousing and Data Mining</p>
					<p>balasubramanian@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-1391-099X" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=T_459pkAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Balasubramanian_Manthiramoorthy" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/vignesh.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/vignesh.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Dr. T. Vignesh</p>
					<p>Artificial Intelligence, Image Processing, Soft Computing</p>
					<p>vignesht@saec.ac.in</p>

					<a href="https://orcid.org/0000-0003-2865-966X?lang=en " target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?user=XU9ZMucAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Vignesh_Thangathurai2" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/muthukumarasamy.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/muthukumarasamy.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr.S.Muthukumarasamy</p>
					<p>Computer Networks, Mobile Computing, Grid & Cloud Computing,Network Security, Wireless Sensor Networks</p>
					<p>muthukumarasamy@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-3171-5186" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=bR5CnwIAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Muthukumarasamy_Sethuraman" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>



		</div>


		<div class="clearfix"></div>
		<div>
			<h4><strong>ASSISTANT PROFESSORS</strong></h4>




			

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/sarasvathi.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/sarasvathi.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.V. Saraswathi</p>
					<p>Data Structures, Object Oriented Programming, Web Technology</p>
					<p>saraswathiv@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-5219-3938" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=b3nzD9QAAAAJ&hl=en&authuser=1" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/prabhu.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/prabhu.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr.S. Prabhu</p>
					<p>Computer Networks, Network Security, Data Base Management Systems</p>
					<p>prabhu@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-9167-5411" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=iFanw5YAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Prabhu_Subramanian4"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/karthik.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/karthik.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr.E.Karthik</p>
					<p>Big Data Analytics</p>
					<p>karthike@saec.ac.in</p>

					<a href="https://orcid.org/0000-0003-2254-3236"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=Q28P0MkAAAAJ&hl=en"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Karthik_Elangovan2"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/ramya-devi.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/ramya-devi.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Ms. K. Ramya Devi</p>
					<p>Network Security, Data Warehousing, Data Mining</p>
					<p>ramyadevik@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-9213-5222" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?hl=en&user=0il_q_UAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Ramyadevi_Kalaiselvan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/sajini.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/sajini.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.S. Sajini</p>
					<p>Mobile Computing, Network Security</p>
					<p>sajinis@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-5914-6405" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?hl=en&user=96kd9rcAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Sajini_Sathianesan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/renuka-devi.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/renuka-devi.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.M.Renuka Devi</p>
					<p>Cloud Computing, Computer Networks</p>
					<p>renukadevi@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-0832-3192?lang=en"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?user=6JkZ72YAAAAJ"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/elavarasi.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/elavarasi.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.K. Elavarasi</p>
					<p>Data Structures, Object Oriented Analysis and Design, Computer Programming</p>
					<p>elavarasi@saec.ac.in</p>

					<a href="https://orcid.org/0000-0003-3881-5777"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=v8-Wc-IAAAAJ&hl=en"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Elavarasi_Karunanidhi"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/sudha.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/sudha.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs. L. Sudha</p>
					<p>Object Oriented Programming, Java Programming, Web Technology</p>
					<p>sudhal@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-3896-5663" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=-PNIIE8AAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Sudha_Lakshmanan2" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/jonisha.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/jonisha.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.S.Jonisha</p>
					<p>Data Structures, Computer Networks, Computer Architecture</p>
					<p>jonisha@saec.ac.in</p>

					<a href="https://orcid.org/0000-0001-9920-9717" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=VsrQl-sAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Jonisha_S?ev=hdr_xprf" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/aruna-k-b.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/aruna-k-b.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs.K.B.Aruna</p>
					<p>Computer Programming, Computer Graphics, Data Mining And Data Warehousing</p>
					<p>arunakb@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-0274-6637"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=6aqY29UAAAAJ&hl=en"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Aruna_K_B" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/karthikeyan.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/karthikeyan.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr.M.P.Karthikeyan</p>
					<p>Data Structures, Programming Languages, Mobile Computing</p>
					<p>karthikeyan@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-0970-1861"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?user=EKtAwvIAAAAJ&hl=en"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Karthikeyan_M_P2"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/rajkamal.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/rajkamal.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr. J.Rajkamal</p>
					<p>Big Data</p>
					<p>rajkamalj@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-2053-2771" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=R2fYEOMAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Rajkamal_Jambulingam" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/thilagam.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/thilagam.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs. T.Thilagam </p>
					<p>Cloud Computing</p>
					<p>thilagamt@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-7657-0726?lang=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=3zxm-CQAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="www.researchgate.net/profile/T_Thilagam_Kannan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/balamurugan.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/balamurugan.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mr Balamurugan</p>
					<p>Wireless network, MANET, WANET</p>
					<p>balamuruganv@saec.ac.in</p>

					<a href="https://orcid.org/0000-0001-6103-259X" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?hl=en&user=m6wBdDsAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Balamurugan_Venkatesan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/ananthi.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/ananthi.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs. S.N.Ananthi</p>
					<p>Computer Networks, Network Security</p>
					<p>ananthisn@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-7640-5210"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?hl=en&user=kD4aUZIAAAAJ"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Sn_Ananthi"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/lallithashri.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/lallithashri.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Ms. A. Lallithashri</p>
					<p>Computer Programming, Data Structure</p>
					<p>lalithashri@saec.ac.in</p>

					<!-- <a href="https://orcid.org/0000-0002-5168-0056" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a> -->
					<a href="https://scholar.google.com/citations?user=t8Z_H80AAAAJ&hl=en&authuser=1" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<!-- <a href="https://www.researchgate.net/profile/Suresh_Subramanian9" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/gowri.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/gowri.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Ms G. Gowri</p>
					<p>​Computer Programming</p>
					<p>gowrig@saec.ac.in</p>

					<a href="https://orcid.org/0000-0001-7807-3697" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?hl=en&user=TGOiun8AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Gowri_Gurumurthy" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/gunanandhini.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/gunanandhini.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs. S.Gunanandhini</p>
					<p>​Mobile Computing</p>
					<p>gunanandhinis@saec.ac.in</p>

					<a href="https://orcid.org/0000-0003-0296-4623" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.com/citations?user=8xM1jc8AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Gunanandhini_Suresh" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>

			<div class="single-eee-fac col-sm-4">
				<div class="eee-photo-col">
					<a class="fancybox" href="<?php echo $siteurl ?>/img/padmavathy.jpg" data-fancybox-group="faculty1">
					<img src="<?php echo $siteurl ?>/img/padmavathy.jpg" class="img-responsive">
					</a>
				</div>
				<div class="eee-fac-desc">
					<p>Mrs. T. Padmavathy</p>
					<p>Programming Languages, Data Base Management Systems</p>
					<p>padmavathy@saec.ac.in</p>

					<a href="https://orcid.org/0000-0002-6391-4630" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
					<a href="https://scholar.google.co.in/citations?user=p5-2KN8AAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
					<a href="https://www.researchgate.net/profile/Padmavathy_Thiru" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>


		<div>
		<h4><strong>Teaching and Learning Process</strong></h4>
						<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/teaching&learning.jpg" data-fancybox-group="teaching1">
                                    <img src="<?php echo $siteurl ?>/img/teaching&learning.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
		
		</div>
			<div>
				<h4>Teaching Learning Methodologies</h4>
				<table width="616">
				<tbody>
				<tr>
				<td width="93"><strong>Year/Sem</strong></td>
				<td width="103"><strong>Sub. Code</strong></td>
				<td width="251"><strong>Name of the Subject</strong></td>
				<td width="169"><strong>Methodology</strong><p></p>
				<p><strong>&nbsp;</strong></p></td>
				</tr>
				<tr>
				<td rowspan="6" width="93"><strong>&nbsp;</strong><p></p>
				<p><strong>&nbsp;</strong></p>
				<p><strong>II/III</strong></p></td>
				<td width="103">MA6351</td>
				<td width="251">Transforms and Partial Differential Equations</td>
				<td width="169">Assignment based&nbsp; Learning</td>
				</tr>
				<tr>
				<td width="103">CS6301</td>
				<td width="251">Programming and Data Structure II</td>
				<td width="169">Tutorial based Learning<p></p>
				<p>Model based Learning</p></td>
				</tr>
				<tr>
				<td width="103">CS6302</td>
				<td width="251">Database Management Systems</td>
				<td width="169">Lab based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6303</td>
				<td width="251">Computer Architecture</td>
				<td width="169">Survey based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6304</td>
				<td width="251">Analog and Digital Communication</td>
				<td width="169">Assignment based Learning</td>
				</tr>
				<tr>
				<td width="103">GE6351</td>
				<td width="251">Environmental Science and Engineering</td>
				<td width="169">Survey based Learning</td>
				</tr>
				<tr>
				<td width="93"><strong>&nbsp;</strong></td>
				<td width="103"></td>
				<td width="251"></td>
				<td width="169"></td>
				</tr>
				<tr>
				<td rowspan="5" width="93"><strong>III/V</strong></td>
				<td width="103">MA6566</td>
				<td width="251">Discrete Mathematics</td>
				<td width="169">Tutorial based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6501</td>
				<td width="251">Internet Programming</td>
				<td width="169">Poster/Web based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6502</td>
				<td width="251">Object Oriented Analysis and Design</td>
				<td width="169">Project&nbsp; based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6503</td>
				<td width="251">Theory of Computation</td>
				<td width="169">Tutorial based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6504</td>
				<td width="251">Computer Graphics</td>
				<td width="169">Lab based Learning<p></p>
				<p>Poster based Learning</p></td>
				</tr>
				<tr>
				<td width="93"><strong>&nbsp;</strong></td>
				<td width="103"></td>
				<td width="251"></td>
				<td width="169"></td>
				</tr>
				<tr>
				<td rowspan="6" width="93"><strong>IV/VII</strong></td>
				<td width="103">CS6701</td>
				<td width="251">Cryptography and Network Security</td>
				<td width="169">Project based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6702</td>
				<td width="251">Graph Theory and Applications</td>
				<td width="169">Model based Learning<p></p>
				<p>Assignment based Learning</p></td>
				</tr>
				<tr>
				<td width="103">CS6703</td>
				<td width="251">Grid and Cloud Computing</td>
				<td width="169">Project based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6704</td>
				<td width="251">Resource Management Techniques</td>
				<td width="169">Tutorial based Learning</td>
				</tr>
				<tr>
				<td width="103">CS6003</td>
				<td width="251">Ad Hoc And Sensor Networks</td>
				<td width="169">Survey based Learning</td>
				</tr>
				<tr>
				<td width="103">IT6006</td>
				<td width="251">Data Analytics</td>
				<td width="169">Survey based Learning</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div>
				<h4><strong>Professional Body Membership</strong></h4>
				<h3>ACM Members</h3>
				<span><a href="pdf/ACM.pdf" target="_blank">List of ACM Members</a></span><br>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/hod.png" data-fancybox-group="ACM1", title="Dr.G.Umarani Srikanth, 4312199">
                                    <img src="<?php echo $siteurl ?>/img/hod.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.E.A. Mary Anita.png" data-fancybox-group="ACM1", title="Dr.G.Umarani Srikanth, 01081015">
                                    <img src="<?php echo $siteurl ?>/img/Dr.E.A. Mary Anita.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
								
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.S.Veena.jpg" data-fancybox-group="ACM1", title="Dr.S.Veena, 7219630">
                                    <img src="<?php echo $siteurl ?>/img/Dr.S.Veena.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.R.Geetha.jpg" data-fancybox-group="ACM1", title="Dr.R.Geetha, 5294031">
                                    <img src="<?php echo $siteurl ?>/img/Dr.R.Geetha.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.R.Prasanna Kumar.jpg" data-fancybox-group="ACM1", title="Dr.R.Prasanna Kumar, 6894496">
                                    <img src="<?php echo $siteurl ?>/img/Dr.R.Prasanna Kumar.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr. M. Preetha.jpg" data-fancybox-group="ACM1", title="Dr. M. Preetha, 0404482">
                                    <img src="<?php echo $siteurl ?>/img/Dr. M. Preetha.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs.N.S.Usha.jpg" data-fancybox-group="ACM1", title="Mrs.N.S.Usha, 9195724">
                                    <img src="<?php echo $siteurl ?>/img/Mrs.N.S.Usha.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/S.Sajini.jpg" data-fancybox-group="ACM1", title="S.Sajini, 4525859">
                                    <img src="<?php echo $siteurl ?>/img/S.Sajini.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs.A.Lalitha Shri.jpg" data-fancybox-group="ACM1", title="Mrs.A.Lalitha Shri, 2035563">
                                    <img src="<?php echo $siteurl ?>/img/Mrs.A.Lalitha Shri.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs.D.Vinodha.jpg" data-fancybox-group="ACM1", title="Mrs.D.Vinodha, 7069625">
                                    <img src="<?php echo $siteurl ?>/img/Mrs.D.Vinodha.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs.V.Sarawathi.jpg" data-fancybox-group="ACM1", title="Mrs.V.Sarawathi, 4066035">
                                    <img src="<?php echo $siteurl ?>/img/Mrs.V.Sarawathi.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Ms.G.Gowri.jpg" data-fancybox-group="ACM1", title="Ms.G.Gowri, 5898602">
                                    <img src="<?php echo $siteurl ?>/img/Ms.G.Gowri.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs.E.Abirami.jpg" data-fancybox-group="ACM1", title="Mrs.E.Abirami, 4119198">
                                    <img src="<?php echo $siteurl ?>/img/Mrs.E.Abirami.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/MRs.T.Padmavathy.jpg" data-fancybox-group="ACM1", title="MRs.T.Padmavathy, 1413547">
                                    <img src="<?php echo $siteurl ?>/img/MRs.T.Padmavathy.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
			
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs. T.Thilagam.png" data-fancybox-group="ACM1", title="Mrs.T.Thilagam">
                                    <img src="<?php echo $siteurl ?>/img/Mrs. T.Thilagam.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
			
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mrs. S.N.Ananthi.png" data-fancybox-group="ACM1", title="Mrs.S.N.Ananthi, 0352223">
                                    <img src="<?php echo $siteurl ?>/img/Mrs. S.N.Ananthi.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mr. J.Rajkamal.jpg" data-fancybox-group="ACM1", title="Mr. J.Rajkamal, 8380981">
                                    <img src="<?php echo $siteurl ?>/img/Mr. J.Rajkamal.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
			
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mr Balamurugan.jpg" data-fancybox-group="ACM1", title="Mr Balamurugan, 2005886">
                                    <img src="<?php echo $siteurl ?>/img/Mr Balamurugan.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
				
			</div>
			<div>
				<h4><strong>CSI Members</strong></h4>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.E.A. Mary Anita.png" data-fancybox-group="ACM1", title="Dr.E.A. Mary Anita, 01081015">
                                    <img src="<?php echo $siteurl ?>/img/Dr.E.A. Mary Anita.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.R.Geetha.png" data-fancybox-group="ACM1", title="Dr.R.Geetha, N1259352">
                                    <img src="<?php echo $siteurl ?>/img/Dr.R.Geetha.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
			</div>
			<div>
				<h4><strong>IACSIT Members</strong></h4>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Mr.T.Vignesh.png" data-fancybox-group="ACM1", title="Mr.T.Vignesh, 80347252">
                                    <img src="<?php echo $siteurl ?>/img/Mr.T.Vignesh.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
				
			</div>
			
			<div>
				<h4><strong>IEEE Members</strong></h4>
				<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.G.Umarani Srikanth.png" data-fancybox-group="ACM1", title="Dr.G.Umarani Srikanth, 92236357">
                                    <img src="<?php echo $siteurl ?>/img/Dr.G.Umarani Srikanth.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/Dr.E.A. Mary Anita.png" data-fancybox-group="ACM1", title="Dr.E.A. Mary Anita, 93780622">
                                    <img src="<?php echo $siteurl ?>/img/Dr.E.A. Mary Anita.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
				</div>
			</div>
			
				
			
	                  
    </div> 
	<div class="tab-content fade" id="menu4">

        <div>
            <h3>Gallery</h3>
               <span>International Conference on Informatics & Computing in Engineering Systems (ICICES 2018) on 21.03.18 and 22.03.18.</span>
            <div class="ach_sec clearfix">
                
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0043.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0043.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0074.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0074.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0076.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0076.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0494.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0494.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0495.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0495.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0500.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0500.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0502.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0502.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0534.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0534.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0545.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0545.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0547.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0547.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>    
            <span>Guest Lecture on Internet of Things on 01-03-18</span>
            <div class="ach_sec clearfix">
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0365.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0365.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0368.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0368.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0369.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0369.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0370.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0370.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
            
            <span>Workshop on quantum computing & cryptography 23 & 24 January 2018</span>
            <div class="ach_sec clearfix">
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0011.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0011.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0031.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0031.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0363.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0363.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0368.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0368.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0372.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0372.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0385.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0385.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/_DSC0412.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/_DSC0412.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_0085.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_0085.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>  

            </div>

            <span>"Inauguration of IEEE Computer Society" on 21.02.2018</span>
            <div class="ach_sec clearfix">
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0142.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0142.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0162.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0162.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/IMG_0165.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/IMG_0165.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>


            <span>TNSCST Sponsored National Level Seminar “Internet of Things for Effective Disaster Management” on 22-02-18 </span>
            <div class="ach_sec clearfix">
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_4153.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_4153.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_4172.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_4172.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/DSC_4176.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/DSC_4176.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>

            <span>CSI State Level Student Convention “TECHWAVES” organized by SAEC- CSI STUDENT CHAPTER on 05-01-18</span>
            <div class="ach_sec clearfix">
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/cs-awards1.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/cs-awards1.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/cs-awards2.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/cs-awards2.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/cs-awards3.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/cs-awards3.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/cs-awards4.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/cs-awards4.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <a class="fancybox" href="img/cs-awards5.jpg" data-fancybox-group="gallerycs1">
                            <img src="<?php echo $siteurl ?>/img/cs-awards5.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>

         
        </div>
         ​

<!-- 		<div>
		<h4><strong>Infrastructure and Facilities</strong></h4>
		<ul class="tic_list">
			<li>The department has currently an intake of 180 students. It has centralized computer center, excellent and well-equipped air-conditioned laboratories with state-of-the art computing facilities, equipped with latest Pentium Systems along with updated versions of latest software and hardware. The department also has 240 KVA power backup and 20&nbsp;Mbps Leased Line Internet connectivity for the benefit of the students. The department has MOU’s with leading companies in Chennai enabling staff and students to work on current trends.</li>
			<li>The infrastructure of the Department includes class rooms equipped with smart boards, modern audio video aids, tutorial rooms, well equipped laboratories, departmental library &amp; seminar room and faculty offices. In laboratories, students experiment with the latest&nbsp;&nbsp;&nbsp; technologies&nbsp;&nbsp; which help them in kindling their latent skills.</li>
			<li>The Department has laboratories namely RDBMS Lab, Graphics and Multimedia Lab, Computer Programming Lab, Case Tools Lab, Project Lab etc., These laboratories include IBM X225 Series server and IBM Series X226 Servers and a cluster of Ethernet LANs and standalone personal computers.</li>
			<li>The software available are Windows 2000 server, Windows XP, Windows NT, Red Hat Linux, Rational Suite,&nbsp;&nbsp; Microsoft visual studio,&nbsp; Microsoft office 2000 professional, Microsoft Front page 2003, Oracle 9i, Adobe In design, Studio MX, Direct MX, Borland Turbo C++ Suite, and Borland Enterprise server.</li>
			<li>The Department has Anna university recognized Research Center.</li>
			<li>The Department is running CISCO Networks Academy.</li>
			<li>The Department Provides Course End Survey with the new three tier application.</li>
		</ul>
		</div>
		<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/department-layout.png" data-fancybox-group="infrastructure1">
                                    <img src="<?php echo $siteurl ?>/img/department-layout.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
		</div> -->
		
		
	</div>
	<div class="tab-content fade" id="menu5">
		<div>
		<h4></strong>INTERNSHIP / INPLANT TRAINING</strong></h4>
			<table width="599">
			<tbody>
			<tr>
			<td rowspan="2" width="244">
			<p><strong>ACADEMIC YEAR</strong></p>
			</td>
			<td width="171">
			<p><strong>INTERNSHIP</strong></p>
			</td>
			<td width="175">
			<p><strong>IN PLANT TRAINING</strong></p>
			</td>
			</tr>
			<tr>
			<td width="171">
			<p><strong>No. of Students</strong></p>
			</td>
			<td width="175">
			<p><strong>No. of Students</strong></p>
			</td>
			</tr>
			<tr>
			<td width="244">
			<p>2017-18</p>
			</td>
			<td width="171">
			<p>0<sup>*</sup></p>
			</td>
			<td width="175">
			<p>101<sup>*</sup></p>
			</td>
			</tr>
			<tr>
			<td width="244">
			<p>2016-17</p>
			</td>
			<td width="171">
			<p>173</p>
			</td>
			<td width="175">
			<p>385</p>
			</td>
			</tr>
			<tr>
			<td width="244">
			<p>2015-16</p>
			</td>
			<td width="171">
			<p>181</p>
			</td>
			<td width="175">
			<p>216</p>
			</td>
			</tr>
			<tr>
			<td width="244">
			<p>2014-15</p>
			</td>
			<td width="171">
			<p>56</p>
			</td>
			<td width="175">
			<p>397</p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2017-18</span>
			<table>
			<tbody>
			<tr>
			<td rowspan="2" width="47">
			<p><strong>SNO</strong></p>
			</td>
			<td rowspan="2" width="340">
			<p><strong>NAME OF THE COMPANIES</strong></p>
			</td>
			<td colspan="2" width="214">
			<p><strong>NO OF STUDENTS</strong></p>
			</td>
			</tr>
			<tr>
			<td width="117">
			<p><strong>IPT</strong></p>
			</td>
			<td width="97">
			<p><strong>INTERNSHIP</strong></p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>1</p>
			</td>
			<td width="340">
			<p>Eyeopen Technologies</p>
			</td>
			<td width="117">
			<p>31</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>2</p>
			</td>
			<td width="340">
			<p>Oneyes Technologies</p>
			</td>
			<td width="117">
			<p>09</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>3</p>
			</td>
			<td width="340">
			<p>Cutting Edge Technologies</p>
			</td>
			<td width="117">
			<p>06</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>4</p>
			</td>
			<td width="340">
			<p>Codebind Technologies</p>
			</td>
			<td width="117">
			<p>20</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>5</p>
			</td>
			<td width="340">
			<p>Uniq Technologies</p>
			</td>
			<td width="117">
			<p>13</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>6</p>
			</td>
			<td width="340">
			<p>Spiro Solution Pvt Ltd</p>
			</td>
			<td width="117">
			<p>07</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>7</p>
			</td>
			<td width="340">
			<p>Proven Digital Web</p>
			</td>
			<td width="117">
			<p>02</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>8</p>
			</td>
			<td width="340">
			<p>O&amp;H Shipping Services (India) Pvt Limited</p>
			</td>
			<td width="117">
			<p>09</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>9</p>
			</td>
			<td width="340">
			<p>Blue Bird Technologies</p>
			</td>
			<td width="117">
			<p>01</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>10</p>
			</td>
			<td width="340">
			<p>Kaashiv Infotech</p>
			</td>
			<td width="117">
			<p>03</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td colspan="2" width="387">
			<p><strong>&nbsp;</strong></p>
			<p><strong>TOTAL</strong></p>
			<p><strong>&nbsp;</strong></p>
			</td>
			<td width="117">
			<p><strong>&nbsp;</strong></p>
			<p><strong>101</strong></p>
			<p><strong>&nbsp;</strong></p>
			</td>
			<td width="97">
			<p><strong>&nbsp;</strong></p>
			<p><strong>0</strong></p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2016-17 EVEN</span>
			<table>
			<tbody>
			<tr>
			<td rowspan="2" width="47">
			<p><strong>SNO</strong></p>
			</td>
			<td rowspan="2" width="342">
			<p><strong>NAME OF THE COMPANIES</strong></p>
			</td>
			<td colspan="2" width="212">
			<p><strong>NO OF STUDENTS</strong></p>
			</td>
			</tr>
			<tr>
			<td width="115">
			<p><strong>IPT</strong></p>
			</td>
			<td width="97">
			<p><strong>INTERNSHIP</strong></p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>1</p>
			</td>
			<td width="342">
			<p>DLK Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>14</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>2</p>
			</td>
			<td width="342">
			<p>UNIQ technologies,Chennai</p>
			</td>
			<td width="115">
			<p>05</p>
			</td>
			<td width="97">
			<p>04</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>3</p>
			</td>
			<td width="342">
			<p>Kaashiv Infotech, Chennai</p>
			</td>
			<td width="115">
			<p>09</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>4</p>
			</td>
			<td width="342">
			<p>IWIZ Hitech Solutions (P) LTD,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>17</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>5</p>
			</td>
			<td width="342">
			<p>Oneyes technologies, Chennai</p>
			</td>
			<td width="115">
			<p>03</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>6</p>
			</td>
			<td width="342">
			<p>Kriatec Services, Chennai</p>
			</td>
			<td width="115">
			<p>06</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>7</p>
			</td>
			<td width="342">
			<p>IWIZ Blue Chip Technologies Pvt Ltd,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>16</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>8</p>
			</td>
			<td width="342">
			<p>XLICON Business Services Private Ltd ,Chennai</p>
			</td>
			<td width="115">
			<p>05</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>9</p>
			</td>
			<td width="342">
			<p>Cutting Edge Technologies, Chennai</p>
			</td>
			<td width="115">
			<p>09</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>10</p>
			</td>
			<td width="342">
			<p>SPINS Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>01</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>11</p>
			</td>
			<td width="342">
			<p>EyeOpen Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>32</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>12</p>
			</td>
			<td width="342">
			<p>Futura Innovative Information Technologie,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>03</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>13</p>
			</td>
			<td width="342">
			<p>Spiro Solution pvt&nbsp; Ltd,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>08</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>14</p>
			</td>
			<td width="342">
			<p>Trace Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>07</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>15</p>
			</td>
			<td width="342">
			<p>Raysoft Solutions,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>03</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>16</p>
			</td>
			<td width="342">
			<p>Callisto IT Solutions,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>05</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>17</p>
			</td>
			<td width="342">
			<p>HNS Infotech,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>01</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>18</p>
			</td>
			<td width="342">
			<p>HCL Career Development centre, Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>01</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>19</p>
			</td>
			<td width="342">
			<p>KM Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>02</p>
			</td>
			</tr>
			<tr>
			<td colspan="2" width="389">
			<p><strong>&nbsp;</strong></p>
			<p><strong>TOTAL</strong></p>
			<p><strong>&nbsp;</strong></p>
			</td>
			<td width="115">
			<p><strong>83</strong></p>
			</td>
			<td width="97">
			<p><strong>68</strong></p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2016-17 ODD</span>
			<table>
			<tbody>
			<tr>
			<td rowspan="2" width="47">
			<p><strong>SNO</strong></p>
			</td>
			<td rowspan="2" width="342">
			<p><strong>NAME OF THE COMPANIES</strong></p>
			</td>
			<td colspan="2" width="212">
			<p><strong>NO OF STUDENTS</strong></p>
			</td>
			</tr>
			<tr>
			<td width="115">
			<p><strong>IPT</strong></p>
			</td>
			<td width="97">
			<p><strong>INTERNSHIP</strong></p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>1</p>
			</td>
			<td width="342">
			<p>Cutting Edge Technologies, Chennai</p>
			</td>
			<td width="115">
			<p>93</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>2</p>
			</td>
			<td width="342">
			<p>Oneyes technologies, Chennai</p>
			</td>
			<td width="115">
			<p>6</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>3</p>
			</td>
			<td width="342">
			<p>XLICON Business Services Private Ltd ,Chennai</p>
			</td>
			<td width="115">
			<p>15</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>4</p>
			</td>
			<td width="342">
			<p>HCL Career Development centre, Chennai</p>
			</td>
			<td width="115">
			<p>30</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>5</p>
			</td>
			<td width="342">
			<p>NSIC Technical Service Centre, Chennai</p>
			</td>
			<td width="115">
			<p>17</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>6</p>
			</td>
			<td width="342">
			<p>MindBridges Technologies, Chennai</p>
			</td>
			<td width="115">
			<p>–</p>
			</td>
			<td width="97">
			<p>4</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>7</p>
			</td>
			<td width="342">
			<p>Kaashiv Infotech, Chennai</p>
			</td>
			<td width="115">
			<p>28</p>
			</td>
			<td width="97">
			<p>5</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>8</p>
			</td>
			<td width="342">
			<p>Kriatec Services, Chennai</p>
			</td>
			<td width="115">
			<p>1</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>9</p>
			</td>
			<td width="342">
			<p>Seo Technologies, Chennai</p>
			</td>
			<td width="115">
			<p>1</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>10</p>
			</td>
			<td width="342">
			<p>Lakshmi Instruments and automation,Chennai</p>
			</td>
			<td width="115">
			<p>3</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>11</p>
			</td>
			<td width="342">
			<p>Lucas India Service Ltd,Chennai</p>
			</td>
			<td width="115">
			<p>1</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>12</p>
			</td>
			<td width="342">
			<p>IWIZ Blue Chip Technologies Pvt Ltd,Chennai</p>
			</td>
			<td width="115">
			<p>9</p>
			</td>
			<td width="97">
			<p>1</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>13</p>
			</td>
			<td width="342">
			<p>DLK Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>2</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>14</p>
			</td>
			<td width="342">
			<p>SPINS Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>1</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>15</p>
			</td>
			<td width="342">
			<p>Hitech Solutions (P) LTD,Chennai</p>
			</td>
			<td width="115">
			<p>1</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>16</p>
			</td>
			<td width="342">
			<p>Codebind Technologies,Chennai</p>
			</td>
			<td width="115">
			<p>5</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>17</p>
			</td>
			<td width="342">
			<p>Spiro Solution pvt&nbsp; Ltd,Chennai</p>
			</td>
			<td width="115">
			<p>5</p>
			</td>
			<td width="97">
			<p>3</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>18</p>
			</td>
			<td width="342">
			<p>UNIQ technologies,Chennai</p>
			</td>
			<td width="115">
			<p>16</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>19</p>
			</td>
			<td width="342">
			<p>Crisp System India Private Ltd,Chennai</p>
			</td>
			<td width="115">
			<p>10</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>20</p>
			</td>
			<td width="342">
			<p>Raysoft Solutions,Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>8</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>21</p>
			</td>
			<td width="342">
			<p>Synchrony Infotech,Chennai</p>
			</td>
			<td width="115">
			<p>27</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>22</p>
			</td>
			<td width="342">
			<p>COMDEX Infotech,Chennai</p>
			</td>
			<td width="115">
			<p>3</p>
			</td>
			<td width="97">
			<p>20</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>24</p>
			</td>
			<td width="342">
			<p>Fubey Technologies India Private Ltd, Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>1</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>25</p>
			</td>
			<td width="342">
			<p>Smartant Technologies, &nbsp;Chennai</p>
			</td>
			<td width="115">
			<p>4</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>26</p>
			</td>
			<td width="342">
			<p>Lionbridge</p>
			</td>
			<td width="115">
			<p>24</p>
			</td>
			<td width="97">
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="47">
			<p>27</p>
			</td>
			<td width="342">
			<p>Obcasio Infotech Solutions,&nbsp; Chennai</p>
			</td>
			<td width="115">
			<p>&nbsp;</p>
			</td>
			<td width="97">
			<p>3</p>
			</td>
			</tr>
			<tr>
			<td colspan="2" width="389">
			<p><strong>&nbsp;</strong></p>
			<p><strong>TOTAL</strong></p>
			<p><strong>&nbsp;</strong></p>
			</td>
			<td width="115">
			<p><strong>&nbsp;</strong></p>
			<p><strong>302</strong></p>
			<p><strong>&nbsp;</strong></p>
			</td>
			<td width="97">
			<p><strong>&nbsp;</strong></p>
			<p><strong>45</strong></p>
			<p><strong>&nbsp;</strong></p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<h4><strong>Industrial Visits</strong></h4>
			<span>2017-2018</span>
			<table width="718">
			<tbody>
			<tr>
			<td width="8%">
			<p><strong>S.NO</strong></p>
			</td>
			<td width="19%">
			<p><strong>DATE OF EVENT</strong></p>
			</td>
			<td width="28%">
			<p><strong>TOPIC/COMPANY</strong></p>
			</td>
			<td width="41%">
			<p><strong>NAME OF THE RESOURCE PERSON/DESIGNATION/COMPANY ADDRESS</strong></p>
			</td>
			</tr>
			<tr>
			<td width="8%">
			<p><strong>1</strong></p>
			</td>
			<td width="19%">
			<p>06.07.2017 and</p>
			<p>07.07.2017</p>
			</td>
			<td width="28%">
			<p>Divisional Engineer,</p>
			<p>Network Management System,</p>
			<p>BSNL, Southern</p>
			<p>Telecom Region,</p>
			<p>Chennai -1</p>
			</td>
			<td width="41%">
			<p>V.Usha</p>
			<p>Divisional Engineer</p>
			<p>Network Management System</p>
			<p>BSNL, Southern Telecom Region, Chennai -1</p>
			<p>Mobile No.9444971261</p>
			</td>
			</tr>
			<tr>
			<td width="8%">
			<p>2.</p>
			</td>
			<td width="19%">
			<p>08.09.2017</p>
			<p>&nbsp;</p>
			</td>
			<td width="28%">
			<p>Power Grid,Cloud</p>
			<p>IGCAR, Kalpakkam</p>
			<p>Mobile No.</p>
			<p>04427480500</p>
			<table style="height: 90px;" width="168">
			<tbody>
			<tr>
			<td width="0">&nbsp;</td>
			<td width="137">
			<p>project-igcar@gov.in</p>
			</td>
			</tr>
			</tbody>
			</table>
			</td>
			<td width="41%">
			<p>Mr.M. Sai Baba<br> Director, Resources Management Group<br> IGCAR, Kalpakkam</p>
			<p>Mobile No.</p>
			<p>04427480500</p>
			<table width="168">
			<tbody>
			<tr>
			<td width="0">&nbsp;</td>
			<td width="162">
			<p>project-igcar@gov.in</p>
			</td>
			</tr>
			</tbody>
			</table>
			</td>
			</tr>
			<tr>
			<td width="8%">
			<p>3.</p>
			</td>
			<td width="19%">
			<p>24.08.2017</p>
			</td>
			<td width="28%">
			<p>Deputy Inspector General of Police (Principal)</p>
			<p>RTC,</p>
			<p>CRPF, Avadi</p>
			<p>Chennai-600065</p>
			</td>
			<td width="41%">
			<p>Mr. Mr.A.K Sajja,Dy.Comdt</p>
			<p>Principal/DIG-RTC</p>
			<p>Avadi,</p>
			<p>Chennai-600 065.</p>
			<p>9444604812</p>
			<p><strong>&nbsp;</strong></p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
		<span>2016-2017</span>
			<table width="718">
			<tbody>
			<tr>
			<td width="8%">
			<p><strong>S.NO</strong></p>
			</td>
			<td width="18%">
			<p><strong>DATE OF EVENT</strong></p>
			</td>
			<td width="29%">
			<p><strong>TOPIC/COMPANY</strong></p>
			</td>
			<td width="41%">
			<p><strong>NAME OF THE RESOURCE PERSON/DESIGNATION/COMPANY ADDRESS</strong></p>
			</td>
			</tr>
			<tr>
			<td width="8%">
			<p><strong>1</strong></p>
			</td>
			<td width="18%">
			<p>27.07.2016</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp; And</p>
			<p>28.07.2016</p>
			<p>10.00 A.M</p>
			</td>
			<td width="29%">
			<p>Divisional Engineer,</p>
			<p>Network Management System,</p>
			<p>BSNL, Southern</p>
			<p>Telecom Region,</p>
			<p>Chennai -1</p>
			</td>
			<td width="41%">
			<p>V.Usha</p>
			<p>Divisional Engineer</p>
			<p>Network Management System</p>
			<p>BSNL, Southern Telecom Region, Chennai -1</p>
			<p>Mobile No.9444971261</p>
			</td>
			</tr>
			<tr>
			<td rowspan="2" width="8%">
			<p>2.</p>
			</td>
			<td width="18%">
			<p>19.08.2016</p>
			<p>10:00 A.M</p>
			</td>
			<td width="29%">
			<p>Web Service in Cloud Application /</p>
			<p>CLOUD n LOUD</p>
			<p>&nbsp;</p>
			</td>
			<td width="41%">
			<p>Mr.Vijabalan Balakrishnan</p>
			<p>Director</p>
			<p>132,Pillaiyar Koil Street,</p>
			<p>Secretariat Colony</p>
			<p>Paduvanchery</p>
			<p>Selaiyur</p>
			<p>Chennai-600073</p>
			<p>044-22292796</p>
			<p>9884561188</p>
			<p>8939984529</p>
			</td>
			</tr>
			<tr>
			<td width="18%">
			<p>22.08.2016</p>
			<p>10:00 A.M</p>
			</td>
			<td width="29%">
			<p>Web Service in Cloud Application /</p>
			<p>CLOUD n LOUD</p>
			<p>&nbsp;</p>
			</td>
			<td width="41%">
			<p>Mr.Vijabalan Balakrishnan</p>
			<p>Director</p>
			<p>132,Pillaiyar Koil Street,</p>
			<p>Secretariat Colony</p>
			<p>Paduvanchery</p>
			<p>Selaiyur</p>
			<p>Chennai-600073</p>
			<p>044-22292796</p>
			<p>9884561188</p>
			<p>8939984529</p>
			</td>
			</tr>
			<tr>
			<td rowspan="2" width="8%">
			<p>3.</p>
			</td>
			<td rowspan="2" width="18%">
			<p>12-08-2016</p>
			<p>And</p>
			<p>13.08.2016</p>
			<p>10:00 A.M</p>
			</td>
			<td width="29%">
			<p>Chips Software Systems (estd.1993),</p>
			<p>Hospital Road, Ernakulam, Kochi – 682 011</p>
			</td>
			<td width="41%">
			<p>T.U.K.Menon, Director,</p>
			<p>CHIPS SOFTWARE SYSTEMS (estd.1993),</p>
			<p>Hospital Road, Ernakulam, Kochi – 682 011</p>
			<p>Tel: (0484) 2370 942 /324 1436</p>
			<p>Email:mail@chipssoft.com</p>
			<p><strong>&nbsp;</strong></p>
			</td>
			</tr>
			<tr>
			<td width="29%">
			<p>Traco Cable Co.Ltd<br> Irimpanam</p>
			<p>&nbsp;</p>
			</td>
			<td width="41%">
			<p>Mr. Ravi Menon</p>
			<p>TRACO CABLE CO.LTD<br> IRIMPANAM-692309</p>
			<p>Email: parth00@vamsoft.in</p>
			<p>9446003260</p>
			</td>
			</tr>
			<tr>
			<td width="8%">
			<p>4.</p>
			</td>
			<td width="18%">
			<p>24.02.2017</p>
			</td>
			<td width="29%">
			<p>Bharath Sanchar Nigam Limited</p>
			<p>Regional Telecom Training Centre</p>
			<p>Maraimalainagar,</p>
			<p>Chennai – 603209</p>
			<p>(A Govt.of India Undertaking)</p>
			<table>
			<tbody>
			<tr>
			<td width="0">&nbsp;</td>
			<td width="188">
			<p>Mail:rttcchennai@gmail.com</p>
			</td>
			</tr>
			</tbody>
			</table>
			</td>
			<td width="41%">
			<p>Mr. R.Baskar SDE Marketing &nbsp; RTTC Chennai.</p>
			<p>Bharath Sanchar Nigam Limited</p>
			<p>Regional Telecom Training Centre</p>
			<p>Maraimalainagar,</p>
			<p>Chennai – 603209</p>
			<p>(A Govt.of India Undertaking)</p>
			<table>
			<tbody>
			<tr>
			<td width="0">&nbsp;</td>
			<td width="188">
			<p>Mail:rttcchennai@gmail.com</p>
			<p>Cell No: 94861 02892</p>
			</td>
			</tr>
			</tbody>
			</table>
			</td>
			</tr>
			<tr>
			<td rowspan="3" width="8%">
			<p>5.</p>
			</td>
			<td width="18%">
			<p>06-02-2017</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			</td>
			<td rowspan="3" width="29%">
			<p>UST Global Technology</p>
			<p>ASV Suntech Park, Old Mahabalipuram Rd, Thoraipakkam, Chennai, Tamil Nadu 600097</p>
			</td>
			<td rowspan="3" width="41%">
			<p>Mr.Raji Mohanlal</p>
			<p>UST Global Technology</p>
			<p>ASV Suntech Park, Old Mahabalipuram Rd, Thoraipakkam, Chennai, Tamil Nadu 600097</p>
			<p>Cell No: 9600001580.</p>
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="18%">
			<p>07.02.2017</p>
			</td>
			</tr>
			<tr>
			<td width="18%">
			<p>08.02.2017</p>
			</td>
			</tr>
			<tr>
			<td rowspan="2" width="8%">
			<p>6.</p>
			</td>
			<td rowspan="2" width="18%">
			<p>25-01-2017</p>
			<p>&nbsp;</p>
			</td>
			<td width="29%">
			<p>Chips Software Systems (estd.1993),</p>
			<p>Hospital Road, Ernakulam, Kochi – 682 011</p>
			<p>Tel: (0484) 2370 942 /324 1436</p>
			</td>
			<td width="41%">
			<p>T.U.K.Menon, Director,</p>
			<p>CHIPS SOFTWARE SYSTEMS (estd.1993),</p>
			<p>Hospital Road, Ernakulam, Kochi – 682 011</p>
			<p>Tel: (0484) 2370 942 /324 1436</p>
			<p>Email:mail@chipssoft.com</p>
			<p>&nbsp;</p>
			</td>
			</tr>
			<tr>
			<td width="29%">
			<p>Webandcrafts</p>
			<p>Infopark, Koratty P.O, Thrissur – 680 308</p>
			<p>Kerala, India</p>
			<p>Call: +91 480 2733999</p>
			<p>Email: info@webandcrafts.com</p>
			</td>
			<td width="41%">
			<p>Mrs .M. Neethu Prashob</p>
			<p>Webandcrafts</p>
			<p>Infopark, Koratty P.O, Thrissur – 680 308</p>
			<p>Kerala, India</p>
			<p>Call: +91 480 2733999</p>
			<p>Email: info@webandcrafts.com</p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2015-2016</span>
			<table width="110%">
			<tbody>
			<tr>
			<td width="6%"><strong>&nbsp;Sl.no</strong></td>
			<td width="12%"><strong>Date of Event</strong></td>
			<td width="21%"><strong>Topic/Company</strong></td>
			<td width="36%"><strong>&nbsp;</strong> <strong>Resource Person Details</strong> <strong>&nbsp;</strong></td>
			<td width="20%"><strong>&nbsp;</strong> <strong>Students Attended</strong></td>
			</tr>
			<tr>
			<td width="6%">1. &nbsp;</td>
			<td width="12%">04.07.2015 (At 2.00pm)</td>
			<td width="21%">Industrial Visit on “Vi&nbsp; Microsystem PVT LTD”</td>
			<td width="36%">Mr.S.Varadharajan Asst.Manager Customer Support Vi&nbsp; Microsystem PVT LTD 75, Electronics Estate, Perungudi, Chennai – 600 096.</td>
			<td width="20%">II CSE A,B &amp; C</td>
			</tr>
			<tr>
			<td width="6%">2.</td>
			<td width="12%">06.07.2015 (At10.00pm)</td>
			<td width="21%">Industrial Visit on “C-DAC”</td>
			<td width="36%">The Director Tidel Park, Taramani, Chennai, Tamil Nadu 600113 Cell No:&nbsp;9443500550 Email&nbsp;&nbsp;:&nbsp;sridh@cdac.in</td>
			<td width="20%">III CSE A,B,&amp; C</td>
			</tr>
			<tr>
			<td width="6%">3.</td>
			<td width="12%">15.08.2015 &nbsp;</td>
			<td width="21%">Industrial Visit on “AADYA IT SOLUTIONS PVT Ltd” &amp; “EXCEL SOFT Ltd” &nbsp;</td>
			<td width="36%">Mr.B.Senthil HR Manager Binary Software Solution Pvt Ltd 101,Anikethan Main Road, Kuvempunagar,Mysore, Karanataka 570023 senthilb@aadyaitsolutions.in Mr.S.Thiyagu HR Manager Mysore, India (HQ) 1-B, Hootagalli Industrial Area, Mysore – 570 018 Tel : +91 821 428 2000</td>
			<td width="20%">IV CSEA,B &amp; C</td>
			</tr>
			<tr>
			<td width="6%">4</td>
			<td width="12%">29.1.2016</td>
			<td width="21%">Industrial Visit on “Computer Networks”</td>
			<td width="36%">R.Baskar SDE Bharath Sanchar Nigam Limited Regional Telecom Chennai-209.</td>
			<td width="20%">II CSE A,B &amp; C</td>
			</tr>
			<tr>
			<td width="6%">5</td>
			<td width="12%">15.3.2016</td>
			<td width="21%">Industrial Visit on “Mobile Computing”</td>
			<td width="36%">V. Usha Divisional Engineer (Training Co-Ordinator) Harbour Telecom Building, BSNL Chennai-1 Cell:9444971261</td>
			<td width="20%">III CSE C</td>
			</tr>
			<tr>
			<td width="6%">6</td>
			<td width="12%">16.3.2016</td>
			<td width="21%">Industrial Visit on “Mobile Computing”</td>
			<td width="36%">V. Usha Divisional Engineer (Training Co-Ordinator) Harbour Telecom Building, BSNL Chennai-1 Cell:9444971261</td>
			<td width="20%">III CSE A &amp; B</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2014-2015</span>
			<table width="679">
			<tbody>
			<tr>
			<td width="48"><strong>&nbsp;Sl.no</strong></td>
			<td width="83"><strong>Date of Event</strong></td>
			<td width="156"><strong>Topic/Company</strong></td>
			<td width="244"><strong>&nbsp;</strong> <strong>Resource Person Details</strong> <strong>&nbsp;</strong></td>
			<td width="137"><strong>&nbsp;</strong> <strong>Students Attended</strong></td>
			</tr>
			<tr>
			<td width="48">1.</td>
			<td width="83">21.8.2014</td>
			<td width="156">1.“Chips Software Solutions”, Ernakulum, Kochi. &nbsp; 2. “Focus InfoTech”, Kochi. &nbsp;</td>
			<td width="244">1. Mr.T.U.K. Menon, Director, Chips Software Solutions, Ernakulum, Kochi. &nbsp; 2. Ms.Sangeetha Raj, Manager Technology &amp; Delivery, Focus InfoTech, Kochi.</td>
			<td width="137">IV CSE A</td>
			</tr>
			<tr>
			<td width="48">2.</td>
			<td width="83">21.8.2014, 23.8.2014</td>
			<td width="156">1.“Centre for Development of Advanced Computing”, Bangalore. &nbsp; 2. “Binary Software Solutions”, Coimbatore.</td>
			<td width="244">1.Mr.Amit Kumar, Project Engineer, CDAC, Bangalore. &nbsp; 2.Mr.C.Magesh, Project Engineer, Binary Software Solutions, Coimbatore..</td>
			<td width="137">IV CSE B</td>
			</tr>
			<tr>
			<td width="48">3.</td>
			<td width="83">24.9.2014</td>
			<td width="156">BSNL-Meenambakkam</td>
			<td width="244">Mr.Baskar Sub Divisional Engineer BSNL, Meenambakkam Mr. Raj Kumar, Sub Divisional Engineer BSNL, Meenambakkam Mob no : 9486103196</td>
			<td width="137">II CSE A, B &amp; C</td>
			</tr>
			<tr>
			<td width="48">4</td>
			<td width="83">03.2.2015</td>
			<td width="156">Industrial Visit on “High Power Transmitter”,</td>
			<td width="244">Mrs.M.Sujatha, Assistant Engineer High Power Transmitter, AIR, Avadi, Chennai-62 airavadi@rediffmail.com</td>
			<td width="137">III CSE C</td>
			</tr>
			<tr>
			<td width="48">5</td>
			<td width="83">04.2.2015</td>
			<td width="156">Industrial Visit on “High Power Transmitter”</td>
			<td width="244">Mrs.M.Sujatha, Assistant Engineer High Power Transmitter, AIR, Avadi, Chennai-62 airavadi@rediffmail.com</td>
			<td width="137">III CSE A</td>
			</tr>
			<tr>
			<td width="48">6</td>
			<td width="83">05.2.2015</td>
			<td width="156">Industrial Visit on “High Power Transmitter”</td>
			<td width="244">Mrs.M.Sujatha, Assistant Engineer High Power Transmitter, AIR, Avadi, Chennai-62 airavadi@rediffmail.com</td>
			<td width="137">III CSE B</td>
			</tr>
			<tr>
			<td width="48">7</td>
			<td width="83">12.3.2015</td>
			<td width="156">Industrial Visit on “Sansbound Solutions Pvt.Ltd.”</td>
			<td width="244">Mr.S.Karthikeyan, SANSBOUND SOLUTIONS PVT LTD, No.40,Ganapathy Complex, Circular Road, United India Colony, Kodambakkam, Chennai-24.</td>
			<td width="137">II CSE A &amp;B</td>
			</tr>
			<tr>
			<td width="48">8</td>
			<td width="83">19.3.2015</td>
			<td width="156">Industrial Visit on “Sansbound Solutions Pvt.Ltd.”</td>
			<td width="244">Mr.S.Karthikeyan, SANSBOUND SOLUTIONS PVT LTD, No.40,Ganapathy Complex, Circular Road, United India colony, Kodambakkam, Chennai24</td>
			<td width="137">II CSE C &amp; D</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
		<span>2013-2014</span>
			<table width="668">
			<tbody>
			<tr>
			<td width="52">Sl.no</td>
			<td width="113">Date of Event</td>
			<td width="148">Topic/Company</td>
			<td width="236">&nbsp; Resource Person Details &nbsp;</td>
			<td width="119">&nbsp; Students Attended</td>
			</tr>
			<tr>
			<td width="52">1</td>
			<td width="113">25.7.13</td>
			<td width="148">SOFTWARE TECHNOLOGY PARKS OF INIDA,CHENNAI</td>
			<td width="236">Mr.J.PARTHASARATHY, Director,STPI, Taramani,Chennai</td>
			<td width="119">III/CSE A</td>
			</tr>
			<tr>
			<td width="52">2</td>
			<td width="113">26.7.13</td>
			<td width="148">SOFTWARE TECHNOLOGY PARKS OF INIDA,CHENNAI</td>
			<td width="236">Mr.J.PARTHASARATHY, Director,STPI, Taramani,Chennai</td>
			<td width="119">II /CSE B</td>
			</tr>
			<tr>
			<td width="52">3</td>
			<td width="113">21.8.13 to 25.8.13</td>
			<td width="148">EDUCATIONAL TOUR TO KERALA,COCHIN</td>
			<td width="236">FOCUS INFOTECH, COCHIN</td>
			<td width="119">IV/CSE (A &amp; B)</td>
			</tr>
			<tr>
			<td width="52">4</td>
			<td width="113">13.2.14 &amp; 20.2.14</td>
			<td width="148">Sansbound Solutions Pvt Ltd</td>
			<td width="236">Mr. Karthikeyan, Business development manager, Sansbound Solutions, Pvt, Ltd.,Chennai.</td>
			<td width="119">III CSE A &amp; B</td>
			</tr>
			<tr>
			<td width="52">5</td>
			<td width="113">3.4.2014</td>
			<td width="148">Doordarshan Kendra</td>
			<td width="236">Shri.V.Gnanarajan, Doordarshan Kendra,Chennai.</td>
			<td width="119">II CSE A,B,C</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
		<h4>Seminars/Workshops/Conferences</H4>
			<h3>DETAILS OF SEMINAR, WORKSHOP, CONFERENCES, CEP, FDP, QIP PROGRAMS ORGANIZED</h3>
			<span>2016-2017</span>
			<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img1.png" data-fancybox-group="SWC1", title="Department of Computer Science and Engineering signed MOU with XILICON on 03-11-16">
                                    <img src="<?php echo $siteurl ?>/img/img1.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img2.png" data-fancybox-group="SWC1", title="CSE Department Conducted National Level Technical Symposium-NAKSHATRA’16 on 13-08- 16 addressed by Mr.S.R.Swaminathan, Senior Director, Virtusa, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img2.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img3.png" data-fancybox-group="SWC1", title="Department of Computer Science and Engineering organized a International Conference on Information Communication and Embedded Systems (ICICES2017) on 23-4-17 addressed by Mr.H.R.Mohan, Vice Chairman, IEEE Madras Section, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img3.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img4.png" data-fancybox-group="SWC1", title="National Level Workshop on Mathematical Modeling of Wireless Networks sponsored by IEEE Madras Section in association with IEEE on 28-07- 2016 by Dr.K.Anitha,Maths Dept, S.A Engineering College, Chennai & Ms.G.Uma, VIT, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img4.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img5.png" data-fancybox-group="SWC1", title="Workshop on INTERNET OF THINGS (IOT) in association with Computer Society of India (CSI) on 30-08-2016 addressed by Mr.Rajamanikam, Bits ‘n Watts Embedded Solutions, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img5.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img6.png" data-fancybox-group="SWC1", title="Department of Science & Technology Government of India Sponsored National Workshop on DATA SCIENCE RESEARCH from 20-12-16 to 22-12-16 addressed by Mr.S.Srinivasan, Delivery Manager, Syntel, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img6.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img7.png" data-fancybox-group="SWC1", title="Engineering Society Sponsored National Level Workshop on NS2 for Network Security in association with IEEE on 23-09-2016 handled by Ms.R.Manimekala, IIS Technologies, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img7.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img8.png" data-fancybox-group="SWC1", title="DepartmeFaculty Development Programme on TEAM BUILDING in association with ICTACT from 25-07- 16 and 26-07-2016 handled by Nirmal Kumar.K, ICT Academy of Tamil Nadu, ELCOT Complex, Industrial Estate, Perungudi Chennaint of Computer Science and Engineering signed MOU with XILICON on 03-11-16">
                                    <img src="<?php echo $siteurl ?>/img/img8.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img9.png" data-fancybox-group="SWC1", title="Seminar on Engineer to Entrepreneur In Associated with IEEE on 09-08-2016 Guest : Mr.Shanthalingam, Deputy director, MSME-Di, Govt of India and Mr.Karthik, Alumni Entrepreneur">
                                    <img src="<?php echo $siteurl ?>/img/img9.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img10.png" data-fancybox-group="SWC1", title="Training Programme on ORACLE CERTIFIED JAVA ASSOCIATE organized from 23-06-16 and 24-06-16 addressed by Mr.Vinoth Subramanian, Chief Operation Officer, Silicon Software Services, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img10.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img11.png" data-fancybox-group="SWC1", title="Seminar on AVOIDING PLAGIARISM on 03-08- 2016 Guest : Dr.Vydeki Vijayakumar, Chair, Madras IEEE WIE & Dr.R.Sakkaravarthi, CSE, VIT, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img11.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img12.png" data-fancybox-group="SWC1", title="Value Added Programme on PHP was organized from 11-07-16 to 19-07-16 (6 days), handled by Mr.Meiyappan, Head, Eye open Technologies, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img12.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img13.png" data-fancybox-group="SWC1", title="Value Added Programme on Mobile Application Development was organized from 20-02-17 to 28- 02-17 (6 days), handled by Mrs.Ponmalar, Corporate Trainer, Eye open Technologies, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img13.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img14.png" data-fancybox-group="SWC1", title="CSI Foundation Day on 06.03.2017">
                                    <img src="<?php echo $siteurl ?>/img/img14.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img15.png" data-fancybox-group="SWC1", title="Value Added Programme on Big Data Analytics from 06-09-16 to 22-09-16 (6 days), handled by Mr.Saravanan Data Engineer, DATA DOTZ, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img15.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img16.png" data-fancybox-group="SWC1", title="CSI Awareness Programme on 22.02.2017 delivered by Mr.Y.Kathiresan, (Ex-Senior manager, CSI Promotions, Computer Society Of India) Director Groomtech Solutions, Chennai.">
                                    <img src="<?php echo $siteurl ?>/img/img16.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
		</div>
		<div>
			<span>2015-2016</span>
			<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img17.jpg" data-fancybox-group="SWC2", title="CSI Awareness Programme On 24-09-2015delivered By Mr.Y.Kathiresan,Senior Manager, CSI Promotions, CSI">
                                    <img src="<?php echo $siteurl ?>/img/img17.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img18.jpg" data-fancybox-group="SWC2", title="Workshop on “MATLAB and its Applications in Computational Intelligence” on  27-08-2015 with Mr. Vinodh Kumar,  ZI Technologies, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img18.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img19.jpg" data-fancybox-group="SWC2", title="National Level Technical Symposium-NAKSHATRA’15 on 08.04.2017 inaugurated by Mr.AswinSadasivaKumar,Global Practice Head,Virtusa,Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img19.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img20.jpg" data-fancybox-group="SWC2", title="Workshop on “MOBILE APPLICATION DEVELOPMENT” from 21-03-16 to 22-03-16 with Ms.Ponmalar, Corporate Trainer, Eye open technologies, Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img20.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img21.jpg" data-fancybox-group="SWC2", title="CSI FOUNDATION DAY celebration on 07-03-2016">
                                    <img src="<?php echo $siteurl ?>/img/img21.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
			</div>
			
		</div>
		<div>
			<table width="692">
			<tbody>
			<tr>
			<td width="126"><strong>&nbsp;</strong>
			<p style="text-align: left;"><strong>Type of the programme</strong></p>
			</td>
			<td width="90"><strong>&nbsp;</strong> <strong>Date</strong></td>
			<td width="220"><strong>&nbsp;</strong> <strong>Name of the resource person with designation </strong></td>
			<td width="255"><strong>&nbsp;</strong> <strong>Title of the programme</strong></td>
			</tr>
			<tr>
			<td width="126">CSI AWARENESS PROGRAMME</td>
			<td width="90">&nbsp; 24-09-2015</td>
			<td width="220">Mr.Y.Kathiresan, Senior Manager,CSI Promotions, Computer Society Of India</td>
			<td width="255">CSI Awareness Programme <strong>(In association with CSI)</strong></td>
			</tr>
			<tr>
			<td width="126">WORKSHOP</td>
			<td width="90">27-08-2015</td>
			<td width="220">Mr.Vinodh Kumar, ZiTechnologies,Chennai</td>
			<td width="255">Workshop on “MATLAB and its Applications in Computational Intelligence” <strong>(In Association with IEEE)</strong></td>
			</tr>
			<tr>
			<td width="126">SYMPOSIUM</td>
			<td width="90">08-08-2015</td>
			<td width="220">Mr.Aswin Sadasiva Kumar, Global Practice Head, Virtusa, Chennai.</td>
			<td width="255">National Level Technical Symposium-NAKSHATRA’15 <strong>(In association with CSI)</strong></td>
			</tr>
			<tr>
			<td rowspan="3" width="126">WORKSHOP</td>
			<td width="90">23.03.2016</td>
			<td width="220">ACM-W Sponsored Event under ACM-W Networking Grant on“TAKE APART YOUR COMPUTER” School, Veeraragavapuram</td>
			<td width="255">Networking&nbsp; team of S.A.Engineering College <strong>(</strong>at Sudharsanam Vidyaashram CBSE <strong>Associated with ACM)</strong></td>
			</tr>
			<tr>
			<td width="90">24.03.2016</td>
			<td width="220">Writing Effective Research Paper For journals at S.A.Polytechnic College, Thiruverkadu</td>
			<td width="255">R&amp;D team of S.A.Engineering College <strong>(Associated with ACM)</strong></td>
			</tr>
			<tr>
			<td width="90">21-03-2016 &amp; 22-03-2016</td>
			<td width="220">Ms.Ponmalar, Corporate Trainer, Eye Open Technologies,Chennai.</td>
			<td width="255">Workshop on”MOBILE APPLICATION DEVELOPMENT” <strong>(In association with CSI)</strong></td>
			</tr>
			<tr>
			<td width="126">CSI Foundation day</td>
			<td width="90">07-03-2016</td>
			<td width="220">Internal Members: Professional Body Coordinator,Department CSI Coordinators, CSI Student Branch Counselor</td>
			<td width="255">“CSI FOUNDATION DAY” (Web Design, Paper Presentation, Debugging) <strong>(In association with CSI)</strong></td>
			</tr>
			<tr>
			<td width="126">CSI-In House Project</td>
			<td width="90">07-03-2016</td>
			<td width="220">Juries: Dr E.A. Mary Anita, Prof/CSE Dept. Dr M.Subramaniam, Prof/IT Dept. Ms V.Sujatha, Assoc. Prof. /MCA</td>
			<td width="255">In House Project in “WEB DESIGN CONTEST” <strong>(In association with CSI)</strong> <strong>&nbsp;</strong> <strong>&nbsp;</strong></td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2014-2015</span>
			<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img22.jpg" data-fancybox-group="SWC3", title="CSI Awareness Programme On 12.03.2015 delivered By Mr.Y.Kathiresan,Senior Manager, CSI Promotions, CSI">
                                    <img src="<?php echo $siteurl ?>/img/img22.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img23.jpg" data-fancybox-group="SWC3", title="National Conference On ”Emerging Trends In Computing”-ETIC 2015 On 06.03.2015 Inaugurated By Dr.A.Kannan, HOD/IT, Anna University,Chennai">
                                    <img src="<?php echo $siteurl ?>/img/img23.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img24.jpg" data-fancybox-group="SWC3", title="Workshop On “Effective Paper Writing Using LATEX” On 24.09.2014 Inaugurated By Dr. REVATHI VENKATRAMAN, M.E.,Ph.D.">
                                    <img src="<?php echo $siteurl ?>/img/img24.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/img125.jpg" data-fancybox-group="SWC3", title="Workshop On “Wireless Sensor Networks” On 03.09.2014 Inaugurated By Prof.J.Santhana Krishnan Adjunct Professor,Nit,Nagaland">
                                    <img src="<?php echo $siteurl ?>/img/img25.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
			</div>
			
		</div>
		<div>
			<table width="692">
			<tbody>
			<tr>
			<td width="129"><strong>&nbsp;</strong>
			<p style="text-align: left;"><strong>Type of the programme</strong></p>
			</td>
			<td width="90"><strong>&nbsp;</strong> <strong>Date</strong></td>
			<td width="217"><strong>&nbsp;</strong> <strong>Name of The Resource Person with Designation </strong></td>
			<td width="255"><strong>&nbsp;</strong> <strong>Title of the programme</strong></td>
			</tr>
			<tr>
			<td width="129">CSI AWARENESS PROGRAMME</td>
			<td width="90">12-03-2015</td>
			<td width="217">Mr.Y.Kathiresan, Senior Manager, CSI Promotions, CSI</td>
			<td width="255">CSI Awareness Program <strong>(Associated With CSI)</strong></td>
			</tr>
			<tr>
			<td width="129">NATIONAL CONFERENCE</td>
			<td width="90">06-03-2015</td>
			<td width="217">Mr.B.Veeraraghavan, Regional Manager, Career Education South India/ Srilanka, Ibm, Southern Group- India</td>
			<td width="255">8<sup>th</sup> National Conference On ”Emerging Trends In Computing”-Etic 2015 <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="129">FDP</td>
			<td width="90">17-12-2014 to 23-12-2014</td>
			<td width="217">Dr.V.Vaidehi, Professor, Mit, Anna University,Chennai</td>
			<td width="255">Faculty Development Programme On “Recent&nbsp; Trends In Wireless Sensor Networks”<strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="129">WORKSHOP</td>
			<td width="90">24-09-2014</td>
			<td width="217">Dr. Revathi Venkatraman, M.E.,Ph.D., Dr. M. Pushpalatha, M.E.,Ph.D.,Srm University, Chennai</td>
			<td width="255">One Day Workshop On “Effective Paper Writing Using Latex” <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="129">WORKSHOP</td>
			<td width="90">03-09-2014</td>
			<td width="217">Prof.J.Santhana Krishnan Adjunct Professor, Nit, Nagaland Mr.A.Sadagopan Cto,Knet Solutions,Chennai.</td>
			<td width="255">One Day Workshop On “Wireless Sensor Networks” <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="129">SYMPOSIUM</td>
			<td width="90">14-08-2014</td>
			<td width="217">Mr.Kalilur Rahman, Vice President, Technology-Communication,Media And High Tech,Accenture, Chennai</td>
			<td width="255">Technical Symposium-Exousia’14 <strong>(Associated with CSI)</strong></td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2013-2014</span>
			<table width="692">
			<tbody>
			<tr>
			<td style="text-align: left;" width="144"><strong>Type of the programme</strong></td>
			<td width="95"><strong>&nbsp;</strong><strong>Date</strong></td>
			<td width="197"><strong>&nbsp;</strong><strong>Name of the resource person with designation </strong></td>
			<td width="255"><strong>&nbsp;</strong><strong>Title of the programme</strong></td>
			</tr>
			<tr>
			<td width="144">FACULTY DEVELOPMENT PROGRAM</td>
			<td width="95">08-05-2014 &amp; 09-05-2014</td>
			<td width="197">Dr.N.Kumarappan Professor of Electrical Engineering, Faculty of Engg. &amp; Tech. Annamalai University, Annamalai Nagar, Chindabaram</td>
			<td width="255">Faculty Development Program On “Soft Computing And Simulation For Power Engineering” <strong>(Association With IEEE)</strong></td>
			</tr>
			<tr>
			<td width="144">NATIONAL CONFERENCE</td>
			<td width="95">19-02-2014 <strong>&nbsp;</strong></td>
			<td width="197">Dr.R.Ranjani Parthasarathi Professor, Department Of Information Science And Technology, Anna University,Chennai.</td>
			<td width="255">National Conference On “Next Generation Computing Technologies” <strong>(Associated with CSI)</strong> <strong>&nbsp;</strong></td>
			</tr>
			<tr>
			<td width="144">WORKSHOP</td>
			<td width="95">23-09-2013</td>
			<td width="197">Mr.R.Kumaraswamy, Senior Engineer, Pantech Solutions, Duraisamy Road, T.Nagar.</td>
			<td width="255">Workshop On NS2 &amp; MAT Lab <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="144">STATE LEVEL&nbsp; SEMINAR</td>
			<td width="95">06-09-2013</td>
			<td width="197">Shri.K.K.Kuriakose Head,Computer Division And Shri R Jeha Deesan Senior Scientific Officer-G,Indira Gandhi Centre For Atomic Research ,Chennai.</td>
			<td width="255">State Level Seminar On Recent Trends In Computing <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="144">SYMPOSIUM</td>
			<td width="95">20-08-2013</td>
			<td width="197">Mr.A.K.Pattabiraman, Head Accrediation Process,Southern Region,Tcs,Chennai</td>
			<td width="255">National Level Technical Symposium Exousia’13 <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="144">GUEST LECTURE</td>
			<td width="95">02-08-2013</td>
			<td width="197">Mr.M.Prakash,System Engineer,Tata Consultancy Services,Chennai.</td>
			<td width="255">Guest Lecture On Android Technology <strong>(Associated with CSI)</strong></td>
			</tr>
			<tr>
			<td width="144">GUEST LECTURE</td>
			<td width="95">19-07-2013</td>
			<td width="197">Dr.B.Muthukumaran, Head, Operation &amp; Information Security, Deputy General Manager, HTC Institute of Management &amp; Research, Chennai.</td>
			<td width="255">Guest Lecture On Network Security – Ns2 <strong>(Associated with CSI)</strong></td>
			</tr>
			</tbody>
			</table>
		</div>
		<div>
			<span>2012-2013</span>
			<table width="692">
				<tbody>
				<tr>
				<td width="135"><strong>&nbsp;</strong>
				<p style="text-align: left;"><strong>Type of the programme</strong></p>
				</td>
				<td width="103"><strong>&nbsp;</strong> <strong>Date</strong></td>
				<td width="198"><strong>&nbsp;</strong> <strong>Name of the resource person with designation </strong></td>
				<td width="255"><strong>&nbsp;</strong> <strong>Title of the programme</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>NATIONAL CONFERENCE</strong></td>
				<td width="103">27-03-2013 &amp; 28-03-2013</td>
				<td width="198">Dr.P.Sivakumar, Director, CVRDE, DRDO</td>
				<td width="255">DRDO Sponsored National Conference</td>
				</tr>
				<tr>
				<td width="135"><strong>WORKSHOP</strong> <strong>&nbsp;</strong></td>
				<td width="103">15-03-2013</td>
				<td width="198">Mr.T.S.Pradeep Kumar, Ns2 Trainer, Chennai.</td>
				<td width="255">Workshop on “Network Simulator-Ns2” <strong>(Associated with CSI)</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>&nbsp;</strong> <strong>WORKSHOP</strong> <strong>&nbsp;</strong> <strong>&nbsp;</strong></td>
				<td width="103">13-02-2013 &amp; 14-02-2013</td>
				<td width="198">Mayur Dev Sewak, Business Manager, Ei Systems Services, Chennai.</td>
				<td width="255">Workshop On “Android OS With Mobile Application Development” <strong>(Associated with CSI)</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>GUEST LECTURE</strong> <strong>&nbsp;</strong> <strong>&nbsp;</strong></td>
				<td width="103">29-01-2013</td>
				<td width="198">Dr.Padmanaban Ramasamy, Director Of Nexgtech, Wireless R&amp;D Lab.</td>
				<td width="255">Guest Lecture On “Grid Computing &amp; Wireless Sensor Networks” <strong>(Associated with CSI)</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>NATIONAL CONFERENCE</strong> <strong>&nbsp;</strong></td>
				<td width="103">23-01-2013</td>
				<td width="198">Mr.Chandrasekhar Chenniappan,Head HR-India, Atmel Corporation,Chennai.</td>
				<td width="255">National Conference On “Advanced Computing And Networking Ncacnet 2013” <strong>(Associated with CSI)</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>STATE LEVEL SEMINAR</strong> <strong>&nbsp;</strong> <strong>&nbsp;</strong></td>
				<td width="103">31-08-2012</td>
				<td width="198">Ms.T.Jayanthi, Head Simulator Section, IGCAR Kalpakkam. Ms.Jemimah Ebenezer Senoir Scientific Officer IGCAR,Kalpakkam</td>
				<td width="255">State Level Symposium On Recent Trends In Computing &amp; Communication <strong>(Associated with CSI)</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>&nbsp;</strong> <strong>SYMPOSIUM</strong> <strong>&nbsp;</strong> <strong>&nbsp;</strong> <strong>&nbsp;</strong></td>
				<td width="103">22-08-2012</td>
				<td width="198">Mr,Paarul Sood, Centre Head Training (Learning &amp;Education), Bangalore.</td>
				<td width="255">National Level Technical Symposium-Exousia’12” <strong>(Associated with CSI)</strong></td>
				</tr>
				<tr>
				<td width="135"><strong>WORKSHOP</strong></td>
				<td width="103">&nbsp; 01-08-2012</td>
				<td width="198">Mr.A.Asokan,B.Com,Mba.IT Senior Manager, Sansbound,Networking Solutions Pvt.Ltd., Chennai</td>
				<td width="255">Workshop On “Computer Networking Theory &amp; Practice” <strong>(Associated with CSI)</strong></td>
				</tr>
				</tbody>
				</table>
		</div>
		<div>
			<h4><strong>MoUs</strong></h4>
			<span>LIST OF MOUS SIGNED</span>
			<div>
				<table width="603">
				<tbody>
				<tr>
				<td width="51"><strong>&nbsp;</strong><p></p>
				<p><strong>S.NO</strong></p></td>
				<td width="391"><strong>&nbsp;</strong><p></p>
				<p><strong>NAME OF THE ORGANIZATION / COMPANY / INSTITUTE</strong></p></td>
				<td width="161"><strong>DATE OF MOU SIGNED</strong></td>
				</tr>
				<tr>
				<td width="51">1.</td>
				<td width="391">Xilicon Business Services Private Ltd.</td>
				<td width="161">03-11-2016</td>
				</tr>
				<tr>
				<td width="51">2.</td>
				<td width="391">Eyeopen Technologies</td>
				<td width="161">18-02-2016</td>
				</tr>
				<tr>
				<td width="51">3.</td>
				<td width="391">Oracle India&nbsp; Private Limited</td>
				<td width="161">21-01-2016</td>
				</tr>
				<tr>
				<td width="51">4.</td>
				<td width="391">Perfint Healthcare Private Limited</td>
				<td width="161">04-01-2016</td>
				</tr>
				<tr>
				<td width="51">5.</td>
				<td width="391">Centre for Development of Advanced Computing(CDAC)</td>
				<td width="161">22-04-2015</td>
				</tr>
				<tr>
				<td width="51">6.</td>
				<td width="391">Milestone Edcom</td>
				<td width="161">12-09-2014</td>
				</tr>
				<tr>
				<td width="51">7.</td>
				<td width="391">IBM India Private Limited</td>
				<td width="161">13-08-2014</td>
				</tr>
				<tr>
				<td width="51">8.</td>
				<td width="391">Pantech ProEd&nbsp; Private Limited</td>
				<td width="161">09-07-2013</td>
				</tr>
				<tr>
				<td width="51">9.</td>
				<td width="391">Sybrant Technologies&nbsp; Private Limited</td>
				<td width="161">13-05-2013</td>
				</tr>
				<tr>
				<td width="51">10.</td>
				<td width="391">IEEE</td>
				<td width="161">07-02-2013</td>
				</tr>
				<tr>
				<td width="51">11.</td>
				<td width="391">Soft Square&nbsp; Solutions</td>
				<td width="161">04-01-2013</td>
				</tr>
				<tr>
				<td width="51">12.</td>
				<td width="391">Excelacom India Private Limited</td>
				<td width="161">31-05-2012</td>
				</tr>
				<tr>
				<td width="51">13.</td>
				<td width="391">Global Techno Solutions</td>
				<td width="161">11-03-2008</td>
				</tr>
				<tr>
				<td width="51">14.</td>
				<td width="391">Noblecare IT Solutions</td>
				<td width="161">05-01-2005</td>
				</tr>
				<tr>
				<td width="51">15.</td>
				<td width="391">Maples ESM Technologies Private Limited</td>
				<td width="161">11-11-2003</td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>

			
		<div>
		<h4>Placements &amp; Higher Studies</h4>
		<h3>LIST OF STUDENTS PLACED THROUGH CAMPUS PLACEMENT</h3>
		<span>ACADEMIC YEAR 2017 – 18</span>
		<p>CAPGEMINI</p>
		<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p1.jpg" data-fancybox-group="teaching1", title=" ASHWIN M ">
                                    <img src="<?php echo $siteurl ?>/img/p1.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p2.jpg" data-fancybox-group="teaching1", title="BALAJI.S">
                                    <img src="<?php echo $siteurl ?>/img/p2.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p3.jpg" data-fancybox-group="teaching1", title="EZHILARASI.V">
                                    <img src="<?php echo $siteurl ?>/img/p3.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p4.jpg" data-fancybox-group="teaching1", title="VISHNU RAJAN R">
                                    <img src="<?php echo $siteurl ?>/img/p4.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p5.jpg" data-fancybox-group="teaching1", title="REJINA NJ">
                                    <img src="<?php echo $siteurl ?>/img/p5.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p6.jpg" data-fancybox-group="teaching1", title="PREETHI R">
                                    <img src="<?php echo $siteurl ?>/img/p6.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/p7.png" data-fancybox-group="teaching1", title="VIVEK.V">
                                    <img src="<?php echo $siteurl ?>/img/p7.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
		
		
		</div>
		<div>
			<p>MINDTREE</p>
						<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/akhil.png" data-fancybox-group="teaching2" title=" AKHIL">
                                    <img src="<?php echo $siteurl ?>/img/akhil.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						
						
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/kir.png" data-fancybox-group="teaching2" title="KARTHIKEYAN P.S">
                                    <img src="<?php echo $siteurl ?>/img/kir.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/rajes.png" data-fancybox-group="teaching2" title="RAJESWARI T">
                                    <img src="<?php echo $siteurl ?>/img/rajes.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
			
		</div>
		<div>
		
			<p>AURUM INFO SOLUTIONS</p>
						<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/joel.png" data-fancybox-group="teaching2" title="JOEL NATHANIEL">
                                    <img src="<?php echo $siteurl ?>/img/joel.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						
						
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/karthikeyans.png" data-fancybox-group="teaching2" title="KARTHIKEYAN.S">
                                    <img src="<?php echo $siteurl ?>/img/karthikeyans.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/saravana.png" data-fancybox-group="teaching2" title="SARAVANA PERUMAL. A">
                                    <img src="<?php echo $siteurl ?>/img/saravana.png" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
			</div>
			<div>
			<span>Academic Year 2016 – 17</span>
			<p>KAAR TECHNOLOGIES</p>
						<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/abhishek.jpg" data-fancybox-group="teaching3" title="ABHISHEK SRIVASTAVA">
                                    <img src="<?php echo $siteurl ?>/img/abhishek.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
				<div>
					<p>CAPGEMINI</p>
					<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/adithya.jpg" data-fancybox-group="teaching4" title="ADITHYA PRIYADHARSHNI">
                                    <img src="<?php echo $siteurl ?>/img/adithya.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/angelin.jpg" data-fancybox-group="teaching4" title="ANGELIN JACINTH.S">
                                    <img src="<?php echo $siteurl ?>/img/angelin.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/celes.jpg" data-fancybox-group="teaching4" title="CELES MONICA.B">
                                    <img src="<?php echo $siteurl ?>/img/celes.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/eswaran.jpg" data-fancybox-group="teaching4" title="ESWARAN.S">
                                    <img src="<?php echo $siteurl ?>/img/eswaran.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/grace.jpg" data-fancybox-group="teaching4" title="GRACE PRIYA SHALINI.S">
                                    <img src="<?php echo $siteurl ?>/img/grace.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/nandhini.jpg" data-fancybox-group="teaching4" title="NANDHINI.A">
                                    <img src="<?php echo $siteurl ?>/img/nandhini.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/nandhinic.jpg" data-fancybox-group="teaching4" title="NANDHINI.C">
                                    <img src="<?php echo $siteurl ?>/img/nandhinic.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/pavithra.jpg" data-fancybox-group="teaching4" title="PAVITHRA.T.V">
                                    <img src="<?php echo $siteurl ?>/img/pavithra.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/preethi.jpg" data-fancybox-group="teaching4" title="PREETHI.R">
                                    <img src="<?php echo $siteurl ?>/img/preethi.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/reshma.jpg" data-fancybox-group="teaching4" title="RESHMA.B">
                                    <img src="<?php echo $siteurl ?>/img/reshma.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/revathi.jpg" data-fancybox-group="teaching4" title="REVATHI.V.B">
                                    <img src="<?php echo $siteurl ?>/img/revathi.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/shobana.jpg" data-fancybox-group="teaching4" title="SHOBANA.G">
                                    <img src="<?php echo $siteurl ?>/img/shobana.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/sindhu.jpg" data-fancybox-group="teaching4" title="SINDHU.V">
                                    <img src="<?php echo $siteurl ?>/img/sindhu.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/sugandhi.jpg" data-fancybox-group="teaching4" title="SUGANTHI.M">
                                    <img src="<?php echo $siteurl ?>/img/sugandhi.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/sushmitha.jpg" data-fancybox-group="teaching4" title="SUSHMITHA.R">
                                    <img src="<?php echo $siteurl ?>/img/sushmitha.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
					</div>
							
				</div>
				<div>
					<p>CALIBRANT TECHNOLOGIES</p>
						<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/archana.jpg" data-fancybox-group="teaching5" title="ARCHANA">
                                    <img src="<?php echo $siteurl ?>/img/archana.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
				</div>
				<div>
					<p>VIRTUSA POLARIS</p>
					<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/deepak.jpg" data-fancybox-group="teaching6" title="DEEPAK.M">
                                    <img src="<?php echo $siteurl ?>/img/deepak.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/sharmila.jpg" data-fancybox-group="teaching6" title="SHARMILA.K">
                                    <img src="<?php echo $siteurl ?>/img/sharmila.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/shree.jpg" data-fancybox-group="teaching6" title="SHREE VARSHA">
                                    <img src="<?php echo $siteurl ?>/img/shree.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
						</div>
				</div>
				<div>
					<p>ASPIRE SYSTEMS</p>
					<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/arun.jpg" data-fancybox-group="teaching7" title="ARUN KUMAR.S">
                                    <img src="<?php echo $siteurl ?>/img/arun.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
					</div>
				</div>
				<div>
					<p>MICROMEN SOFTWARE SOLUTIONS</p>
					<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/keerthana.jpg" data-fancybox-group="teaching8" title="KEERTHANA.S">
                                    <img src="<?php echo $siteurl ?>/img/keerthana.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
					</div>
				</div>
				<div>
					<p>CSS CORP</p>
					<div class="ach_sec clearfix">
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/madhu.jpg" data-fancybox-group="teaching9" title="MADHUBALA.M">
                                    <img src="<?php echo $siteurl ?>/img/madhu.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/mubarak.jpg" data-fancybox-group="teaching9" title="MUBARAK.S">
                                    <img src="<?php echo $siteurl ?>/img/mubarak.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/saa.jpg" data-fancybox-group="teaching9" title="SAAKETHA.P">
                                    <img src="<?php echo $siteurl ?>/img/saa.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/akshaya.jpg" data-fancybox-group="teaching9" title="AKSHAYA.S">
                                    <img src="<?php echo $siteurl ?>/img/akshaya.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/ancy.jpg" data-fancybox-group="teaching9" title="ANCY.J">
                                    <img src="<?php echo $siteurl ?>/img/ancy.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/avinash.jpg" data-fancybox-group="teaching9" title="AVINASH.B">
                                    <img src="<?php echo $siteurl ?>/img/avinash.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/sus.jpg" data-fancybox-group="teaching9" title="SUSHMITA.P">
                                    <img src="<?php echo $siteurl ?>/img/sus.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
							<div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/swetha.jpg" data-fancybox-group="teaching9" title="SWETHA.S">
                                    <img src="<?php echo $siteurl ?>/img/swetha.jpg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
					</div>
					
				</div>
				
			</div>
			<div>
				<h3>Consolidation of Placed, Higher Studies, Entrepreneur Count</h3>
				<div>
					<table width="832">
					<tbody>
					<tr>
					<td colspan="8" width="832">&nbsp;</td>
					</tr>
					<tr>
					<td width="126">Academic Year</td>
					<td width="65">In Take</td>
					<td width="88">Actual strength</td>
					<td width="75">Placed</td>
					<td width="85">ON Campus</td>
					<td width="77">OFF Campus</td>
					<td width="106">Higher Studies</td>
					<td width="209">Entrepreneurs</td>
					</tr>
					<tr>
					<td width="126">2011 – 2012</td>
					<td width="65">120</td>
					<td width="88">130</td>
					<td width="75">99</td>
					<td width="85">60</td>
					<td width="77">39</td>
					<td width="106">17</td>
					<td width="209">02</td>
					</tr>
					<tr>
					<td width="126">2012 – 2013</td>
					<td width="65">120</td>
					<td width="88">117</td>
					<td width="75">92</td>
					<td width="85">51</td>
					<td width="77">41</td>
					<td width="106">14</td>
					<td width="209">03</td>
					</tr>
					<tr>
					<td width="126">2013 – 2014</td>
					<td width="65">120</td>
					<td width="88">141</td>
					<td width="75">110</td>
					<td width="85">69</td>
					<td width="77">41</td>
					<td width="106">16</td>
					<td width="209">02</td>
					</tr>
					<tr>
					<td width="126">2014 – 2015</td>
					<td width="65">180</td>
					<td width="88">121</td>
					<td width="75">104</td>
					<td width="85">78</td>
					<td width="77">28</td>
					<td width="106">07</td>
					<td width="209">–</td>
					</tr>
					<tr>
					<td width="126">2015 – 2016</td>
					<td width="65">180</td>
					<td width="88">174</td>
					<td width="75">170</td>
					<td width="85">129</td>
					<td width="77">41</td>
					<td width="106">6</td>
					<td width="209">01</td>
					</tr>
					<tr>
					<td width="126">2016-2017</td>
					<td width="65">180</td>
					<td width="88">159</td>
					<td colspan="4" width="344">131* ONGOING PROCESS</td>
					<td width="209">01</td>
					</tr>
					</tbody>
					</table>
				</div>
			</div>
			<div>
				<h3>ENTREPRENEURS</h3>
				<div>
					<table style="height: 1097px;" width="952">
					<tbody>
					<tr>
					<td width="62"><strong>SL.NO</strong></td>
					<td width="95"><strong>PASSED OUT YEAR</strong></td>
					<td width="190"><strong>NAME OF THE ENTREPRENEUR</strong></td>
					<td width="293"><strong>COMPANY NAME</strong></td>
					</tr>
					<tr>
					<td width="62">1.</td>
					<td width="95">2016-2017</td>
					<td width="190">KARTHIKEYAN.R</td>
					<td width="293">DREAMERZWILL S.A Engineering College,Chennai-600 077</td>
					</tr>
					<tr>
					<td width="62">2.</td>
					<td width="95">2015-2016</td>
					<td width="190">GEETHA PRIYA.K</td>
					<td width="293">SRI BALAJI ENGINEERS 52,Mandapam Road, Kilpauk Road, Kilpauk, Chennai-600010</td>
					</tr>
					<tr>
					<td width="62">3.</td>
					<td width="95">2013-2014</td>
					<td width="190">KALAIVANAN HARIHARAN.M</td>
					<td width="293">HALO INFOTECH No:4/741, Periyappu Mudali Street, Dharnampet, Gudiyattam-632602</td>
					</tr>
					<tr>
					<td width="62">4.</td>
					<td width="95">2013-2014</td>
					<td width="190">ADITHYAN.R</td>
					<td width="293">DOMAINCER (MARKETING CONSULTANCY) 35/17,Kanniah Chetty Street, Venkatapuram, Ambatur town, Thiruvallur-600053</td>
					</tr>
					<tr>
					<td width="62">5.</td>
					<td width="95">2012-2013</td>
					<td width="190">AAMIL MOHAMMED</td>
					<td width="293">THE SVAP GROUP Plot no:2253,H.I.G.,T.N.H.B,Avadi,Chennai-600054</td>
					</tr>
					<tr>
					<td width="62">6.</td>
					<td width="95">2012-2013</td>
					<td width="190">G.MOHAN KUMAR</td>
					<td width="293">ENNAM SUPER MARKET 4/389-1,Bhavani Towers, Shoolagiri, Hosur-635117</td>
					</tr>
					<tr>
					<td width="62">7.</td>
					<td width="95">2012-2013</td>
					<td width="190">DEEPAK SINGH</td>
					<td width="293">PADMAVATHI JEWELLERS AND BANKERS, Banglore</td>
					</tr>
					<tr>
					<td width="62">8.</td>
					<td width="95">2011-2012</td>
					<td width="190">MADHAN RAJ.K</td>
					<td width="293">SEAWAY EXIM No.12,Nethaji Street, Ponniammanmedu,Chennai-600110</td>
					</tr>
					<tr>
					<td width="62">9.</td>
					<td width="95">2011-2012</td>
					<td width="190">MADHAN RAJ.K</td>
					<td width="293">SIMMA CORP No 20B, VGP Santhosh Nagar 2<sup>nd</sup> st, Ponniammanmedu, Chennai-600110</td>
					</tr>
					<tr>
					<td width="62">10.</td>
					<td width="95">2010-2011</td>
					<td width="190">BOOBALAKRISHNAN.G</td>
					<td width="293">SHREE SERVICE SUPPORT #60, Vanniyar Street, kuppam,Ambattur, CH-58</td>
					</tr>
					<tr>
					<td width="62">11.</td>
					<td width="95">2010-2011</td>
					<td width="190">MADHAN RAJ.U</td>
					<td width="293">HI SECURE No.6,4<sup>th</sup> Street, Janagi Nagar, Madhuravoyal, Chennai-&nbsp;95</td>
					</tr>
					<tr>
					<td width="62">12.</td>
					<td width="95">2001-2002</td>
					<td width="190">ANUSHA &nbsp;RAKESH</td>
					<td width="293">CMS IT TRAINING INSTITUTE No 995-P, 1<sup>st</sup> Floor, 2<sup>nd</sup> Avnue, Anna Nagar, Chennai –&nbsp;40</td>
					</tr>
					</tbody>
					</table>
				</div>
			</div>
			<div>
				<h3>PLACEMENT DETAILS</h3>
				<p>2016-2017</p>
				<table style="height: 4211px;" width="492">
				<tbody>
				<tr>
				<td width="46">S.NO</td>
				<td width="210">STUDENT NAME</td>
				<td width="311">&nbsp;COMPANY NAME</td>
				</tr>
				<tr>
				<td>1.</td>
				<td>ABHISHEK SRIVASTAVA</td>
				<td>KAAR TECHNOLOGIES</td>
				</tr>
				<tr>
				<td>2.</td>
				<td>SUSHMITHA.R</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>3.</td>
				<td>ANGELIN JACINTH.S</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>4.</td>
				<td>CELES MONICA.B</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>5.</td>
				<td>ESWARAN.S</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>6.</td>
				<td>GRACE PRIYA SHALINI.S</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>7.</td>
				<td>NANDHINI.A</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>8.</td>
				<td>NANDHINI.C (1/1/1996)</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>9.</td>
				<td>PAVITHRA.T.V</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>10.</td>
				<td>PREETHI.R</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>11.</td>
				<td>RESHMA.B</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>12.</td>
				<td>REVATHI.V.B</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>13.</td>
				<td>SANJAY.R</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>14.</td>
				<td>SHOBANA.G</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>15.</td>
				<td>SINDHU.V</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>16.</td>
				<td>SUGANTHI.M</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>17.</td>
				<td>ADITHYA PRIYADHARSHNI</td>
				<td>CAPGEMINI</td>
				</tr>
				<tr>
				<td>18.</td>
				<td>AKSHAYA PRIYA.S</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>19.</td>
				<td>MADHUBALA.M</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>20.</td>
				<td>JEEVITHA.S</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>21.</td>
				<td>RANGESH.T.V</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>22.</td>
				<td>SUNDARAMURTHY.G</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>23.</td>
				<td>VANMATHI.S</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>24.</td>
				<td>SWETHA.S</td>
				<td>AON HEWITT</td>
				</tr>
				<tr>
				<td>25.</td>
				<td>ARCHANA.C</td>
				<td>CALIBRAINT TECHNOLOGIES</td>
				</tr>
				<tr>
				<td>26.</td>
				<td>DEEPAK.M</td>
				<td>VIRTUSA POLARIS</td>
				</tr>
				<tr>
				<td>27.</td>
				<td>SHARMILA.G</td>
				<td>VIRTUSA POLARIS</td>
				</tr>
				<tr>
				<td>28.</td>
				<td>SHREE VARSHA.K</td>
				<td>VIRTUSA POLARIS</td>
				</tr>
				<tr>
				<td>29.</td>
				<td>ARUN KUMAR.S</td>
				<td>ASPIRE SYSTEMS</td>
				</tr>
				<tr>
				<td>30.</td>
				<td>KEERTHANA.S</td>
				<td>MICROMEN SOFTWARE SOLUTIONS</td>
				</tr>
				<tr>
				<td>31.</td>
				<td>MADHUBALA.M</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>32.</td>
				<td>MUBARAK.S</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>33.</td>
				<td>SAAKETHA.P</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>34.</td>
				<td>AKSHAYA.S</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>35.</td>
				<td>ANCY.J</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>36.</td>
				<td>AVINASH.B</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>37.</td>
				<td>SWETHA.S</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>38.</td>
				<td>SUSHMITA.P</td>
				<td>CSS CORP</td>
				</tr>
				<tr>
				<td>39.</td>
				<td>RAVINDER CHOUDRY.B</td>
				<td>SOPRA STERIA</td>
				</tr>
				<tr>
				<td>40.</td>
				<td>AKSHAYA PRIYA.S</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>41.</td>
				<td>DHAKCHAYANI.L</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>42.</td>
				<td>JENIFER.M</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>43.</td>
				<td>KEERTHANA.M</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>44.</td>
				<td>SARAVANAN.G</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>45.</td>
				<td>SHALINI.D</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>46.</td>
				<td>VINITHA.R</td>
				<td>QRUIZE</td>
				</tr>
				<tr>
				<td>47.</td>
				<td>YUGANTHI.S</td>
				<td>AURUM INFOSOL</td>
				</tr>
				<tr>
				<td>48.</td>
				<td>NARENDRA RAO.J</td>
				<td>AURUM INFOSOL</td>
				</tr>
				<tr>
				<td>49.</td>
				<td>SAAKETHA.P</td>
				<td>AURUM INFOSOL</td>
				</tr>
				<tr>
				<td>50.</td>
				<td>MADHUBALA. E</td>
				<td>GAVS TECHNOLOGIES</td>
				</tr>
				<tr>
				<td>51.</td>
				<td>VINITHA.R</td>
				<td>OFS</td>
				</tr>
				<tr>
				<td>52.</td>
				<td>MUBARAK.S</td>
				<td>OFS</td>
				</tr>
				<tr>
				<td>53.</td>
				<td>SAAKETHA.P</td>
				<td>ZOHO</td>
				</tr>
				<tr>
				<td>54.</td>
				<td>MANOLASYA</td>
				<td>ZOHO</td>
				</tr>
				<tr>
				<td>55.</td>
				<td>SAAKETHA.P</td>
				<td>NEEYAMO</td>
				</tr>
				<tr>
				<td>56.</td>
				<td>ANITHA</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>57.</td>
				<td>DIVYA.S</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>58.</td>
				<td>ANUPAMA.K</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>59.</td>
				<td>MONISH RAJ</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>60.</td>
				<td>TAMIL SELVI.V</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>61.</td>
				<td>SANGEETHA.G</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>62.</td>
				<td>RADHA.S</td>
				<td>SUTHERLAND</td>
				</tr>
				<tr>
				<td>63.</td>
				<td>ANUJA.A.N</td>
				<td>CROYEZ</td>
				</tr>
				<tr>
				<td>64.</td>
				<td>NANDHINI.C (3/3/1996)</td>
				<td>CROYEZ</td>
				</tr>
				<tr>
				<td>65.</td>
				<td>SHYAM KUMAR.T</td>
				<td>CROYEZ</td>
				</tr>
				<tr>
				<td>66.</td>
				<td>SIBIN RAJ.S</td>
				<td>CROYEZ</td>
				</tr>
				<tr>
				<td>67.</td>
				<td>KARTHIK.M</td>
				<td>CROYEZ</td>
				</tr>
				<tr>
				<td>68.</td>
				<td>MUTHU PRADEEPA.S.P</td>
				<td>COOPER LYBRANTH</td>
				</tr>
				<tr>
				<td>69.</td>
				<td>THIRUPURASUNDARI</td>
				<td>PRO HUNTERS HR SERVICES</td>
				</tr>
				<tr>
				<td>70.</td>
				<td>S.DURGA</td>
				<td>ARCUS AUTOMATION PVT LTD</td>
				</tr>
				<tr>
				<td>71.</td>
				<td>PRIYANGA.B</td>
				<td>ARGON SOLUTIONS</td>
				</tr>
				<tr>
				<td>72.</td>
				<td>AADHILAKSHMI.N</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>73.</td>
				<td>ANUPAMA.K</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>74.</td>
				<td>DIVYA.H</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>75.</td>
				<td>HARI KRISHNAN.R</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>76.</td>
				<td>JAYAPRADHA.S</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>77.</td>
				<td>KAVITHA.V</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>78.</td>
				<td>KAVIYA.P</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>79.</td>
				<td>KRISHNAMOORTHY. E</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>80.</td>
				<td>MOHAMMED KASIM.M</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>81.</td>
				<td>MURUGAN.R</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>82.</td>
				<td>PATHMINI.C</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>83.</td>
				<td>PRABHU.M</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>84.</td>
				<td>PRASANTH.J</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>85.</td>
				<td>PRASHANTH .R</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>86.</td>
				<td>PRIYANGA.B</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>87.</td>
				<td>PRIYANKA.S</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>88.</td>
				<td>SABARI DEVI.P</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>89.</td>
				<td>SARALA.T</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>90.</td>
				<td>SATHISH KUMAR.R (01.08.1995)</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>91.</td>
				<td>SATHISH KUMAR.R (29.11.1995)</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>92.</td>
				<td>SATHYA PRIYA.G</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>93.</td>
				<td>SELVA MANIGANDAN.K</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>94.</td>
				<td>SHALINI.D</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>95.</td>
				<td>SIVA SUBRAMANIAN.S</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>96.</td>
				<td>TAMIL SELVI.M</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>97.</td>
				<td>THIRIPURASUNDARI.S</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>98.</td>
				<td>UMA MAHESHWARI.G</td>
				<td>HDFC</td>
				</tr>
				<tr>
				<td>99.</td>
				<td>ANCY.J</td>
				<td>VDART</td>
				</tr>
				<tr>
				<td>100.</td>
				<td>SARALA.T</td>
				<td>SUNDARAM BUSINESS SERVICES</td>
				</tr>
				<tr>
				<td>101.</td>
				<td>DHYAMANI.D</td>
				<td>AQUA PURE PLUS</td>
				</tr>
				<tr>
				<td>102.</td>
				<td>RAVINDER CHOUDRY.B</td>
				<td>SOPRA STERIA</td>
				</tr>
				<tr>
				<td>103.</td>
				<td>SANGEETHA.A</td>
				<td>AQUA PURE PLUS</td>
				</tr>
				<tr>
				<td>104.</td>
				<td>SIVA SUBRAMANIAN.S</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>105.</td>
				<td>SELVA MANIGANDAN.K</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>106.</td>
				<td>SRI MEENAKSHI.N</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>107.</td>
				<td>VASANTH.D</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>108.</td>
				<td>PRIYANKA.S</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>109.</td>
				<td>MAKESH.M</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>110.</td>
				<td>MATHAN KUMAR.C</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>111.</td>
				<td>S.SUNDHAR</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>112.</td>
				<td>R.SURESH</td>
				<td>ZEALOUS SERVICES</td>
				</tr>
				<tr>
				<td>113.</td>
				<td>TAMIL SELVI.V</td>
				<td>NATION STAR</td>
				</tr>
				<tr>
				<td>114.</td>
				<td>RENE . V.V.SUNDAR</td>
				<td>JUST DIAL</td>
				</tr>
				<tr>
				<td>115.</td>
				<td>SIVA SUBRAMANIYAN</td>
				<td>JUST DIAL</td>
				</tr>
				<tr>
				<td>116.</td>
				<td>SELVA MANIGANDAN</td>
				<td>JUST DIAL</td>
				</tr>
				<tr>
				<td>117.</td>
				<td>SATHISH KUMAR.R (129)</td>
				<td>JUST DIAL</td>
				</tr>
				<tr>
				<td>118.</td>
				<td>B.SANJANA</td>
				<td>HEMAVATHY &amp; CO</td>
				</tr>
				<tr>
				<td>119</td>
				<td>PREETHI.K</td>
				<td>P.V.RAJ &amp; CO</td>
				</tr>
				<tr>
				<td>120.</td>
				<td>K.ROHINEE SHREE</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>121.</td>
				<td>R.SARANYA</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>122.</td>
				<td>S.SANGEETHA</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>123.</td>
				<td>R.PRIYA</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>124.</td>
				<td>S.PRIYADHARSHINI</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>125.</td>
				<td>K.MOHAMMED ASADHULLAH KHAN</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>126.</td>
				<td>K.PRABHU</td>
				<td>SMP AUTO CONSULTANT</td>
				</tr>
				<tr>
				<td>127.</td>
				<td>C.MONISHA</td>
				<td>PARAM PROJECTS PVT LTD</td>
				</tr>
				<tr>
				<td>128.</td>
				<td>R.KARTHIKEYAN</td>
				<td>PARAM PROJECTS PVT LTD</td>
				</tr>
				<tr>
				<td>129.</td>
				<td>R.SARANYA</td>
				<td>MARUTHI AIR CONDITIONER PVT LTD</td>
				</tr>
				<tr>
				<td>130</td>
				<td>REVANTH KUMAR.U</td>
				<td>OMEGA HEALTHCARE</td>
				</tr>
				<tr>
				<td>131.</td>
				<td>PRASHANTH.J</td>
				<td>OMEGA HEALTHCARE</td>
				</tr>
				</tbody>
				</table>
                <br/>
				<h4>2015-2016</h4>
				<table width="654">
				<tbody>
				<tr>
				<td width="62"><strong>Sl.no</strong></td>
				<td width="173"><strong>Name</strong></td>
				<td width="210"><strong>Company Name</strong></td>
				<td width="209"><strong>Designation</strong></td>
				</tr>
				<tr>
				<td width="62">1.</td>
				<td width="173">DINESH KUMAR S</td>
				<td width="210">TECHMAHINDRA</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">2.</td>
				<td width="173">ABIN PHILIP</td>
				<td width="210">TECHMAHINDRA</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">3.</td>
				<td width="173">HARISH N.R</td>
				<td width="210">TECHMAHINDRA</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">4.</td>
				<td width="173">SURESH KUMAR.S</td>
				<td width="210">TECHMAHINDRA</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">5.</td>
				<td width="173">KIRUTHIKA.R</td>
				<td width="210">TECH MAHINDRA</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">6.</td>
				<td width="173">AMUTHA U</td>
				<td width="210">EXCELACOM</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">7.</td>
				<td width="173">DINESH KUMAR</td>
				<td width="210">EXCELACOM</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">8.</td>
				<td width="173">PRIYANKA.J</td>
				<td width="210">EXCELACOM</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">9.</td>
				<td width="173">DINESH KUMAR</td>
				<td width="210">KAAR TECHNOLOGIES</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">10.</td>
				<td width="173">BARATH KUMAR.G</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">11.</td>
				<td width="173">BHARATHI PRIYA.R</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">12.</td>
				<td width="173">BOOBALAN.V</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">13.</td>
				<td width="173">JAYA BHARATHY.Y</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">14.</td>
				<td width="173">KAMINI.K</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">15.</td>
				<td width="173">NANDHA KUMAR.V</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">16.</td>
				<td width="173">PRASANTH.D</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">17.</td>
				<td width="173">SAI SHANKARI.T</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">18.</td>
				<td width="173">SANDIYA.S</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">19.</td>
				<td width="173">SARANYA.A.M</td>
				<td width="210">L&amp;T INFOTECH</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">20.</td>
				<td width="173">ANUSHA.R</td>
				<td width="210">UST GLOBAL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">21.</td>
				<td width="173">JAYASHREE.G</td>
				<td width="210">UST GLOBAL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">22.</td>
				<td width="173">RAMYA.R</td>
				<td width="210">UST GLOBAL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">23.</td>
				<td width="173">SANDHIYAA.B</td>
				<td width="210">UST GLOBAL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">24.</td>
				<td width="173">SHARMILA.N</td>
				<td width="210">UST GLOBAL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">25.</td>
				<td width="173">VISHVESH KARTHIK.S</td>
				<td width="210">UST GLOBAL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">26.</td>
				<td width="173">SHRIE SATHEYAA.K.K</td>
				<td width="210">Ideas2IT</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">27.</td>
				<td width="173">PRIYA.N</td>
				<td width="210">FSS</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">28.</td>
				<td width="173">SHOBANA J</td>
				<td width="210">MAVERIC Systems</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">29.</td>
				<td width="173">SHOBANA J</td>
				<td width="210">OFS</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">30.</td>
				<td width="173">SUGANYA.P</td>
				<td width="210">NETTV4U</td>
				<td width="209">Business Analyst</td>
				</tr>
				<tr>
				<td width="62">31.</td>
				<td width="173">INDHUMATHI</td>
				<td width="210">NTT DATA</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">32.</td>
				<td width="173">NASHEETASADAF M.P</td>
				<td width="210">CTS</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">33.</td>
				<td width="173">RIYANA FATHIMA.M</td>
				<td width="210">CTS</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">34.</td>
				<td width="173">MEENA.R</td>
				<td width="210">CTS</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">35.</td>
				<td width="173">ANUVARSHINI</td>
				<td width="210">INFOSYS</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">36.</td>
				<td width="173">SHAMEEL AHMED.N</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">37.</td>
				<td width="173">BHAVYA BHARATHI.H</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">38.</td>
				<td width="173">MONISHA.B.P</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">39.</td>
				<td width="173">PRIYANKA.V</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">40.</td>
				<td width="173">BABYSHALINI.P</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">41.</td>
				<td width="173">SARANYA.A.M</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">42.</td>
				<td width="173">DEVIBALA</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">43.</td>
				<td width="173">PAVITHRA.V.H</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">44.</td>
				<td width="173">BARATH.R</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">45.</td>
				<td width="173">BALACHANDAR.S</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">46.</td>
				<td width="173">JAYASHREE.S</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">47.</td>
				<td width="173">JAGADEESH</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">48.</td>
				<td width="173">JAYARAM PRAVEEN</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">49.</td>
				<td width="173">KALAIVANI.M</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">50.</td>
				<td width="173">KARTHIKEYAN</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">51.</td>
				<td width="173">HARISH KUMAR. L</td>
				<td width="210">AON HEWITT</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">52.</td>
				<td width="173">VENKATESH.D</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">53.</td>
				<td width="173">SUGANYA.S</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">54.</td>
				<td width="173">MEENA.M</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">55.</td>
				<td width="173">PRIYANKA.V</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">56.</td>
				<td width="173">SANDHYA.S</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">57.</td>
				<td width="173">MONISHA.B.P</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">58.</td>
				<td width="173">KRITHIGA.S</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">59.</td>
				<td width="173">KSHITIJA.T.G</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">60.</td>
				<td width="173">EESHA.K</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">61.</td>
				<td width="173">AJITH ABRAHAM.J</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">62.</td>
				<td width="173">ABINESH</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">63.</td>
				<td width="173">BHAVYA BHARATHI</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">64.</td>
				<td width="173">JAYASHREE.S</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">65.</td>
				<td width="173">JEMIMA CATHERINE</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">66.</td>
				<td width="173">DIVYA.C</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">67.</td>
				<td width="173">BALACHANDAR.S</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">68.</td>
				<td width="173">SHALINI.K</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">69.</td>
				<td width="173">SNEHA.S.MENON</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Support Executive</td>
				</tr>
				<tr>
				<td width="62">70.</td>
				<td width="173">KARTHIKEYAN.G</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">71.</td>
				<td width="173">JAGADESH.N</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">72.</td>
				<td width="173">ASHOK KUMAR.P</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">73.</td>
				<td width="173">VENKATESH.D</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">74.</td>
				<td width="173">AMIT KUMAR</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">75.</td>
				<td width="173">SANTHOSH</td>
				<td width="210">UB TECHNOLOGIES</td>
				<td width="209">Associate Recruiter</td>
				</tr>
				<tr>
				<td width="62">76.</td>
				<td width="173">JEMIMA CATHERINE.M</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">77.</td>
				<td width="173">KRITHIGA.S</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">78.</td>
				<td width="173">KSHITIJA.T.G.</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">79.</td>
				<td width="173">MAHA LAKSHMI.V.S</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">80.</td>
				<td width="173">POOJA.R</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">81.</td>
				<td width="173">SHWETHA.S</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">82.</td>
				<td width="173">SOWMIYA.K</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">83.</td>
				<td width="173">SUNITHA.M</td>
				<td width="210">CSS CORP</td>
				<td width="209">Jr Tech Support</td>
				</tr>
				<tr>
				<td width="62">84.</td>
				<td width="173">VARUN RAJ.M</td>
				<td width="210">CODILAR</td>
				<td width="209">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">85.</td>
				<td width="173">SRIRAMAN.B</td>
				<td width="210">MPHASIS</td>
				<td width="209">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">86.</td>
				<td width="173">ASHOK KUMAR.P</td>
				<td width="210">GENXLEAD</td>
				<td width="209">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">87.</td>
				<td width="173">ASHOK KUMAR.P</td>
				<td width="210">CAPEGEMINI</td>
				<td width="209">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">88.</td>
				<td width="173">MONISHA.B.P</td>
				<td width="210">CAPEGEMINI</td>
				<td width="209">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">89.</td>
				<td width="173">PRATIP.V</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">90.</td>
				<td width="173">MADHAN KUMAR</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">91.</td>
				<td width="173">MANIGANDAN.D</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">92.</td>
				<td width="173">BARATH.R</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">93.</td>
				<td width="173">SNEHA.S.MENON</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">94.</td>
				<td width="173">VIGNESHWAR.A</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">95.</td>
				<td width="173">ABINESH.A</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">96.</td>
				<td width="173">GAYATHRI.S</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">97.</td>
				<td width="173">DEEPIKA.R</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">98.</td>
				<td width="173">DIVYA.C</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">99.</td>
				<td width="173">GIRIJA.R</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">100.</td>
				<td width="173">SATHISH.K</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">101.</td>
				<td width="173">DEVI BALA.K</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">102.</td>
				<td width="173">SAFEENA ANJUM.S</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">103.</td>
				<td width="173">MEENA.M</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">104.</td>
				<td width="173">MONISHA.M</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">105.</td>
				<td width="173">HARISH KUMAR.L</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">106.</td>
				<td width="173">JAGADESH.N</td>
				<td width="210">QSPIDER</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">107.</td>
				<td width="173">ABINESH.A</td>
				<td width="210">MAXIT</td>
				<td width="209">Business Analyst</td>
				</tr>
				<tr>
				<td width="62">108.</td>
				<td width="173">NIRUP.P.M</td>
				<td width="210">QRUIZE</td>
				<td width="209">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">109.</td>
				<td width="173">VENKATESH.D</td>
				<td width="210">QRUIZE</td>
				<td width="209">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">110.</td>
				<td width="173">VISHAL.R</td>
				<td width="210">QRUIZE</td>
				<td width="209">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">111.</td>
				<td width="173">SHARMI.L</td>
				<td width="210">QRUIZE</td>
				<td width="209">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">112.</td>
				<td width="173">POORNIMA.R</td>
				<td width="210">BIO SIGN TECHNOLOGIES</td>
				<td width="209">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">113.</td>
				<td width="173">GIRISH.R</td>
				<td width="210">BLITZ Media Productions</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">114.</td>
				<td width="173">BALACHANDAR.S</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">115.</td>
				<td width="173">R.BARATH</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">116.</td>
				<td width="173">NITHYA.D</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">117.</td>
				<td width="173">POORNIMA.R</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">118.</td>
				<td width="173">DEVI BALA</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">119.</td>
				<td width="173">SNEHA.S.MENON</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">120.</td>
				<td width="173">SUGANYA.S</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">121.</td>
				<td width="173">MAHALAKSHMI.V.S</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">122.</td>
				<td width="173">S.JAYASHREE</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">123.</td>
				<td width="173">EESHA.K</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">124.</td>
				<td width="173">NIVEDHA.R</td>
				<td width="210">VDART</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">125.</td>
				<td width="173">KARTHIKEYAN</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">126.</td>
				<td width="173">BHARATH KUMAR.A</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">127.</td>
				<td width="173">MAGESH BABU.R.V</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">128.</td>
				<td width="173">SUBASH RAM.K</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">129.</td>
				<td width="173">MANIKANDAN.D</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">130.</td>
				<td width="173">JEYARAM PRAVEEN.M</td>
				<td width="210">HDFC</td>
				<td width="209">Sales Executive</td>
				</tr>
				<tr>
				<td width="62">131.</td>
				<td width="173">SHARMILA.M</td>
				<td width="210">SCOPE EKNOWLEDGE CENTER</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">132.</td>
				<td width="173">POORNIMA.R</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">133.</td>
				<td width="173">SUSHMITHA.K</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">134.</td>
				<td width="173">RAVINDRAN.E</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">135.</td>
				<td width="173">NANDHINI.T</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">136.</td>
				<td width="173">PUNITHA.D</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">137.</td>
				<td width="173">JYOTHSANA.P</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">138.</td>
				<td width="173">NIRUP.P.M</td>
				<td width="210">SUTHERLAND (NV)</td>
				<td width="209">Jr Executive Trainee</td>
				</tr>
				<tr>
				<td width="62">139.</td>
				<td width="173">NIRMALA DEVI.V</td>
				<td width="210">VENTURES HRD PRIVATE LTD CENTRE</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">140.</td>
				<td width="173">EESHA.K</td>
				<td width="210">TRUE TECH</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">141.</td>
				<td width="173">ABINESH.A</td>
				<td width="210">TRUE TECH</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">142.</td>
				<td width="173">SAFEENA ANJUM.S</td>
				<td width="210">TRUE TECH</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">143.</td>
				<td width="173">BARATH.R</td>
				<td width="210">TRUE TECH</td>
				<td width="209">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">144.</td>
				<td width="173">JEMIMA CATHERINE.M</td>
				<td width="210">MANYA</td>
				<td width="209">Executive Trainer</td>
				</tr>
				<tr>
				<td width="62">145.</td>
				<td width="173">SUGANYA.S(05/05/94)</td>
				<td width="210">AURUM INFOSOL</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">146.</td>
				<td width="173">VARUN RAJ.M</td>
				<td width="210">AURUM INFOSOL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">147.</td>
				<td width="173">VIJAYAKUMAR.R</td>
				<td width="210">AURUM INFOSOL</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">148.</td>
				<td width="173">SAFEENA ANJUM.S</td>
				<td width="210">CAREER TREE HR SOLUTION</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">149.</td>
				<td width="173">SAFEENA ANJUM.S</td>
				<td width="210">HCL Technologies</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">150.</td>
				<td width="173">PREETHI.G</td>
				<td width="210">HCL Technologies</td>
				<td width="209">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">151.</td>
				<td width="173">RENUGA DEVI.D</td>
				<td width="210">Kochar Infotech</td>
				<td width="209">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">152.</td>
				<td width="173">SATHISHKUMAR. R</td>
				<td width="210">Kochar Infotech</td>
				<td width="209">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">153.</td>
				<td width="173">SHOBANA.M</td>
				<td width="210">Kochar Infotech</td>
				<td width="209">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">154.</td>
				<td width="173">BHARATH KUMAR.A</td>
				<td width="210">JUST DIAL</td>
				<td width="209">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">155.</td>
				<td width="173">NANDHINI.A</td>
				<td width="210">JUST DIAL</td>
				<td width="209">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">156.</td>
				<td width="173">NIVEDHA.R</td>
				<td width="210">JUST DIAL</td>
				<td width="209">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">157.</td>
				<td width="173">DEEPIKA.S</td>
				<td width="210">JUST DIAL</td>
				<td width="209">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">158.</td>
				<td width="173">VAISALI.P</td>
				<td width="210">SUTHERLAND</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">159.</td>
				<td width="173">PRASHANTH.K</td>
				<td width="210">GENXLEAD</td>
				<td width="209">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">160.</td>
				<td width="173">JAGADEESWARI.R</td>
				<td width="210">JUST DIAL</td>
				<td width="209">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">161.</td>
				<td width="173">SATHISH.B</td>
				<td width="210">JUST DIAL</td>
				<td width="209">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">162.</td>
				<td width="173">SATHISHKUMAR. R</td>
				<td width="210">SOLARTIS TECHNOLOGY</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">163.</td>
				<td width="173">RENUGA DEVI.D</td>
				<td width="210">SOLARTIS TECHNOLOGY</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">164.</td>
				<td width="173">SHRIE SATHEYAA</td>
				<td width="210">ZILOGIC SYSTEMS</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">165.</td>
				<td width="173">JEYARAM PRAVEEN</td>
				<td width="210">ZILOGIC SYSTEMS</td>
				<td width="209">Trainee</td>
				</tr>
				<tr>
				<td width="62">166.</td>
				<td width="173">JEMIMA CATHERINE</td>
				<td width="210">ZILOGIC SYSTEMS</td>
				<td width="209">Sotware Trainee – L1</td>
				</tr>
				<tr>
				<td width="62">167.</td>
				<td width="173">GANESAN .B</td>
				<td width="210">KADAMBA TECHNOLOGIES</td>
				<td width="209">Sotware Trainee – L1</td>
				</tr>
				<tr>
				<td width="62">168.</td>
				<td width="173">MUKESH.M</td>
				<td width="210">ZEALOUS</td>
				<td width="209">Jr. Executive</td>
				</tr>
				<tr>
				<td width="62">169.</td>
				<td width="173">RAJASEKAR.N</td>
				<td width="210">ZEALOUS</td>
				<td width="209">Jr. Executive</td>
				</tr>
				<tr>
				<td width="62">170.</td>
				<td width="173">KIRUTHIKA.R</td>
				<td width="210">AM AVIATION</td>
				<td width="209">Engineer Trainee</td>
				</tr>
				</tbody>
				</table>
                <br/>
				<h4>2014 –2015</h4>
				<table width="639">
				<tbody>
				<tr>
				<td width="67"><strong>Sl.No</strong></td>
				<td width="186"><strong>Name</strong></td>
				<td width="160"><strong>Company Name</strong></td>
				<td width="226"><strong>Designation</strong></td>
				</tr>
				<tr>
				<td width="67">1.</td>
				<td width="186">AKSHAYA&nbsp; SRINITHI.M</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Software Programmer – Trainee</td>
				</tr>
				<tr>
				<td width="67">2.</td>
				<td width="186">AMALAN SENTHAMIZHINIYAN.S.J</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Software Programmer – Trainee</td>
				</tr>
				<tr>
				<td width="67">3.</td>
				<td width="186">ANJALI PANDEY</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Software Programmer – Trainee</td>
				</tr>
				<tr>
				<td width="67">4.</td>
				<td width="186">ARVIND.B</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Software Programmer – Trainee</td>
				</tr>
				<tr>
				<td width="67">5.</td>
				<td width="186">AZARUDEEN.A</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="67">6.</td>
				<td width="186">DHEEPIKA .B</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="67">7.</td>
				<td width="186">DIVYA.S</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Software Programmer</td>
				</tr>
				<tr>
				<td width="67">8.</td>
				<td width="186">EZHIL KANNAN.R</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">9.</td>
				<td width="186">GOWRI T</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">10.</td>
				<td width="186">JOYCE SILVIA.J</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">11.</td>
				<td width="186">KANCHANA SHARMA.V</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">12.</td>
				<td width="186">MOUNICA.CH</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">13.</td>
				<td width="186">NIKHILA.K</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">14.</td>
				<td width="186">NITHISH.S</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">15.</td>
				<td width="186">PRABU SANKAR .M</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">16.</td>
				<td width="186">SARATH.B</td>
				<td width="160">TECH MAHINDRA</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">17.</td>
				<td width="186">YARALAGADDA DIVYA TEJA</td>
				<td width="160">EXCELACOM</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">18.</td>
				<td width="186">MANASA K R</td>
				<td width="160">EXCELACOM</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">19.</td>
				<td width="186">SACHIN PRADEEP</td>
				<td width="160">EXCELACOM</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">20.</td>
				<td width="186">VIGNESH E T</td>
				<td width="160">EXCELACOM</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">21.</td>
				<td width="186">NIKHILA.K</td>
				<td width="160">MPHASIS</td>
				<td width="226">Project Trainee</td>
				</tr>
				<tr>
				<td width="67">22.</td>
				<td width="186">NIVETHITHA L</td>
				<td width="160">MPHASIS</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">23.</td>
				<td width="186">ARCHANA S</td>
				<td width="160">L&amp;T INFOTECH</td>
				<td width="226">Program Analyst Trainee</td>
				</tr>
				<tr>
				<td width="67">24.</td>
				<td width="186">VIJAYALAKSHMI K</td>
				<td width="160">L&amp;T INFOTECH</td>
				<td width="226">Program Analyst Trainee</td>
				</tr>
				<tr>
				<td width="67">25.</td>
				<td width="186">MUHIL M</td>
				<td width="160">CONGURENT</td>
				<td width="226">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="67">26.</td>
				<td width="186">PREETHI A</td>
				<td width="160">ACCENTURE</td>
				<td width="226">Junior Trainee Programmer</td>
				</tr>
				<tr>
				<td width="67">27.</td>
				<td width="186">STEPHEN S TIMOTHY</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">28.</td>
				<td width="186">SNEHA M</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">29.</td>
				<td width="186">PRATHIBA K</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">30.</td>
				<td width="186">MEENAKSHI.K</td>
				<td width="160">IBM</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">31.</td>
				<td width="186">MANIGANDAN R</td>
				<td width="160">HCL COMNET</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">32.</td>
				<td width="186">RAJESH.S</td>
				<td width="160">SRM INFOTECH</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">33.</td>
				<td width="186">KISHORE KUMAR.R</td>
				<td width="160">SRM INFOTECH</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">34.</td>
				<td width="186">GANESH.V</td>
				<td width="160">SRM INFOTECH</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">35.</td>
				<td width="186">JANANI.P</td>
				<td width="160">MAVERIC SYSTEMS</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">36.</td>
				<td width="186">STEPHEN S. TIMOTHY</td>
				<td width="160">MAVERIC SYSTEMS</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">37.</td>
				<td width="186">KISHORE KUMAR.R</td>
				<td width="160">VIRTUSA</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">38.</td>
				<td width="186">A.VINODHINI</td>
				<td width="160">OMEGA</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">39.</td>
				<td width="186">V.NISHANTHI</td>
				<td width="160">OMEGA</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">40.</td>
				<td width="186">ARVINDH S</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">41.</td>
				<td width="186">MERCY BRIGTH R</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">42.</td>
				<td width="186">MOHAN S</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">43.</td>
				<td width="186">ASWINI N K</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">44.</td>
				<td width="186">JOTHI R</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">45.</td>
				<td width="186">GANESH V</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">46.</td>
				<td width="186">CHITHARANJANI V.K</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">47.</td>
				<td width="186">MANIKANDAN R</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">48.</td>
				<td width="186">PRASHANTH S</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">49.</td>
				<td width="186">KARTHICK V</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">50.</td>
				<td width="186">KOKILA K</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">51.</td>
				<td width="186">MONICA T.G</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">52.</td>
				<td width="186">HEMATH KRISHNA U</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">53.</td>
				<td width="186">GAUTHAM R</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">54.</td>
				<td width="186">ARAVIND B</td>
				<td width="160">SUTHERLAND</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">55.</td>
				<td width="186">V.NISHANTHI</td>
				<td width="160">POLARIS</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">56.</td>
				<td width="186">V.NISHANTHI</td>
				<td width="160">CSS CORP</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">57.</td>
				<td width="186">SNEHA.M</td>
				<td width="160">CSS CORP</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">58.</td>
				<td width="186">K PRATHIBA</td>
				<td width="160">CSS CORP</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">59.</td>
				<td width="186">A.VINODHINI</td>
				<td width="160">CSS CORP</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">60.</td>
				<td width="186">RAJESHWARI J</td>
				<td width="160">CSS CORP</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">61.</td>
				<td width="186">S.S SWATHI</td>
				<td width="160">CSS CORP</td>
				<td width="226">Junior Trainee</td>
				</tr>
				<tr>
				<td width="67">62.</td>
				<td width="186">SOWMYA A</td>
				<td width="160">AMI</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">63.</td>
				<td width="186">EZHIL KANNAN.R</td>
				<td width="160">ARICENT</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">64.</td>
				<td width="186">K NANCY</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">65.</td>
				<td width="186">SANTHOSH KUMAR P</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">66.</td>
				<td width="186">SARANYA R</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">67.</td>
				<td width="186">SUGANYA N</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">68.</td>
				<td width="186">SARAVANAPRASANTH.E</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">69.</td>
				<td width="186">JANANI PRIYA K</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">70.</td>
				<td width="186">LOKESHWARAN R</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">71.</td>
				<td width="186">DIVYA BHARATHI</td>
				<td width="160">QSPIDER</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">72.</td>
				<td width="186">PRIYA M</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">73.</td>
				<td width="186">KOKILA K</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">74.</td>
				<td width="186">MERCY BRIGITH</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">75.</td>
				<td width="186">DEEPIKA LAKSHMI U</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">76.</td>
				<td width="186">PREETHI R</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">77.</td>
				<td width="186">NIVASINI R</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">78.</td>
				<td width="186">RAJESWARI K</td>
				<td width="160">QSPIDER</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">79.</td>
				<td width="186">AMALAN SENTHAMIZHINIYAN.S.J</td>
				<td width="160">TCS</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">80.</td>
				<td width="186">NASRIN BANU.A</td>
				<td width="160">HEXAWARE TECHNOLOGY</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">81.</td>
				<td width="186">SARAVANA PRASANTH</td>
				<td width="160">INNOPPL</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">82.</td>
				<td width="186">PRASANTH.S</td>
				<td width="160">INNOPPL</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">83.</td>
				<td width="186">V. DEEPIKA</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">84.</td>
				<td width="186">S. GOPINATH</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">85.</td>
				<td width="186">R.NANDHA KUMAR</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">86.</td>
				<td width="186">V.M. NANDHINI</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">87.</td>
				<td width="186">PRIYADHARSHINI.M</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">88.</td>
				<td width="186">SUGANYA M</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">89.</td>
				<td width="186">P.SUJI PRIYA</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">90.</td>
				<td width="186">NANCY</td>
				<td width="160">IPRO SOLUTIONS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">91.</td>
				<td width="186">SHESHAPRIYA K</td>
				<td width="160">ATOS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">92.</td>
				<td width="186">DINESH M</td>
				<td width="160">AON HEWITT</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">93.</td>
				<td width="186">SUGANYA.N</td>
				<td width="160">AON HEWITT</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">94.</td>
				<td width="186">NIVASINI R</td>
				<td width="160">AON HEWITT</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">95.</td>
				<td width="186">KOKILA K</td>
				<td width="160">AON HEWITT</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">96.</td>
				<td width="186">MERCY BRIGITH R</td>
				<td width="160">AON HEWITT</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">97.</td>
				<td width="186">LOGESWARAN R</td>
				<td width="160">AON HEWITT</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">98.</td>
				<td width="186">JOTHI R</td>
				<td width="160">PERFINT HEALTH CARE</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">99.</td>
				<td width="186">NANCY K</td>
				<td width="160">FSS</td>
				<td width="226">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="67">100.</td>
				<td width="186">MUHIL M</td>
				<td width="160">ZOHO CORPORATION</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">101.</td>
				<td width="186">EZHIL KANNAN.R</td>
				<td width="160">ZOHO CORPORATION</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">102.</td>
				<td width="186">SUGANYA M</td>
				<td width="160">CIITE</td>
				<td width="226">Jr. Software Engineer</td>
				</tr>
				<tr>
				<td width="67">103.</td>
				<td width="186">DIVYA BHARATHI</td>
				<td width="160">STERIA</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="67">104.</td>
				<td width="186">RAJESHWARI J</td>
				<td width="160">CTS</td>
				<td width="226">Software Engineer Trainee</td>
				</tr>
				</tbody>
				</table>
                <br/>
				<h4>2013-2014</h4>
				<table width="672">
				<tbody>
				<tr>
				<td width="62"><strong>SL.NO</strong></td>
				<td width="172"><strong>NAME</strong></td>
				<td width="199"><strong>COMPANY NAME</strong></td>
				<td width="239"><strong>Designation</strong></td>
				</tr>
				<tr>
				<td width="62">1.</td>
				<td width="172">RAJESHWARI U</td>
				<td width="199">OFS</td>
				<td width="239">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">2.</td>
				<td width="172">LOGANATHAN M</td>
				<td width="199">OFS</td>
				<td width="239">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="62">3.</td>
				<td width="172">ABIRAMI J</td>
				<td width="199">HCL TECHNOLOGIES</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">4.</td>
				<td width="172">SHOBANA</td>
				<td width="199">HCL TECHNOLOGIES</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">5.</td>
				<td width="172">JAYASHREE N</td>
				<td width="199">HCL TECHNOLOGIES</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">6.</td>
				<td width="172">GAYATHRI M</td>
				<td width="199">HCL TECHNOLOGIES</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">7.</td>
				<td width="172">SIBI CHAKRAVARTHY.G</td>
				<td width="199">HCL TECHNOLOGIES</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">8.</td>
				<td width="172">AKSHAYA S</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">9.</td>
				<td width="172">DIVYA R</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">10.</td>
				<td width="172">KIRTHIKA K</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">11.</td>
				<td width="172">ABIRAMI J</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">12.</td>
				<td width="172">ARAVINTH B</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">13.</td>
				<td width="172">ELAKKIYA B</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">14.</td>
				<td width="172">RAJESWARI U</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">15.</td>
				<td width="172">SINDHUJA E</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">16.</td>
				<td width="172">SIBI CHAKRAVARTHY G</td>
				<td width="199">KUMARAN SYSTEMS</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">17.</td>
				<td width="172">DIVYA G</td>
				<td width="199">MPHASIS</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">18.</td>
				<td width="172">PALLAVI A INDUKAR</td>
				<td width="199">MPHASIS</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">19.</td>
				<td width="172">JAI KISHORE J</td>
				<td width="199">MPHASIS</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">20.</td>
				<td width="172">JAI KISHORE J</td>
				<td width="199">INFOSYS</td>
				<td width="239">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">21.</td>
				<td width="172">SHAHINA BEGUM</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">22.</td>
				<td width="172">SHAMILI M</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">23.</td>
				<td width="172">SWETHA PRIYA</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">24.</td>
				<td width="172">CHARANYA T</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">25.</td>
				<td width="172">ASHWIN S</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">26.</td>
				<td width="172">DEEPIKA M</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">27.</td>
				<td width="172">JAGADEESH BABU P</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">28.</td>
				<td width="172">PAVUNU PANDI R</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">29.</td>
				<td width="172">JAYASHREE N</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">30.</td>
				<td width="172">POONKUZHALI M</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">31.</td>
				<td width="172">SRI MUTHRA K</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">32.</td>
				<td width="172">PUSHPA D</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">33.</td>
				<td width="172">UMA SHANKAR D</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">34.</td>
				<td width="172">SUGUMAR R</td>
				<td width="199">CTS</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">35.</td>
				<td width="172">PRIYADARSHINI H</td>
				<td width="199">MPHASIS</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">36.</td>
				<td width="172">ARTHI C</td>
				<td width="199">TECH MAHINDRA</td>
				<td width="239">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">37.</td>
				<td width="172">LAKSHMI PRIYA G</td>
				<td width="199">TECH MAHINDRA</td>
				<td width="239">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">38.</td>
				<td width="172">ANITA MARY A</td>
				<td width="199">TECH MAHINDRA</td>
				<td width="239">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">39.</td>
				<td width="172">AKSHAYA S</td>
				<td width="199">TECH MAHINDRA</td>
				<td width="239">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">40.</td>
				<td width="172">DIVYA G</td>
				<td width="199">TECH MAHINDRA</td>
				<td width="239">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="62">41.</td>
				<td width="172">NIVETHA R G</td>
				<td width="199">MPHASIS</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">42.</td>
				<td width="172">PALLAVI A INDUKAR</td>
				<td width="199">L&amp;T INFOTECH</td>
				<td width="239">Graduate Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">43.</td>
				<td width="172">ANIS FATHIMA</td>
				<td width="199">INFOSYS</td>
				<td width="239">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">44.</td>
				<td width="172">ARAVINDHAN P</td>
				<td width="199">CONGRUENT</td>
				<td width="239">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">45.</td>
				<td width="172">RAKESH G</td>
				<td width="199">CONGRUENT</td>
				<td width="239">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">46.</td>
				<td width="172">BHARGAVI N</td>
				<td width="199">SOFT SQUARE</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">47.</td>
				<td width="172">GAYATHRI M</td>
				<td width="199">SOFT SQUARE</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">48.</td>
				<td width="172">JAYASRI S</td>
				<td width="199">SOFT SQUARE</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">49.</td>
				<td width="172">KARTHIGA G R</td>
				<td width="199">SOFT SQUARE</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">50.</td>
				<td width="172">ABIRAMI J</td>
				<td width="199">MAZENET SOLUTION</td>
				<td width="239">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">51.</td>
				<td width="172">ANITHA T</td>
				<td width="199">MAZENET SOLUTION</td>
				<td width="239">Jr Technical Trainer</td>
				</tr>
				<tr>
				<td width="62">52.</td>
				<td width="172">HEMALATHA D</td>
				<td width="199">EXCELACOM</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">53.</td>
				<td width="172">BALA ABIRAMI</td>
				<td width="199">EXCELACOM</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">54.</td>
				<td width="172">RANJITH KUMAR B</td>
				<td width="199">AON HEWITT</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">55.</td>
				<td width="172">DIVYA M</td>
				<td width="199">AON HEWITT</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">56.</td>
				<td width="172">LOGANATHAN M</td>
				<td width="199">AURUM INFOSOL</td>
				<td width="239">Software Trainee – L1</td>
				</tr>
				<tr>
				<td width="62">57.</td>
				<td width="172">UMA SHANKAR D</td>
				<td width="199">AURUM INFOSOL</td>
				<td width="239">Software Trainee – L1</td>
				</tr>
				<tr>
				<td width="62">58.</td>
				<td width="172">VINOTHINI R</td>
				<td width="199">AURUM INFOSOL</td>
				<td width="239">Software Trainee – L1</td>
				</tr>
				<tr>
				<td width="62">59.</td>
				<td width="172">DHILIP KUMAR</td>
				<td width="199">COMFYI SOLUTION</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">60.</td>
				<td width="172">SUMATHI</td>
				<td width="199">COMFYI SOLUTION</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">61.</td>
				<td width="172">SUVITHA</td>
				<td width="199">COMFYI SOLUTION</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">62.</td>
				<td width="172">DEEPIKA</td>
				<td width="199">CMC LIMITED</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">63.</td>
				<td width="172">VINOTH</td>
				<td width="199">CMC LIMITED</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">64.</td>
				<td width="172">PUSHPA D</td>
				<td width="199">CMC LIMITED</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">65.</td>
				<td width="172">GOPI N V</td>
				<td width="199">CMC LIMITED</td>
				<td width="239">Programmer Analyst Trainee</td>
				</tr>
				<tr>
				<td width="62">66.</td>
				<td width="172">MAHALAKSHMI V S</td>
				<td width="199">ASS SYSTEMS INDIA (P) LTD</td>
				<td width="239">System Trainee</td>
				</tr>
				<tr>
				<td width="62">67.</td>
				<td width="172">MADHAN</td>
				<td width="199">ASS SYSTEMS INDIA (P) LTD</td>
				<td width="239">System Trainee</td>
				</tr>
				<tr>
				<td width="62">68.</td>
				<td width="172">PRIYA K</td>
				<td width="199">I GENESIS</td>
				<td width="239">Jr Software Developer</td>
				</tr>
				<tr>
				<td width="62">69.</td>
				<td width="172">ROOPA U</td>
				<td width="199">I GENESIS</td>
				<td width="239">Jr Software Developer</td>
				</tr>
				<tr>
				<td width="62">70.</td>
				<td width="172">POONKUZHALI M</td>
				<td width="199">TATA CONSULTANCY SERVICES(TCS)</td>
				<td width="239">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">71.</td>
				<td width="172">JAYASRI S</td>
				<td width="199">ACCENTURE</td>
				<td width="239">Software Engineer</td>
				</tr>
				<tr>
				<td width="62">72.</td>
				<td width="172">KARTHIK M</td>
				<td width="199">CSS CORP</td>
				<td width="239">Tech Support</td>
				</tr>
				<tr>
				<td width="62">73.</td>
				<td width="172">SENTHIL NATHAN V</td>
				<td width="199">DELL</td>
				<td width="239">Tech Support</td>
				</tr>
				<tr>
				<td width="62">74.</td>
				<td width="172">PAVUNU PANDI R</td>
				<td width="199">ASPIRANT LABS</td>
				<td width="239">Trainee</td>
				</tr>
				<tr>
				<td width="62">75.</td>
				<td width="172">PRIYA K</td>
				<td width="199">TSQUARE HR CONSULTING (P) LTD</td>
				<td width="239">Associate Recruiter</td>
				</tr>
				<tr>
				<td width="62">76.</td>
				<td width="172">THIRUMENINATHAN V</td>
				<td width="199">AEGIS</td>
				<td width="239">Support Executive</td>
				</tr>
				<tr>
				<td width="62">77.</td>
				<td width="172">PARTHIBAN R</td>
				<td width="199">AEGIS</td>
				<td width="239">Support Executive</td>
				</tr>
				<tr>
				<td width="62">78.</td>
				<td width="172">MOHAN RAJ S</td>
				<td width="199">BAHWAN CYBERTEK</td>
				<td width="239">Trainee – Software Engineer</td>
				</tr>
				<tr>
				<td width="62">79.</td>
				<td width="172">SUGUMAR R</td>
				<td width="199">BAHWAN CYBERTEK</td>
				<td width="239">Trainee – Software Engineer</td>
				</tr>
				<tr>
				<td width="62">80.</td>
				<td width="172">AMURU REVANTH KUMAR</td>
				<td width="199">BAHWAN CYBERTEK</td>
				<td width="239">Trainee – Software Engineer</td>
				</tr>
				<tr>
				<td width="62">81.</td>
				<td width="172">SASI KUMAR U</td>
				<td width="199">BAHWAN CYBERTEK</td>
				<td width="239">Trainee – Software Engineer</td>
				</tr>
				<tr>
				<td width="62">82.</td>
				<td width="172">GANESH K</td>
				<td width="199">BAHWAN CYBERTEK</td>
				<td width="239">Trainee – Software Engineer</td>
				</tr>
				<tr>
				<td width="62">83.</td>
				<td width="172">ELAKKIYA B</td>
				<td width="199">DEXETERITY BUSINESS SOLUTIONS</td>
				<td width="239">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">84.</td>
				<td width="172">SURESH KUMAR S</td>
				<td width="199">DEXETERITY BUSINESS SOLUTIONS</td>
				<td width="239">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">85.</td>
				<td width="172">KARTHIK V</td>
				<td width="199">DEXETERITY BUSINESS SOLUTIONS</td>
				<td width="239">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">86.</td>
				<td width="172">NITHIN A N</td>
				<td width="199">DEXETERITY BUSINESS SOLUTIONS</td>
				<td width="239">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="62">87.</td>
				<td width="172">SATHYAVENI</td>
				<td width="199">SOLUTIONS UNLIMITED</td>
				<td width="239">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">88.</td>
				<td width="172">SHYVYA R V</td>
				<td width="199">SOLUTIONS UNLIMITED</td>
				<td width="239">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">89.</td>
				<td width="172">SRI RANJANI E</td>
				<td width="199">SOLUTIONS UNLIMITED</td>
				<td width="239">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">90.</td>
				<td width="172">SANDHIYA S</td>
				<td width="199">SOLUTIONS UNLIMITED</td>
				<td width="239">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">91.</td>
				<td width="172">PRASANTH M P V S</td>
				<td width="199">SOLUTIONS UNLIMITED</td>
				<td width="239">Software Trainee</td>
				</tr>
				<tr>
				<td width="62">92.</td>
				<td width="172">SOWMIYA R</td>
				<td width="199">ADHITYA A INFOMEDIA SOLUTIONS</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">93.</td>
				<td width="172">UVASRI P</td>
				<td width="199">ADHITYA A INFOMEDIA SOLUTIONS</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">94.</td>
				<td width="172">INDHUMATHI K S</td>
				<td width="199">ADHITYA A INFOMEDIA SOLUTIONS</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">95.</td>
				<td width="172">SENTHIL KUMAR D</td>
				<td width="199">ADHITYA A INFOMEDIA SOLUTIONS</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">96.</td>
				<td width="172">DHIVYA V</td>
				<td width="199">ADHITYA A INFOMEDIA SOLUTIONS</td>
				<td width="239">Engineer Trainee</td>
				</tr>
				<tr>
				<td width="62">97.</td>
				<td width="172">GANESH BABU H</td>
				<td width="199">IF ELSE TECHNOLOGIES (P) LTD</td>
				<td width="239">SOFTWARE TRAINEE</td>
				</tr>
				<tr>
				<td width="62">98.</td>
				<td width="172">SATHISH KUMAR J</td>
				<td width="199">IF ELSE TECHNOLOGIES (P) LTD</td>
				<td width="239">SOFTWARE TRAINEE</td>
				</tr>
				<tr>
				<td width="62">99.</td>
				<td width="172">MONIGA E</td>
				<td width="199">IF ELSE TECHNOLOGIES (P) LTD</td>
				<td width="239">SOFTWARE TRAINEE</td>
				</tr>
				<tr>
				<td width="62">100.</td>
				<td width="172">JAYAKUMARI D</td>
				<td width="199">IF ELSE TECHNOLOGIES (P) LTD</td>
				<td width="239">SOFTWARE TRAINEE</td>
				</tr>
				<tr>
				<td width="62">101.</td>
				<td width="172">PANDIAN K</td>
				<td width="199">IPRO SOLUTIONS</td>
				<td width="239">Voice Executive</td>
				</tr>
				<tr>
				<td width="62">102.</td>
				<td width="172">SRIKANTH S</td>
				<td width="199">IPRO SOLUTIONS</td>
				<td width="239">Voice Executive</td>
				</tr>
				<tr>
				<td width="62">103.</td>
				<td width="172">RAMESH E</td>
				<td width="199">IPRO SOLUTIONS</td>
				<td width="239">Voice Executive</td>
				</tr>
				<tr>
				<td width="62">104.</td>
				<td width="172">RAJAVEL T</td>
				<td width="199">IPRO SOLUTIONS</td>
				<td width="239">Voice Executive</td>
				</tr>
				<tr>
				<td width="62">105.</td>
				<td width="172">RAJKUMAR V</td>
				<td width="199">IPRO SOLUTIONS</td>
				<td width="239">Voice Executive</td>
				</tr>
				<tr>
				<td width="62">106.</td>
				<td width="172">SAI MOHIT M S</td>
				<td width="199">MOBIUS KNOWLEDGE SERVICES (P) LTD</td>
				<td width="239">EXECUTIVE WEB RESEARCH</td>
				</tr>
				<tr>
				<td width="62">107.</td>
				<td width="172">SANTHAN KRISHNAN R</td>
				<td width="199">MOBIUS KNOWLEDGE SERVICES (P) LTD</td>
				<td width="239">EXECUTIVE WEB RESEARCH</td>
				</tr>
				<tr>
				<td width="62">108.</td>
				<td width="172">RAJAN A</td>
				<td width="199">MOBIUS KNOWLEDGE SERVICES (P) LTD</td>
				<td width="239">EXECUTIVE WEB RESEARCH</td>
				</tr>
				<tr>
				<td width="62">109.</td>
				<td width="172">RAJ KUMAR N</td>
				<td width="199">MOBIUS KNOWLEDGE SERVICES (P) LTD</td>
				<td width="239">EXECUTIVE WEB RESEARCH</td>
				</tr>
				<tr>
				<td width="62">110.</td>
				<td width="172">RAMYA N</td>
				<td width="199">MOBIUS KNOWLEDGE SERVICES (P) LTD</td>
				<td width="239">EXECUTIVE WEB RESEARCH</td>
				</tr>
				</tbody>
				</table>
                <br/>
				<h4>2012-2013</h4>
				<table width="654">
				<tbody>
				<tr>
				<td width="73"><strong>S.NO</strong></td>
				<td width="179"><strong>NAME</strong></td>
				<td width="186"><strong>COMPANY NAME</strong></td>
				<td width="216"><strong>Designation</strong></td>
				</tr>
				<tr>
				<td width="73">1.</td>
				<td width="179">Karthicka S</td>
				<td width="186">CTS</td>
				<td width="216">Program Analyst Trainee</td>
				</tr>
				<tr>
				<td width="73">2.</td>
				<td width="179">Revathi A</td>
				<td width="186">Virtusa</td>
				<td width="216">Program Analyst Trainee</td>
				</tr>
				<tr>
				<td width="73">3.</td>
				<td width="179">Sooriya Raghuraj</td>
				<td width="186">Virtusa</td>
				<td width="216">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">4.</td>
				<td width="179">Lokesh P</td>
				<td width="186">Soft Square</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">5.</td>
				<td width="179">Meghala P S</td>
				<td width="186">Soft Square</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">6.</td>
				<td width="179">Rajaraman S</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">7.</td>
				<td width="179">Hemavathi H</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">8.</td>
				<td width="179">Sathish Kumar V</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">9.</td>
				<td width="179">Nanmohan M</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">10.</td>
				<td width="179">Athiq Ur Raza Ahamed M</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">11.</td>
				<td width="179">Sonia Jain S</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">12.</td>
				<td width="179">Mukesh Kumar P G</td>
				<td width="186">Quest</td>
				<td width="216">Software Trainee Associate</td>
				</tr>
				<tr>
				<td width="73">13.</td>
				<td width="179">Prasath S</td>
				<td width="186">MPhasis</td>
				<td width="216">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">14.</td>
				<td width="179">Aathira Jayan</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">15.</td>
				<td width="179">Arul Tamil Elakkiya&nbsp; S</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">16.</td>
				<td width="179">Bala Subramani M</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">17.</td>
				<td width="179">Divya K</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">18.</td>
				<td width="179">Manikandan A</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">19.</td>
				<td width="179">Rajiv J</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">20.</td>
				<td width="179">Rekha V</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">21.</td>
				<td width="179">Sathya S</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">22.</td>
				<td width="179">Vakathi Swathi</td>
				<td width="186">Exl Infotel</td>
				<td width="216">Site Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">23.</td>
				<td width="179">Suganya S(70)</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">24.</td>
				<td width="179">Suganya S(71)</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">25.</td>
				<td width="179">Madhumitha G</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">26.</td>
				<td width="179">Anish Kumar R</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">27.</td>
				<td width="179">Gunasekar G</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">28.</td>
				<td width="179">Sridevi R</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">29.</td>
				<td width="179">Vaishnavi R</td>
				<td width="186">Zenoova</td>
				<td width="216">Software programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">30.</td>
				<td width="179">Ranjith P K</td>
				<td width="186">ABCO Adivisory</td>
				<td width="216">Junior Programmer</td>
				</tr>
				<tr>
				<td width="73">31.</td>
				<td width="179">Hemavathi H</td>
				<td width="186">OFS</td>
				<td width="216">Programmer Trainee</td>
				</tr>
				<tr>
				<td width="73">32.</td>
				<td width="179">Syed Nadeem K</td>
				<td width="186">Maveric Systems</td>
				<td width="216">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="73">33.</td>
				<td width="179">Shyam Sundar R</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">34.</td>
				<td width="179">Hemalatha R</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">35.</td>
				<td width="179">Karthika S P</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">36.</td>
				<td width="179">Pandi Kannan B</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">37.</td>
				<td width="179">Kavitha M</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">38.</td>
				<td width="179">Hemavathi V</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">39.</td>
				<td width="179">Vishal M</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">40.</td>
				<td width="179">Sharmila M</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">41.</td>
				<td width="179">Sasikala K</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">42.</td>
				<td width="179">Malathi D</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">43.</td>
				<td width="179">Karthik P</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">44.</td>
				<td width="179">Divya Lakshmi V</td>
				<td width="186">Focus Infotech (P).Ltd.,</td>
				<td width="216">System Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">45.</td>
				<td width="179">Divya M</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">46.</td>
				<td width="179">Kiran Kumar V</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">47.</td>
				<td width="179">Madan Kumar C</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">48.</td>
				<td width="179">Mohan Kumar G</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">49.</td>
				<td width="179">Mukesh V</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">50.</td>
				<td width="179">Sathyaseelan&nbsp; S</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">51.</td>
				<td width="179">Sridevi R</td>
				<td width="186">Transvelo</td>
				<td width="216">Android Developer Trainee</td>
				</tr>
				<tr>
				<td width="73">52.</td>
				<td width="179">NanMohan M</td>
				<td width="186">CGI</td>
				<td width="216">Associate Software Engineer</td>
				</tr>
				<tr>
				<td width="73">53.</td>
				<td width="179">Suresh Kumar K</td>
				<td width="186">Hardland Clarke Holding Software</td>
				<td width="216">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="73">54.</td>
				<td width="179">Sridhar Velmurugan J</td>
				<td width="186">Flextronics</td>
				<td width="216">Software Trainee</td>
				</tr>
				<tr>
				<td width="73">55.</td>
				<td width="179">Gunasekar R</td>
				<td width="186">HCL Technologies</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">56.</td>
				<td width="179">Hemalatha R</td>
				<td width="186">HCL Technologies</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">57.</td>
				<td width="179">Bashetha A</td>
				<td width="186">Bahwan Cyber Tek</td>
				<td width="216">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="73">58.</td>
				<td width="179">Deepak Kumar R</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">59.</td>
				<td width="179">Balraj L</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">60.</td>
				<td width="179">Divya R</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">61.</td>
				<td width="179">Bharath S</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">62.</td>
				<td width="179">Jayachandran K</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">63.</td>
				<td width="179">Kavitha K</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">64.</td>
				<td width="179">Saranya S</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">65.</td>
				<td width="179">Ramajayanthi T</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">66.</td>
				<td width="179">Sandhya B</td>
				<td width="186">Web &amp; Crafts</td>
				<td width="216">Junior Software Engineer</td>
				</tr>
				<tr>
				<td width="73">67.</td>
				<td width="179">Sridhar Velmurugan J</td>
				<td width="186">Saisun Solution(P) Ltd.,</td>
				<td width="216">Junior Programmer</td>
				</tr>
				<tr>
				<td width="73">68.</td>
				<td width="179">Srithar K</td>
				<td width="186">Saisun Solution(P) Ltd.,</td>
				<td width="216">Junior Programmer</td>
				</tr>
				<tr>
				<td width="73">69.</td>
				<td width="179">Suganya Gandhi D</td>
				<td width="186">Saisun Solution(P) Ltd.,</td>
				<td width="216">Junior Programmer</td>
				</tr>
				<tr>
				<td width="73">70.</td>
				<td width="179">Sundaresan P</td>
				<td width="186">Saisun Solution(P) Ltd.,</td>
				<td width="216">Junior Programmer</td>
				</tr>
				<tr>
				<td width="73">71.</td>
				<td width="179">Deepak Singh P</td>
				<td width="186">L&amp;T Infotech</td>
				<td width="216">Graduate Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">72.</td>
				<td width="179">Prasanth Srinivasan S</td>
				<td width="186">L&amp;T Infotech</td>
				<td width="216">Graduate Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">73.</td>
				<td width="179">Manikandan A</td>
				<td width="186">Excelacom</td>
				<td width="216">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="73">74.</td>
				<td width="179">Karthick Kumar S</td>
				<td width="186">Excelacom</td>
				<td width="216">Trainee Software Engineer</td>
				</tr>
				<tr>
				<td width="73">75.</td>
				<td width="179">Vimal Raj S</td>
				<td width="186">Temenos</td>
				<td width="216">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">76.</td>
				<td width="179">Vinoth Kumar D</td>
				<td width="186">Temenos</td>
				<td width="216">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">77.</td>
				<td width="179">Suganya J</td>
				<td width="186">Temenos</td>
				<td width="216">Software Engineer Trainee</td>
				</tr>
				<tr>
				<td width="73">78.</td>
				<td width="179">Sathish Kumar S</td>
				<td width="186">CIITE</td>
				<td width="216">Software Trainee</td>
				</tr>
				<tr>
				<td width="73">79.</td>
				<td width="179">Sri Devi R</td>
				<td width="186">Atees Infomedia(P)Ltd.,</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">80.</td>
				<td width="179">Suresh S</td>
				<td width="186">Atees Infomedia(P)Ltd.,</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">81.</td>
				<td width="179">Vakati Swathi</td>
				<td width="186">Atees Infomedia(P)Ltd.,</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">82.</td>
				<td width="179">Kathikeyan S</td>
				<td width="186">Atees Infomedia(P)Ltd.,</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">83.</td>
				<td width="179">Sathya S</td>
				<td width="186">Atees Infomedia(P)Ltd.,</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">84.</td>
				<td width="179">PremKumar J</td>
				<td width="186">Kumaran Systems</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">85.</td>
				<td width="179">Sathish Kumar V</td>
				<td width="186">Kumaran Systems</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">86.</td>
				<td width="179">Rajiv J</td>
				<td width="186">Kumaran Systems</td>
				<td width="216">Software Engineer</td>
				</tr>
				<tr>
				<td width="73">87.</td>
				<td width="179">Sathya S</td>
				<td width="186">CTS</td>
				<td width="216">Programmer Analysts Trainee</td>
				</tr>
				<tr>
				<td width="73">88.</td>
				<td width="179">Rajaraman</td>
				<td width="186">AIG Systems</td>
				<td width="216">Software Trainee</td>
				</tr>
				<tr>
				<td width="73">89.</td>
				<td width="179">Ranjith P K</td>
				<td width="186">AIG Systems</td>
				<td width="216">Software Trainee</td>
				</tr>
				<tr>
				<td width="73">90.</td>
				<td width="179">Saravanan P</td>
				<td width="186">AIG Systems</td>
				<td width="216">Software Trainee</td>
				</tr>
				<tr>
				<td width="73">91.</td>
				<td width="179">Sunil S</td>
				<td width="186">AIG Systems</td>
				<td width="216">Software Trainee</td>
				</tr>
				<tr>
				<td width="73">92.</td>
				<td width="179">Suresh S</td>
				<td width="186">AIG Systems</td>
				<td width="216">Software Trainee</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div>
				<h3>HIGHER STUDIES</h3>
				<div>
					<table width="592">
					<tbody>
					<tr>
					<td colspan="4" width="592"><strong>Year&nbsp; – 2009-2013</strong></td>
					</tr>
					<tr>
					<td width="46"><strong>Sl.no</strong></td>
					<td width="188"><strong>Name of the Student</strong></td>
					<td width="108"><strong>Course Taken</strong></td>
					<td width="251"><strong>University/ College Name</strong></td>
					</tr>
					<tr>
					<td width="46">1.</td>
					<td width="188">BAGYALAKSHMI.M</td>
					<td width="108">M.E</td>
					<td width="251">Francis Xavier Engineering College, Tirunelveli</td>
					</tr>
					<tr>
					<td width="46">2.</td>
					<td width="188">KAVITHA.V</td>
					<td width="108">M.E</td>
					<td width="251">S.A.Engineering College, Chennai</td>
					</tr>
					<tr>
					<td width="46">3.</td>
					<td width="188">BASHETHA.A</td>
					<td width="108">M.E</td>
					<td width="251">S.A.Engineering College, Chennai</td>
					</tr>
					<tr>
					<td width="46">4.</td>
					<td width="188">SATHISH KUMAR.S</td>
					<td width="108">M.B.A</td>
					<td width="251">University of Madras, Chennai</td>
					</tr>
					<tr>
					<td width="46">5.</td>
					<td width="188">RAJIV</td>
					<td width="108">M.B.A</td>
					<td width="251">Velammal Institute of Technology, Chennai</td>
					</tr>
					<tr>
					<td width="46">6.</td>
					<td width="188">RAJARAM</td>
					<td width="108">M.B.A</td>
					<td width="251">Symbiosis Centre of Distance Learning, Chennai</td>
					</tr>
					<tr>
					<td width="46">7.</td>
					<td width="188">RANJITH</td>
					<td width="108">M.B.A</td>
					<td width="251">Velammal Institute of Technology, Chennai</td>
					</tr>
					<tr>
					<td width="46">8.</td>
					<td width="188">VISHAL</td>
					<td width="108">M.B.A</td>
					<td width="251">Valliammal Engineering College, Chennai</td>
					</tr>
					<tr>
					<td width="46">9.</td>
					<td width="188">JOHN FERNANDO.R</td>
					<td width="108">M.E</td>
					<td width="251">Meenakshi College of Engineering, Chennai</td>
					</tr>
					<tr>
					<td width="46">10.</td>
					<td width="188">PRASATH.S</td>
					<td width="108">M.E</td>
					<td width="251">Meenakshi College of Engineering, Chennai</td>
					</tr>
					<tr>
					<td width="46">11.</td>
					<td width="188">MEGALA.P.S</td>
					<td width="108">M.TECH</td>
					<td width="251">VIT University, Chennai</td>
					</tr>
					<tr>
					<td width="46">12.</td>
					<td width="188">DIVYA.R</td>
					<td width="108">M.E</td>
					<td width="251">JEPPIAR Engineering College, Chennai</td>
					</tr>
					<tr>
					<td width="46">13.</td>
					<td width="188">DEEPAK SINGH</td>
					<td width="108">M.E</td>
					<td width="251">St.Peter’s University, Chennai</td>
					</tr>
					<tr>
					<td width="46">14.</td>
					<td width="188">AAMIL MOHAMMED.F</td>
					<td width="108">M.B.A</td>
					<td width="251">Loyola College, Chennai</td>
					</tr>
					</tbody>
					</table>
				</div>
				<div>
				<table width="597">
				<tbody>
				<tr>
				<td colspan="4" width="597"><strong>Year&nbsp; – 2010-2014</strong></td>
				</tr>
				<tr>
				<td width="46"><strong>Sl.no</strong></td>
				<td width="197"><strong>Name of the Student</strong></td>
				<td width="113"><strong>Course Taken</strong></td>
				<td width="241"><strong>University/ College Name</strong></td>
				</tr>
				<tr>
				<td width="46">1.</td>
				<td width="197">SHYVYA</td>
				<td width="113">P.G Diploma In Packaging</td>
				<td width="241">Indian Institute of Packaging, Hyderabad</td>
				</tr>
				<tr>
				<td width="46">2.</td>
				<td width="197">SATHYAVENI.S</td>
				<td width="113">M.B.A</td>
				<td width="241">SRM University, Chennai</td>
				</tr>
				<tr>
				<td width="46">3.</td>
				<td width="197">CHARANYA.T</td>
				<td width="113">M.E</td>
				<td width="241">S.A.Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">4.</td>
				<td width="197">SASI KUMAR</td>
				<td width="113">M.S</td>
				<td width="241">Stratford University,Virginia,US</td>
				</tr>
				<tr>
				<td width="46">5.</td>
				<td width="197">GURUPRASANTH</td>
				<td width="113">M.B.A</td>
				<td width="241">SRM University, Chennai</td>
				</tr>
				<tr>
				<td width="46">6.</td>
				<td width="197">S.ALLEN DANIEL</td>
				<td width="113">M.B.A</td>
				<td width="241">Lingaya’s University, Faridabad</td>
				</tr>
				<tr>
				<td width="46">7.</td>
				<td width="197">POLURU JAGADHEESH BABU</td>
				<td width="113">M.B.A</td>
				<td width="241">Lingaya’s University, Faridabad</td>
				</tr>
				<tr>
				<td width="46">8.</td>
				<td width="197">A.KRISHNAVENI</td>
				<td width="113">M.E</td>
				<td width="241">S.A.Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">9.</td>
				<td width="197">SRI RANJANI</td>
				<td width="113">M.S</td>
				<td width="241">University of Hertfordshire, UK</td>
				</tr>
				<tr>
				<td width="46">10.</td>
				<td width="197">ARAVIND.B</td>
				<td width="113">M.TECH</td>
				<td width="241">SRM University, Chennai</td>
				</tr>
				<tr>
				<td width="46">11.</td>
				<td width="197">SENTHIL NATHAN</td>
				<td width="113">M.E</td>
				<td width="241">Valliammai Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">12.</td>
				<td width="197">YUVASHREE</td>
				<td width="113">M.B.A</td>
				<td width="241">University of Madras, Chennai</td>
				</tr>
				<tr>
				<td width="46">13.</td>
				<td width="197">PADMINI</td>
				<td width="113">M.B.A</td>
				<td width="241">Velammal Institute of Technology, Chennai</td>
				</tr>
				<tr>
				<td width="46">14.</td>
				<td width="197">MOHAN RAJ</td>
				<td width="113">M.B.A</td>
				<td width="241">Velammal Institute of Technology, Chennai</td>
				</tr>
				<tr>
				<td width="46">15.</td>
				<td width="197">SUMATHI SUBRAMANI</td>
				<td width="113">M.S</td>
				<td width="241">Stratford University,Virginia</td>
				</tr>
				<tr>
				<td width="46">16.</td>
				<td width="197">DIVYA.R</td>
				<td width="113">MBA</td>
				<td width="241">Anna University, Chennai</td>
				</tr>
				</tbody>
				</table>
				</div>
				<div>
				<table width="593">
				<tbody>
				<tr>
				<td colspan="4" width="593"><strong>Year&nbsp; – 2011-2015</strong></td>
				</tr>
				<tr>
				<td width="46"><strong>Sl.no</strong></td>
				<td width="209"><strong>Name of the Student</strong></td>
				<td width="119"><strong>Course Taken</strong></td>
				<td width="220"><strong>University/ College Name</strong></td>
				</tr>
				<tr>
				<td width="46">1.</td>
				<td width="209">AMALAN SENTHAMIZHINIYAN</td>
				<td width="119">P.G.D.M</td>
				<td width="220">XIME, Kochi</td>
				</tr>
				<tr>
				<td width="46">2.</td>
				<td width="209">MOUNICA CHILUKURI</td>
				<td width="119">M.S</td>
				<td width="220">Washington University, Washington</td>
				</tr>
				<tr>
				<td width="46">3.</td>
				<td width="209">RAVEENA.S</td>
				<td width="119">M.E</td>
				<td width="220">R.M.D Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">4.</td>
				<td width="209">S.MALARVIZHI</td>
				<td width="119">M.B.A</td>
				<td width="220">Easwari Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">5.</td>
				<td width="209">MITHRA MATHIVATHANAN</td>
				<td width="119">M.S</td>
				<td width="220">Curtin University, West Australia</td>
				</tr>
				<tr>
				<td width="46">6.</td>
				<td width="209">N.K. ASWINI</td>
				<td width="119">M.B.A</td>
				<td width="220">Panimalar Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">7.</td>
				<td width="209">ARAVIND.S</td>
				<td width="119">M.B.A</td>
				<td width="220">AUBURN University, Sweden</td>
				</tr>
				</tbody>
				</table>
				</div>
				<div>
				<table width="595">
				<tbody>
				<tr>
				<td colspan="4" width="595"><strong>Year&nbsp; – 2012-2016</strong></td>
				</tr>
				<tr>
				<td width="46"><strong>Sl.no</strong></td>
				<td width="195"><strong>Name of the Student</strong></td>
				<td width="118"><strong>Course Taken</strong></td>
				<td width="237"><strong>University/ College Name</strong></td>
				</tr>
				<tr>
				<td width="46">1.</td>
				<td width="195">DIVYA.C</td>
				<td width="118">M.B.A</td>
				<td width="237">Vel Tech University, Chennai</td>
				</tr>
				<tr>
				<td width="46">2.</td>
				<td width="195">SHRUTHI</td>
				<td width="118">M.B.A</td>
				<td width="237">Saveetha Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">3.</td>
				<td width="195">PREETHI.G</td>
				<td width="118">ME</td>
				<td width="237">R.M.D.Engineering College, Chennai</td>
				</tr>
				<tr>
				<td width="46">4.</td>
				<td width="195">DEEPIKA.G</td>
				<td width="118">M.B.A</td>
				<td width="237">SRM University, Chennai</td>
				</tr>
				<tr>
				<td width="46">5.</td>
				<td width="195">MAHALAKSHMI.V.S</td>
				<td width="118">M.B.A</td>
				<td width="237">Madras University, Chennai</td>
				</tr>
				<tr>
				<td width="46">6.</td>
				<td width="195">PUNITHA ZUDITH.G</td>
				<td width="118">M.B.A</td>
				<td width="237">Saveetha Engineering College, Chennai</td>
				</tr>
				</tbody>
				</table>
				</div>
			</div>
			
	</div>


		
    <div class="tab-content fade" id="menu6">

        <div class="container">
            <h2 class="title_line">FDP</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a class="fancybox" href="img/cse/fdp1.jpg" data-fancybox-group="gallery20" title="Automata Theory and Compilers">
                                        <img src="<?php echo $siteurl ?>/img/cse/fdp1.jpg" class="img-responsive">
                                        </a>
                                    </div>    
                                    <div class="text">FDP on “Automata Theory and Compilers” from 13-12-17 to 15-12-17. <br />
										 Chief Guest <strong>Dr.R.Rama</strong>, Professor, IIT Madras, Chennai</div>                      
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/fdp2.jpg" data-fancybox-group="gallery20" title="Ad-hoc Sensor Networks">
                                        <img src="<?php echo $siteurl ?>/img/cse/fdp2.jpg" class="img-responsive">
                                        </a>
                                    </div> 
                                    <div class="text">FDP on "Ad-hoc Sensor Networks” from 07.06.17 to 14.06.17. <br />
										<strong>Mr. J.Santhana Krishnan</strong>, Chief Technical officer, Tamil Nadu Arasu Cable TV Operation, Chennai</div>                        
                                </div>
                    

                                <div class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/fdp3.jpg" data-fancybox-group="gallery20" title="Graph Theory and Applications">
                                        <img src="<?php echo $siteurl ?>/img/cse/fdp3.jpg" class="img-responsive">
                                        </a>
                                    </div>   
                                    <div class="text">FDP on "Graph Theory and Applications” from 07.06.17 to 09.06.17. <br />
									Chief Guest <strong>Dr.G.Sethuraman</strong>, Department of Mathematics, Anna University, Chennai</div>                        
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/fdp4.jpg" data-fancybox-group="gallery20" title="INTERNET OF THINGS (IOT)">
                                        <img src="<?php echo $siteurl ?>/img/cse/fdp4.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="text">FDP on “INTERNET OF THINGS (IOT)” from 29.12.17 to 30.12.17. <br />
										Chief Guest <strong>Mr.Ravi Chandar</strong>, Corporate Trainer, ICT Academy, Chennai</div>                        
                                </div>
                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a class="fancybox" href="img/cse/fdp5.jpg" data-fancybox-group="gallery20" title="Team Building">
                                        <img src="<?php echo $siteurl ?>/img/cse/fdp5.jpg" class="img-responsive">
                                        </a>
                                    </div>
         
                                    <div class="text">FDP on “Team Building” from 25.07.16 to 26.07.16 (2 days).<br /> Chief Guest <strong>Mr.K.Nirmal Kumar</strong>, Trainer, ICTACT, Chennai</div>                        
                                </div>
                    
                        <div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/fdp6_1.jpg" data-fancybox-group="gallery20" title="OnWireless Sensor Networks and The Internet of Things">
                                <img src="<?php echo $siteurl ?>/img/cse/fdp6_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Faculty Development Programme OnWireless Sensor Networks and The Internet of Things<br /> – A Research Perspective in Association with CSI and ACM from 10.12.2018 to 12.12.2018</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/fdp6_2.jpg" data-fancybox-group="gallery20" title="OnWireless Sensor Networks and The Internet of Things">
                                <img src="<?php echo $siteurl ?>/img/cse/fdp6_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Faculty Development Programme OnWireless Sensor Networks and The Internet of Things<br /> – A Research Perspective in Association with CSI and ACM from 10.12.2018 to 12.12.2018</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/fdp6_3.jpg" data-fancybox-group="gallery20" title="OnWireless Sensor Networks and The Internet of Things">
                                <img src="<?php echo $siteurl ?>/img/cse/fdp6_3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Faculty Development Programme OnWireless Sensor Networks and The Internet of Things<br /> – A Research Perspective in Association with CSI and ACM from 10.12.2018 to 12.12.2018</div>                        
                        </div>

				
            </div>

            </div>


			<h2 class="title_line">Guest Lecture</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a class="fancybox" href="img/cse/gu1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                        <img src="<?php echo $siteurl ?>/img/cse/gu1.jpg" class="img-responsive">
                                        </a>
                                    </div>    
                                    <div class="text">Guest Lecture on “Software Development Life Cycle in the Real World” on 05.02.2018 <br/>
							Chief Guest Mr. Kartic Vaidyanathan, IT Consultant, Chennai delivered an inspiring speech to the second year CSE Students.</div>                      
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/guest2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                        <img src="<?php echo $siteurl ?>/img/cse/guest2.jpg" class="img-responsive">
                                        </a>
                                    </div> 
                                    <div class="text">Guest lecture was conducted in S.A.Engineering College on the Topic “Engineering as Experimentation –Latest Technological Trends” on 15.02.2018.<br />
										Chief Guest Ms. Suganya Subbaraman, Corporate Trainer, Cognizant Technology Solutions, Chennai</div>                        
                                </div>
                    

                                <div class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/guest3.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                        <img src="<?php echo $siteurl ?>/img/cse/guest3.jpg" class="img-responsive">
                                        </a>
                                    </div>   
                                    <div class="text">Guest lecture on the Topic “Machine Learning Techniques” on 16.02.2018<br />
											Chief Guest Ms. S.Bharathi, Corporate Trainer, Cognizant Technology Solutions, Chennai</div>                        
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/guest4.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                        <img src="<?php echo $siteurl ?>/img/cse/guest4.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="text">Guest Lecture on “Exploring Big Data and its real time Use cases” on 21-02-18.<br />
											Chief Guest Mr Gowtham S.B, Corporate Trainer, AstraZeneca India Pvt Ltd, Chennai</div>                        
                                </div>
								<div  class="board_trustes_box" >
                                    <div >
                                        <a class="fancybox" href="img/cse/guest4(2).jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                        <img src="<?php echo $siteurl ?>/img/cse/guest4(2).jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="text">Guest Lecture on “Exploring Big Data and its real time Use cases” on 21-02-18.<br />
											Chief Guest Mr Gowtham S.B, Corporate Trainer, AstraZeneca India Pvt Ltd, Chennai</div>                        
                                </div>
                                <div class="board_trustes_box">
                                    <div >
                                        <a class="fancybox" href="img/cse/guest5.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                        <img src="<?php echo $siteurl ?>/img/cse/guest5.jpg" class="img-responsive">
                                        </a>
                                    </div>
         
                                    <div class="text">Guest lecture was conducted in S.A.Engineering College on the Topic “Evolution of NoSql Databases” on 26.02.2018<br />
									Chief Guest Mr Kaja Muyenudeen.H, Technical Specialist – MongoDB, Datavail Infotech Pvt Ltd, Bengaluru</div>                        
                                </div>
                    
                        <div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest6.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest6.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Department of Computer Science and Engineering organized a Guest Lecture on “INTERNET OF THINGS (IOT)” on 01.03.2018.<br />
								Chief Guest Mr. Venkatesh Varadachari, Chief Executive Officer, Makerdemy, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest7.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest7.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on “Introduction to Cloud Computing using Microsoft Technologies” handled by Mr. Alagarsamy Rajamannar, Consultant, Microsoft<br />- London UK on 30-06-17</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest8.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest8.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest lecture on “BIG DATA & CLOUD COMPUTING” on 24.07.2017 <br />
									Chief Guest Mr.V. Sridharan, Principal Engineer, CDAC, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest9.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest9.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest lecture was conducted in S.A.Engineering College on the Topic “Cloud Computing Tools” on 25.07.2017.<br />
							 Chief Guest Mr. S. Jeevan Kumar, Developer Analyst – I, Aon Hewitt, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest10.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest10.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest lecture was conducted in S.A.Engineering College on the Topic “Applying Object Orientation in Real Time Environment” on 27.07.2017. <br />
							Chief Guest Mr. Prakash, Corporate Trainer, Eyeopen Technologies, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest11.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest11.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest lecture was conducted in S.A.Engineering College on the Topic “Web Development & Angular JS” on 27.07.2017.<br />
							 Chief guest Mr. Prabhu Ram, Corporate Trainer, Eyeopen Technologies, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest12.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest12.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">12.Guest lecture was conducted in S.A.Engineering College on the Topic “Demistifying IT & Technology” on 28.07.2017.<br />
							 Chief Guest Mr. Ranjith Kumar Panjabikesan, IT Leadership, UST Global, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest13.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest13.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on BIG DATA & DATA MINING for III Year CSE Students dellivered by Mr. John Kennedy, Sindhya Software, Chennai on 06-11-2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest14.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest14.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on Mobile Application Development  for III Year CSE Students handled by Ms.Ponmalar, Eyeopen Technologies, Chennai on 12-01-2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest15.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest15.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on Computer Networks and Security for II Year CSE Students addressed by VINOD KUMAR AVINANE, HCL Technologies, Chennai on  23.2.2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest16.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest16.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on Managing Real Time Projects for II Year CSE Students handled by Dr. H. R Mohan Vice Chairman IEEE Madras Section on 23.2.2017</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest17.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest17.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on Resource Planning using  Software Engineering for II Year CSE Students handled by Mr.Manish Kumar, AON Hewitt, Chennai on 27.2.2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest18.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest18.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on  Privacy and Security in Online Social Media in association with ACM  for IV Year CSE Students dellivered by Dr. Ponnurangan Kumaraguru, Associate Professor, Indraprasatha Institute of Information Technology, Delhi on 06-11-2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest19.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest19.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on FUTURE OF CAREERS IN IT for III Year CSE Students dellivered by Mr.Shiva Shankar Director, Xlicon Business Services Private Limited, Chennai on 06-09-2016.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest20.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest20.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on BIG DATA for II Year CSE Students handled by Mr.Vignesh Raja,  Senior Engineer, CDAC, Chennai on 06-09-2016</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest21.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest21.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on Saas In Cloud Computing for IV Year CSE Students addressed by Ms.Vidhya Prabhu, Program  Manager, North America, DLF INFO CITY,  Chennai on  15-07-2016.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest22.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest22.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on IBM CAREER EDUCATION AWARENESS  PROGRAMME for II Year CSE Students handled by Mrs.T.Vidhya Sri and Mrs.J.A.Vishalatchi, HR Manager, Android Technologies, IBM Business Partner, Coimbatore on 06-07-16.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest23.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest23.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on  PHYGITAL ENTERPRISE IN RETAIL for III Year CSE Students handled by Mr.Bala Arunachalam, Head IT Delivery, Home Depot, Chennai on 24-08-2016.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest24_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest24_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on RECENT OPEN SOURCE DATABASE TOOLS On 30-01-2019 for II year CSE students handled by Mr. AMJATH IBRAHIMS Technology Specialist, UST Global, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest24_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest24_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on RECENT OPEN SOURCE DATABASE TOOLS On 30-01-2019 for II year CSE students handled by Mr. AMJATH IBRAHIMS Technology Specialist, UST Global, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest25_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest25_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture On RECENT TRENDS IN SOFTWARE ENGINEERING-DEUOPS AND AGILE-SCRUM On 31-01-2019 handled by Mr. VENKATESH RAJAMANI Founder, TryScrum Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest25_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest25_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture On RECENT TRENDS IN SOFTWARE ENGINEERING-DEUOPS AND AGILE-SCRUM On 31-01-2019 handled by Mr. VENKATESH RAJAMANI Founder, TryScrum Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest26_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest26_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on “Java Applets” was organized on 02.08.2018 for second year students</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest26_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest26_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture on “Java Applets” was organized on 02.08.2018 for second year students</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest27_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest27_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture was conducted on “Software Testing”  for III year students on 03.08.2018.<br /> handled by Mrs. E. BENITA CLARE, Wipro Technologies Pvt Ltd, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest27_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest27_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture was conducted on “Software Testing”  for III year students on 03.08.2018.<br /> handled by Mrs. E. BENITA CLARE, Wipro Technologies Pvt Ltd, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest28_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest28_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture was conducted on “Recent Trends-Internet Programming” for III year students on 24.08.18. handled by Mr. Vinoth Rajagopal Project Manager, Mphasis, DLF SEZ IT Park, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest28_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest28_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture was conducted on “Recent Trends-Internet Programming” for III year students on 24.08.18. handled by Mr. Vinoth Rajagopal Project Manager, Mphasis, DLF SEZ IT Park, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest29_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest29_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture was conducted on “Recent Trends – Advance Data Structure using java”  for II year students on 01.09.18. handled by Mr. PALANIAPPAN.S.P, Technical Architecture, Tech Mahindra Limited, Chennai,</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest29_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest29_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest Lecture was conducted on “Recent Trends – Advance Data Structure using java”  for II year students on 01.09.18. handled by Mr. PALANIAPPAN.S.P, Technical Architecture, Tech Mahindra Limited, Chennai,</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/guest30_1.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest30_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest lecture was conducted on the Topic “Recent Trends in Cloud computing” on 24.07.2018 organized by the department of Computer Science and Engineering.<br /> Addressed by Mr. UMASHANKAR NEDUNCHEZHIAN, CTO, 1 Cloud Hub Pvt. Ltd Singapore delivered an innovative speech to CSE Students</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/guest30_2.jpg" data-fancybox-group="gallery20" title="Guest Lecture">
                                <img src="<?php echo $siteurl ?>/img/cse/guest30_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Guest lecture was conducted on the Topic “Recent Trends in Cloud computing” on 24.07.2018 organized by the department of Computer Science and Engineering.<br /> Addressed by Mr. UMASHANKAR NEDUNCHEZHIAN, CTO, 1 Cloud Hub Pvt. Ltd Singapore delivered an innovative speech to CSE Students</div>                        
                        </div>




            </div>

            
			<h2 class="title_line">Industrial Visit</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial1.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on SANSBOUND SOLUTIONS, KODAMBAKKAM on 29-1-18 and 30-1-18</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial2.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on TECHNO VALLEY, COCHIN on 11-12-17</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial3.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit to CRPF, Avadi on 24th August 2017 </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial4.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial4.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit to Indira Gandhi Centre for Atomic Research, Resources Management Group IGCAR, Kalpakkam on 08.09.2017 </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial5.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial5.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on Bharat Sanchar Nigam Limited, Southern Telecom Region, Chennai on 24-02-2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial6.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial6.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on UST Global Technology, Chennai from 06-02-2017 to 08-02-2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial7.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial7.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on CHIPS SOFTWARE SYSTEMS, Ernakulam, Kochi on 25-01-2017</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial8.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial8.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on Web & crafts,Infopark on 25-01-2017</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial9_1.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial9_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on Ashok Leyland, Ennore, Chennai on 6.7.2018</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial9_2.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial9_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on Ashok Leyland, Ennore, Chennai on 6.7.2018</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial10_1.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial10_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on BSNL, Chennai on 08.01.2019 & 09.01.2019 for Third year Students.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial10_2.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial10_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on BSNL, Chennai on 08.01.2019 & 09.01.2019 for Third year Students.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial11_1.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial11_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on  Web Design and Multicore Technology in web and crafts, Cochin for Final Year Students on 06.06.2018</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial11_2.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial11_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on  Web Design and Multicore Technology in web and crafts, Cochin for Final Year Students on 06.06.2018</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial12.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial12.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial Visit on Resource Management Techniques in Chips Software Systems,Eranakulam,Kerala for Final Year Students on 06.06.2018.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/industrial13.jpg" data-fancybox-group="gallery20" title="Industrial visit">
                                <img src="<?php echo $siteurl ?>/img/cse/industrial13.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Industrial visit on Data Analytics in Spectrum Soft tech Solutions ( P ) Ltd,Kochi,Kerala for Final Year Students on 06.06.2018.</div>                        
                        </div>

				
            </div>

			<h2 class="title_line">Seminar</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/seminar1.jpg" data-fancybox-group="gallery20" title="seminar">
                                <img src="<?php echo $siteurl ?>/img/cse/seminar1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">TNSCST Sponsored National Level Seminar on “Internet of Things for Effective Disaster Management” on 22-02-18.<br /> Chief Guest Mr Shabarinath Premlal, Founder, ResPro Labs, CIIET</div>                        
                        </div>
						        
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/seminar2.jpg" data-fancybox-group="gallery20" title="seminar">
                                <img src="<?php echo $siteurl ?>/img/cse/seminar2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Inauguration of IEEE Computer Society on 21.02.2018. Chief Guest Mr. Aravindhan Anbazhagan, Chair of IEEE Computer Society India, Council Student Activities Committee, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/seminar3.jpg" data-fancybox-group="gallery20" title="seminar">
                                <img src="<?php echo $siteurl ?>/img/cse/seminar3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Seminar on “Internet of Things (IoT)” on 16-08-17.  Chief Guest Dr.Swarna Ravindra Babu, Managing Director, Coovam Smart Systems & Services Private Limited, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/seminar4.jpg" data-fancybox-group="gallery20" title="seminar">
                                <img src="<?php echo $siteurl ?>/img/cse/seminar4.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Seminar on “AVOIDING PLAGIARISM” in association with the inauguration of IEEE WIE affinity group on 03.08.16. Chief Guest Dr.Vydeki Vijayakumar, Chair, Madras IEEE WIE & Dr.R.Sakkaravarthi, SCSE, VIT, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/seminar5.jpg" data-fancybox-group="gallery20" title="seminar">
                                <img src="<?php echo $siteurl ?>/img/cse/seminar5.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Seminar on Engineer to Entrepreneur In Associated with IEEE on 09-08-2016  
									Guest : Mr.Shanthalingam, Deputy director, MSME-Di, Govt of India and Mr.Karthik, Alumni Entrepreneur</div>                        
                        </div>


            </div>


			<h2 class="title_line">Sponsored Events</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored1.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">National Workshop On Quantum Computation and Cryptography Sponsored by SERB-DST & BRNS-DAE from 23.01.18 to 24.01.18 (2 days). Chief Guest Prof. Anil Kumar,NASI Senior Scientist Platinum Jubilee Fellow, Indian Institute of Science, Bangalore</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored2.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">TNSCST Sponsored National Level Seminar on “Internet of Things for Effective Disaster Management” on 22-02-18. Chief Guest Mr Shabarinath Premlal, Founder, ResPro Labs, CIIET</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored3.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Department of Science & Technology Government of India Sponsored National Workshop on DATA SCIENCE RESEARCH  from 20-12-16 to 22-12-16 addressed by Mr.S.Srinivasan, Delivery Manager, Syntel, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored4.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored4.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">IEEE Production Safety Engineering Society  Sponsored National Level Workshop on NS2 for Network Security in association with IEEE on 23-09-2016  handled by Ms.R.Manimekala, IIS Technologies, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored5.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored5.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">National Level Workshop on Mathematical Modeling of Wireless Networks sponsored by IEEE Madras Section in association with IEEE on  28-07-2016 by Dr.K.Anitha,Maths Dept, S.A Engineering College, Chennai & Ms.G.Uma, VIT, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored6.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored6.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Department of Science & Technology Government of India Sponsored National Workshop on DATA SCIENCE RESEARCH  from 20-12-16 to 22-12-16 addressed by Mr.S.Srinivasan, Delivery Manager, Syntel, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored7.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored7.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">7.Indian Society for Technical Education (Tamilnadu & Pondichery State Section) Sponsored Workshop on Python Programming on 22.09.17.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/sponsored8_1.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored8_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">“ MLH Local Hack Day” (A Global Hackathon) Hosted by GitHub Organized by SAEC ACM-W Student Community on 01.12.18</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored8_1.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored8_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">“ MLH Local Hack Day” (A Global Hackathon) Hosted by GitHub Organized by SAEC ACM-W Student Community on 01.12.18</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored8_2.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored8_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">“ MLH Local Hack Day” (A Global Hackathon) Hosted by GitHub Organized by SAEC ACM-W Student Community on 01.12.18</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/sponsored8_3.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored8_3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">“ MLH Local Hack Day” (A Global Hackathon) Hosted by GitHub Organized by SAEC ACM-W Student Community on 01.12.18</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/sponsored8_4.jpg" data-fancybox-group="gallery20" title="sponsored_events">
                                <img src="<?php echo $siteurl ?>/img/cse/sponsored8_4.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">“ MLH Local Hack Day” (A Global Hackathon) Hosted by GitHub Organized by SAEC ACM-W Student Community on 01.12.18</div>                        
                        </div>

            </div>


			<h2 class="title_line">Students Clubs</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/student_club1.jpg" data-fancybox-group="gallery20" title="students_club">
                                <img src="<?php echo $siteurl ?>/img/cse/student_club1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">IoT Club was inaugurated to develop student’s skill in latest technology and tools on 27.09.2018.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/student_club2.jpg" data-fancybox-group="gallery20" title="students_club">
                                <img src="<?php echo $siteurl ?>/img/cse/student_club2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text"> FOSS club was inaugurated for the enhancement of intellectual traits of the students on 15.09.2017.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/student_club3.jpg" data-fancybox-group="gallery20" title="students_club">
                                <img src="<?php echo $siteurl ?>/img/cse/student_club3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Coding Club was established on 27.08.2018 to improve the practical skills of the students on 27.08.2019.</div>                        
                        </div>
			</div>



			<h2 class="title_line">Value Added Course</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value1.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value added programme on the Topic “INTERNET OF THINGS (IOT)” from 18-06-18 to 22-06-18, 25-6-18 to 29-6-18 and 02-07-18 to 06-07-18. Chief Guest Mr.Swarna RavindraBabu, Head, Coovum Advanced Institute for Science & Engineering Research, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value2.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value added course On Python with MySql & MongoDB from 18-06-18 to 20-06-18, 25-06-18 to 27-06-18 and 28-06-18 to 02-07-18. Chief Guest Mr.Arun Suresh,Trainer,S.A Techno Solutions, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value3.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Programme on Big Data Hadoop from 22-02-18 to 23-02-18 and 28-02-18 to 01-03-18 (4 days). Chief Guest Mr.S.Vinoth, Technical Trainer, Silicon Software Services, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value4.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value4.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value added Course on “PHP”from 12.02.18 - 15.02.18 & 20.02.18 - 21.02.18. Chief Guest Mr.Meyappan, Technical Head & Mr.Ramesh, Technical Lead, Eye Open Technologies Pvt.Ltd, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value5.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value5.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Programme on Python from 26-02-18 to 27-02-18.  Chief Guest Mr.S.Vinoth, Technical Trainer, Silicon Software Services, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value6.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value6.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">“Cloud Computing” for six days from 18.12.17 - 19.12.17, 21.12.17 - 22.12.17, 03.01.18 - 04.01.18. Chief Guest Mr.Vigneswaran Paramathayalam, Chief Knowledge Officer, Cloud Bull, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value7.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value7.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Programme on “BIG DATA ANALYTICS” from 06.09.16 to 26.09.16. Chief Guest Mr.Saravanan, Data Engineer, DATA DOTZ, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value8.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value8.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Programme was conducted in S.A.Engineering College on the Topic “PHP” on 11.07.2016. Chief Guest Mr.Meiyappan, Head, Eyeopen Technologies, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/value9.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value9.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Programme on Mobile Application Development was organized from 20-02-17 to 28-02-17 (6 days), handled by Mrs.Ponmalar, Corporate Trainer, Eye open Technologies, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value10_1.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value10_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Course On "PHP and MySQL" Addressed by Mr.Shan N Eyeopen Technologies. from 17.12.2018 to 11.01.2019 </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value10_2.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value10_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Course On "PHP and MySQL" Addressed by Mr.Shan N Eyeopen Technologies. from 17.12.2018 to 11.01.2019 </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value11_1.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value11_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Course On "Advanced Java-J2EE" was conducted Mr.Vinoth.S and Team Silicon Software Services from 24.12.2018 to 23.01.2019 </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/value11_2.jpg" data-fancybox-group="gallery20" title="value_added_course">
                                <img src="<?php echo $siteurl ?>/img/cse/value11_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Value Added Course On "Advanced Java-J2EE" was conducted Mr.Vinoth.S and Team Silicon Software Services from 24.12.2018 to 23.01.2019 </div>                        
                        </div>
						
						
			</div>

			<h2 class="title_line">Workshop</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1">

                    
                                
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop1.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">National Workshop On Object Oriented Programming using Java (In association with Computer Society of India) from 07.06.18 to 09.06.18 (3 days). Chief Guest Mr.Karthik Arumugam, Java Trainer, Silicon Software Services, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop2.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">National Workshop On Quantum Computation and Cryptography Sponsored by SERB-DST & BRNS-DAE from 23.01.18 to 24.01.18 (2 days). Chief Guest Prof. Anil Kumar,NASI Senior Scientist Platinum Jubilee Fellow, Indian Institute of Science, Bangalore</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop3.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Workshop on “Setting up Flipped Class Room using DIGIMAT Digital Learning Platform” on 06.12.17. Chief Guest Mr.S.Baskar, Chief Executive Officer, LinuXpert Systems, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop4.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop4.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Department of Science & Technology Government of India Sponsored National Workshop on DATA SCIENCE RESEARCH  from 20-12-16 to 22-12-16 addressed by Mr.S.Srinivasan, Delivery Manager, Syntel, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop5.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop5.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">IEEE Production Safety Engineering Society  Sponsored National Level Workshop on NS2 for Network Security in association with IEEE on 23-09-2016  handled by Ms.R.Manimekala, IIS Technologies, Chennai.</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop6.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop6.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">National Level Workshop on Mathematical Modeling of Wireless Networks sponsored by IEEE Madras Section in association with IEEE on  28-07-2016 by Dr.K.Anitha,Maths Dept, S.A Engineering College, Chennai & Ms.G.Uma, VIT, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop7.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop7.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Workshop on INTERNET OF THINGS (IOT) in association with  Computer Society of India (CSI) on  30-08-2016 addressed by Mr.Rajamanikam, Bits ‘n Watts Embedded Solutions, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/workshop8.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop8.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Workshop on the “Applied CS with Android by Google” from 22-01-18 to 29-01-18.
											Google Facilitators: Our Final Year Students Mr.M.Akhil and Mr.P.S.Karthikeyan </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop9_1.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop9_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">The Coding Club of CSE Department in association with CSI organized “SA (HA) CKATHON 2K18” on 07.09.2018 from 8.30 am to 5.30 pm in the college premises.  </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop9_2.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop9_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">The Coding Club of CSE Department in association with CSI organized “SA (HA) CKATHON 2K18” on 07.09.2018 from 8.30 am to 5.30 pm in the college premises.  </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/workshop10_1.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop10_1.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Workshop on Ethical Hacking was conducted on  27.08.18 </div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/workshop10_2.jpg" data-fancybox-group="gallery20" title="workshop">
                                <img src="<?php echo $siteurl ?>/img/cse/workshop10_2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">Workshop on Ethical Hacking was conducted on  27.08.18 </div>                        
                        </div>


					</div>

					<h2 class="title_line">Conference</h2>
         		   <div class="board_navslide"></div>
         		   <div class="board_trustes_inner">
              		  <div class="board_trustes_slder_hh1">
        
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/conference1a.jpg" data-fancybox-group="gallery20" title="Conference">
                                <img src="<?php echo $siteurl ?>/img/cse/conference1a.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">International Conference on Informatics & Computing in Engineering Systems (ICICES 2018) on 21.03.18 and 22.03.18. Chief Guest Mr.Lakshmi Narayanan, Vice President-Projects, Cognizant Technology Solutions, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/conference1b.jpg" data-fancybox-group="gallery20" title="Conference">
                                <img src="<?php echo $siteurl ?>/img/cse/conference1b.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">International Conference on Informatics & Computing in Engineering Systems (ICICES 2018) on 21.03.18 and 22.03.18. Chief Guest Mr.Lakshmi Narayanan, Vice President-Projects, Cognizant Technology Solutions, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/conference1c.jpg" data-fancybox-group="gallery20" title="Conference">
                                <img src="<?php echo $siteurl ?>/img/cse/conference1c.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">International Conference on Informatics & Computing in Engineering Systems (ICICES 2018) on 21.03.18 and 22.03.18. Chief Guest Mr.Lakshmi Narayanan, Vice President-Projects, Cognizant Technology Solutions, Chennai</div>                        
                        </div>
							<div class="board_trustes_box">
								<div>
									<a class="fancybox" href="img/cse/conference1d.jpg" data-fancybox-group="gallery20" title="Conference">
									<img src="<?php echo $siteurl ?>/img/cse/conference1d.jpg" class="img-responsive">
									</a>
								</div>

								<div class="text">International Conference on Informatics & Computing in Engineering Systems (ICICES 2018) on 21.03.18 and 22.03.18. Chief Guest Mr.Lakshmi Narayanan, Vice President-Projects, Cognizant Technology Solutions, Chennai</div>                        
							</div>
							<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/conference2.jpg" data-fancybox-group="gallery20" title="Conference">
                                <img src="<?php echo $siteurl ?>/img/cse/conference2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">International Conference on Information, Communication and Embedded System (ICICES 2017) on 23.02.17 and 24.02.17. Chief Guest Mr.H.R.Mohan, Vice Chairman, IEEE Madras Section, Chennai</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a  class="fancybox" href="img/cse/conference3.jpg" data-fancybox-group="gallery20" title="Conference">
                                <img src="<?php echo $siteurl ?>/img/cse/conference3.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">International Conference on Information, Communication and Embedded System (ICICES 2016) on 25.02.16 and 26.02.16. Chief Guest Dr.J.Kumar, Director, Centre for Technology Development and transfer (CTDT), Anna University, Chennai</div>                        
                        </div>
					</div>


					<h2 class="title_line">Convention</h2>
         		   <div class="board_navslide"></div>
         		   <div class="board_trustes_inner">
              		  <div class="board_trustes_slder_hh1">
        
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/convention1aa.jpg" data-fancybox-group="gallery20" title="Convention">
                                <img src="<?php echo $siteurl ?>/img/cse/convention1aa.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">CSI State Level Student Convention “TECHWAVES” Organized by SAEC- CSI STUDENT CHAPTER on 05.01.2018.  
												Chief Guest Mr. T.R. Vasudeva Rao, Chairman, CSI Chennai Chapter</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/convention1b.jpg" data-fancybox-group="gallery20" title="Convention">
                                <img src="<?php echo $siteurl ?>/img/cse/convention1b.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">CSI State Level Student Convention “TECHWAVES” Organized by SAEC- CSI STUDENT CHAPTER on 05.01.2018.  
												Chief Guest Mr. T.R. Vasudeva Rao, Chairman, CSI Chennai Chapter</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/convention1c.jpg" data-fancybox-group="gallery20" title="Convention">
                                <img src="<?php echo $siteurl ?>/img/cse/convention1c.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">CSI State Level Student Convention “TECHWAVES” Organized by SAEC- CSI STUDENT CHAPTER on 05.01.2018.  
												Chief Guest Mr. T.R. Vasudeva Rao, Chairman, CSI Chennai Chapter</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox"  href="img/cse/convention1d.jpg" data-fancybox-group="gallery20" title="Convention">
                                <img src="<?php echo $siteurl ?>/img/cse/convention1d.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">CSI State Level Student Convention “TECHWAVES” Organized by SAEC- CSI STUDENT CHAPTER on 05.01.2018.  
												Chief Guest Mr. T.R. Vasudeva Rao, Chairman, CSI Chennai Chapter</div>                        
                        </div>
						<div class="board_trustes_box">
                            <div>
                                <a class="fancybox" href="img/cse/convention2.jpg" data-fancybox-group="gallery20" title="Convention">
                                <img src="<?php echo $siteurl ?>/img/cse/convention2.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="text">CSI student chapter organized CSI Foundation day on 06.03.18</div>                        
                        </div>
					</div>


			
			
			</div>


             <!-- <h2 class="title_line">FDTP/STTP</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1 ">

                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a  href="img/lfdtp1.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                        <img src="<?php echo $siteurl ?>/img/fdtp1.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr. Selvakumar,</span>      
                                    <div class="text">ABB Global Industries, Chennai, <br />“Power System Studies For Renewable Integration Using PSSE” on 14-12-2017 & 15-12-2017.
                                </div>                      
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lfdtp2.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/fdtp2.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Dr.Mounagurusami,</span>     
                                    <div class="text">TNEB, Chennai <br />“FDTP on EE6504 - Electrical Machines – II” on  9.06.2015 to 16.06.2015.</div>                        
                                </div>
                    

                                


            </div>

            </div> -->

             <!-- <h2 class="title_line">INDUSTRIAL VISIT</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1 ">

                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a  href="img/lind_1.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                        <img src="<?php echo $siteurl ?>/img/ind_1.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “Sriperumbudur 400 KV Sub-Station” on 24.01.2018.</span>      
                                    <div class="text">
                                </div>                      
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_2.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_2.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “Korattur 110/230 KV Sub-Station” on 24.01.2018.</span>     
                                    <div class="text"></div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_3.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_3.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “Integral Coach Factory” on 05.01.2018.</span>     
                                    <div class="text"></div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_4.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_4.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “North Chennai Thermal Power Station” on 22.06.2017</span>     
                                    <div class="text"></div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_5.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_5.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “North Chennai Thermal Power Station” on 23.06.2017</span>     
                                    <div class="text"></div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_6.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_6.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “Sriperumbudur 400 KV Sub-Station” on 10.07.2017.</span>     
                                    <div class="text"></div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_7.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_7.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">IIndustrial visit on “Korattur 110/230 KV Sub-Station” on 10.07.2017.</span>     
                                    <div class="text"></div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lind_8.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/ind_8.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial visit on “Integral Coach Factory” on 17.03.2017.</span>     
                                    <div class="text"></div>                        
                                </div>
                    

                                


            </div>

            </div> -->



             <!-- <h2 class="title_line">SEMINAR</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1 ">

                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a  href="img/lsemi_1.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                        <img src="<?php echo $siteurl ?>/img/semi_1.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.P.Senthilkumar,</span>      
                                    <div class="text">Technical Engineer, Enthu Technology, Coimbatore<br />“One day seminar on multi-body design and state flow using MATLAB-advancement” on 25.01.2018.
                                </div> 
                                </div>
                                 <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_2.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_2.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.AravindhanAnbazhagan,</span>     
                                    <div class="text">Chair of IEEE Computer Society India Council,<br />“IEEE Awareness Program”  on 21.02.2018.</div>                        
                                </div>                     
                             
                                   <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_3.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_3.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.Vinod Sharma,</span>     
                                    <div class="text">IEEE Young Professional<br />“IEEE Awareness Program”  on 28.02.2017.</div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_4.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_4.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">ShriHari Prasad,</span>     
                                    <div class="text">CEO, IP DOME Strategy Advisors Pvt, Ltd, Chennai,<br />“Projects and Patent”   on 28.9.2016.</div>                        
                                </div>
                                 <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/semi_5.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_5.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Vydeki Vijayakumar,</span>     
                                    <div class="text">Chair, Madras IEEE WIE,<br />“IEEE WIE Affinity Group Inauguration followed by a Seminar on Avoiding Plagiarism”  <br />on 03.08.2016.</div>                        
                                </div>
                                  <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_6.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_6.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.AnandDavid.R,</span>     
                                    <div class="text">Delivery Operations Manager,IBM India Pvt Ltd, Bangalore.<br />“Alumni Interaction – Current Trends in Industry”  on 29.07.2016.</div>                        
                                </div>

                              

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_7.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_7.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Dr. P. Somasundaram,</span>     
                                    <div class="text">Associate Professor, Department of EEE,  Anna University, CEG, Chennai.<br />“International seminar on Smart Grid Technologies And Applications” on 18.03.2016 & 19.03.2016.</div>                        
                                </div>


                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_8.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_8.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.T.D.Subash M.E., (Ph.d),</span>     
                                    <div class="text">Assistant Professor, Holy Grace Academy of Engineering,<br />“IEEE Awareness program”  on 29.2.2016.</div>                        
                                </div>

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lsemi_9.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/semi_9.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.N.Gopalan,</span>     
                                    <div class="text">Chair of IEEE Computer Society India Council,<br /> “Energy management system”   on 8-9-2015.</div>                        
                                </div>

                               
                    

                                


            </div>

            </div> -->



             <!-- <h2 class="title_line">Value Added Course</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1 ">

  
 
                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a  href="img/lval_1.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                        <img src="<?php echo $siteurl ?>/img/val_1.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Hands on training on Arduino</span>      
                                    <div class="text">on 15.03.2018 &amp; 16.03.2018.
                                </div>                      
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lval_2.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/val_2.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Core JAVA – OCJP Syllabus</span>     
                                    <div class="text">on 05.03.2018 to 09.03.2018.</div>                        
                                </div>
                                 <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lval_3.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/val_3.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Industrial Automation</span>     
                                    <div class="text">on 23/01/2018 – 25/01/2018 &amp; 12/02/2018 – 14/02/2018.</div>                        
                                </div>
                                 <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lval_4.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/val_4.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Embedded system – ARM</span>     
                                    <div class="text">on 27.06.2017 – 30.06.2017.</div>                        
                                </div>
                                 <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lval_5.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/val_5.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Robotics and industrial automation on</span>     
                                    <div class="text">28.08.2017 - 05.08.2017.</div>                        
                                </div>
                                 <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lval_6.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/val_6.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">MATLAB programming and Simulink fundamentals</span>     
                                    <div class="text">on 18.09.2017 &amp; 19.09.2017.</div>                        
                                </div>
                    

                                


            </div>

            </div> -->

  
 

             <!-- <h2 class="title_line">WORKSHOP</h2>
            <div class="board_navslide"></div>
            <div class="board_trustes_inner">
                <div class="board_trustes_slder_hh1 ">

                    
                                <div class="board_trustes_box">
                                    <div >
                                        <a  href="img/lwork_1.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                        <img src="<?php echo $siteurl ?>/img/work_1.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.P.Shabarish,</span>      
                                    <div class="text">Founder, Resprolabs, Chennai, <br />“Internet of Robotic Things (IoRT) and IoRT Challenge”  on 18.08.2017 &amp; 19.08.2017.
                                </div>                      
                                </div>
                    

                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lwork_2.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/work_2.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.M.Sudharson,</span>     
                                    <div class="text">Team Leader, Networking Division, UTL Technologies, Bangalore <br />“ Workshop on Embedded System” on 11.12.2017 &amp; 12.11.2017.</div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lwork_3.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/work_3.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Mr.Selvakumar,</span>     
                                    <div class="text">Technical Engineer, ABB, Chennai, <br /> “International Workshop on Power system studies using ETAP &amp;DIgSILENT” on 04.05.2017 &amp; 05.05.2017.</div>                        
                                </div>
                                <div  class="board_trustes_box" >
                                    <div >
                                        <a  href="img/lwork_4.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                        <img src="<?php echo $siteurl ?>/img/work_4.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                    <span class="text1">Ganapathy.R,</span>     
                                    <div class="text">EPR LABS, Chennai.<br />“Workshop on Robotics” on 21-9- 2015 &amp; 22-9-2015.</div>                        
                                </div>
                    

                                


            </div>

            </div> -->


        









        </div>



    </div>


        <div class="tab-content fade" id="menu4">

     <div>                    
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/lab1.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/lab1.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -01</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                            <tr>
                                                <td>Area in Sq.mts:</td>
                                               <td>66</td>
                                            </tr>
                                            <tr>
                                                
                                                 <td>Cost:</td>
                                                <td>Rs. 10,05,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                        
                        
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                    <a class="fancybox" href="img/lab2.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/lab2.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -02</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>66</td>
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs. 9,68,000</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                       
                   
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/lab3.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/lab3.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -03 & 04</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                             <td>132</td>
                                            
                                        </tr>
                                        <tr>
                                           
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.18,08,000</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                        
                            
                        
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/lab5.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/lab5.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -05</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>66</td>
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.12,56,966</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                
                     
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/lab6.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/lab6.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -06</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>66</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.12,56,966</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/lab7.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/lab7.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -07</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>66</td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.12,56,966</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
         
                      
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/lab8.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/lab8.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Lab -08</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>66</td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.13,33,500</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/rd.jfif" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/rd.jfif" class="img-responsive">
                                    </a>
                                    <a class="fancybox" href="img/rd1.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/rd1.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Research & Development(Lab 9 & Lab 10)</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>132</td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.33,40,328
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                            <!-- <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/Digital Classroom.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/Digital Classroom.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>DIGITAL CLASSROOM</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>     
                                                   
                            </div>
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/smart board class room.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/smart board class room.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>SMART BOARD CLASS ROOM</span> 
                                 <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>   
                                                     
                            </div> -->
                         
                        </div>

    </div>

</div>
<div class="clearfix"></div>




			
            <div class="external_links course_ext-links">
                <label>Also check: </label>
                <ul class="share_icons clearfix">
                   
                    <li><a href="pdf/AICTE-APPROVAL-2015-16.pdf" target="_blank"> AICTE Approval</a></li>             
                    <li><a href="pdf/Deficiency Report-15-16.PDF" target="_blank">Zero Deficiency Report</a></li>              
                    <li><a href="https://www.annauniv.edu/academic_courses/curr_aff.html" target="_blank">Curriculum and Syllabi</a></li>
                    <li><a href="pdf/committee.pdf" target="_blank">List of Committees</a></li>
                    <li><a href="pdf/DAC.pdf" target="_blank">Department Advisory Committee Meeting(2016-2017)</a></li>   
                    <li><a href="pdf/PAC.pdf" target="_blank">Programme&nbsp;Assessment Committee Meeting(2015-16 – Even)</a></li>
                    <li><a href="pdf/IAENG.pdf" target="_blank">List of IAENG Members</a></li>
                    <li><a href="pdf/ISTE.pdf" target="_blank">List of ISTE Members</a></li>
                    <li><a href="pdf/newsletter-final.pdf" target="_blank">Academic Year:2016-2017(Odd)</a></li>
                    <li><a href="pdf/Newsletter-final-1.pdf" target="_blank">Academic Year:2016-2017(Even)</a></li>
                    <li><a href="pdf/15-16.pdf" target="_blank">Academic Year: 2015-2016</a></li>
                    <li><a href="pdf/14-15.pdf" target="_blank">Academic Year: 2014-2015</a></li>
                    <li><a href="pdf/13-14.pdf" target="_blank">Academic Year : 2013-2014</a></li>
                    <li><a href="pdf/BEST-PROJECTS-16-17.pdf" target="_blank">List of Best projects</a></li>

                </ul>
            </div>
        </div>
    </div>
	</div>






<?php include('virtual-tour-strip.php'); ?>



	<?php include('counter-part.php'); ?>

<?php include('footer.php'); ?>


<script>
    $(document).ready(function(){
        $(document).on('click','#listmenu6',function(){

             setTimeout(function () {
                $('.board_trustes_slder_hh1').owlCarousel({
        loop: true,
        items: 6,
        margin:30,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 900,
        autoplaySpeed: 900,
        autoplayHoverPause: true,
        navContainer: '.board_navslide',
        responsive: {
            0: {
                items:1,
                mouseDrag : true,
                autoplay : true,
                loop:true,
                dots:true
            },
            700: {
                items:2,
                mouseDrag : true,
                autoplay : true,
                loop:true
            },
            1200: {
                items:3,
                mouseDrag : true,
                autoplay : true,
                loop:true
            },
            1440: {
                items:4,
                mouseDrag : true,
                autoplay : true,
                loop:true
            }
        }
    });
            }, 150);
        });

    });



    

</script>
