<?php
/**
  Template Name: index

 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.

 */

get_header();
?>

<!--headnews-scroller-->

<div class="headnews-scroller">
  <div class="container">
    <span><?php the_field('title', 'option'); ?></span>
    <div class="news-wrapper">
      <?php if( have_rows('whats_new_contents', 'option') ): ?>
      <ul class="whats_new_list">
        <?php while( have_rows('whats_new_contents', 'option') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('link'); ?>" target="_blank">
            <p><?php the_sub_field('sliding_data'); ?></p>
          </a>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
    </div>
  </div>
</div>

<!--Banner section-->
<div class="home-banner">
  <?php if( have_rows('banner_section') ): $i = 1;?>
  <div class="home-bnr-slider">
    <?php while( have_rows('banner_section') ): the_row(); 
      // vars
      $image = get_sub_field('banner_image');
      ?>
    <div class="item" data-index="<?php echo $i; ?>" style="background-image: url('<?php echo $image; ?>')"></div>
    <?php $i++; endwhile; ?>
  </div>
  <?php endif; ?>
  <div class="banner-content">
    <div class="container">
      <div>
        <div class="banner-left">
          <div class="header-wrap">

            <?php if( have_rows('banner_section') ):  $i = 1;?>
            <?php while( have_rows('banner_section') ): the_row(); 
                // vars
                $content = get_sub_field('banner_content');
                ?>
            <h2 class="h1" data-index="<?php echo $i; ?>"> <?php echo $content; ?></h2>
            <?php $i++; endwhile; ?>
            <?php endif; ?>
          </div>

          <div class="banner-controls">
            <div class="dots-contain"></div>
            <div class="paginate-contain">
              <p><span class="currrent-num">1</span>/<span class="final-num">4</span></p>
            </div>
          </div>

          <div class="banner-boxcontent">
            <div class="row">
              <?php if( have_rows('box_content') ): ?>
              <?php while( have_rows('box_content') ): the_row(); ?>
              <div class="col-xs-3">
                <div>
                  <span><?php the_sub_field('date'); ?><sup><?php _e('Th') ?></sup></span>
                  <p><?php the_sub_field('title'); ?></p>
                  <div class="hov_overlay"></div>
                </div>
              </div>

              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>


        <div class="banner-right">
          <div class="banner-media media-video">
            <a href="#" class="full_cont-link modal-link" data-target="#mediamodal" data-url="<?php the_field('banner_video'); ?>"></a>
            <div class="ply-btn">
              <svg width="80" height="80" class="play-icon" viewBox="0 0 100 100">
                <rect x="4" y="4" width="92" height="92" class="box" rx="50" ry="50"></rect>
                <polygon points="44,42 62,52 44,62" class="play"></polygon>
              </svg>
            </div>
            <img src="<?php the_field('video_backkground'); ?>" alt="<?php the_field('alt_text'); ?>" />
          </div>

          <div class="event-container">
            <div class="event-nav-wrap"></div>
            <div class="event-slider-wrap">
              <h4><?php _e('News Updates') ?> </h4>
              <div class="event-slider">
                <?php 
                    query_posts(array( 
                        'post_type' => 'news',
                        'showposts' => 10 
                    ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                <div class="item">
                  <span class="event-date"><?php the_date(); ?> </span>
                  <div class="full_height_para"><?php the_content(); ?></div>
                  <div class="learn-more">
                    <a href="<?php echo get_page_link( get_page_by_path( 'news' ) ); ?>" target="">
                      <label><?php _e('Learn more') ?> </label>
                      <span></span>
                    </a>
                  </div>
                </div>
                <?php endwhile;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Banner section end here -->

<!--achieve_sec -->
<section class="achieve_sec">
  <div class="container">
    <div class="achieve_top">
      <div class="number-block clearfix">
        <?php if( have_rows('about_achiev', 9) ): ?>
        <?php while( have_rows('about_achiev', 9) ): the_row(); ?>

        <div class="block_item">
          <label><span class="counter-num"><?php the_sub_field('counter_number'); ?></span>+</label>
          <h3><?php the_sub_field('main_title'); ?></h3>
          <p><?php the_sub_field('sub_content'); ?></p>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>

      <div class="more-block">
        <div>
          <div>
            <a href="about" class="full_cont-link"></a>
            <label>Know More</label>
            <h3>ABOUT SAEC</h3>
            <p class="learn-more"><span></span></p>
          </div>
        </div>
      </div>
    </div>

    <div class="achieve_bottom clearfix">
      <div class="ach_vision">
        <label class="title_line"><?php the_field('vission_tile', 9); ?></label>
        <p>
          <?php the_field('vision_content', 9); ?>

        </p>
        <a href="about" class="btn_web"> <?php the_field('vision_button_title', 9); ?> </a>
      </div>
      <div class="ach_moments">
        <div class="media-video">
          <a href="#" class="full_cont-link modal-link" data-target="#mediamodal" data-url="<?php the_field('apj_video_url'); ?>"></a>
          <div class="ply-btn">
            <svg width="80" height="80" class="play-icon" viewBox="0 0 100 100">
              <rect x="4" y="4" width="92" height="92" class="box" rx="50" ry="50"></rect>
              <polygon points="44,42 62,52 44,62" class="play"></polygon>
            </svg>
          </div>
          <img src="<?php the_field('video_bg-image', 9); ?>" alt="SAEC - APJ Video" />
        </div>

        <h2><?php the_field('title', 9); ?> </h2>
        <h3><?php the_field('name', 9); ?></h3>
        <label class="desc"><?php the_field('designation', 9); ?></label>
        <p><?php the_field('details', 9); ?></p>
        <label class="title_line"><?php the_field('bottom_title', 9); ?></label>
      </div>

      <div class="ach_slide-wrap">
        <div class="achieve-slider">
          <label class="title_line">Achievements</label>
          <div class="slider">

            <?php 
              query_posts(array( 
                  'post_type' => 'achievements',
                  'showposts' => 10 
              ) );  
            ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
              <a href="<?php echo get_page_link( get_page_by_path( 'achievements' ) ); ?>" class="full_cont-link"></a>
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ach_icon1.png" />
              <p class=""><?php the_content(); ?></p>
            </div>
            <?php endwhile;?>
          </div>
        </div>

        <div class="ach_lifeat">
          <a href="<?php echo get_page_link( get_page_by_path( 'campus-life' ) ); ?>" class="full_cont-link"></a>
          <h2>A Campus To <br />Fall In Love With!</h2>
          <label class="title_line">Life @ SAEC</label>
        </div>
      </div>
    </div>
  </div>
</section>


<!--studies_sec -->

<section class="studies_sec">
  <div class="container">
    <h2 class="title_line">Our Studies</h2>
    <div class="study_ug clearfix">
      <div class="course-box">
        <h3><?php the_field('ug_cource_title', 9); ?></h3>
      </div>

      <?php 
            query_posts(array( 
                'post_type' => 'ug_programmes',
            ) );  
        ?>
      <?php while (have_posts()) : the_post(); ?>
      <div class="course-box">
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <p><?php the_title(); ?></p>
        <a href="<?php the_permalink(); ?>" class="full_cont-link"></a>
      </div>
      <?php endwhile;?>

    </div>
    <div class="study_pg clearfix">
      <div class="course-box">
        <h3><?php the_field('pg_cource_title', 9); ?></h3>
      </div>

      <?php 
            query_posts(array( 
                'post_type' => 'pg_programmes',
            ) );  
        ?>
      <?php while (have_posts()) : the_post(); ?>
      <div class="course-box">
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <p><?php the_title(); ?></p>
        <?php 
           $pId= get_the_ID();
          if($pId==663) { 
              $plink="https://saec.ac.in/mca";
              }
              else { $plink = get_permalink();
            }
          ?>
        <a href="<?php echo $plink; ?>" class="full_cont-link"></a>
      </div>
      <?php endwhile;?>
    </div>
  </div>
</section>


<!--Ranking -->
<section class="ranking clearfix">
  <div class="container container2">
    <!-- <div class="row"> -->
    <div class="ranking-left">
      <figure>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ranking_stud.jpg" class="img-responsive" alt="" />
        <div class="it-hub" alt="">
          <h3 class="title_line">
            <?php the_field('it_hub_title', 9); ?>
          </h3>
          <p> <?php the_field('it_hub_content', 9); ?></p>
          <div class="learn-more"><label><?php the_field('it_hub_button_label', 9); ?></label><span></span></div>
          <a href="http://saec.ac.in/IT-HUB/index.html" target="_blank" class="full_cont-link"></a>
        </div>
      </figure>
    </div>

    <div class="ranking-right">
      <div class="ranking_right_inner">
        <span class="title_line"><?php the_field('ranking-title', 9); ?></span>
        <h2><?php the_field('ranking-sub_title', 9); ?></h2>
        <div class="ranking_item">
          <div class="ranking_slider owl-carousel">

            <?php if( have_rows('ranking', 9) ): ?>
            <?php while( have_rows('ranking', 9) ): the_row(); ?>

            <div class="ranking_innr">
              <img src="<?php the_sub_field('brand_logo', 9); ?>" class="img-responsive">
              <p><?php the_sub_field('detail_content'); ?></p>
            </div>

            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>

        <div class="virtual_tour">
          <a href="virtual-campus-tour" class="full_cont-link"></a>
          <div class="ply-btn">
            <svg width="80" height="80" class="play-icon" viewBox="0 0 100 100">
              <rect x="4" y="4" width="92" height="92" class="box" rx="50" ry="50"></rect>
              <polygon points="44,42 62,52 44,62" class="play"></polygon>
            </svg>
          </div>
          <h3 class="virtual_title">
            <div class="">360<sup>&deg;</sup><br />Virtual Tour</div>
          </h3>
        </div>


      </div>
    </div>
    <!-- </div> -->
  </div>
</section>


<!--infra -->
<section class="infra_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 infra_sec_left">
        <div class="review_bx">
          <div class="review_slider owl-carousel">

            <?php 
              query_posts(array( 
                  'post_type' => 'testimonials',
                  'showposts' => 10 
              ) );  
            ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="review_innr">

              <?php
                if ( has_post_thumbnail() ) { 
                    the_post_thumbnail( 'full', array( 'class'  => 'img-circle' ) );
                } 
            ?>

              <h3><?php the_title(); ?></h3>
              <span><?php the_field('testimonials_designation'); ?><br> <?php the_field('batch'); ?></span>
              <div><?php the_content(); ?></div>
            </div>
            <?php endwhile;?>

          </div>
          <div class="quot_div"></div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="great_infra">
          <div class="col-sm-12">
            <h3 class="title_line">Great <br>
              Infrastructure</h3>
          </div>

          <?php 
            query_posts(array( 
                'post_type' => 'resource',
            ) );  
        ?>
          <?php while (have_posts()) : the_post(); ?>
          <div class="col-sm-4">
            <div>
              <span>
                <img src="<?php the_field('resources_icon'); ?>" alt="">
              </span>
              <p><?php the_title(); ?></p>
              <div class="hov_overlay"></div>
              <a href="<?php the_permalink(); ?>" class="full_cont-link"></a>
            </div>
          </div>
          <?php endwhile;?>

          <div class="col-sm-4">
            <div>
              <p>Explore Facilities</p>
              <p class="learn-more"><span></span> </p>
              <div class="hov_overlay"></div>
              <a href="resources" class="full_cont-link"></a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>


<!--news_events -->

<section class="news_events">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="title_line">News & Events</h2>
      </div>
      <div class="col-sm-8 news_events_left">
        <?php 
              query_posts(array( 
                  'post_type' => 'news_events',
                  'showposts' => 1,
                  'cat' =>'1'
              ) );  
            ?>

        <?php while (have_posts()) : the_post();   ?>

        <div class="news_events_box">
          <div class="news_events_box_top_img">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="news_events_box_top_content">
            <ul>
              <li><a href="#">#<?php the_category(', '); ?></a></li>
            </ul>
            <h3><?php the_title(); ?></h3>
            <div><?php the_content(); ?></div>
            <a href="<?php echo $siteurl ?>saec-new/news" class="full_cont-link"></a>
            <div class="evnts_bx_btm">
              <span><?php the_date(); ?> </span>
            </div>
          </div>
        </div>
        <?php endwhile;?>

        <?php 
              query_posts(array( 
                  'post_type' => 'news_events',
                  'showposts' => 2,
                  'cat' =>'7'
              ) );  
            ?>
        <?php while (have_posts()) : the_post();   ?>
        <div class="col-sm-6">
          <div class="news_events_box">
            <div class="news_events_box_top_content news_events_box_btm_content">
              <ul>
                <li><a href="#">#<?php the_category(', '); ?></a></li>
              </ul>
              <h3><?php the_title(); ?></h3>
              <div><?php the_content(); ?></div>
              <a href="<?php echo $siteurl ?>saec-new/news" class="full_cont-link"></a>
              <div class="evnts_bx_btm">
                <span><?php the_date(); ?> </span>
              </div>
            </div>
          </div>
        </div>
        <?php endwhile;?>

      </div>
      <div class="col-sm-4 news_events_rgt">
        <div class="news_events_box">
          <div class="news_events_box_top_content news_events_box_btm_content">
            <ul>
              <li><a href="#">@saec.ac.in</a></li>
              <li><svg>
                  <use xlink:href="#twitter-icon"></use>
                </svg>
              </li>
            </ul>
            <h3>Our Department of H&S organised second National Conference on emerging trends in Mathematical Sciences (NCETMS-2017), On the occasion of 130th Birth Anniversary of Srinivasa Ramanujan 22nd December 2017.</h3>
            <span>6 Jan 2018</span>
          </div>
          <a href="https://twitter.com/saectweets" target="_blank" class="full_cont-link"></a>
        </div>
        <div class="news_events_box">
          <div class="news_events_box_top_content news_events_box_btm_content">
            <ul>
              <li><a href="#">@saec.ac.in</a></li>
              <li><svg>
                  <use xlink:href="#twitter-icon"></use>
                </svg>
              </li>

            </ul>
            <h3>Our college hosted the event for 7up's 'Tamizhnatin Kural' on the eve of AR Rahman's 25 year musical journey. Our students took part in the event and posted their voices for the competition.</h3>
            <span>29 Dec 2017 </span>
          </div>
          <a href="https://twitter.com/saectweets" target="_blank" class="full_cont-link"></a>
        </div>

        <div class="news_events_box">
          <div class="news_fb_bx">
            <h3>facebook</h3>
            <p>Department of Civil Engineering organized two day Workshop on the Topic “Analysis and Design of R.C.C Building with Detailing”.
              <a class="fb_link" href="#">facebook.com/SAEngineeringCollegeOfficial</a>
            </p>

            <span>27Feb 2017</span>
            <div class="hver_bg"></div>
          </div>
          <a href="https://www.facebook.com/SAEngineeringCollegeOfficial/" target="_blank" class="full_cont-link"></a>
        </div>

      </div>
    </div>
  </div>
</section>

<?php
get_footer();
                           
