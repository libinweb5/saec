<?php

/**
  Template Name: internal-quality-assurance-cell
*/


get_header();
?>

<section>
  <div class="container content-only">
    <h2 class="title_line">IQAC - Internal Quality Assurance Cell</h2>
    <div>
      <?php the_field('internal_quality_assurance_cell');?>
    </div>
    <h3><strong>IQAC Composition</strong></h3>
    <ul class="tic_list">
      <?php if( have_rows('iqac_composition') ): ?>
      <?php while( have_rows('iqac_composition') ): the_row(); ?>
      <li><a href="<?php the_sub_field ('iqac_composition_pdf')?>" target="_blank">
          <?php the_sub_field ('iqac_composition_list')?>
        </a></li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>

    <h3><strong>IQAC Reports</strong></h3>
    <ul class="tic_list">
      <?php if( have_rows('iqac_reports') ): ?>
      <?php while( have_rows('iqac_reports') ): the_row(); ?>
      <li><a href=" <?php the_sub_field ('iqac_reports_pdf')?>" target="_blank"> <?php the_sub_field ('iqac_reports_list')?></a></li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>

    <h3><strong>IQAC Minutes</strong></h3>
    <ul class="tic_list">
      <?php if( have_rows('iqac_minutes') ): ?>
      <?php while( have_rows('iqac_minutes') ): the_row(); ?>
      <li><a href="<?php the_sub_field ('iqac_minutes_pdf')?>" target="_blank"><?php the_sub_field ('iqac_minutes_list')?></a></li>
      <?php endwhile; ?>
      <?php endif; ?>


    </ul>


  </div>
</section>


<?php
get_footer();
