<?php

/**
  Template Name: iste
*/

get_header();
?>

<section>
  <div class="container content-only">
    <div>
      <h2>ABOUT ISTE</h2>
      <div class="ach_sec clearfix">
        <div class="col-sm-2">
          <div>
            <a class="fancybox" href="img/iste.png" data-fancybox-group="iste1">
              <img src=" <?php the_field('about_iste_icon');?>" class="img-responsive">
            </a>
          </div>
        </div>
      </div>
    </div>

    <div>
      <?php the_field('about_iste_details');?>
    </div>
</section>
<?php
get_footer();
