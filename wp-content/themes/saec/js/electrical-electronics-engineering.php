<!DOCTYPE html>

<style>
.course-tab-contain .tab-content-wrap .tab-content > div {
     margin-bottom: 0px !important;
}
.course-tab-contain .tab-content-wrap .tab-content table {
   
    margin-bottom: 35px !important;
}
.course-tab-contain .tab-content-wrap .tab-content > div table tr td{
    padding: 10px !important;
}
.course-tab-contain .tab-content-wrap .tab-content > div span{
    margin-bottom: 0px !important;
}
.infra_table tr td{
    border: none !important;
} 
.infra_table tr:first-of-type td{
    font-weight: unset !important;
}
.eee-fac-desc p {
    line-height: 20px;
    margin-top: 10px;
}
.eee-fac-desc {
    display: inline-block;
    width: calc(100% - 150px);
}
.eee-photo-col {
    width: 150px;
    height: 160px !important;
    display: inline-block;
    float: left;
    padding-right: 5%;
}
.eee-photo-col a.fancybox {
    height: 100%;
}
.eee-photo-col .img-responsive {
    max-width: 100%;
    display: block;
    height: 100%;
}
.eee-fac-desc a img {
    width: 35px;
    height: auto;
    padding-top: 18px;
    padding-right: 6px;
}
.single-eee-fac.col-sm-4 {
    margin: 25px 0px;
    min-height: 210px; 
}
.row .text1{
    padding-top: 10px !important;
    padding-bottom: 10px !important;
}
.text{
    font-weight: 400 !important;
    text-transform: unset !important;
    font-size: 14px !important;
    padding-bottom: 70px !important;
    line-height: 1.5;
    padding-top: 10px !important;
}
.course-tab-contain .tab-content-wrap #menu1 > div p:first-of-type{
    text-indent: 80px;
    text-align: justify;
}
.course-tab-contain .tab-content-wrap .tab-content > div h4{
    width: 70%;
    display: inline-block;
}
.course-tab-contain .tab-content-wrap .tab-content > div .ach_sec{
    margin: 0px 
}

.course-tab-contain .tab-content-wrap .tab-content > div .ach_sec .col-sm-4 div{
    border: 1px solid #ccc;
    padding: 20px;
    margin-bottom: 20px;
}
.col-sm-4 .ach_sec.clearfix {
    margin: 0px !important;
    padding: 15px;
    border: 1px solid #ddd;
    height: 200px;
    text-align: center;
}
.col-sm-4 .ach_sec.clearfix a, .col-sm-4 .ach_sec.clearfix a img{
    height: 100%;
}
.tab-content .row{
    margin-top: 25px;
}

.course-tab-contain .tab-content-wrap .tab-content .col-sm-4 span{
    text-align: center;
    margin-top: 10px; 
    height: 30px;
}
.owl-stage .col-sm-4 {
    width: 100%;
}
.board {
  background: #f8f8f8;
}
.board .container {
  position: relative;
}
.board h2 {
  font-family: 'Montserrat', sans-serif;
  font-size: 30px;
  line-height: 1.3;
  letter-spacing: 0.05em;
  font-weight: 600;
}
.board .board_trustes_inner {
  margin-top: 80px;
}
.board .board_trustes_inner .board_trustes_slder {
  text-align: center;
}
.board .board_trustes_inner .board_trustes_slder .trustes_img {
  background: #fff;
  padding: 20px;
  border-radius: 14px;
  margin-bottom: 60px;
}
.board .board_trustes_inner .board_trustes_slder span {
  font-family: 'Montserrat', sans-serif;
  margin-bottom: 20px;
  font-weight: 600;
}
.board .board_trustes_inner .board_trustes_slder p {
  font-weight: 500;
}
.board .board_trustes_inner .vision_mision {
  margin-top: 220px;
}
.board .board_trustes_inner .vision_mision .col-sm-8 {
  padding-left: 100px;
}
.board .board_trustes_inner .vision_mision .col-sm-8 p {
  font-weight: 400;
}
.board_trustes_slder_hh.owl-carousel.owl-theme.owl-loaded .owl-controls {
    text-align: center !important;
}
@media (max-width: 1200px){
    .single-eee-fac.col-sm-4{
        width: 50% !important;
    }
    .eee-fac-desc p{
        font-size: 14px;
    }
    .course-tab-contain .tab-content-wrap .tab-content .col-sm-4{
        width: 48% !important;
        float: left;
    }
}
@media (max-width: 991px){
.course-tab-contain .tab-content-wrap .tab-content .col-sm-4 span {
   
    margin-bottom: 35px !important;
    }
}
@media (max-width: 768px){
    .single-eee-fac.col-sm-4{
        width: 100% !important;
    }
    .course-tab-contain .tab-content-wrap .tab-content .col-sm-4{
        width: 50% !important;
        display: inline-block;
    }
    .course-tab-contain .tab-content-wrap .tab-content table {
    min-width: 170px !important;
   
    }
    .course-tab-contain .tab-content-wrap .tab-content > div table tr td{
        font-size: 12px !important;
    }
    .text{
        font-size: 12!important;
    }
    span.text1{
        font-size: 12!important;
    }
}

@media (max-width: 640px){
    .col-sm-4 .ach_sec.clearfix {
        height: 200px;
    }
    .board .board_trustes_inner {
    margin-top: 60px;
    }

}

@media (max-width: 400px){
    .course-tab-contain .tab-content-wrap .tab-content .col-sm-4{
        width: 100% !important;
    }


}
@media(max-width: 600px){
.course-tab-contain .tab-content-wrap .tab-content > div {
    overflow-x: hidden !important;
    overflow-y: hidden;
    }
}

</style>



<html>
<head lang="en">
    <title>Electrical and Electronics Engg - S.A. Engineering College</title>
    <meta name="description"
          content="The Department of Electrical and Electronics Engineering was established in the year 2001-2002. The department was recognized as a research center, approved by Anna University, where 5 research scholars completed their Ph.D. programme and 23 candidates pursuing their research programme.">

    <?php include('header.php'); ?>

    <div class="courses_banner" style="background-image: url('img/banner-ug-eee.jpg')">
        <div class="container">
            <div class="course_title">
                <div>
                    <span>
                        <svg>
                            <use xlink:href="#ug-eee"></use>
                        </svg>
                    </span>

                    <h1>Electrical and <br/>Electronics Engg.</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="course-tab-contain">
        <div class="container">
            <div class="tab-menu">
            <!-- <a href="<?php echo $siteurl ?>/online-application?ug=EEE" class="btn_web">Apply Now</a> -->
                <ul>
                  <li class="active"><a data-toggle="tab" href="#menu1">About</a></li>
                    <li><a data-toggle="tab" href="#menu2">Vision & Mission</a></li>
                    <li><a data-toggle="tab" href="#menu3">Faculty</a></li>
                    <li><a data-toggle="tab" href="#menu4"> Infrastructure</a></li>
                    <li><a data-toggle="tab" href="#menu5">Department Activities</a></li>
                    <li><a href="http://saec.ac.in/eee-temp/placement/" target="_blank">Student Activities </a></li>
                    <li><a href="http://saec.ac.in/eee-temp/student-performance/" target="_blank">Student’s Performance</a></li>
                    <li><a href="http://saec.ac.in/eee-temp/research-and-development/" target="_blank">Research and Development</a></li>



                </ul>
            </div>
            <div class="tab-content-wrap clearfix">
                <div class="tab-content fade in active" id="menu1">
                    <div>
                    <p>The Department of Electrical and Electronics Engineering was established in the year 2001-2002, with the intake of 60, subsequently revised to the intake of 120 in 2012-2013.  So far, 13-batches have been graduated successfully. The department has been accredited by National Board of Accreditation from 2009-2020. The department offers One Post Graduate programme viz Embedded System and Technologies, in the academic year 2006-2007 with the intake of 18. So far,10 batches have been graduated successfully.The department is recognized as a research centre, approved by Anna University. Through our research centre, 11-research scholars completed their Ph.D. programme and 24 candidates pursuing their research programme. Career guidance programmes and entrepreneurship awareness programmes are carried out through the association of EEE. We are happy to inform that the Department of Electrical and Electronics Engineering won the best department award for past four years. </p>
                    </div>
                    <div>                    
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/image1.jpeg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                    <img src="<?php echo $siteurl ?>/img/eee/image1.jpeg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Best MRM Award Academic Year (2017-2018)</span>                        
                            </div>

                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/image2.jpeg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                    <img src="<?php echo $siteurl ?>/img/eee/image2.jpeg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Best Department Award Academic Year (2016-2017)</span>                        
                            </div>

                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/image3.jpeg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2015-2016)">
                                    <img src="<?php echo $siteurl ?>/img/eee/image3.jpeg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Best MRM Award Academic Year (2015-2016)</span>                        
                            </div>

                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/image4.jpeg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
                                    <img src="<?php echo $siteurl ?>/img/eee/image4.jpeg" class="img-responsive">
                                    </a>
                                </div>
                                <span>Best Department Award Academic Year (2014-2015)</span>                        
                            </div>
                        </div>

                    </div>

                    <div>
                        <p>The Department is facilitated with 28-highly qualified, committed and expert faculty members. Our placement records have always been very impressive, with a large number of students being placed year after year in highly reputed core companies and public sector units. Our alumni hold senior positions in industries as well as in academic institutions, both in India and abroad. The high-quality research work being pursued by the faculty and students is very evident from a large number of research papers being published in refereed journals. In recognition of expertise and commitment to the development of the department in developing renewable energy resources, the proposal for the funded projects has been submitted. As a part of the academic plan, the department is making several plans to conduct International conference, National conference, FTDP, Guest lectures, Seminars, Inplant training, Industrial visit and Internship training for student's development, organized with industrial collaboration.</p>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/image5.jpeg" data-fancybox-group="gallery20" title="Rank Holders Academic Year (2016-2017)">
                                    <img src="<?php echo $siteurl ?>/img/eee/image5.jpeg" class="img-responsive">
                                    </a>
                                </div>            
                                <span>Rank Holders Academic Year (2016-2017)</span>            
                            </div>

                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/image6.jpeg" data-fancybox-group="gallery20" title="Rank Holders Academic Year (2017-2018)">
                                    <img src="<?php echo $siteurl ?>/img/eee/image6.jpeg" class="img-responsive">
                                    </a>
                                </div>            
                                <span>Rank Holders Academic Year (2017-2018)</span>            
                            </div>
                        </div>
                    </div>

                    <div class="course_feature">
                        <div class="feat-box">
                            <i>
                                <svg>
                                    <use xlink:href="#icon-program"></use>
                                </svg>
                            </i>

                            <div>
                                <label>Program</label>

                                <p>Under Graduate Degree</p>
                            </div>
                        </div>
                        <div class="feat-box">
                            <i>
                                <svg>
                                    <use xlink:href="#icon-duration"></use>
                                </svg>
                            </i>

                            <div>
                                <label>Duration</label>

                                <p>04 Years</p>
                            </div>
                        </div>
                        <div class="feat-box">
                            <i>
                                <svg>
                                    <use xlink:href="#icon-language"></use>
                                </svg>
                            </i>

                            <div>
                                <label>Language</label>

                                <p>English</p>
                            </div>
                        </div>
                        <div class="feat-box">
                            <a class="full_cont-link" href="pdf/saec.pdf" target="_blank"></a>
                            <i>
                                <svg>
                                    <use xlink:href="#icon-download"></use>
                                </svg>
                            </i>

                            <div>
                                <label>Download</label>

                                <p>Brochure</p>
                            </div>
                        </div>
                    </div>

                    <div>
                        <h3>Innovation using ARM Processor</h3>
                        <p>The department is very keen on promoting research activities among students. Particularly, in the field of ARM technology, in which ARM kits worth Rs.50,000 and Rs.5,000 were received in the academic year 2016-17&2017-18.As a feather in the cap, our final year students team won the first prize in the ARM design challenge contest 2017, held atTVS, Hosur, in the category of Most innovative project. The event is jointly organized by TVS motors (ACCS) and ARM University, UK. The prize money is worth of Rs.25,000 and the team members were assured of internship in TVS  group.</p>
                    </div>
                    <div>
                        <div class="ach_sec clearfix">
                            <div class="col-sm-3">
                            <div>
                                <a class="fancybox" href="img/eee/image7.jpeg" data-fancybox-group="gallery20" title="">
                                <img src="<?php echo $siteurl ?>/img/eee/image7.jpeg" class="img-responsive">
                                </a>
                            </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/eee/image8.jpeg" data-fancybox-group="gallery20" title="">
                                    <img src="<?php echo $siteurl ?>/img/eee/image8.jpeg" class="img-responsive">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/eee/image9.jpeg" data-fancybox-group="gallery20" title="">
                                    <img src="<?php echo $siteurl ?>/img/eee/image9.jpeg" class="img-responsive">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/eee/image10.jpeg" data-fancybox-group="gallery20" title="">
                                    <img src="<?php echo $siteurl ?>/img/eee/image10.jpeg" class="img-responsive">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <a class="fancybox" href="img/eee/image11.jpeg" data-fancybox-group="gallery20" title="">
                                    <img src="<?php echo $siteurl ?>/img/eee/image11.jpeg" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        </div>                        
                    </div>

                    <div>
                        <h3>Collaboration –(Foreign Country) Malaysia:</h3>
                        <p>The department of EEE is having an active tie-up with Power Research Group of Nasional Tenaga universiti, Malaysia.Our faculties and students have been motivated continually to participate in workshops and to present papers in International workshops, in Malaysia. Also, the department organizes workshops, conferences regularly in association with Nasional Tenaga University, as a part of academic enrichment.</p>
                        <p>Dr.A.Suresh, Prof/EEE, has received a grant of RM 12,000 in the year 2015-16, RM 1000 for International Seminar in the year 2016-17 and RM 13500 for the project work in 2017-18.</p>
                    </div>




                    

                  
                </div>
                <div class="tab-content fade" id="menu2">


                     <div>
                        <h3>Our Vision</h3>

                        <p>Conferring Excellent Technical Education with Greater Emphasis on Quality Systems, Moulding Persons for the National Development.</p>
                    </div>
                    <div>
                        <h3>Our Mission</h3>
                        <ul class="tic_list">
                            <li><p>To Enhance Quality Education by Providing State-of-Art Infrastructure with Committed
                                    Faculty.</p></li>
                            <li><p>To Provide Prerequisite Skills for the Needs of Industries, Higher Education and Research
                                    Establishments.</p></li>
                            <li><p>To Handle Socio Economic Challenges of Society by Instilling Human Values and Ethical
                                    Responsibilities.</p></li>
                        </ul>
                    </div>

                    <div>
                        <h3>PROGRAMME EDUCATIONAL OBJECTIVES (PEOs) </h3>
                        <ul class="tic_list">
                            <li><p>PEO1. To practice the students for their fruitful career in Electrical and Electronics, Allied fields and stimulate them for Higher Education and Research.</p></li>
                            <li><p>PEO2. To provide strong foundation in Basic Science, Mathematics, Circuit Theory, Field Theory,
                                    Control Theory, Signals and Systems, Power System in Electrical Power Gadgets and Basic
                                    Electronics to Power Electronics – necessary to formulate, solve and analyze Electrical and
                                    Electronics problems.</p></li>
                            <li><p>PEO3. To foster student cognizance for all-time learning and instill professional ethics.</p></li>
                            <li><p>PEO4. To adapt evolving technologies and stay contemporary with their profession.</p></li>


                        </ul>
                    </div>
                    <div>
                        <h3> PROGRAMME OUTCOMES(POs)</h3>
                        <ul class="tic_list">
                            <li><p>PO1. Engineering knowledge: Apply the knowledge of mathematics, science, engineering fundamentals,
                                    and an engineering specialization to the solution of complex engineering problems.</p></li>
                            <li><p>PO2. Problem analysis: Identify, formulate, review research literature, and analyze complex
                                    engineering problems reaching substantiated conclusions using first principles of mathematics,
                                    natural sciences, and engineering sciences.</p></li>
                            <li><p>PO3. Design/development of solutions: Design solutions for complex engineering problems and design
                                    system components or processes that meet the specified needs with appropriate consideration
                                    for the public health and safety, and the cultural, societal, and environmental
                                    considerations.</p></li>
                            <li><p>PO4. Conduct investigations of complex problems: Use research-based knowledge and research methods
                                    including design of experiments, analysis and interpretation of data, and synthesis of the
                                    information to provide valid conclusions.</p></li>
                            <li><p>PO5. Modern tool usage: Create, select, and apply appropriate techniques, resources, and modern
                                    engineering and IT tools including prediction and modeling to complex engineering activities
                                    with an understanding of the limitations.</p></li>
                            <li><p>PO6. The engineer and society: Apply reasoning informed by the contextual knowledge to assess
                                    societal, health, safety, legal and cultural issues and the consequent responsibilities
                                    relevant to the professional engineering practice.</p></li>
                            <li><p>PO7. Environment and sustainability: Understand the impact of the professional engineering solutions
                                    in societal and environmental contexts, and demonstrate the knowledge of, and need for
                                    sustainable development.</p></li>
                            <li><p>PO8. Ethics: Apply ethical principles and commit to professional ethics and responsibilities and
                                    norms of the engineering practice.</p></li>
                            <li><p>PO9. Individual and team work: Function effectively as an individual, and as a member or leader in
                                    diverse teams, and in multidisciplinary settings.</p></li>
                            <li><p>PO10.   Communication: Communicate effectively on complex engineering activities with the engineering
                                    community and with society at large, such as, being able to comprehend and write effective
                                    reports and design documentation, make effective presentations, and give and receive clear
                                    instructions.</p></li>
                            <li><p>PO11.Project management and finance: Demonstrate knowledge and understanding of the engineering and
                                    management principles and apply these to one’s own work, as a member and leader in a team, to
                                    manage projects and in multidisciplinary environments.</p></li>
                            <li><p>PO12.Life-long learning: Recognize the need for, and have the preparation and ability to engage in
                                    independent and life-long learning in the broadest context of technological change.</p></li>
                        </ul>
                    </div>
                    <div>
                        <h3>PROGRAMME SPECIFIC OUTCOMES(PSOs)</h3>
                        <ul class="tic_list">  
                            <li><p>PSO1. Utilize coherent theoretical and practical methodologies to design and implement Electrical and Electronics systems.</p></li>
                            <li><p>PSO2. Assimilate facts of basic Electronics to Power Electronics and recent embedded technologies for governing, consistent and workable Electrical and Electronics systems.</p></li>
                            <li><p>PSO3. Apply computing platform and developing software for power grids and hybridizing the new renewable energy to overcome the power demand.</p></li>
                        </ul>
                    </div>


                    


                </div>
                <div class="tab-content fade" id="menu3">
                   
            <div class="clearfix"></div>            
    <div>
        <h4><strong>PROFESSORS</strong></h4>


        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/s-priya.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/s-priya.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Dr.S.Priya</p>
                 <p>Power Electronics, Embedded Systems and Soft Computing</p>
                 <p>priya@saec.ac.in</p>

                 <a href="orcid.org/0000-0003-2843-7127" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.com/citations?hl=en&user=oEaIfv8AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Priya_Sakthi_Kumar" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="img/sathiyasekar.png" data-fancybox-group="faculty1">
                <img src="img/sathiyasekar.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Dr.K.Sathiyasekar M.Tech., Ph.D</p>
                 <p>High voltage Engineering, Soft computing Techniques</p>
                 <p>sathiyasekar@gmail.com</p>

                 <a href="orcid.org/0000-0002-4836-0991" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=Gsj23aMAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Kumarasamy_Sathiyasekar" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/suresh.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/suresh.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Dr.A.Suresh M.E., Ph.D</p>
                 <p>Power Electronics and Drives</p>
                 <p>drsuresha@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0002-9498-9790" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.com/citations?user=xEyVGGgAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Suresh_A2" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <div>   
        <h4><strong>ASSOCIATE PROFESSORS</strong></h4>
       

       <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/magadevi.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/magadevi.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.N.Magadevi M.E., (Ph.D)</p>
                 <p>Data Mining, Theoretical Computing, Compilers</p>
                 <p>magadevi@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0001-7398-1696" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=q_UypwUAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Magadevi_Nirmalkumar2" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/bharathi.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/bharathi.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.S.Bharathi M.E</p>
                 <p>Applied Electronics</p>
                 <p>bharathi@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a> -->
                 <a href="https://scholar.google.com/citations?view_op=list_works&hl=en&user=ZhI_K-oAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/vinidha_roc_alphonse" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/prabha.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/prabha.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.A.Prabha M.E</p>
                 <p>Energy Engineering</p>
                 <p>prabhaa@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0002-5168-2510" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=Wu9AliwAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Prabha_Arumugam" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/karthikeyan.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/karthikeyan.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.B.Karthikeyan M.E., (Ph.D)</p>
                 <p>Energy Engineering</p>
                 <p>karthikeyanb@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a> -->
                 <a href="https://www.researchgate.net/profile/Karthikeyan_Balakrishnan3" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/Irin-Loretta-G.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/Irin-Loretta-G.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs. Irin Loretta G G M.E., (Ph.D)</p>
                 <p>Wireless Sensor Networks</p>
                 <p>irinlorettag@gmail.com</p>

                 <a href="https://orcid.org/0000-0003-1956-4319" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=ZOv24c4AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Irin_George" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>




    </div>


    <div class="clearfix"></div>
    <div>
        <h4><strong>ASSISTANT PROFESSORS</strong></h4>




        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/margaret.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/margaret.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.K.S.Margaret M.E., (Ph.D)</p>
                 <p>Energy Engineering</p>
                 <p>margaret@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0002-8636-7609 " target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=Ue_YDAYAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Margaret_Sowriapppan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/sudheer.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/sudheer.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.S.Sudheer M.E., (Ph.D)</p>
                 <p>Applied Electronics</p>
                 <p>sudheer@saec.ac.in</p>

                 <a href="http://orcid.org/0000-0003-0069-958X" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=XHhaf24AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Sudheer_Sukumaran" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/vinidha.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/vinidha.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.A.Vinidha Roc  M.Tech</p>
                 <p>VLSI design</p>
                 <p> vinidharoc@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0003-2212-0559" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=tGOtR98AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/rajavinu.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/rajavinu.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.N.Rajavinu M.E</p>
                 <p>Power Electronics and Drives</p>
                 <p>rajavinun@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/sathya.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/sathya.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.S.Sathya M.E</p>
                 <p>Power Electronics and Drives</p>
                 <p>sathyas@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0003-0028-1028" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=27WQydkAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Sathya_Suresh_Kumar " target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/sathish.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/sathish.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.T.Sathish Kumar M.E., (Ph.D)</p>
                 <p>Power System Engineering</p>
                 <p>sathishkumart@saec.ac.in</p>

                 <a href=" https://orcid.org/0000-0002-7324-7106" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=QMI61K8AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Sathishkumar_Thayalan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/ramya.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/ramya.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.M.Ramya M.E., (Ph.D)</p>
                 <p>Embedded System Technologies</p>
                 <p>ramya@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/baskaran.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/baskaran.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.S.Baskaran M.E., (Ph.D)</p>
                 <p>Power Electronics and Drives </p>
                 <p>baskaran@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/alex.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/alex.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.S.P.AlexPrabhu M.E., (Ph.D)</p>
                 <p>Instrumentation, Power systems, Power Electronics</p>
                 <p>alexprabu@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0003-3522-0081" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=xlHNTFYAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Alexprabu_S_P" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/sakthi.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/sakthi.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr. N. Sakthisaravanan M.E</p>
                 <p>Power Electronics and Drives</p>
                 <p>sakthisaravanann@gmail.com</p>

                 <a href="https://orcid.org/0000-0003-3164-0572" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=1UAksKYAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Sakthisaravanan_Natarajan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/prakash.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/prakash.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.D.Prakash M.Tech., (Ph.D)</p>
                 <p>Instrumentation, Power systems, Power Electronics </p>
                 <p>prakashd@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a> -->
                 <a href="https://www.researchgate.net/profile/Prakash_Devarajan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/valliammal.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/valliammal.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.L.Valliammal M.E., (Ph.D)</p>
                 <p>Electronics and Control. </p>
                 <p>valliammal@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/nagalakshmi.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/nagalakshmi.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.L.Naga Lakshmi M.E</p>
                 <p>Power system Engineering</p>
                 <p>nagalakshmil@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0002-2859-327X" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=NbnPkh8AAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Nagalakshmi_Lakshmipathy" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/divya.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/divya.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.V.DivyaPrabha M.E., (Ph.D)</p>
                 <p>Power Electronics and Drives</p>
                 <p>dhivyapraba@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0003-0396-3673" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=9DekgpMAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Divyaprabha_Varadharajan" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/mahendran.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/mahendran.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.R.Mahendran M.E</p>
                 <p>Power systems engineering</p>
                 <p>mahendranr@saec.ac.in</p>

                 <a href=" https://orcid.org/0000-0001-7039-8614" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=6TjcwtoAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Ar_Mahendran" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/archana.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/archana.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mrs.S.Archana M.E</p>
                 <p>VLSI design</p>
                 <p>archanas@saec.ac.in</p>

                 <!-- <a href=""><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href=""><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a> -->
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/suresh1.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/suresh1.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.S.Suresh M.E</p>
                 <p>Embedded system Technologies</p>
                 <p>sureshs@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0002-5168-0056" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?user=fPKY1KUAAAAJ&hl=en" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Suresh_Subramanian9" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/Mr.R.Kamalakannan.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/Mr.R.Kamalakannan.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Mr.R.KamalaKannan M.E</p>
                 <p>​Embedded system Technologies</p>
                 <p>kamalakannanr@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0003-4468-5622" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.com/citations?hl=en&authuser=1&user=lYvZGhoAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Kamalakannan_Ramachandran" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/priyanka.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/priyanka.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Ms.R.Priyanka M.E</p>
                 <p>​Power Electronics and Drives</p>
                 <p>priyankar@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0001-9203-7393" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=SjCTxT8AAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Priyanka_Ramesh8" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>

        <div class="single-eee-fac col-sm-4">
            <div class="eee-photo-col">
                <a class="fancybox" href="<?php echo $siteurl ?>/img/indumathi.png" data-fancybox-group="faculty1">
                <img src="<?php echo $siteurl ?>/img/indumathi.png" class="img-responsive">
                </a>
            </div>
            <div class="eee-fac-desc">
                 <p>Ms.M.Indumathi M.E</p>
                 <p>Power Electronics and Drives</p>
                 <p>indhumathim@saec.ac.in</p>

                 <a href="https://orcid.org/0000-0001-5526-1329" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/ID.png"></a>
                 <a href="https://scholar.google.co.in/citations?hl=en&user=541FRygAAAAJ" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/GS.png"></a>
                 <a href="https://www.researchgate.net/profile/Indumathi_Marimuthu" target="_blank"><img src="<?php echo $siteurl ?>/img/eee/RG.png"></a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>



        <div>
            <ul class="tic_list">
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2016-17 odd.pdf">FDP Attende:Odd 2016-2017</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2016-17 even.pdf">FDP Attende:Even 2016-2017</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2015-2016 odd.pdf">FDP Attende:Odd 2015-2016</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2015-16 EVEN.pdf">FDP Attende:Even 2015-2016</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2014-15 ODD.pdf">FDP Attende:Odd 2014-2015</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2014-15 EVEN.pdf">FDP Attende:Even 2014-2015</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2013-14 ODD.pdf">FDP Attende:Odd 2013-2014</a> </li>
                <li><a href="<?php echo $siteurl ?>/pdf/FDP attended 2013-14 even.pdf">FDP Attende:Even 2013-2014</a> </li>
            </ul>
        </div>
        <div>
            <h3>Awards Received </h3>
            <div>
                <ul class="tic_list">
                    <li>Dr. K.Sathiyasekar - Received Best Paper Award in the International Conference on Digital Factory – 2008, held at CIT, Coimbatore.</li>
                    <li>Dr. K.Sathiyasekar - Best Reviewer Award     Elsevier – Journal of Electrical Power and Energy System</li>
                    <li>Dr. K.Sathiyasekar - INVITED SPEAKER in the Baltic Conference Series 2017, Sweden </li>
                    <li>Dr. A. Suresh - Universal Achievers Gold Medal Award    Universal Achievers Foundation 2017, New Delhi</li>
                    <li>Dr. Suresh A - Dr. A P J Abdul Kalam Gold Medal Award  All India Achievers and Research Academy 2017</li>
                    <li>Dr. Suresh A - Bharat Ratna Rajiv Gandhi Gold Medal Award from Global Economic Progress and Research Association</li>
                    <li>Dr. Suresh A - Received Bharat Ratna Mother Teresa Gold medal award,2016 from Global Economic Progress Research Association,Tamilnadu.</li>
                    <li>Dr. Suresh A - Won best paper award in the International Conference on Advances in Applied Engineering and Technology, 2015 at AMET University, Chennai.</li>
                    <li>Dr. Suresh A - Received Indira Gandhi Sadbhavana Gold medal award, Nov 19 th 2014</li>
                    <li>Dr. Sathiyasekar Inaugurated South India First Zonal Pradhan Mantri Kaushal Kendra Centre at S.A. Engineering College </li>

                </ul>
            </div>
        </div>
        <div>
            <h3>Country Visited</h3>
            <div>
                <ul class="tic_list">
                    <li>Dr. K.Sathiyasekar - Awarded Travel Grant by the Department and Science and Technology, Government of India, to present research paper in the International Conference INDUCTICA-2010, at Messe Berlin, Germany in 2010.</li>
                    <li>Dr. Suresh A - Presented paper in Conference held at Bankok    International Institute of education, Research and Development</li>
                    <li>Dr. Suresh A - Session Chair for Conference held at Bankok International Institute of Education, Research and Development</li>
                    <li>Dr. Suresh A - Session Chair for International conference 4th EURECA15, Taylor’s University, Malaysia.  </li>
                    <li>Dr. Suresh A - Project Expo jury for International conference 4th EURECA15, Taylor’s University, Malaysia.</li>
                    <li>Dr. Suresh A - Presented paper in International conference 4th EURECA15, Taylor’s University, Malaysia.</li>
                    <li>Dr. Suresh A - Universiti Tenaga Nasional, Malaysia  –Post Doctoral Fellowship  </li>
                </ul>
            </div>
        </div>
        </div>

<div class="tab-content fade" id="menu4">

     <div>                    
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/electrical machines lab.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/electrical machines lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>ELECTRICAL MACHINES LABORATORY</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                            <tr>
                                                <td>Area in Sq.mts:</td>
                                               <td>232.53</td>
                                            </tr>
                                            <tr>
                                                
                                                 <td>Major Cost of Equipment:</td>
                                                <td>Rs. 15,39,355</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                        
                        
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                    <a class="fancybox" href="img/contol and instrumentation lab.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/contol and instrumentation lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>CONTROL & INSTRUMENTATION LAB</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>77.53</td>
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.6,14,303</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                       
                   
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/Electronics lab.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/Electronics lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>ELECTRONICS LABORATORY</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                             <td>84.43</td>
                                            
                                        </tr>
                                        <tr>
                                           
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.3,80,060</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                        
                            
                        
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/power electronics lab.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/power electronics lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>POWER ELECTRONICS&DRIVES LABORATORY</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>84.43</td>
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.9,65,930</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                
                     
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/microprocessor and microcontroller lab.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/microprocessor and microcontroller lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>MICROPROCESSOR AND MICROCONTROLLER LABORATORY</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>198.44</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.16,87,068</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
                
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/power system simulation lab.jpg" data-fancybox-group="gallery1" title="">
                                        <img src="<?php echo $siteurl ?>/img/power system simulation lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>POWER SYSTEM SIMULATION LABORATORY</span> 
                                <div>
                                    <table class="infra_table">
                                        <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>198.44</td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.23,73,763</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                         
         
                      
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/engineering practices lab.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/engineering practices lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>ENGINEERING PRACTICES LABORATORY</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>140</td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.1,51,622</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/r&d lab.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/r&d lab.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>R&D LAB</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td>Area in Sq.mts:</td>
                                            <td>66</td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>Major Cost of Equipment:</td>
                                            <td>Rs.6,14,306</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>                          
                            </div>
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/Digital Classroom.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/Digital Classroom.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>DIGITAL CLASSROOM</span> 
                                <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>     
                                                   
                            </div>
                            <div class="col-sm-4">
                                <div class="ach_sec clearfix">
                                
                                     <a class="fancybox" href="img/smart board class room.jpg" data-fancybox-group="gallery1" title="">
                                    <img src="<?php echo $siteurl ?>/img/smart board class room.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span>SMART BOARD CLASS ROOM</span> 
                                 <div>
                                    <table class="infra_table">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>   
                                                     
                            </div>
                         
                        </div>

    </div>

</div>

    <div class="board" id="menu5">
        <div class="container">
        <h2 class="title_line">Guest lecture</h2>
        <div class="board_navslide"></div>
        <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh owl-carousel">





<div class="row">
                            <div<!--  class="col-sm-4" -->
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/1.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2017-2018)">
                                    <img src="<?php echo $siteurl ?>/img/eee/1.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span class="text1">Dr.UDHAYAKUMAR</span>      
                                <div class="text">Professor/EEE, Anna University, Chennai <br />“Measurements And
                                Instrumentation” on 19-01- 2018.</div>                      
                            </div>

                            <div <!-- class="col-sm-4" -->
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/2.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2016-2017)">
                                    <img src="<?php echo $siteurl ?>/img/eee/2.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span class="text1">DR.N.BOOMA</span>     
                                <div class="text">Professor /EEE, Jerusalam College of Engineering, Chennai <br />“High Voltage
                                Direct Current Transmission System” on 02-02- 2018.</div>                        
                            </div>

                            <div <!-- class="col-sm-4" -->
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/3.jpg" data-fancybox-group="gallery20" title="Best MRM Award Academic Year (2015-2016)">
                                    <img src="<?php echo $siteurl ?>/img/eee/3.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span class="text1">SWAMY.PRYGYANANDHA</span>     
                                <div class="text">Motivation And Discourse Speaker, Assistant Engineer, TNEB
                                (Retd), Chennai <br />“Professional Ethics In Life” on 20-02- 2018</div>                        
                            </div>

                            <div <!-- class="col-sm-4" -->
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/4.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
                                    <img src="<?php echo $siteurl ?>/img/eee/4.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span class="text1">Mr.P.KARTHIKEYAN</span>     
                                <div class="text">(Alumni), Assistant Manager, IFF India Pvt. Ltd,
                                Chennai <br />“Key Traits That Corporate From Employee” on 24.02.2018.</div>                        
                            </div>
                            <div <!-- class="col-sm-4" -->
                                <div class="ach_sec clearfix">
                                    <a class="fancybox" href="img/eee/5.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
                                    <img src="<?php echo $siteurl ?>/img/eee/5.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span class="text1">Dr.RATHINA KANNAN</span>     
                                <div class="text">Professor, Department of EEE, Anna University, Chennai <br />“Recent Trends in
                                Embedded System” on 24.02.2018.</div>                        
                            </div>
                            <div class="col-sm-4">
                                <div <!-- class="ach_sec clearfix" -->
                                    <a class="fancybox" href="img/eee/6.jpg" data-fancybox-group="gallery20" title="Best Department Award Academic Year (2014-2015)">
                                    <img src="<?php echo $siteurl ?>/img/eee/6.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <span class="text1">Mr.M.BALACHANDER</span>  

                                <div class="text">Research and Development Engineer, Standard Engineering Associates,
                                Chennai<br /> “Current Trends in Signal Processing” on 09.03.2018.</div>                        
                            </div>


            </div>



            

        </div>

    </div>


</div>















                    
                



                <div class="tab-content fade" id="menu6">
                    <div>
                        <h3>Research and Development</h3>
                        <p>Research at S.A. Engineering College, aims to develop innovative solutions to the world&rsquo;s most alarming problems. From addressing the energy needs of tomorrow to improving power quality, our research efforts are enhanced through creative collaborations with leading research institutes and organization around the world. Compiled here are just some of the R&amp;D labs, centers and programs where cutting-edge research is taking place. Our department has been recognized as a research centre by Centre of Research, Anna University, Chennai 600 025. (Approval No: Lr.No.114/IR/EEE/AR1 Dated: 07/01/2013).</p>
                    </div>
                    <div>
                        <h3>R&amp;D Centre Excellence</p>
                        <p>National Institute for Wind Energy: Inaugurated on 29-3-2016.</p>
                        <p>The main objective is to train the students with keen practical knowledge on basics of Power and energy systems. Initiate the students and faculties in research and developments in recent scenario on power systems.</p>
                        <p>Trainings were given to students regarding to the high end placements in reputed companies.</p>
                    </div>
                    <div>
                        <h3>Centre of Excellence in Industrial Automation and Robotics.</h3>
                        <p>Centre for Industrial Automation and Robotics is based in the department of Electrical and Electronics Engineering at S.A. Engineering College. This centre provides academic and industrial research in the key areas including industrial automation training, industrial drives, industrial SCADA/HMI, industrial robotics training and solution. The centre has two labs</p>
                        
                        <ul class="tic_list">
                            <li>1. Six Axis Robotics Lab</li>
                            <li>2. Spark Automation Centre</li>
                        </ul>   
                    </div>
                    <div>
                        <h3>Supervisors</h3>
                            <table class="c13">
                                <tbody>
                                    <tr class="c11">
                                        <td class="c87" colspan="1" rowspan="1">
                                            <p>Dr. Priya S., M.E., Ph.D</p>
                                            <p>HoD/EEE</p>
                                            <p class="c0"><img alt="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/s-priya.png" src="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/s-priya.png"></p>
                                        </td>
                                        <td class="c55" colspan="1" rowspan="1">
                                            <p>Dr. SathiyaSekar&nbsp; K., M.Tech., Ph.D</p>
                                            <p>Professor</p>
                                            <p class="c123"><img alt="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/sathiyasekar.png" src="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/sathiyasekar.png" style="width: 133.62px; height: 144.95px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
                                        </td>
                                        <td class="c124" colspan="1" rowspan="1">
                                            <p>Dr. Suresh A.,&nbsp; M.E., Ph.D</p>
                                            <p>Professor</p>
                                            <p class="c0"><img alt="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/suresh-234x300.png" src="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/suresh-234x300.png"></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                        
                    
                </div>

                <div class="external_links course_ext-links" style="padding: 20px 0px;">
                    <label>Also check: </label>
                    <ul class="share_icons clearfix">
                        <li><a href="https://www.annauniv.edu/academic_courses/curr_aff.html" target="_blank">Curriculum &amp; Syllabi</a></li>
                        <li><a href="pdf/alumni-eee.pdf" target="_blank">EEE-Alumni</a></li>
                    </ul>
                </div>




            </div>
        </div>
    </div>


<?php include('virtual-tour-strip.php'); ?>

    <?php include('counter-part.php'); ?>

<?php include('footer.php'); ?>