$(document).ready(function () {

  $('.webform select').selectric();

  $("input[type=file]").change(function () {
    var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
    if (filename != "") {
      $(this).parent().find('p').text(filename);
    } else {
      $(this).parent().find('p').text("No file choosen");
    }
  });

  $('.webform .academic-table td input').focusout(function () {
    $('.applynow-wrap .academic-table tr').removeClass('active');
  });
  $('.webform .academic-table td input').focus(function () {
    $('.applynow-wrap .academic-table tr').removeClass('active');
    $(this).parents('tr').addClass('active');
  });





  $(document).on('click', '.hammenu-wrap .hammenu', function () {
    $('.main-menu-nav').toggleClass('menu-open');
    $(this).toggleClass('open');
  });

  $(document).on('click', '.hammenu-wrap .hammenu.open', function () {
    setTimeout(function () {
      $('.drop-menu_wrap').removeClass('menu-opened-state');
    }, 1000);
  });


  $(document).on('click', '.main-menu-nav li.menu-dropdown', function () {
    $(this).parents('.main-menu-nav').addClass('menu-opened-state');
    $(this).find('.drop-menu_wrap').addClass('menu-opened-state');
  });

  $(document).on('click', 'a.back-button-link', function (e) {
    e.stopPropagation();
    $(this).parents('.main-menu-nav').removeClass('menu-opened-state');
    $(this).parents('.drop-menu_wrap').removeClass('menu-opened-state');
  });


  /* news scroller */
  var scroll_list = $('.headnews-scroller ul li');
  var scroll_len = scroll_list.length;
  var scroll_init = 0;
  scroll_list.eq(scroll_init).addClass('active');
  // var scroll_inter = setInterval(function scrollfn(){
  //     scroll_init++;
  //     scroll_list.removeClass('active inactive');
  //     if(scroll_init < scroll_len){
  //         scroll_list.eq(scroll_init-1).addClass('inactive ');
  //         scroll_list.eq(scroll_init).addClass('active ');
  //     }
  //     else{
  //         scroll_init = 0;
  //         scroll_list.eq(scroll_len-1).addClass('inactive ');
  //         scroll_list.eq(scroll_init).addClass('active ');
  //     }
  //     //var scroll_wid =  $('.headnews-scroller ul').outerWidth();
  //     //var list_wid = scroll_list.eq(scroll_init).outerWidth();

  //     //if(list_wid > scroll_wid){
  //     //    clearInterval(scroll_inter);
  //     //    var diff_wid = list_wid - scroll_wid;
  //     //    scroll_list.eq(scroll_init).animate({
  //     //        'left': -1 * diff_wid
  //     //    },2000,'swing',function(){
  //     //        setInterval(scrollfn,2000);
  //     //    });
  //     //}
  // },4000);



  /* banner-event-slider */
  var banner_event_slider = $('.banner-content .banner-right .event-slider');
  banner_event_slider.owlCarousel({
    loop: true,
    items: 1,
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 8000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true,
    navText: ["<span class='web_next'></span>", "<span class='web_prev'></span>"]
  });

  /* home-ranking slider */
  $('.ranking_slider').owlCarousel({
    loop: true,
    items: 1,
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true,
    navText: ["<span class='web_next'></span>", "<span class='web_prev'></span>"]
  });

  /* home-achievement slider */
  $('.achieve_sec .achieve-slider .slider').owlCarousel({
    loop: true,
    items: 1,
    nav: true,
    dots: false,
    //autoplay: true,
    autoplayTimeout: 3000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true,
    navText: ["<span class='web_next'></span>", "<span class='web_prev'></span>"]
  });

  /* home-review slider*/
  $('.review_slider').owlCarousel({
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    autoplay: false,
    autoplayTimeout: 3000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true
  });

  $('.board_trustes_slder').owlCarousel({
    loop: true,
    items: 4,
    margin: 30,
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true,
    navContainer: '.board_navslide',
    responsive: {
      0: {
        items: 2,
        mouseDrag: true,
        autoplay: true,
        loop: true,
        dots: true
      },
      480: {
        items: 3,
        mouseDrag: true,
        autoplay: true,
        loop: true
      },
      991: {
        items: 4,
        mouseDrag: true,
        autoplay: true,
        loop: true
      }
    }
  });
  $('.board_trustes_slder_hh').owlCarousel({
    loop: true,
    items: 4,
    margin: 30,
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true,
    navContainer: '.board_navslide',
    responsive: {
      0: {
        items: 1,
        mouseDrag: true,
        autoplay: true,
        loop: true,
        dots: true
      },
      480: {
        items: 2,
        mouseDrag: true,
        autoplay: true,
        loop: true
      },
      991: {
        items: 4,
        mouseDrag: true,
        autoplay: true,
        loop: true
      }
    }
  });

  $('.our_recruiters_slide').owlCarousel({
    loop: true,
    items: 6,
    margin: 30,
    nav: true,
    dots: false,
    autoplay: false,
    autoplayTimeout: 3000,
    smartSpeed: 900,
    autoplaySpeed: 900,
    autoplayHoverPause: true,
    navContainer: '.board_navslide',
    responsive: {
      0: {
        items: 2,
        mouseDrag: true,
        autoplay: true,
        loop: true,
        dots: true,
        margin: 10
      },
      480: {
        items: 3,
        mouseDrag: true,
        autoplay: true,
        loop: true,
        margin: 10
      },
      991: {
        items: 6,
        mouseDrag: true,
        autoplay: true,
        loop: true
      }
    }
  });



  /* home-banner hover animation */
  $('.banner-left .banner-boxcontent .row > div').each(function () {
    $(this).hoverdir();
  });


  /* home-infra hover animation */
  $('.infra_sec .col-sm-8 .great_infra .col-sm-4 > div').each(function () {
    $(this).hoverdir();
  });

  $('.gallery_page .gallery_sec ul li').each(function () {
    $(this).hoverdir();
  });

  /* SVG sprite including*/
  //$.get('img/sprite.txt', function(data) {
  //    console.log(data);
  //});


  $(document).on('click', '.main-menu-nav .container > ul > li .search-menu a', function (e) {
    e.preventDefault();
    $('.main-menu-nav .container .search-container').slideToggle();
    $('.main-menu-nav .container .search-container input').focus();
  });
  $(document).on('click', '.main-menu-nav .container .search-container span.close-search', function (e) {
    e.preventDefault();
    $('.main-menu-nav .container .search-container').slideUp();
    $('.main-menu-nav .container .search-container input').val("");
  });




  $('.fancybox').fancybox();

  $(".map_pin").click(function () {
    $('.contact_banner').toggleClass("map_class");
  });


  $('.grid-layout').masonry({
    // options
    itemSelector: '.grid-item'
  });


  $('body').append("<div class='modal-backdrop'></div>");
  $(document).on('click', '.modal-link', function (e) {
    e.preventDefault();
    var target = $(this).attr('data-target');
    $(target).addClass('modal-open');
    $('.modal-backdrop').addClass('overlay');
  });


  $('li#mega-menu-item-53 .mega-menu-link').click(function () {
    $('#car_modal').addClass("modal-open");
  });



  $('body').append("<div class='modal-backdrop'></div>");
  $(document).on('click', 'li#mega-menu-item-53 .mega-menu-link', function (e) {
    e.preventDefault();
    var target = $(this).attr('data-target');
    $('#car_modal').addClass('modal-open');
    $('.modal-backdrop').addClass('overlay');
  });


  $(document).on('click', '.web-modal.modal-open', function (e) {
    e.preventDefault();
    $('.modal-backdrop').removeClass('overlay');
    $('.web-modal').removeClass('modal-open');
  });

  $(document).on('click', '.web-modal.modal-open .modal-dialog', function (e) {
    e.stopPropagation();
  });
  
  $(document).on('click', '.web-modal.modal-open button.close', function (e) {
    e.preventDefault();
    $('.modal-backdrop').removeClass('overlay');
    $('.web-modal').removeClass('modal-open');
  });

  
//  $(document).on('click', '.media-video .modal-link', function () {
//    var data_url = $(this).attr('data-url');
//    var final_url = data_url + "?start=0&amp;rel=0&amp;autoplay=1&amp;showinfo=0";
//    $('#mediamodal').find('video').attr('src', final_url);
//  });

  $(document).on('click', '#mediamodal.web-modal,#mediamodal button.close', function (e) {
    $('#mediamodal').find('video').attr('src', "");
  });


  $(document).on('click', '.applynow-wrap .tab-menu li a', function (e) {
    var tabindex = $(this).parent().index();
    var leftpos = 0;
    for (var i = 0; i < tabindex; i++) {
      leftpos = leftpos + $('.applynow-wrap .tab-menu li').eq(i).outerWidth() - 1;
    }
    $('.applynow-wrap .tab-menu').animate({
      scrollLeft: leftpos
    }, 500);
  });


  $(document).on('click', '.course-tab-contain .tab-menu ul li a', function (e) {
    var course_tabindex = $(this).parent().index();
    var course_leftpos = 0;
    for (var i = 0; i < course_tabindex; i++) {
      course_leftpos = course_leftpos + $('.course-tab-contain .tab-menu ul li').eq(i).outerWidth();
    }
    $('.course-tab-contain .tab-menu ul').animate({
      scrollLeft: course_leftpos
    }, 500);
  });


  document.addEventListener('gesturestart', function (e) {
    e.preventDefault();
  });


  //$('.news-wrapper ul').marquee({
  //    duration: 20000,
  //    duplicated: true
  //});

  //$('marquee').marquee(optionalClass);
});

$(window).scroll(function (event) {
  var scroll = $(window).scrollTop();
  if (scroll >= 250)
    $('.main-menu-nav').addClass('sticky-head');
  else
    $('.main-menu-nav').removeClass('sticky-head');


  if (scroll >= 500)
    $('.main-menu-nav').addClass('show');
  else
    $('.main-menu-nav').removeClass('show');
});

var tween;
var init = $('.headnews-scroller .container').width();

function marq(init) {
  tween = TweenMax.fromTo($(".headnews-scroller ul"), 500, {
    x: init
  }, {
    x: -($('.headnews-scroller ul').width()),
    ease: Linear.easeNone,
    repeat: -1
  });
}
$(document).ready(function () {
  marq(init);
  $(".headnews-scroller").mouseover(function () {
    tween.pause();
  });
  $(".headnews-scroller").mouseout(function () {
    tween.play();
  });
});


// $('.content-only img').wrap('<div><div class="ach_sec"><div class="col-sm-3"><div><a class="fancybox" title="Best alumni award the best alumni of the year was awarded to dr.j.umarani,2002 batch it student who is successful in her career"></a></div></div></div></div>');

$('.content-only h4').addClass('clearfix');
$('.content-only h3').addClass('clearfix');
$('.content-only span').addClass('clearfix');
