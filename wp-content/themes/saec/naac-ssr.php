<?php

  /**
    Template Name: naac-ssr
  */

get_header();
?>

<section>
  <div class="container content-only">
    <h1 class="title_line">NAAC SSR</h1>
    <?php $loop = new WP_Query( array( 'post_type' => 'naac_ssr') ); ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <h3><a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a></h3>
    <?php endwhile; wp_reset_query(); ?>
  </div>
</section>
<?php
get_footer();
