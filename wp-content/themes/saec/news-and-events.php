<?php

/**
  Template Name: news-and-events
*/


get_header();
?>

<section class="nws_evnts_inner">
  <div class="events_container">
    <h2 class="title_line">News & Events</h2>
    <?php 
        query_posts(array( 
            'post_type' => 'news_events',
        ) );  
      ?>

    <?php while (have_posts()) : the_post();   ?>
    <?php if ( has_post_thumbnail() ) { ?>
    <div class="nws_evnts_box nws_evnts_img_sec">
      <div class="row">
        <div class="col-sm-5">
          <div class="thumbnail-img-wrap">
            <?php the_post_thumbnail(); ?>
          </div>
        </div>
        <div class="col-sm-7">
          <ul>
            <li><a href="#">#<?php the_category(', '); ?></a></li>
          </ul>
          <h3><?php the_title(); ?></h3>
          <div><?php the_content(); ?></div>
          <div class="evnts_bx_btm">
            <span><?php the_date(); ?> </span>
            <ul>
              <li>
                <a href="<?php the_field('upload_your_pdf_file', 144); ?> " target="_blank">Download Brochure
                  <svg>
                    <use xlink:href="#evnts_dwnlod"></use>
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php 
      }
    else{ 
      ?>
    <div class="nws_evnts_box">
      <ul>
        <li><a href="#">#<?php the_category(', '); ?></a></li>
      </ul>
      <h3><?php the_title(); ?></h3>
      <div><?php the_content(); ?></div>
      <a href="<?php the_field('upload_your_pdf_file', 144); ?>" target="_blank" class="full_cont-link"></a>
      <div class="evnts_bx_btm">
        <span><?php the_date(); ?> </span>
            <ul>
              <li>
                <a href="<?php the_field('upload_your_pdf_file', 144); ?> " target="_blank">Download Brochure
                  <svg>
                    <use xlink:href="#evnts_dwnlod"></use>
                  </svg>
                </a>
              </li>
            </ul>
      </div>
    </div>
    <?php
      } 
      ?>
    <?php endwhile;?>

  </div>
</section>

<?php
get_footer();
