<?php

/**
  Template Name: nirf
*/


get_header();
?>


<section>
  <div class="container content-only">
    <h3>National Institutional Ranking Framework (NIRF)</h3>
    <ul class="tic_list">
      <?php if( have_rows('nirf') ): ?>
      <?php while( have_rows('nirf') ): the_row(); ?>
      <li>
        <a href="<?php the_sub_field('nirf_pdf'); ?>">
          <?php the_sub_field('nirf_list'); ?>
        </a>
      </li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>
  </div>
</section>


<?php
get_footer();
