<?php

/**
  Template Name: online-application
*/

get_header();
?>

<section class="applynow-wrap">
  <div class="container">
    <h1 class="title_line">Admission 2019 - Apply Now</h1>
    <div class="tab-menu">
      <ul>
        <li class="active">
          <a data-toggle="tab">
            <div>
              <span>01</span>
              <h3>Course Details</h3>
            </div>
            <span class="progress"></span>
          </a>
        </li>
        <li>
          <a data-toggle="tab">
            <div>
              <span>02</span>
              <h3>Applicant Details</h3>
            </div>
            <span class="progress"></span>
          </a>
        </li>
        <li>
          <a data-toggle="tab">
            <div>
              <span>03</span>
              <h3>Contact Details</h3>
            </div>
            <span class="progress"></span>
          </a>
        </li>
        <li>
          <a data-toggle="tab">
            <div>
              <span>04</span>
              <h3>Academic details</h3>
            </div>
            <span class="progress"></span>
          </a>
        </li>
      </ul>
    </div>
    <form class="webform" id="online-application-form">
      <div class="tab-content-wrap clearfix">
        <div class="tab-content fade in active" id="menu1">
          <div class="col-xs-6">
            <div class="form-input-wrap">
              <div class="form-group select_degree">
                <label for="ugselect">Select Degree</label>
                <select id="ugselect" onchange="getbranch(this.value);" required="required">
                  <option disabled selected>Select</option>
                  <option value="ug">UG Programe</option>
                  <option value="pg">PG Programe</option>
                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_file">
                <input type="file" id="fileselect" accept="image/*">
                <label for="fileselect"><span><svg>
                      <use xlink:href="#uploadimg"></use>
                    </svg></span>upload image</label>
                <p>No file choosen</p>
                <span class="apply-form-error-msg">Please fill this field.</span>
                <input type="hidden" id="fileselect_hidden" name="fileselect_hidden">
              </div>
              <div class="form-group select_course_type">
                <div class="checkbox-group" required="required">
                  <input type="radio" id="typereg" name="studtype" value="Regular" />
                  <label for="typereg">Regular</label>
                </div>
                <div class="checkbox-group">
                  <input type="radio" id="typelat" name="studtype" value="Lateral" />
                  <label for="typelat">Lateral</label>
                </div>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_year">
                <label for="ugselect">Select Year</label>
                <select id="year-select" required="required">
                  <option disabled selected>Select</option>
                  <option value="Iyear">I Year</option>
                  <option value="IIyear">II Year</option>

                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_branch">
                <label for="ugselect">Select Branch</label>
                <select id="course_select" required="required">
                  <option disabled selected>Select</option>
                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
            </div>
          </div>
          <div class="clearfix">
            <div class="col-sm-6">
              <div class="form-input-wrap nextprevwrap">
                <a class="btn_web col-xs-6 next-button" data-toggle="tab" href="#" id="menu1-next-button">Next</a>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-content fade" id="menu2">
          <div class="col-xs-6">
            <div class="form-input-wrap">
              <h2 class="inner_sub_hd">Personal Information</h2>
              <div class="form-group aplicants_name">
                <label for="nametext">Applicant Name (As In HSC Mark Sheet)</label>
                <input type="text" id="nametext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="row multiple-form-group">
                <label>Date of birth</label>
                <div class="form-group col-xs-4 dob_date">
                  <select required="required" id="dob_date">
                    <option disabled selected>DD</option>
                    <?php
                                              for($i=1; $i<32; $i++)
                                                echo "<option value='$i'>".$i."</option>";
                                             ?>
                  </select>
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="form-group col-xs-4 dob_month">
                  <select required="required" id="dob_month">
                    <option disabled selected>MM</option>
                    <option value="1">JAN</option>
                    <option value="2">FEB</option>
                    <option value="3">MAR</option>
                    <option value="4">APR</option>
                    <option value="5">MAY</option>
                    <option value="6">JUN</option>
                    <option value="7">JUL</option>
                    <option value="8">AUG</option>
                    <option value="9">SEP</option>
                    <option value="10">OCT</option>
                    <option value="11">NOV</option>
                    <option value="12">DEC</option>
                  </select>
                  <span class="apply-form-error-msg">Please fill this field.</span>

                </div>
                <div class="form-group col-xs-4 dob_year">
                  <select required="required" id="dob_year">
                    <option disabled selected>YYYY</option>
                    <?php
                                              for($i=1980; $i<2006; $i++)
                                                echo "<option value='$i'>".$i."</option>";
                                             ?>
                  </select>
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-6 age">
                  <label for="agetext">Age</label>
                  <input type="text" id="agetext" onkeypress="return isNumber(event)">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="form-group col-xs-6 blood_group">
                  <label for="bloodgselect">Blood Group</label>
                  <select id="bloodgselect" required="required">
                    <option disabled selected>Select</option>
                    <option>A+</option>
                    <option>A-</option>
                    <option>B+</option>
                    <option>B-</option>
                    <option>AB+</option>
                    <option>AB-</option>
                    <option>O+</option>
                    <option>O-</option>
                  </select>
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
              </div>
              <div class="form-group select_mother_toung">
                <label for="mothtongselect">Mother Tounge</label>
                <select id="mothtongselect" required="required">
                  <option disabled selected>Select</option>
                  <option value="Tamil">Tamil</option>
                  <option value="Telgu">Telgu</option>
                  <option value="Hindi">Hindi</option>
                  <option value="Malayalam">Malayalam</option>
                  <option value="Kannada">Kannada</option>
                  <option value="Gujrati">Gujrati</option>
                  <option value="Punjabi">Punjabi</option>
                  <option value="Bengali">Bengali</option>
                  <option value="Others">Others</option>
                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_religion">
                <label for="relegselect">Religion</label>
                <select id="relegselect" required="required">
                  <option disabled selected>Select</option>
                  <option value="Hindu">Hindu</option>
                  <option value="Christian">Christian</option>
                  <option value="Muslims">Muslims</option>
                  <option value="Others">Others</option>

                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_cast">
                <label for="castselect">Cast</label>
                <input type="text" id="castselect">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_comminity">
                <label for="communityselect">Community</label>
                <select id="communityselect" required="required">
                  <option disabled selected>Select</option>
                  <option value="General">General</option>
                  <option value="BC">BC</option>
                  <option value="OC">OC</option>
                  <option value="BCM">BCM</option>
                  <option value="MBC">MBC</option>
                  <option value="SC">SC</option>
                  <option value="ST">ST</option>
                  <option value="SCA">SCA</option>
                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group select_reserved_category">
                <label for="rescatselect">Reserved Category</label>
                <select id="rescatselect">
                  <option disabled selected>Select</option>
                  <option value="Ex-service Man">Ex-service Man</option>
                  <option value="Sports">Sports</option>
                  <option value="None">None</option>
                </select>
              </div>
              <div class="form-group physically_handicapped">
                <label>Physically Handicaped?</label>
                <div class="checkbox-group">
                  <input type="radio" id="typehandiyes" name="handicaped" value="Yes" />
                  <label for="typehandiyes">Yes</label>
                </div>
                <div class="checkbox-group">
                  <input type="radio" id="typehandino" name="handicaped" value="No">
                  <label for="typehandino">No</label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-input-wrap">
              <h2 class="inner_sub_hd">Parent/Guardian Details</h2>
              <div class="form-group father_name">
                <label for="fathsnametext">Name of Father/Guardian</label>
                <input type="text" id="fathsnametext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group father_occupation">
                <label for="fathsoccuptext">Father's Occupation</label>
                <input type="text" id="fathsoccuptext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group father_income">
                <label for="fathsinctext">Father's Income</label>
                <input type="text" id="fathsinctext" onkeypress="return isNumber(event)">
              </div>
              <div class="form-group mother_name">
                <label for="mothsnametext">Name of Mother</label>
                <input type="text" id="mothsnametext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group mother_occupation">
                <label for="mothsoccuptext">Mother's Occupation</label>
                <input type="text" id="mothsoccuptext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group mother_income">
                <label for="mothsinctext">Mother's Income</label>
                <input type="text" id="mothsinctext" onkeypress="return isNumber(event)">
              </div>
              <div class="form-group guarian_name">
                <label for="guardnamtext">Name of Guardian</label>
                <input type="text" id="guardnamtext">
              </div>
              <div class="form-group guarian_occupation">
                <label for="guardoccuptext">Guardian's Occupation</label>
                <input type="text" id="guardoccuptext">
              </div>
              <div class="form-group guarian_income">
                <label for="guardinctext">Guardian's Income</label>
                <input type="text" id="guardinctext" onkeypress="return isNumber(event)">
              </div>
            </div>
          </div>
          <div class="clearfix">
            <div class="col-xs-6">
              <div class="form-input-wrap nextprevwrap">
                <a href="javascrip:void(0);" class="btn_web" id="menu2-prev-button">Previous</a>
                <a class="btn_web" data-toggle="tab" href="#" id="menu2-next-button">Next</a>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-content fade" id="menu3">
          <div class="col-xs-6">
            <div class="form-input-wrap">
              <h2 class="inner_sub_hd">Applicant/Student Address</h2>
              <div class="form-group nationtext">
                <label for="nationtext">Nationality</label>
                <input type="text" id="nationtext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group native_place">
                <label for="placetext">Native Place</label>
                <input type="text" id="native_place" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group civicstattext">
                <label for="civicstattext">Civic Status of Native Place</label>
                <input type="text" id="civicstattext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group stateselect">
                <label for="stateselect">State</label>
                <select id="stateselect" required="required">
                  <option disabled selected>Select</option>
                  <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                  <option value="Andhra Pradesh">Andhra Pradesh</option>
                  <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                  <option value="Assam">Assam</option>
                  <option value="Bihar">Bihar</option>
                  <option value="Chandigarh">Chandigarh</option>
                  <option value="Chhattisgarh">Chhattisgarh</option>
                  <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                  <option value="Daman and Diu">Daman and Diu</option>
                  <option value="Delhi">Delhi</option>
                  <option value="Goa">Goa</option>
                  <option value="Gujarat">Gujarat</option>
                  <option value="Haryana">Haryana</option>
                  <option value="Himachal Pradesh">Himachal Pradesh</option>
                  <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                  <option value="Jharkhand">Jharkhand</option>
                  <option value="Karnataka">Karnataka</option>
                  <option value="Kerala">Kerala</option>
                  <option value="Lakshadweep">Lakshadweep</option>
                  <option value="Madhya Pradesh">Madhya Pradesh</option>
                  <option value="Maharashtra">Maharashtra</option>
                  <option value="Manipur">Manipur</option>
                  <option value="Meghalaya">Meghalaya</option>
                  <option value="Mizoram">Mizoram</option>
                  <option value="Nagaland">Nagaland</option>
                  <option value="Orissa">Orissa</option>
                  <option value="Pondicherry">Pondicherry</option>
                  <option value="Punjab">Punjab</option>
                  <option value="Rajasthan">Rajasthan</option>
                  <option value="Sikkim">Sikkim</option>
                  <option value="Tamil Nadu">Tamil Nadu</option>
                  <option value="Tripura">Tripura</option>
                  <option value="Uttaranchal">Uttaranchal</option>
                  <option value="Uttar Pradesh">Uttar Pradesh</option>
                  <option value="West Bengal">West Bengal</option>
                </select>
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group phonetext">
                <label for="phonetext">Phone</label>
                <input type="text" id="phonetext" required="required" onkeypress="return isNumber(event)">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
              <div class="form-group emailtext">
                <label for="emailtext">Email</label>
                <input type="text" id="emailtext" required="required">
                <span class="apply-form-error-msg">Please fill this field.</span>
              </div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-input-wrap">
              <h2 class="inner_sub_hd">Parent/Guardian Address</h2>
              <div class="permanent-address">
                <div class="form-group perma_addrline1text">
                  <label for="addrline1text">Address Line 1</label>
                  <input type="text" id="addrline1text" required="required">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="form-group perma_addrline2text">
                  <label for="addrline2text">Address Line 2</label>
                  <input type="text" id="addrline2text" required="required">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="form-group perma_citytext">
                  <label for="citytext">City</label>
                  <input type="text" id="citytext" required="required">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="row">
                  <div class="form-group col-xs-6 perma_pincodetext">
                    <label for="citytext">Pincode</label>
                    <input type="text" id="pincodetext" required="required" onkeypress="return isNumber(event)">
                    <span class="apply-form-error-msg">Please fill this field.</span>
                  </div>
                  <div class="form-group col-xs-6 perma_addressphonetext">
                    <label for="citytext">Phone</label>
                    <input type="text" id="addressphonetext" required="required" onkeypress="return isNumber(event)">
                    <span class="apply-form-error-msg">Please fill this field.</span>
                  </div>
                </div>
              </div>

              <div class="form-group communication_adress">
                <label>Use Same for Future Communication?</label>
                <div class="checkbox-group">
                  <input type="radio" id="sameaddrfutyes" name="sameaddrfut" required="required" value="yes" />
                  <label for="sameaddrfutyes">Yes</label>
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="checkbox-group">
                  <input type="radio" id="sameaddrfutno" name="sameaddrfut" required="required" value="no" />
                  <label for="sameaddrfutno">No</label>
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
              </div>
              <div class="different-address">
                <div class="form-group diff_addrline1text">
                  <label for="addrline1text">Address Line 1</label>
                  <input type="text" id="addrline1text" required="required">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="form-group diff_addrline2text">
                  <label for="addrline2text">Address Line 2</label>
                  <input type="text" id="addrline2text" required="required">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="form-group diff_citytext">
                  <label for="citytext">City</label>
                  <input type="text" id="citytext" required="required">
                  <span class="apply-form-error-msg">Please fill this field.</span>
                </div>
                <div class="row">
                  <div class="form-group col-xs-6 diff_pincodetext">
                    <label for="citytext">Pincode</label>
                    <input type="text" id="pincodetext" required="required" onkeypress="return isNumber(event)">
                    <span class="apply-form-error-msg">Please fill this field.</span>
                  </div>
                  <div class="form-group col-xs-6 diff_addressphonetext">
                    <label for="citytext">Phone</label>
                    <input type="text" id="addressphonetext" required="required" onkeypress="return isNumber(event)">
                    <span class="apply-form-error-msg">Please fill this field.</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix">
            <div class="col-xs-6">
              <div class="form-input-wrap nextprevwrap">
                <a href="javascrip:void(0);" class="btn_web" id="menu3-prev-button">Previous</a>
                <a class="btn_web" data-toggle="tab" href="#" id="menu3-next-button">Next</a>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-content fade" id="menu4">
          <div class="academic-content">
            <div class="academic-intro">
              <div class="row">
                <div class="form-group availconcess">
                  <p class="col-lg-6">
                    Whether availing First Graduate Concession. If yes,certificate from Revenue
                    Department is to be enclosed (Goverment Quota Candidates only).

                  </p>
                  <div class="col-xs-6">
                    <div class="checkbox-group">
                      <input type="radio" id="availconcessyes" name="availconcess" required="required" value="yes" />
                      <label for="availconcessyes">Yes</label>
                    </div>
                    <div class="checkbox-group">
                      <input type="radio" id="availconcessno" name="availconcess" required="required" value="no" />
                      <label for="availconcessno">No</label>
                    </div>
                    <span class="apply-form-error-msg">Please fill this field.</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-8">
                  <div class="row">
                    <div class="form-group col-sm-6 tcnumtext">
                      <label for="tcnumtext">(i) T.C. No</label>
                      <input type="text" id="tcnumtext" required="required">
                      <span class="apply-form-error-msg">Please fill this field.</span>
                    </div>
                    <div class="col-sm-6">
                      <div class="row multiple-form-group">
                        <label>(ii) T.C Date</label>
                        <div class="form-group col-xs-4 tc_date">
                          <select required="required" id="tc_date">
                            <?php
                                                              for($i=1; $i<=31; $i++)
                                                                echo "<option value='$i'>".$i."</option>";
                                                             ?>
                          </select>
                          <span class="apply-form-error-msg">Please fill this field.</span>
                        </div>
                        <div class="form-group col-xs-4 tc_month">
                          <select required="required" id="tc_month">
                            <option disabled selected>MMM</option>
                            <option value="1">JAN</option>
                            <option value="2">FEB</option>
                            <option value="3">MAR</option>
                            <option value="4">APR</option>
                            <option value="5">MAY</option>
                            <option value="6">JUN</option>
                            <option value="7">JUL</option>
                            <option value="8">AUG</option>
                            <option value="9">SEP</option>
                            <option value="10">OCT</option>
                            <option value="11">NOV</option>
                            <option value="12">DEC</option>
                          </select>
                          <span class="apply-form-error-msg">Please fill this field.</span>
                        </div>
                        <div class="form-group col-xs-4 tc_year">
                          <select required="required" id="tc_year">
                            <?php
                                                              for($i=1999; $i<=2005; $i++)
                                                                echo "<option value='$i'>".$i."</option>";
                                                             ?>
                          </select>
                          </select>
                          <span class="apply-form-error-msg">Please fill this field.</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-sm-6 qualifyexamtext">
                      <label for="qualifyexamtext">Qualifying of Examination</label>
                      <input type="text" id="qualifyexamtext" required="required">
                      <span class="apply-form-error-msg">Please fill this field.</span>
                    </div>
                    <div class="form-group col-sm-6 instrmedtext">
                      <label for="instrmedtext">Medium of Instruction</label>
                      <input type="text" id="instrmedtext" required="required">
                      <span class="apply-form-error-msg">Please fill this field.</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="academic-table">
              <div class="acadtab-head">
                <h3>academic details of (x std & hsc / equivalent)</h3>
              </div>
              <table>
                <thead>
                  <tr>
                    <th>
                      <h4>Class</h4>
                    </th>
                    <th width="15%">
                      <h4>Year of Passing</h4>
                    </th>
                    <th width="15%">
                      <h4>% of Marks</h4>
                    </th>
                    <th width="13%">
                      <h4>Class</h4>
                    </th>
                    <th width="35%">
                      <h4>Name of the School/Polytechnic</h4>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <h4>X Std. /Matric</h4>
                    </td>
                    <td>
                      <div class="form-group xm11">
                        <input type="text" id="xm11">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm12">
                        <input type="text" id="xm12">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm13">
                        <input type="text" id="xm13">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm14">
                        <input type="text" id="xm14">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>X Std. / Intermediate</h4>
                    </td>
                    <td>
                      <div class="form-group xm21">
                        <input type="text" id="xm21">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm22">
                        <input type="text" id="xm22">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm23">
                        <input type="text" id="xm23">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm24">
                        <input type="text" id="xm24">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>Diploma</h4>
                    </td>
                    <td>
                      <div class="form-group xm31">
                        <input type="text" id="xm31">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm32">
                        <input type="text" id="xm32">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm33">
                        <input type="text" id="xm33">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm34">
                        <input type="text" id="xm34">
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="academic-table">
              <div class="acadtab-head">
                <h3>HSC (aCADEMIC OR EQUIVALENT)</h3>
              </div>
              <div class="col-md-7 input-wrap">
                <div class="form-group col-xs-6 boardnametext">
                  <label for="boardnametext">Name of Board</label>
                  <input type="text" id="boardnametext">
                </div>
                <div class="form-group col-xs-6 regnumtext">
                  <label for="regnumtext">Register Number</label>
                  <input type="text" id="regnumtext">
                </div>
              </div>
              <table>
                <thead>
                  <tr>
                    <th>
                      <h4>Subjects</h4>
                    </th>
                    <th width="20%">
                      <h4>Month and Year of Passing</h4>
                    </th>
                    <th width="20%">
                      <h4>Marks Obtained</h4>
                    </th>
                    <th width="20%">
                      <h4>Maxm2mum Marks</h4>
                    </th>
                    <th width="20%">
                      <h4>Percentage</h4>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <h4>Language</h4>
                    </td>
                    <td>
                      <div class="form-group xm41">
                        <input type="text" id="xm41">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm42">
                        <input type="text" id="xm42">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm43">
                        <input type="text" id="xm43">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm44">
                        <input type="text" id="xm44">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>English</h4>
                    </td>
                    <td>
                      <div class="form-group xm51">
                        <input type="text" id="xm51">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm52">
                        <input type="text" id="xm52">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm53">
                        <input type="text" id="xm53">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm54">
                        <input type="text" id="xm54">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>Physics</h4>
                    </td>
                    <td>
                      <div class="form-group xm61">
                        <input type="text" id="xm61">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm62">
                        <input type="text" id="xm62">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm63">
                        <input type="text" id="xm63">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm64">
                        <input type="text" id="xm64">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>Chemistry</h4>
                    </td>
                    <td>
                      <div class="form-group xm71">
                        <input type="text" id="xm71">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm72">
                        <input type="text" id="xm72">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm73">
                        <input type="text" id="xm73">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm74">
                        <input type="text" id="xm74">
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>Mathematics</h4>
                    </td>
                    <td>
                      <div class="form-group xm81">
                        <input type="text" id="xm81">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm82">
                        <input type="text" id="xm82">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm83">
                        <input type="text" id="xm83">
                      </div>
                    </td>
                    <td>
                      <div class="form-group xm84">
                        <input type="text" id="xm84">
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="academic-table-bottom">
              <p>Eligible percentage (Maths+Chemistry+Physcis)</p>
              <div class="form-group eligible_percentage">
                <input type="text" id="eligible_percentage">
              </div>
            </div>
            <div class="clearfix">
              <div class="clearfix form-input-wrap nextprevwrap academic-table-bottom">
                <a href="javascrip:void(0);" class="btn_web" id="menu4-prev-button">Previous</a>
                <a class="btn_web" data-toggle="tab" href="#" id="form-submit-button">Submit</a>
                <svg class="gen_svg-loader">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#uploadimg"></use>
                </svg>
              </div>

            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>



<?php
get_footer();
