<?php

/**
  Template Name: placed-students-details
*/


get_header();
?>


<section>
  <div class="content-only">
    <div class="console-div container">
      <div class="custom-table">
        <h2 class="title_line"><?php the_field('placed_students_detail_main_title');?></h2>
        <h3><?php the_field('placed_students_batch_csc');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_cse_table');?>
        </div>
      </div>

      <div class="custom-table">
        <h3><?php the_field('placed_students_batch_it');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_it_table');?>
        </div>
      </div>

      <div class="custom-table">
        <h3><?php the_field('placed_students_batch_ece');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_batch_ece_table');?>
        </div>
      </div>

      <div class="custom-table">
        <h3><?php the_field('placed_students_batch_eee');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_batch_eee_table');?>
        </div>
      </div>

      <div class="custom-table">
        <h3><?php the_field('placed_students_batch_mech');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_batch_mech_table');?>
        </div>
      </div>

      <div class="custom-table">
        <h3><?php the_field('placed_students_batch_civil');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_batch_civil_table');?>
        </div>
      </div>


      <div class="custom-table">
        <h3><?php the_field('placed_students_batch_mba');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_batch__mba_table');?>
        </div>
      </div>


      <div class="console-log">
        <h3><?php the_field('placed_students_batch_mca');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_batch__mca_table');?>
        </div>
      </div>
    </div>
    <br>
    <br>
    <div class="container content-only">
      <div>
        <h2 class="title_line"><?php the_field('placed_studentsmain_title_17–18');?></h2>
        <h3><?php the_field('placed_students_batch_csc__2017–18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_cse_2017-2018_table');?>
        </div>
      </div>
    </div>

    <div>
      <div class="container content-only">
        <h3><?php the_field('placed_students_batch_it_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_it_table_17-18');?>
        </div>
      </div>
    </div>

    <div>
      <div class="container content-only">
        <h3><?php the_field('placed_students_batch_ece_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_ece_table_17-18');?>
        </div>
      </div>
    </div>

    <div>
      <div class="container content-only">
       <h3><?php the_field('placed_students_batch_eee_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_eee_table_17-18');?>
        </div>
      </div>
    </div>

    <div>
      <div class="container content-only">
       <h3><?php the_field('placed_students_batch_mech_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_mech_table_17-18');?>
        </div>
      </div>
    </div>
    
    <div>
      <div class="container content-only">
       <h3><?php the_field('placed_students_batch_civil_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_civil_table_17-18');?>
        </div>
      </div>
    </div>

    
    <div>
      <div class="container content-only">
       <h3><?php the_field('placed_students_batch_mca_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_mca_table_17-18');?>
        </div>
      </div>
    </div>

    
    <div>
      <div class="container content-only">
       <h3><?php the_field('placed_students_batch_mba_17-18');?></h3>
        <div style="overflow-x: auto;">
          <?php the_field('placed_students_mba_table_17-18');?>
        </div>
      </div>
    </div>
  </div>

</section>

<?php
get_footer();
