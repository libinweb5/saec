<?php

/**
  Template Name: placements
*/


get_header();
?>

<section class="placements_training">
  <div class="container">
    <div class="placements_training_lft">
      <h2><?php the_field('students_placed_title');?></h2>
      <div class="placed_count">
        <div>
          <span><?php the_field('students_placed_count');?><sup>+</sup></span>
          <h2><?php the_field('students_placed_sub_title');?></h2>
          <label><?php the_field('students_placed_till_date');?></label>
        </div>
      </div>
    </div>
    <div class="placements_training_rgt">
      <h2 class="inner_sub_hd"><?php the_field('expectation_commitment_title');?></h2>
      <p> <?php the_field('corporat-commitment_details');?></p>
    </div>
  </div>
  <div class="placement_bg_img"></div>
</section>



<section class="our_recruiters">
  <div class="container">


    <div class="our_recruiters_sldr">
      <h2 class="title_line"><?php the_field('our_recruiters_title');?></h2>
      <div class="board_navslide"></div>
      <div class="our_recruiters_slide owl-carousel">

        <?php if( have_rows('our_recruiters') ): $i = 0; ?>
        <?php while( have_rows('our_recruiters') ): the_row(); ?>
        <?php if($i%4 == 0) { ?>
        <div class="recruiters_box">
          <ul>
            <?php } ?>
            <li>
              <div><img src="<?php the_sub_field('our_recruiters_logo');?>" alt="<?php the_sub_field('our_recruiters_alt');?>"></div>
            </li>
            <?php if($i%4 == 3) { ?>
          </ul>
        </div>
        <?php } ?>
        <?php $i++; endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="plcmnt_ofc">
      <h2 class="inner_sub_hd"><?php the_field('placement_office_title');?></h2>
      <p><?php the_field('placement_office_details');?></p>
    </div>
    <div class="plcmnt_ofc">
      <h2 class="inner_sub_hd"><?php the_field('training_development_title');?></h2>
      <p><?php the_field('training_and_development_details');?></p>
    </div>
  </div>
</section>

<section class="major_fnctin clearfix">
  <div class="container">
    <h2 class="title_line"><?php the_field('major_functions_title');?></h2>
    <ul class="tic_list">
      <?php if( have_rows('major_functions') ): $i = 1; ?>
      <?php while( have_rows('major_functions') ): the_row(); ?>
      <li><?php the_sub_field('major_functions_list');?></li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>
  </div>
</section>

<section class="strngth_plcmnt_ofc">
  <div class="container">
    <div class="plcmnt_ofc_sec">
      <h2 class="inner_sub_hd"><?php the_field('strength_of_placement_office_title');?></h2>
      <p><?php the_field('strength_of_placement_office_sub_title');?></p>
      <ul class="tic_list">
        <?php if( have_rows('strength_of_placement_office') ): $i = 1; ?>
        <?php while( have_rows('strength_of_placement_office') ): the_row(); ?>
        <li><?php the_sub_field('strength_list');?></li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>

    <div class="indstry_inst">
      <h2 class="inner_sub_hd"><?php the_field('interaction_title');?></h2>
      <p><?php the_field('interaction_sub_title');?></p>
      <ul class="tic_list">
        <?php if( have_rows('industry_institution_interaction') ): $i = 1; ?>
        <?php while( have_rows('industry_institution_interaction') ): the_row(); ?>
        <li><?php the_sub_field('industry_institution_list');?></li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>

    </div>
    <div class="external_links course_ext-links">
      <label>Also check: </label>
      <ul class="share_icons clearfix">

        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>

  </div>
</section>

<?php include('counter.php');?>

<?php
get_footer();
