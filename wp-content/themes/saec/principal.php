<?php

/**
  Template Name: principal
*/


get_header();
?>


<div class="resource_inner principal_sec">
  <section class="hostl_facility">
    <div class="container">
      <div class="row">

        <div class="col-sm-6 principal_sec_lft">
          <div class="hostel_rgt_img">
            <?php the_post_thumbnail()?>
          </div>
        </div>
        <div class="col-sm-6 principal_sec_rgt">
          <div>
            <h3 class="title_line"><?php the_field('main_title');?></h3>
            <h1><?php the_field('principal_name');?></h1>
            <h3><?php the_field('detail_content');?></h3>
          </div>
        </div>
      </div>
    </div>
  </section>


  <div class="course-tab-contain">
    <div class="container">
      <div class="tab-menu">
        <ul>
          <li class="active"><a data-toggle="tab" href="#menu1">Professional Summary</a></li>
          <li><a data-toggle="tab" href="#menu2">Area of Interest</a></li>
          <li><a data-toggle="tab" href="#menu3">Academic Qualification</a></li>
          <li><a data-toggle="tab" href="#menu4">Professional Activities</a></li>
        </ul>
      </div>

      <div class="tab-content-wrap clearfix">
        <div class="tab-content fade active in" id="menu1">
          <div class="professional-summary_content">
            <?php the_field('professional_summary_content');?>
          </div>

        </div>
        <div class="tab-content fade" id="menu2">
          <ul class="tic_list">
            <?php if( have_rows('area_Interest_contents') ): ?>
            <?php while( have_rows('area_Interest_contents') ): the_row(); ?>
            <li class="">
              <?php the_sub_field('area_of_interest_list'); ?>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>
        <div class="tab-content fade" id="menu3">
          <ul class="tic_list">

            <?php if( have_rows('academic_qualification') ): ?>
            <?php while( have_rows('academic_qualification') ): the_row(); ?>
            <li class="">
              <?php the_sub_field('academic_qualification_list'); ?>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>
        <div class="tab-content fade" id="menu4">
          <!--<h3>Membership</h3>-->
          <ul class="tic_list">
            <?php if( have_rows('professional_activities') ): ?>
            <?php while( have_rows('professional_activities') ): the_row(); ?>
            <li class="">
              <?php the_sub_field('professional_activities_content'); ?>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>

        </div>
      </div>

    </div>
  </div>

</div>

<?php
get_footer();
