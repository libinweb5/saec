<?php

/**
  Template Name: research
*/


get_header();
?>

<!--
    <section class="research-section">
      <div class="container content-only">
        <h1 class="title_line"><?php the_title();?></h1>
        <div>
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
                endwhile; else: ?>
          <p>Sorry, no posts matched your criteria.</p>
          <?php endif; ?>
        </div>
      </div>
      </section>
-->


<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1">Research Centre</a></li>
        <li><a data-toggle="tab" href="#menu2">Anna University Recognized Supervisors</a></li>
        <li><a data-toggle="tab" href="#menu3">Research Scholars</a></li>
        <li><a data-toggle="tab" href="#menu4">PhD Awarded</a></li>
        <li><a data-toggle="tab" href="#menu5">Funded Research</a></li>
        <li><a data-toggle="tab" href="#menu6">Publications</a></li>
        <li><a data-toggle="tab" href="#menu7">Awards</a></li>
        <li><a data-toggle="tab" href="#menu8">Institution Innovation Cell</a></li>
        <li><a data-toggle="tab" href="#menu9">Events </a></li>
      </ul>
    </div>
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">

        <section>
          <div class="container content-only">
            <h1 class="title_line">research centre</h1>
            <div>
              <?php the_field('research_centre');?>
            </div>
          </div>
        </section>
      </div>

      <div class="tab-content fade" id="menu2">
        <div>
          <?php the_field('recognized_supervisors');?>
        </div>
      </div>

      <div class="tab-content fade" id="menu3">
        <div>
          <?php the_field('research_scholars');?>
        </div>
      </div>


      <div class="tab-content fade" id="menu4">
        <div>
          <?php the_field('phd_awarded');?>
        </div>
      </div>


      <div class="tab-content fade" id="menu5">
        <div>
          <?php the_field('funded_research');?>
        </div>

      </div>


      <div class="tab-content fade" id="menu6">

        <div class="tab-menu">
          <ul>
            <li class="active"><a data-toggle="tab" href="#menu61">Code of Ethics </a></li>
            <li><a data-toggle="tab" href="#menu62">Journals</a></li>
            <li><a data-toggle="tab" href="#menu63">Conferences</a></li>
            <li><a data-toggle="tab" href="#menu64">Patents</a></li>

          </ul>
        </div>
        <div class="tab-content-wrap clearfix">
          <div class="tab-content fade in active" id="menu61">
            1
          </div>
          <div class="tab-content fade " id="menu62">
        
            <div style=" overflow: auto;">
              <?php
                $file = get_field( 'journals' );
                if ( $file ) {
                    if ( ( $handle = fopen( $file["url"], "r" ) ) !== FALSE ) {
                  ?>
                <table class="csvupload" width="100%" border="1" cellspacing="0">
                <?php
                  $i = 0;
                 while ( ( $data_row = fgetcsv( $handle, 1000, "," ) ) !== FALSE ) {
                      $class ="";
                      if($i==0) {
                         $class = "header";
                      }
                      ?>
                <tr>
                  <td class="<?php echo $class; ?>"><?php echo $data_row[0]; ?></td>
                  <td width="240" class="<?php echo $class; ?>"><?php echo $data_row[1]; ?></td>
                  <td width="240" class="<?php echo $class; ?>"><?php echo $data_row[2]; ?></td>
                  <td width="240" class="<?php echo $class; ?>"><?php echo $data_row[3]; ?></td>
                  <td width="240" class="<?php echo $class; ?>"><?php echo $data_row[4]; ?></td>
                  <td width="240" class="<?php echo $class; ?>"><?php echo $data_row[5]; ?></td>
                </tr>
                <?php
                      $i ++;
                  }
                  fclose($fp);
                  ?>
              </table>
              <?php
                  $response = array("type" => "success", "message" => "CSV is converted to HTML successfully");
                  } else {
                      $response = array("type" => "error", "message" => "Unable to process CSV");
                  }
              }
              ?>
            </div>
            
            <?php if(!empty($response)) { ?>
            <div class="response <?php echo $response["type"]; ?>">
              <?php echo $response["message"]; ?>
            </div>
            <?php } ?>



          </div>
          <div class="tab-content fade" id="menu63">
            3
          </div>
          <div class="tab-content fade " id="menu64">
            <div>
              <?php the_field('patents');?>
            </div>
          </div>
        </div>



      </div>


      <div class="tab-content fade" id="menu7">
        <div>
          <?php the_field('awards');?>
        </div>
      </div>
      <div class="tab-content fade" id="menu8">
        <div class="tab-menu">
          <ul>
            <li class="active"><a data-toggle="tab" href="#menu81"> Innovation Cell </a></li>
            <li><a data-toggle="tab" href="#menu82"> Centre of Excellence</a></li>
          </ul>
        </div>
        <div class="tab-content-wrap clearfix">
          <div class="tab-content fade in active" id="menu81">
            <div class=content-only innvocationcell-content>
              <div>
                <?php the_field('innovation_cell');?>
              </div>
            </div>
          </div>
          <div class="tab-content fade " id="menu82">
            <div class=content-only coe-content>
              <div>
                <?php the_field('centre_of_excellence');?>
              </div>
              <!--
             
              <h2>CENTER FOR EXCELLENCE IN ENERGY AND NANO TECHNOLOGY</h2>
              <p>CEENT is a platform to explore the intensified domain and potential hidden behind the ample and abundant resources in the arena of sustainable and green energy. We clog together as a brain behind to discover the possibilities of harnessing and harvesting the alternate energy through technologies like solar desalination, solar thermal, green vehicles and green fuels, in lieu of meeting the future clean energy demands.</p>
              <h4>SUPPORT STRUCTURES & PERKY PERFORMANCES</h4>
              <ul class="tic_list">
                <li>Synthesizing nanocoolants for Automobile Heat Reduction..</li>
                <li>Set up of Bio fuel Production Unit.</li>
                <li>Green Vehicle / Hybrid Vehicle Fabrication Unit.</li>
                <li>Hybrid Energy Harvester Unit [ Solar Desalination & Solar Trough-Collector ]</li>
                <li>The membership validity is life time for faculty and 4 years / 3 years as per duration of the course for the students.</li>
                <li>Nanofluid Performance Analysis Setup</li>
                <li>Research Consultant provided related to industrial problems.</li>

              </ul>
              <h4>STARDOME OUTCOMES</h4>
              <div>

                <table>
                  <tbody>
                    <tr>
                      <td colspan="2" style="text-align: center;" width="52">
                        <h4>INTERNATIONAL COLLABORATIONS</h4>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Dr. El-Sayed El-Agouz</td>
                      <td style="text-align: center;" width="240">Tanta University, Egypt</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Dr.AmimulAhsan</td>
                      <td style="text-align: center;" width="240">University Putra Malaysia, Malaysia</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Dr.A.E.Kabeel</td>
                      <td style="text-align: center;" width="240">Tanta University, Egypt</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Dr.SomchaiWongwises</td>
                      <td style="text-align: center;" width="240">King Mongkut's University of Technology, Thailand</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Dr.RizalmanMamat</td>
                      <td style="text-align: center;" width="240">University of Malaysia, Malaysia</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Dr.OmidMahian</td>
                      <td style="text-align: center;" width="240">Ferdowsi University of Mashhad, Iran</td>
                    </tr>
                    </tboby>
                </table>
              </div>

              <a class="fancybox" href="img/research/img1.png">
                <img src="https://saec.ac.in/img/research/img1.png" class="img-responsive">
              </a>
              <h4>List of Products developed:</h4>
              <ul class="tic_list">
                <li>Conventional and stepped solar still with different nanofluids</li>
                <li>Inclined solar panel basin solar still in passive and active mode</li>
                <li>Solar parabolic trough collector using nano fluids</li>
                <li>Heat transfer enhancement of twisted tape inserts using TiO2-Water Nano fluids in micro finned tube</li>
                <li>Photo voltaic panel submerged and partially submerged in water</li>
                <li>Desalination using FPC and SPC</li>
                <li>Single slope solar still with low cost energy storage material.</li>
                <li>Solar still using jute cloth knitted with sand heat energy storage</li>
                <li>Biodiesel from<i> Apismellifera</i></li>
                <li>Biodiesel from mutton suet</li>

              </ul>
              <h2>CENTRE FOR EXCELLENCE IN ENERGY AND NANO TECHNOLOGY<br>(SAEC – FUNDED PROJECTS)</h2>
              <h4>1. Solar E-Bike</h4>
              <a class="fancybox" href="img/research/img2.png">
                <img src="https://saec.ac.in/img/research/img2.png" class="img-responsive">
              </a>
              <p>The solar energy is consumed and stored in the battery with the help of ac-dc converter. The alert system in the controller helps the battery from over charging. Due to this process the power wastage gets prevented and also the battery life is improved. In this process, even though the vehicle is on running condition. The charging is done simultaneously and this can be controlled with help of dongle switch. Since we are using 150 watt solar panel it requires 2 hours for the battery to get fully charged.This bike can also be charged with the help of electric charger hence it is suitable for any climatic condition.</p>

              <h4>2. Hybrid Vehicle Challenge-16</h4>
              <h4>3. ISIE Electric Solar Vehicle Championship</h4>
              <a class="fancybox" href="img/research/img3.png">
                <img src="https://saec.ac.in/img/research/img3.png" class="img-responsive">
              </a>
              <a class="fancybox" href="img/research/img4.png">
                <img src="https://saec.ac.in/img/research/img4.png" class="img-responsive">
              </a>
              <h4>4. PVT active solar Still (Inclined) / Solar still with FPC</h4>
              <a class="fancybox" href="img/research/img5.png">
                <img src="https://saec.ac.in/img/research/img5.png" class="img-responsive">
              </a>
              <p>Schematic diagram and experimental setup of an inclined solar panel basin solar still in passive and active mode is shown in Fig. 1 and Fig. 2. It consists of a basin made of PV panel mounted at an inclined position. The dimension of solar still is 1365mm× 670mm× 150 mm. The solar still and collector cover was fabricated using 4mm thickness transparent glass. The ISPB still was fully sealed by silicon paste to avoid the vapor losses from still basin to the surroundings. Experiments were carried out by two different conditions i) inclined solar panel basin still in passive mode ii) inclined solar panel basin still in active mode.</p>
              <a class="fancybox" href="img/research/img6.png">
                <img src="https://saec.ac.in/img/research/img6.png" class="img-responsive">
              </a>
              <h4>5. Biodiesel From Apismellifera</h4>
              <a class="fancybox" href="img/research/img7.png">
                <img src="https://saec.ac.in/img/research/img7.png" class="img-responsive">
              </a>
              <p>The setup consist of a flat bottomed flask with two opening mouths mounted on a hot plate magnetic stirrer . One of the mouth is connected with condenser unit whereas another mouth is closed with a single holed rubber stopper. Thermometer used for measuring the temperature of the flask is inserted into this setup through the means of the hole in rubber stopper. Initially, the mixture of potassium hydroxide and methanol is added and is mixed well using the magnetic pellet. After mixing, the fat/oil is added to the mixture and is then continuously stirred. Once the mixing stats, the temperature in the hot plate is turned on to 60oC and the stirring speed is set to 350 rpm. The maximum capacity of the mixture decides the hot plate magnetic stirrer and should not overload the stirrer machine. The water supply to the water inlet of the condenser is turned on and can be reused after getting collected at the water outlet. This condensation tube avoids the evaporation of the methanol from the flask and makes it available throughout the reaction. The temperature is noted periodically using the thermometer and must be maintained between 50-70oC in order to avoid over heating of oil/fat and also prevent the evaporation of the methanol. Once the time duration of the reaction is completed, the hot plate must be turned off and the water supply to the water jacket of the condenser must be turned off. The condenser setup must be removed carefully and the stirring speed must be reduced gradually. Once the reaction is stopped, the mixture must be immediately transferred into separating funnel for settling and separation of biodiesel & glycerol.</p>
              <h4>6. Wheel Chair for Physically Challenged</h4>
              <p>The main problem in this project is to find different part to the right material for the paraplegic wheelchair to fit the different targets group, to solve that it was required to find out an appropriate power source, battery size and many elements. The reason for the design is to benefit all kind of the paraplegic people for their daily life. Make them as normal people as possible. The promotion of the production should also take into considering. The project started by brainstorming and trying to figure out the most right one to fit the most target group.</p>
              <a class="fancybox" href="img/research/img8.png">
                <img src="https://saec.ac.in/img/research/img8.png" class="img-responsive">
              </a>
              <a class="fancybox" href="img/research/img9.png">
                <img src="https://saec.ac.in/img/research/img9.png" class="img-responsive">
              </a>
              <p>The project was carried out successfully according to the project plan. The outcome of the hydraulic scissors lift design meets the objective of the project. As a result, the project designed the electro-hydraulic parallelogram lift. The general section described the classification, purpose and technical characteristics of the lift, and the mechanism and operation principle of the designed lift. In the design section, the lift calculation is done, where the forces acting in the cylinder and emerging stresses in the system were calculated. A 3D model was created. </p>
              <h4>7. Low Cost 3D Printing Machine</h4>
              <p>The present invention generally relates to the advanced manufacturing processes. More specifically, the invention describes about the design and fabrication of an Innovative Desktop 3D Printing Machine working on the principle of Fused Deposition Modelling (FDM). The fabricated 3D Printing Machine belongs to the Desktop category and operates in normal electrical power supply. The machine is basically developed with the objective of printing Components & parts using ABS as well as PLA Plastics. </p>
              <a class="fancybox" href="img/research/img10.png">
                <img src="https://saec.ac.in/img/research/img10.png" class="img-responsive">
              </a>
              <a class="fancybox" href="img/research/img11.png">
                <img src="https://saec.ac.in/img/research/img11.png" class="img-responsive">
              </a>
              <a class="fancybox" href="img/research/img12.png">
                <img src="https://saec.ac.in/img/research/img12.png" class="img-responsive">
              </a>
              <h2>CENTRE FOR SMART AND ADVANCED MANUFACTURING(CSAM)</h2>
              <p><b>CSAM</b> is a state of art technology lab dedicated for the field of advanced manufacturing with machines capable of handling smart and advanced materials. We put our best efforts in developing manufacturing techniques for materials which gain special attention in the industry. Friction stir welding, mist machining, 3D printing are the priced assets of our Centre</p>
              <h4>Facilities Available</h4>
              <ul class="tic_list">
                <li>Friction Stir Welding machine</li>
                <li>Mist Machining Setup</li>
                <li>3D Printer</li>
              </ul>
              <h4>PRODUCT OUTCOMES</h4>
              <div>

                <table>
                  <tbody>
                    <tr>
                      <td colspan="2" style="text-align: center;" width="52">
                        <h4>PRODUCTS DEVELOPED</h4>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Solar E-Bike</td>

                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Wheel Chair for physically challenged</td>

                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Anti-theft fuel monitoring system</td>

                    </tr>
                    <tr>
                      <td style="text-align: center;" width="240">Low cost 3D printing machine</td>

                    </tr>

                    </tboby>
                </table>
              </div>

              <a class="fancybox" href="img/research/img13.png">
                <img src="https://saec.ac.in/img/research/img13.png" class="img-responsive">
              </a>
              <a class="fancybox" href="img/research/img14.png">
                <img src="https://saec.ac.in/img/research/img14.png" class="img-responsive">
              </a>
              <h2>SPARK AUTOMATION CENTRE</h2>
              <p>“We train to make Automate”</p>
              <p>In the world everything is to be automated in order to increase the productivity and save the time. In major Industries like Automobile Industries, Chemical Industries, Food Processing Industries and Moulding machines is to be fully automated or semi automated. Spark Automation Centre providing technical solutions to overcome the drawback or to upgrade the operating performance of the machines. In our Laboratory we are teaching the current scenario in Industries, how the machines are operating and how they do retrofit.</p>
              <p>This centre which helps to provide Technical Training to the students about Industrial Automation which includes Basics of Industrial Automation, Sensors, Instrumentation and Mechatronics,Switchgears, Sensors and TransducersInstrumentation, Programmable Logic Controllers (PLC), Human Machine Interface (HMI), Smart Positioning using Servo motor and VFDs, Barcode Automation systems, Communication systems, Pneumatics, Vision Machine System, Electrical panel wirings</p>
              <p>Students can learn about from the basics to advanced level of Industrial Automation and also it will be act as a bridge between academic careers to Industry level. Everyone can practice with individually with each and every Industrial component in our laboratory. Students can also do their own innovative ideas into real time.</p>
              <p>We have planned to conduct the Training Program into two modes. One is Three days Value Added Program and the other one is Regular Training Program. From the Value Added Program students thinking level can change into real time. The Regular Training Program it takes next level of Industrial Automation.</p>
              <p>Apart from the Training program, we executed Industrial Projects like Automatic Bike washing system, Eye drop Bottle Inspection system using Image Processing, Remote Machine status Monitoring and Leak Testing Machines, etc.,</p>
              <p>Our working areas are Special Purpose Machines (SPM’S), PLC & HMI programming, Inspection stations using Image Processing, Assembly stations, Pick and place robots, Electrical Control Panel wiring and Retro-fit. And we also contribute to Hybrid Vehicle Championship (HVC) and Electrical Solar Vehicle Championship (ESVC). </p>
              <h2>SAEC-CISCO ACADEMY</h2>
              <p>The CISCO networking academy of the department was started in the year 2014 through fund received from AICTE under MODROBS scheme by Dr.E.A.Mary Anita, Prof/CSE. The Academy conducts CCNA certification course for both faculty and students. The academy has its exclusive lab named CISCO lab with routers, switches and all networking equipment to enable the learners with hands-on experience. In the year 2018, CISCO has conducted Learn-A-Thon on cyber security and 319 students of our college have participated and earned a digital badge. The Academy was awarded with 5-Years active participation award by CISCO.</p>
              
              
              
              
-->
            </div>

          </div>
        </div>


      </div>
      <div class="tab-content fade" id="menu9">

        <div class="tab-menu">
          <ul>
            <li class="active"><a data-toggle="tab" href="#menu91"> List of events </a></li>
            <li><a data-toggle="tab" href="#menu92"> Event Gallery</a></li>


          </ul>
        </div>
        <div class="tab-content-wrap clearfix">
          <div class="tab-content fade in active" id="menu91">

          </div>
          <div class="tab-content fade " id="menu92">

          </div>
        </div>
      </div>




    </div>
  </div>
</div>

<?php
get_footer();
