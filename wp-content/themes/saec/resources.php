<?php

/**
  Template Name: resources
*/


get_header();
?>


<section class="inner res_intro">
  <div class="container">
    <h1 class="title_line">Resources</h1>
  </div>
</section>

<section class="nopadding res_course-wrap">
 <?php $loop = new WP_Query( array( 'post_type' => 'resource') ); ?>
     <?php if( $loop->have_posts() ) :  ?>
     <?php $i = 0; ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      
  <div class="<?php echo ($i%2 == 0) ? "course-pg": "course-ug"; ?> clearfix">
    <div class="container">
      
      <div class="img-contain col-md-6 col-xs-5 nopadding">
        <?php the_post_thumbnail(); ?>
      </div>
      <div class="text-contain col-sm-6">
        <div>
          <h2><?php the_title()?></h2>
          <p><?php the_content()?></p>
          <a href="<?php the_permalink(); ?>" class="btn_web">Learn More</a>
        </div>
      </div>
    </div>
  </div>
  <?php $i++; endwhile; wp_reset_query(); ?>
    <?php endif; ?>
</section>




<?php
get_footer();
