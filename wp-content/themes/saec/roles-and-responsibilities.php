<?php

/**
  Template Name: roles-and-responsibilities
*/


get_header();
?>

<section class="responsibilities_sec">
  <div class="container">
    <h1 class="title_line">Roles And Responsibilities</h1>
    <div>
      <h3><?php the_field('role_title_principal');?></h3>
      <ul>
        <?php if( have_rows('principal_responsibilities') ): ?>
        <?php while( have_rows('principal_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_hod');?></h3>
      <ul>
        <?php if( have_rows('hod_responsibilities') ): ?>
        <?php while( have_rows('hod_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('responsibilities_list_hod'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>

    </div>
    <div>
      <h3><?php the_field('role_title_administrative_officer');?></h3>
      <ul>
        <?php if( have_rows('administrative_officer_responsibilities') ): ?>
        <?php while( have_rows('administrative_officer_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('responsibilities_list_administrative_officer'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_accounts_officer');?></h3>
      <ul>
        <?php if( have_rows('accounts_officer_responsibilities') ): ?>
        <?php while( have_rows('accounts_officer_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('responsibilities_list_accounts_officer'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_placement_officer');?></h3>
      <ul>
        <?php if( have_rows('placement_officer_responsibilities') ): ?>
        <?php while( have_rows('placement_officer_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('responsibilities_list_placement_officer'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_system_manager');?></h3>
      <ul>
        <?php if( have_rows('system_manager_responsibilities') ): ?>
        <?php while( have_rows('system_manager_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('system_manager_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_librarians');?></h3>
      <ul>
        <?php if( have_rows('librarians_responsibilities') ): ?>
        <?php while( have_rows('librarians_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('librarians_responsibilities_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_physical_education_director');?></h3>
      <ul>
        <?php if( have_rows('physical_education_director_responsibilities') ): ?>
        <?php while( have_rows('physical_education_director_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('physical_education_director_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_workshop_superintendent');?></h3>
      <ul>
        <?php if( have_rows('workshop_superintendent_responsibilities') ): ?>
        <?php while( have_rows('workshop_superintendent_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('responsibilities_list_workshop_superintendent'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>

    </div>
    <div>
      <h3><?php the_field('role_title_transport_manager');?></h3>
      <ul>
        <?php if( have_rows('transport_manager_responsibilities') ): ?>
        <?php while( have_rows('transport_manager_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('transport_manager_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_hostel_warden');?></h3>
      <ul>
        <?php if( have_rows('hostel_warden_responsibilities') ): ?>
        <?php while( have_rows('hostel_warden_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('hostel_warden_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_estate_officer');?></h3>
      <ul>
        <?php if( have_rows('estate_officer_responsibilities') ): ?>
        <?php while( have_rows('estate_officer_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('estate_officer_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_store_incharge');?></h3>
      <ul>
        <?php if( have_rows('store_incharge_responsibilities') ): ?>
        <?php while( have_rows('store_incharge_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('store_incharge_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_security_officer');?></h3>
      <ul>
        <?php if( have_rows('security_officer_responsibilities') ): ?>
        <?php while( have_rows('security_officer_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('security_officer_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_technical_staff');?></h3>
      <ul>
        <?php if( have_rows('technical_staff_responsibilities') ): ?>
        <?php while( have_rows('technical_staff_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('technical_staff_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_supporting_staff');?></h3>
      <ul>
        <?php if( have_rows('supporting_staff_responsibilities') ): ?>
        <?php while( have_rows('supporting_staff_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('technical_staff_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_electrician_plumber');?></h3>
      <ul>
        <?php if( have_rows('electrician_plumber_responsibilities') ): ?>
        <?php while( have_rows('electrician_plumber_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('electrician_plumber_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_attenders');?></h3>
      <ul>
        <?php if( have_rows('attenders_responsibilities') ): ?>
        <?php while( have_rows('attenders_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('attenders_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_site_engineers');?></h3>
      <ul>
        <?php if( have_rows('site_engineers_responsibilities') ): ?>
        <?php while( have_rows('site_engineers_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('site_engineers_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_office_staff');?></h3>
      <ul>
        <?php if( have_rows('office_staff_responsibilities') ): ?>
        <?php while( have_rows('office_staff_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('office_staff_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    <div>
      <h3><?php the_field('role_title_canteen&_guest_house');?></h3>
      <ul>
        <?php if( have_rows('canteen&_guest_house_responsibilities') ): ?>
        <?php while( have_rows('canteen&_guest_house_responsibilities') ): the_row(); ?>
        <li class="">
          <?php the_sub_field('canteen&_guest_house_responsibilities_list'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</section>

<?php
get_footer();
