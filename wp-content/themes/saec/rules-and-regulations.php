<?php

/**
  Template Name: rules-and-regulations
*/

get_header();
?>

<section class="responsibilities_sec">
  <div class="container">
    <h1><?php the_title(); ?></h1>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
    the_content();
    endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
  </div>
</section>
<?php
get_footer();
