<?php

/**
  Template Name: service-rules
*/


get_header();
?>

<section>
  <div class="container content-only">
    <h1 class="title_line">Service conduct rules</h1>

    <div>
      <?php the_field('service-rules');?>
    </div>

  </div>
</section>
<?php
get_footer();
