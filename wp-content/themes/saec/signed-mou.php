<?php

/**
  Template Name: signed-mou
*/


get_header();
?>

<section>
  <div class="container content-only">
    <div><?php the_field('signed-mou');?></div>
  </div>
</section>

<?php
get_footer();
