<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<section class="single-campus-wrap">
  <div class="container content-only">
    <h1 class="title_line"><?php the_title(); ?></h1>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
      the_content();
      endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>

    <h3><?php the_field('sports_title');?></h3>
    <p><?php the_field('sports_content');?></p>

    <h3><?php the_field('achievements_title');?></h3>
    <ul class="tic_list">
      <?php if( have_rows('achievements') ): ?>
      <?php while( have_rows('achievements') ): the_row(); ?>
      <li><?php the_sub_field('achievements_list');?></li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>

    <!--
    <div class="table-wrap">
      <h3><strong><?php the_field('activities_2014-15');?></strong></h3>
      <div><?php the_field('curricular_activities');?></div>
      <h3><strong><?php the_field('activities_2013-14');?></strong></h3>
      <div><?php the_field('curricular_activities_2013-14');?></div>
      <h3><strong><?php the_field('activities_2012-13');?></strong></h3>
      <div><?php the_field('curricular_activities_2012-13');?></div>
      <h3><strong><?php the_field('activities_2011-12');?></strong></h3>
      <div><?php the_field('curricular_activities_2011-12');?></div>
    </div>
-->
    <h3><strong>Women Empowerment Cell</strong></h3>
    <div class="course-tab-contain extra_curricular_act" style="background:none;padding:30px 0px;">
      <div class="tab-content-wrap" style="margin-top:0px;">
        <div class="tab-content" style="display:block; border-bottom:none;padding:0px;">
          <div>
            <div class="ach_sec clearfix">
              <?php if( have_rows('women_empowerment_cell') ): ?>
              <?php while( have_rows('women_empowerment_cell') ): the_row(); ?>
              <div class="col-sm-3">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('women_empowerment_cell_gallery');?>" data-fancybox-group="gallery1" title="<?php the_sub_field('women_empowerment_cell_image_title');?>">
                    <img src="<?php the_sub_field('women_empowerment_cell_gallery');?>" class="img-responsive">
                  </a>
                </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <ul class="tic_list article-wrap">
      <?php if( have_rows('articles') ): ?>
      <?php while( have_rows('articles') ): the_row(); ?>
      <li><a href="<?php the_sub_field('article_download');?>" target="_blank"><?php the_sub_field('article_name');?></a></li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>

    <h3><strong>NSS </strong></h3>
    <ul class="tic_list">
      <?php if( have_rows('nss') ): ?>
      <?php while( have_rows('nss') ): the_row(); ?>
      <li><a href="<?php the_sub_field('nss_download');?>" target="_blank"><?php the_sub_field('nss_name');?></a></li>
      <?php endwhile; ?>
      <?php endif; ?>
    </ul>


  </div>


</section>

<?php
get_footer();
