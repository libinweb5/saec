<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<section class="gallery_page clearfix">
  <div class="container container2">
    <div class="gallery_page_title">
      <h1><?php the_title()?></h1>
    </div>
    <div class="gallery_sec clearfix">
      <ul>


        <ul>
          <?php if( have_rows('gallery_section') ): ?>
          <?php while( have_rows('gallery_section') ): the_row(); ?>
          <li>
           <a href="">
             <img src="<?php the_sub_field('background_image'); ?>" class="img-responsive">
             <div class="gallery_sec_content inr_glry_cntnt">
              <p><?php the_sub_field('gallery_title'); ?></p>
            </div>
             <div class="hov_overlay"></div>
           </a>
          </li>
          <?php endwhile; ?>
          <?php endif; ?>
        </ul>

      </ul>
    </div>
  </div>

  <div class="container">
    <div class="external_links clearfix">
      <label>Also check: </label>
      <ul class="share_icons clearfix">
        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    
  </div>
</section>

  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 596);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 596);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 596);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 596);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 596);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 596);?></span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
