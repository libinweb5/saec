<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
<section>
  <div class="container content-only">
    <h1 class="title_line">NAAC SSR</h1>
    <?php if( have_rows('naac') ): ?>
    <?php while( have_rows('naac') ): the_row();?>
    <h3><a href="<?php the_sub_field('criteria_download');?>" target="_blank">
        <?php the_sub_field('criteria_title');?></a></h3>
    <?php endwhile; ?>
    <?php endif; ?>
  </div>
</section>

<?php
get_footer();
