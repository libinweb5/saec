<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>


<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"><?php _e('Vision & Mission') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"><?php _e('Faculty') ?></a></li>
        <li><a data-toggle="tab" href="#menu4"> <?php _e('Infrastructure') ?></a></li>
        <li><a data-toggle="tab" id="listmenu5" href="#menu5"> <?php _e('Department Activities') ?></a></li>
        <li><a data-toggle="tab" href="#menu6"> <?php _e('Student Activities ') ?></a></li>
        <li class="admission"><a data-toggle="tab" href="#menu7"> <?php _e('Admission') ?> </a></li>
      </ul>
    </div>
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <div>
          <?php the_field('mba_about');?>
        </div>
        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <h3 class="mba_h3"> <?php _e('DEPARTMENT HIGHLIGHTS') ?></h3>
        <div class="row">
          <?php if( have_rows('department_highlights') ): ?>
          <?php while( have_rows('department_highlights') ): the_row(); ?>
          <div class="col-sm-4 highlights">
            <div class="ach_sec clearfix">
              <a class="fancybox" href="<?php the_sub_field('highlights_img')?>" data-fancybox-group="gallery20" title="<?php the_sub_field('highlights_img_title')?>">
                <img src="<?php the_sub_field('highlights_img')?>" class="img-responsive">
              </a>
            </div>
            <div class="mba_span"><?php the_sub_field('highlights_content')?></div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="tab-content fade " id="menu2">
        <div>
          <?php the_field('vision_&_mission'); ?>
        </div>
        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <?php the_field('peo_po'); ?>
        </div>

      </div>
      <div class="tab-content fade" id="menu3">

        <div>
          <h3><?php _e('Head of Department') ?> </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>
          <div>
            <?php the_field('head_of_department_details');?>
          </div>
        </div>
        <div>
          <h3><?php _e('PROFESSOR') ?></h3>
          <div>
            <?php the_field('professor');?>
          </div>
        </div>

        <div>
          <h3><?php _e('ASSOCIATE PROFESSOR') ?></h3>
          <div>
            <?php the_field('associate_professor');?>
          </div>
        </div>

        <div>
          <h3><?php _e('ASSISTANT PROFESSOR') ?> </h3>
          <div>
            <?php the_field('assistant_professor');?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu4">
        <div>
          <div class="row">
            <?php if( have_rows('infrastructure') ): ?>
            <?php while( have_rows('infrastructure') ): the_row(); ?>
            <div class="col-sm-4">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('infrastructure_image');?>" data-fancybox-group="gallery1" title="<?php the_sub_field('infrastructure_image_title');?>">
                  <img src="<?php the_sub_field('infrastructure_image');?>" class="img-responsive">
                </a>
              </div>
              <span><?php the_sub_field('infrastructure_title');?></span>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="tab-content fade" id="menu5">
        <section class="board_trustes" id="board-trustes-mba">
          <div class="container">
            <div class="board_navslide"></div>
            <h2 class="title_line"><?php the_field('guest_lecture_title');?></h2>
            <div class="board_trustes_inner">
              <div class="board_trustes_slder_hh1 ">
                <?php if( have_rows('guest_lecture') ): ?>
                <?php while( have_rows('guest_lecture') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('guest_lecture_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('guest_lecture_slide_image_title');?>">
                      <img class="mba_gl" src="<?php the_sub_field('guest_lecture_slide_image');?>" class="img-responsive">
                    </a>
                  </div>

                  <div class="text mba"><?php the_sub_field('guest_lecture_slide_content');?></div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

            <h2 class="title_line"><?php the_field('industrial_visit_title');?></h2>
            <div class="board_trustes_inner">
              <div class="board_trustes_slder_hh1 ">
                <?php if( have_rows('industrial_visit') ): ?>
                <?php while( have_rows('industrial_visit') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('industrial_visit_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('industrial_visit_slide__title');?>">
                      <img class="mba_iv" src="<?php the_sub_field('industrial_visit_slide_image');?>" class="img-responsive">
                    </a>
                  </div>
                  <div class="text mba"><?php the_sub_field('industrial_visit_slide__content');?></div>
                  <div class="text">
                  </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>


            <h2 class="title_line"><?php the_field('workshop_title');?></h2>
            <div class="board_trustes_inner">
              <div class="board_trustes_slder_hh1 ">
                <?php if( have_rows('workshop_cours') ): ?>
                <?php while( have_rows('workshop_cours') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('workshop_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('workshop_img_title');?>">
                      <img class="mba_ws" src="<?php the_sub_field('workshop_image');?>" class="img-responsive">
                    </a>
                  </div>
                  <div class="text mba"><?php the_sub_field('workshop_content');?> </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

            <h2 class="title_line"><?php the_field('management_day_title');?></h2>
            <div class="board_trustes_inner">
              <div class="board_trustes_slder_hh1 ">
                <?php if( have_rows('management_day') ): ?>
                <?php while( have_rows('management_day') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('management_day_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('management_day_img_title');?>">
                      <img src="<?php the_sub_field('management_day_image');?>" class="img-responsive">
                    </a>
                  </div>
                  <div class="text mba"><?php the_sub_field('management_day_content');?> </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

            <h2 class="title_line"><?php the_field('conference_title');?></h2>
            <div class="board_trustes_inner">
              <div class="">
                <?php if( have_rows('conference') ): ?>
                <?php while( have_rows('conference') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('conference_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('conference_img_title');?>">
                      <img src="<?php the_sub_field('conference_image');?>" class="img-responsive">
                    </a>
                  </div>

                  <div class="text mba extra"><?php the_sub_field('conference_content');?>
                  </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

            <h2 class="title_line"><?php the_field('ideation_title');?></h2>
            <div class="board_trustes_inner">
              <div class="board_trustes_slder_hh1 ">
                <?php if( have_rows('ideation') ): ?>
                <?php while( have_rows('ideation') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('ideation_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('ideation_img_title');?>">
                      <img src="<?php the_sub_field('ideation_image');?>" class="img-responsive">
                    </a>
                  </div>
                  <div class="text mba "><?php the_sub_field('ideation_content');?>
                  </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

            <h2 class="title_line"><?php the_field('eventia_title');?></h2>
            <div class="board_trustes_inner">
              <div class=" ">
                <?php if( have_rows('eventia') ): ?>
                <?php while( have_rows('eventia') ): the_row(); ?>
                <div class="board_trustes_box">
                  <div>
                    <a href="<?php the_sub_field('eventia_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('eventia_img_title');?>">
                      <img src="<?php the_sub_field('eventia_image');?>" class="img-responsive">
                    </a>
                  </div>

                  <div class="text mba "><?php the_sub_field('eventia_content');?>
                  </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="tab-content fade" id="menu6">
        <div>
          <div>
            <?php the_field('professional_affiliations');?>
          </div>
          <div class="row">
            <?php if( have_rows('affiliations_images') ): ?>
            <?php while( have_rows('affiliations_images') ): the_row(); ?>
            <div class="col-sm-4 student_activity">
              <div class="ach_sec clearfix img_prof">
                <a class="fancybox" href="<?php the_sub_field('affiliations_image');?>" data-fancybox-group="gallery1" title="<?php the_sub_field('affiliations_image_title');?>">
                  <img src="<?php the_sub_field('affiliations_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>


        <div>
          <h3 class="tab6_h3">ROTARACT CLUB</h3>
          <div class="row">
            <?php if( have_rows('rotaract_club') ): ?>
            <?php while( have_rows('rotaract_club') ): the_row(); ?>
            <div class="col-sm-4 student_activity">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('rotaract_club_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('rotaract_club_image');?>" class="img-responsive">
                </a>
              </div>
              <div class="span_val"><?php the_sub_field('rotaract_club_content');?></div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h3 class="tab6_h3">VALUE ADDED COURSE</h3>
          <div class="row">
            <?php if( have_rows('value_added_course') ): ?>
            <?php while( have_rows('value_added_course') ): the_row(); ?>
            <div class="col-sm-4 student_activity">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('value_added_course_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('value_added_course_image');?>" class="img-responsive">
                </a>
              </div>
              <div class="mba_span"><?php the_sub_field('value_added_course_content');?>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <div>
            <?php the_field('incubation_data');?>
          </div>
          <div class="row">
            <?php if( have_rows('incubation') ): ?>
            <?php while( have_rows('incubation') ): the_row(); ?>
            <div class="col-sm-4 student_activity ">
              <div class="ach_sec clearfix">

                <a class="fancybox" href="<?php the_sub_field('incubation_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('incubation_image');?>" class="img-responsive">
                </a>
              </div>
              <div class="mba_span"> <?php the_sub_field('Incubation_content');?></div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>


        <div>
          <h3 class="tab6_h3">EXTRA CURRICULAR</h3>
          <div class="row">
            <?php if( have_rows('extra_curricular') ): ?>
            <?php while( have_rows('extra_curricular') ): the_row(); ?>
            <div class="col-sm-4 student_activity">
              <div class="ach_sec clearfix">

                <a class="fancybox" href="<?php the_sub_field('extra_curricular_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('extra_curricular_image');?>" class="img-responsive">
                </a>
              </div>
              <div class="mba_span"><?php the_sub_field('extra_curricular_content');?></div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade " id="menu7">
        <img src="<?php the_field('addmission');?>" class="img-responsive">
      </div>
    </div>
    <div class="external_links course_ext-links" style="padding: 20px 0px;">
      <label>Also check: </label>
      <ul class="share_icons clearfix">
        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</div>

<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
