<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>


<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About Course') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"><?php _e('Department') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"><?php _e('Academics') ?></a></li>
        <li><a data-toggle="tab" href="#menu4"><?php _e('Activities') ?></a></li>
      </ul>
    </div>
    
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <div>
          <?php the_field('our_vision'); ?>
        </div>
        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <?php the_field('our_mission_&_details'); ?>
        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div>
          <h3><a href="<?php the_field('course_material_and_videos'); ?>" target="_blank">NPTEL Course material and videos</a></h3>
        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div>
          <h3><?php _e('Subject') ?> </h3>
          <table>
            <thead>
              <tr>
                <th><?php _e('SUBJECT NAME') ?>  </th>
                <th><?php _e('VIDEO HOMEPAGE LINK') ?>  </th>
              </tr>
            </thead>
            <tbody>

              <?php if( have_rows('subject') ): ?>
              <?php while( have_rows('subject') ): the_row(); ?>
              <tr>
                <td><?php the_sub_field('subject_name'); ?></td>
                <td><a href="<?php the_sub_field('video_homepage_link'); ?>" target="_blank"><?php the_sub_field('video_homepage_link_text'); ?></a></td>
              </tr>
              <?php endwhile; ?>
              <?php endif; ?>

            </tbody>
          </table>
        </div>

        <div>
          <?php the_field('academics_details'); ?>
        </div>

      </div>
      <div class="tab-content fade" id="menu4">
        <p><span><a href="pdf/me2016-17.pdf" target="_blank">Some best Post Graduate Projects of Academic Year : 2016 - 2017</a></span><br></p>
        <p><span><a href="pdf/me2014-15.pdf" target="_blank">Some best Post Graduate Projects of Academic Year : 2014 - 2015</a></span><br></p>
        <p><span><a href="pdf/me2015-16.pdf" target="_blank">Some best Post Graduate Projects of Academic Year : 2015 - 2016</a></span><br></p>

      </div>
      <div class="external_links course_ext-links">
        <label>Also check: </label>
        <ul class="share_icons clearfix">

          <?php if( have_rows('external_links') ): ?>
          <?php while( have_rows('external_links') ): the_row(); ?>
          <li>
            <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
          </li>
          <?php endwhile; ?>
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
