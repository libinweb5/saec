<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>


<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About Course') ?></a></li>
      </ul>
    </div>
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <p> <?php the_field('about_course');?></p>
        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('about_course_img') ): ?>
            <?php while( have_rows('about_course_img') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('course_img'); ?>" data-fancybox-group="communication2">
                  <img src="<?php the_sub_field('course_img'); ?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <p>
           <?php the_field('about_course_details');?>
          </p>
        </div>

      </div>
    </div>

  </div>
</div>

<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
