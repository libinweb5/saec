<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<div class="resource_inner">
  <section class="hostl_facility">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 rigt_aln">
          <div class="hostel_rgt_img">
            <img src="<?php the_field('resources_inner_image');?>" class="img-responsive">
          </div>
        </div>
        <div class="col-sm-6 lft_aln">
          <div>
            <span>
              01
            </span>
            <h1><?php the_title()?></h1>
            <h3 class="inner_sub_hd"><?php the_field('resources_inner_title');?></h3>
            <p><?php the_field('resources_inner_details');?></p>
          </div>
        </div>

      </div>
    </div>
  </section>
  <?php if( $post->ID == 796) { ?>
  <section class="hostl_sec4">
    <div class="container">
      <div class="course-tab-contain " style="padding-top:0px">
        <div class="tab-content-wrap" style="margin-top:0px;">
          <div class="tab-content" style="display:block; border-bottom:none;">
            <div>
              <?php if( have_rows('resource_gallery') ): ?>
              <div class="ach_sec clearfix">
                <?php while( have_rows('resource_gallery') ): the_row(); ?>
                <div class="col-sm-3">
                  <div>
                    <a class="fancybox" href="<?php the_sub_field('resource_gallery_image'); ?>" data-fancybox-group="gallery1" title="<?php the_sub_field('resource_gallery_image_title'); ?>">
                      <img src="<?php the_sub_field('resource_gallery_image'); ?>" class="img-responsive">
                    </a>
                  </div>
                </div>
                <?php endwhile; ?>
              </div>
              <?php endif; ?>
            </div>

            <!--  BUS ROUTES  -->

            <div class="bus routes-wrap">
              <h3 class="title_line"><?php the_field('bus_routes_title')?></h3>
              <table style="height: 429px;" border="0" width="835">
                <tbody>
                  <?php 
                  $i=1;
                  if( have_rows('bus_routes') ): ?>
                  <tr>
                    <?php while( have_rows('bus_routes') ): the_row(); ?>

                    <td width="145">
                      <div align="center"><a href="<?php the_sub_field('bus_routes_list_pdf'); ?>" target="_blank"><?php the_sub_field('bus_routes_list'); ?> </a></div>
                    </td>
                    <?php if($i % 4==0) echo '</tr><tr>' ?>

                    <?php
                  $i++;
                  endwhile; ?>
                    <?php endif; ?>

                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>

      <!--  LIBRARY RULES -->

      <?php if( have_rows('library_rules') ): ?>
      <h3 class="title_line fdsfdsf"><?php the_field('library_rules_main_title')?>:</h3>
      <span class="sb_hd"><?php the_field('working_hours_title')?> :</span>
      <p><?php the_field('working_hours_content')?></p>
      <ul class="tic_list">
        <?php while( have_rows('library_rules') ): the_row(); ?>
        <li>
          <?php the_sub_field('rules'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </section>
  <section class="hostl_sec4 cntrl_library">
    <div class="container">

      <!--Information and Reference Center-->

      <div class="cntrl_library_inner">
        <?php if( have_rows('information_content_list') ): ?>
        <h3><?php the_field('information_content_main_title'); ?></h3>
        <p><?php the_field('information_content_main_content'); ?></p>
        <h4><?php the_field('information_sub_title'); ?></h4>
        <ul class="tic_list">

          <?php while( have_rows('information_content_list') ): the_row(); ?>
          <li>
            <?php the_sub_field('contents'); ?>
          </li>
          <?php endwhile; ?>
        </ul>
        <?php endif; ?>
      </div>

      <!-- Special features of central library-->

      <div class="cntrl_library_inner">
        <?php if( have_rows('special_features') ): ?>
        <h3 class="inner_sub_hd"><?php the_field('special_features_title');?></h3>
        <ul class="tic_list">

          <?php while( have_rows('special_features') ): the_row(); ?>
          <li>
            <?php the_sub_field('special_features_list'); ?>
          </li>
          <?php endwhile; ?>

        </ul>
        <?php endif; ?>
      </div>

      <!-- Computer Infrastructure -->

      <div class="cntrl_library_inner">
        <?php if( have_rows('computer_infrastructure') ): ?>
        <h3 class="inner_sub_hd"><?php the_field('computer_infrastructure_title');?></h3>
        <ul class="tic_list">

          <?php while( have_rows('computer_infrastructure') ): the_row(); ?>
          <li>
            <?php the_sub_field('computer_infrastructure_list'); ?>
          </li>
          <?php endwhile; ?>

        </ul>
        <?php endif; ?>
      </div>

      <!-- Library Resources-->

      <div class="cntrl_library_inner">
        <?php if( have_rows('library_resources') ): ?>
        <h4><?php the_field('library_resources_title');?></h4>
        <table>
          <tbody>

            <?php while( have_rows('library_resources') ): the_row(); ?>
            <tr>
              <td><?php the_sub_field('number_title'); ?> </td>
              <td><?php the_sub_field('number_count'); ?></td>
            </tr>
            <?php endwhile; ?>

          </tbody>
        </table>
        <?php endif; ?>
      </div>

      <!-- Library Services-->

      <div class="cntrl_library_inner">
        <?php if( have_rows('library_services') ): ?>
        <h4><?php the_field('library_services_title');?></h4>
        <ul class="tic_list">

          <?php while( have_rows('library_services') ): the_row(); ?>
          <li>
            <?php the_sub_field('library_services_list'); ?>
          </li>
          <?php endwhile; ?>

        </ul>
        <?php endif; ?>
      </div>

      <!-- Institutional Membership -->

      <div class="cntrl_library_inner">
        <?php if( have_rows('institutional_membership') ): ?>
        <h4><?php the_field('institutional_membership_title');?></h4>
        <ul class="tic_list">

          <?php while( have_rows('institutional_membership') ): the_row(); ?>
          <li>
            <?php the_sub_field('institutional_membership_list'); ?>
          </li>
          <?php endwhile; ?>

        </ul>
        <?php endif; ?>
      </div>

      <!-- BORROWING ELIGIBILITY -->

      <div class="cntrl_library_inner">
        <?php if( have_rows('borrowing_eligibility') ): ?>
        <h4><?php the_field('borrowing_eligibility_title');?></h4>
        <table>
          <tbody>

            <?php while( have_rows('borrowing_eligibility') ): the_row(); ?>
            <tr>
              <td><?php the_sub_field('eligibility_list'); ?> </td>
              <td><?php the_sub_field('eligibility_block'); ?></td>
            </tr>
            <?php endwhile; ?>

          </tbody>
        </table>
        <?php endif; ?>
      </div>

      <!-- WORKING HOURS  -->

      <div class="cntrl_library_inner">
        <?php if( have_rows('working_hours') ): ?>
        <h4><?php the_field('hours_title');?></h4>
        <table>
          <tbody>

            <?php while( have_rows('working_hours') ): the_row(); ?>
            <tr>
              <td><?php the_sub_field('working_days'); ?> </td>
              <td><?php the_sub_field('working_time'); ?></td>
            </tr>
            <?php endwhile; ?>

          </tbody>
        </table>
        <?php endif; ?>
      </div>

      <!-- LIBRARY E – RESOURCES  -->

      <div class="cntrl_library_inner">
        <?php if( have_rows('library_e_resources') ): ?>
        <h4><?php the_field('library_e_resources_title');?></h4>
        <table>
          <tbody>

            <?php while( have_rows('library_e_resources') ): the_row(); ?>
            <tr>
              <td><?php the_sub_field('library_resources_list'); ?> </td>
              <td><?php the_sub_field('resources_num'); ?></td>
            </tr>
            <?php endwhile; ?>

          </tbody>
        </table>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <?php }
  ?>

  <?php if( $post->ID == 800) { ?>
  <section class="hostl_sec2 clearfix">
    <div class="container">
      <div class="col-sm-5">
        <div class="hostl_sec2_left">
          <img src="<?php the_field('room_image'); ?>" class="img-responsive">
        </div>
      </div>
      <div class="col-sm-7">
        <div>
          <h3 class="inner_sub_hd"><?php the_field('room_caption_title'); ?></h3>
          <p><?php the_field('room_caption_details'); ?></p>
        </div>
      </div>
    </div>
  </section>
  <section class="hostl_sec3">
    <div class="hostl_sec3_content">
      <div class="container">
        <h3 class="inner_sub_hd"><?php the_field('dining_health_care_title'); ?></h3>
        <p><?php the_field('dining_health_care_content'); ?></p>
      </div>
    </div>

    <div class="hostl_sec3_hlth">
      <div class="container">
        <div class="hostl_sec3_hlth_box">
          <h3 class="title_line"><?php the_field('gymnasium_title'); ?></h3>
          <p><?php the_field('gymnasium_content'); ?></p>
        </div>
      </div>
      <div class="hostl_sec3_img"><?php //the_field('gymnasium_image'); ?></div>
    </div>
  </section>
  <section class="hostl_sec4">
    <div class="container">
      <h3 class="title_line"><?php the_field('rules_regulations_title'); ?></h3>
      <p><?php the_field('rules_regulations_sub_title'); ?></p>
      <?php if( have_rows('rules_regulations') ): ?>
      <ul class="tic_list">
        <?php while( have_rows('rules_regulations') ): the_row(); ?>
        <li>
          <?php the_sub_field('rules_regulations_list'); ?>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
    </div>
  </section>
  <?php }
  ?>

 <?php if( $post->ID == 798) { ?>
<section class="hostl_sec4">
    <div class="container">
      <div class="course-tab-contain " style="padding-top:0px">
        <div class="tab-content-wrap" style="margin-top:0px;">
          <div class="tab-content" style="display:block; border-bottom:none;">
            <div>
              <?php if( have_rows('resource_gallery') ): ?>
              <div class="ach_sec clearfix">
                <?php while( have_rows('resource_gallery') ): the_row(); ?>
                <div class="col-sm-3">
                  <div>
                    <a class="fancybox" href="<?php the_sub_field('resource_gallery_image'); ?>" data-fancybox-group="gallery1" title="<?php the_sub_field('resource_gallery_image_title'); ?>">
                      <img src="<?php the_sub_field('resource_gallery_image'); ?>" class="img-responsive">
                    </a>
                  </div>
                </div>
                <?php endwhile; ?>
              </div>
              <?php endif; ?>
            </div>

            <!--  BUS ROUTES  -->

            <div class="bus routes-wrap">
              <h3 class="title_line"><?php the_field('bus_routes_title')?></h3>
              <table style="height: 429px;" border="0" width="835">
                <tbody>
                  <?php 
                  $i=1;
                  if( have_rows('bus_routes') ): ?>
                  <tr>
                    <?php while( have_rows('bus_routes') ): the_row(); ?>

                    <td width="145">
                      <div align="center"><a href="<?php the_sub_field('bus_routes_list_pdf'); ?>" target="_blank"><?php the_sub_field('bus_routes_list'); ?> </a></div>
                    </td>
                    <?php if($i % 4==0) echo '</tr><tr>' ?>

                    <?php
                  $i++;
                  endwhile; ?>
                    <?php endif; ?>

                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>

      <!--  LIBRARY RULES -->

      <?php if( have_rows('library_rules') ): ?>
      <h3 class="title_line fdsfdsf"><?php the_field('library_rules_main_title')?>:</h3>
      <span class="sb_hd"><?php the_field('working_hours_title')?> :</span>
      <p><?php the_field('working_hours_content')?></p>
      <ul class="tic_list">
        <?php while( have_rows('library_rules') ): the_row(); ?>
        <li>
          <?php the_sub_field('rules'); ?>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </section>
  <?php }
  ?>

</div>

<?php
get_footer();
