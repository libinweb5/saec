<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About Course') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"><?php _e('Department') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"><?php _e('Faculty') ?></a></li>
        <li><a data-toggle="tab" href="#menu4"> <?php _e('Gallery') ?></a></li>
        <li><a data-toggle="tab" href="#menu5"><?php _e('Activities') ?> </a></li>
        <li><a data-toggle="tab" id="listmenu6" href="#menu6"><?php _e('Department Activities') ?> </a></li>
        <li><a data-toggle="tab" href="#menu7"><?php _e('Infrastructure') ?></a></li>
        <li><a data-toggle="tab" href="#menu8"><?php _e('Vision & Mission') ?> </a></li>
      </ul>
    </div>
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <p><?php the_field('about_course');?></p>
        <div>
          <h3><strong>Genesis :</strong></h3>
          <ul class="tic_list genesis-wrap">
            <?php if( have_rows('genesis') ): ?>
            <?php while( have_rows('genesis') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('genesis_field'); ?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3><strong>Highlights :</strong></h3>
          <ul class="tic_list">
            <?php if( have_rows('highlights') ): ?>
            <?php while( have_rows('highlights') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('highlights_fields'); ?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <div class="row">
            <?php if( have_rows('awards') ): ?>
            <?php while( have_rows('awards') ): the_row(); ?>
            <div class="col-sm-4 custom-h">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('award_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('award_image_title');?>">
                  <img src="<?php the_sub_field('award_image');?>" class="img-responsive">
                </a>
              </div>
              <span><?php the_sub_field('award_label');?></span>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <div>
            <h3><strong>Intel Lab</strong></h3>
            <div class="row">
              <?php if( have_rows('intel_lab') ): ?>
              <?php while( have_rows('intel_lab') ): the_row(); ?>
              <div class="col-sm-4 custom-h">
                <div class="ach_sec clearfix">
                  <a class="fancybox" href="<?php the_sub_field('intel_lab_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('intel_lab_image_title');?>">
                    <img src="<?php the_sub_field('intel_lab_image');?>" class="img-responsive">
                  </a>
                </div>
                <span><?php the_sub_field('intel_lab_title');?></span>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <div class="course_feature">
            <?php if( have_rows('course_feature') ): ?>
            <?php while( have_rows('course_feature') ): the_row(); ?>
            <div class="feat-box">
              <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
              <i>
                <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
              </i>
              <div>
                <label><?php the_sub_field('feature_label'); ?></label>
                <p><?php the_sub_field('feature_sub_text'); ?></p>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <div class="courses-offered-wrap">
            <h3><strong>Courses Offered</strong></h3>
            <?php if( have_rows('courses_offered') ): ?>
            <?php while( have_rows('courses_offered') ): the_row(); ?>
            <p><?php the_sub_field('courses_offered_list');?></p>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <div>
            <h3><?php the_field('newsletter_title');?></h3>
            <?php if( have_rows('newsletter') ): ?>
            <?php while( have_rows('newsletter') ): the_row(); ?>
            <p>
              <a href="<?php the_sub_field('pdf'); ?>" target="_blank"><?php the_sub_field('newsletter_content'); ?></a>
            </p>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div>
          <p><?php the_field('department_description');?></p>
        </div>
        <br>
        <div>
          <h3>Student's Achievements:</h3>
          <h3>Other Events</h3>
          <div>
            <?php the_field('other_events');?>
          </div>
        </div>
        <div>
          <span>2017-2018</span>
          <div>
            <?php the_field('other_events2017-2018');?>
          </div>
        </div>
        <div>
          <span>Student's Publications</span>
          <div>
            <?php the_field('students_publications');?>
          </div>
        </div>
        <div>
          <h4>Sports</h4>
          <div>
            <?php the_field('sports');?>
          </div>
        </div>
        <div>
          <span>2017-2018</span>
          <div>
            <?php the_field('sports_2017-2018');?>
          </div>
        </div>
        <div class="list_of-projects">
          <h3>Research and Development</h3>
          <h4>BEST PROJECTS</h4>
          <span><a href="<?php the_field('list_of_best_projects');?>" target="_blank">List of best projects</a></span>
        </div>
        <div>
          <h4>BOOK PUBLICATIONS</h4>
          <div>
            <?php the_field('book_publications');?>
          </div>
        </div>
        <div>
          <h3>Sponsored Research – Completed</h3>
          <div>
            <?php the_field('sponsored_research');?>
          </div>
        </div>
        <div>
          <h3>Grants Received</h3>
          <?php if( have_rows('grants_received_new') ): ?>
          <?php while( have_rows('grants_received_new') ): the_row(); ?>
          <h4><?php the_sub_field('grants_received_years');?> </h4>
          <div>
            <p><?php the_sub_field('grants_receivedtable');?> </p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <h3>CONSULTANCY PROJECTS</h3>
          <?php if( have_rows('consultancy_projects') ): ?>
          <?php while( have_rows('consultancy_projects') ): the_row(); ?>
          <h4><strong><u><?php the_sub_field('consultancy_projects_years');?></u></strong></h4>
          <div>
            <p><?php the_sub_field('consultancy_projects_table');?> </p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <span>List of staff publications in 2016-2017.</span>
          <?php if( have_rows('list_of_staff_publications') ): ?>
          <?php while( have_rows('list_of_staff_publications') ): the_row(); ?>
          <h4><strong><u><?php the_sub_field('list_of_staff_publications_years');?></u></strong></h4>
          <div>
            <p><?php the_sub_field('list_of_staff_publications_table');?> </p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <h3 class="mmtl-heading mmtl-space-medium">UNIVERSITY RANK HOLDERS</h3>
          <h4>2015-2016&nbsp; M.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('2015-2016__me_cse') ): ?>
            <?php while( have_rows('2015-2016__me_cse') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('2015-2016__me_cse_image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('2015-2016__me_cse_title');?>">
                  <img src="<?php the_sub_field('2015-2016__me_cse_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <h4>M.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('me_cse') ): ?>
            <?php while( have_rows('me_cse') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('me_cse_image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('me_cse_title');?>">
                  <img src="<?php the_sub_field('me_cse_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h4>2014-2015&nbsp; M.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('2014-2015__me_cse') ): ?>
            <?php while( have_rows('2014-2015__me_cse') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('2014-2015__me_cse__image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('2014-2015__me_cse_title');?>">
                  <img src="<?php the_sub_field('2014-2015__me_cse__image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h4>2012-2013-2016&nbsp; B.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('2012-2013-2016__be_cse') ): ?>
            <?php while( have_rows('2012-2013-2016__be_cse') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('2012-2013-2016__be_cse_image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('2012-2013-2016__be_cse_title');?>">
                  <img src="<?php the_sub_field('2012-2013-2016__be_cse_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h4>M.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('me_cse_2012-2013') ): ?>
            <?php while( have_rows('me_cse_2012-2013') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('me_cse_image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('me_cse_title');?>">
                  <img src="<?php the_sub_field('me_cse_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h4>2011-2012&nbsp;B.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('2011-2012_be_cse') ): ?>
            <?php while( have_rows('2011-2012_be_cse') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('2011-2012_be_cse_image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('2011-2012_be_cse_title');?>">
                  <img src="<?php the_sub_field('2011-2012_be_cse_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>

        <div>
          <h4>M.E (CSE)</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('me_cse_2011-2012') ): ?>
            <?php while( have_rows('me_cse_2011-2012') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('me_cse_image');?>" data-fancybox-group="univ rank holder1" title="<?php the_sub_field('me_cse_title');?>">
                  <img src="<?php the_sub_field('me_cse_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div class="clearfix"></div>
        <div>
          <h3>Head of Department </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>

          <div>
            <?php the_field('head_of_department_details');?>
          </div>
        </div>
        <div>
          <h3></h3>
          <h3>PROFESSOR</h3>
          <div>
            <?php the_field('professor');?>
          </div>
        </div>
        <div>
          <h3>ASSOCIATE PROFESSOR</h3>
          <div>
            <?php the_field('associate_professor');?>
          </div>
        </div>
        <div>
          <h3>ASSISTANT PROFESSOR</h3>
          <div>
            <?php the_field('assistant_professor');?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu4">
        <div>
          <h3>Gallery</h3>
          <span>International Conference on Informatics & Computing in Engineering Systems (ICICES 2018) on 21.03.18 and 22.03.18.</span>

          <div class="ach_sec clearfix">
            <?php if( have_rows('international_conference') ): ?>
            <?php while( have_rows('international_conference') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('international_conference_images');?>" data-fancybox-group="gallerycs1" title="<?php the_sub_field('international_conference_title');?>">
                  <img src="<?php the_sub_field('international_conference_images');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <span>Guest Lecture on Internet of Things on 01-03-18</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('guest_lecture_Internet') ): ?>
            <?php while( have_rows('guest_lecture_Internet') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('guest_lecture_internet_images');?>" data-fancybox-group="gallerycs1" title="<?php the_sub_field('guest_lecture_internet_title');?>">
                  <img src="<?php the_sub_field('guest_lecture_internet_images');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <span>Workshop on quantum computing & cryptography 23 & 24 January 2018</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('workshop_on_quantum') ): ?>
            <?php while( have_rows('workshop_on_quantum') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('workshop_on_quantum_images');?>" data-fancybox-group="gallerycs1" title="<?php the_sub_field('workshop_on_quantum_title');?>">
                  <img src="<?php the_sub_field('workshop_on_quantum_images');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <span>"Inauguration of IEEE Computer Society" on 21.02.2018</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('inauguration_of_ieee') ): ?>
            <?php while( have_rows('inauguration_of_ieee') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('inauguration_of_ieee_images');?>" data-fancybox-group="gallerycs1" title="<?php the_sub_field('inauguration_of_ieee_title');?>">
                  <img src="<?php the_sub_field('inauguration_of_ieee_images');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <span>TNSCST Sponsored National Level Seminar “Internet of Things for Effective Disaster Management” on 22-02-18 </span>
          <div class="ach_sec clearfix">

            <?php if( have_rows('tnscst') ): ?>
            <?php while( have_rows('tnscst') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('tnscst_images');?>" data-fancybox-group="gallerycs1" title="<?php the_sub_field('tnscst_title');?>">
                  <img src="<?php the_sub_field('tnscst_images');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <span>CSI State Level Student Convention “TECHWAVES” organized by SAEC- CSI STUDENT CHAPTER on 05-01-18</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('csi_state_level') ): ?>
            <?php while( have_rows('csi_state_level') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('csi_state_level__images');?>" data-fancybox-group="gallerycs1" title="<?php the_sub_field('csi_state_level__title');?>">
                  <img src="<?php the_sub_field('csi_state_level__images');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu5">
        <div>
          <h4><strong>INTERNSHIP / INPLANT TRAINING</strong></h4>
          <div>
            <?php the_field('inplant_training');?>
          </div>
        </div>
        <div>
          <span>2017-18</span>
          <div>
            <?php the_field('inplant_training_2017-18');?>
          </div>
        </div>
        <div>
          <span>2016-17 EVEN</span>
          <div>
            <?php the_field('inplant_training_2016-17_even');?>
          </div>
        </div>
        <div>
          <span>2016-17 ODD</span>
          <div>
            <?php the_field('inplant_training_2016-17_odd');?>
          </div>
        </div>

        <div>
          <h4><strong>Industrial Visits</strong></h4>
          <span>2017-2018</span>
          <div>
            <?php the_field('industrial_visits_2017-18');?>
          </div>

        </div>
        <div>
          <span>2016-2017</span>
          <div>
            <?php the_field('industrial_visits_2016-17');?>
          </div>
        </div>
        <div>
          <span>2015-2016</span>
          <div>
            <?php the_field('industrial_visits_2015-16');?>
          </div>
        </div>
        <div>
          <span>2014-2015</span>
          <div>
            <?php the_field('industrial_visits_2014-15');?>
          </div>
        </div>
        <div>
          <span>2013-2014</span>
          <div>
            <?php the_field('industrial_visits_2013-14');?>
          </div>
        </div>

        <div>
          <h4>Seminars/Workshops/Conferences</H4>
          <h3>DETAILS OF SEMINAR, WORKSHOP, CONFERENCES, CEP, FDP, QIP PROGRAMS ORGANIZED</h3>
          <span>2016-2017</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('seminars_workshops_2016-17') ): ?>
            <?php while( have_rows('seminars_workshops_2016-17') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('seminars_workshops_2016-17_image');?>" data-fancybox-group="SWC1" title="<?php the_sub_field('csi_state_level__title');?>">
                  <img src="<?php the_sub_field('seminars_workshops_2016-17_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <span>2015-2016</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('seminarsworkshops_2015-16') ): ?>
            <?php while( have_rows('seminarsworkshops_2015-16') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('seminars_workshops_2016-17_image');?>" data-fancybox-group="SWC1" title="<?php the_sub_field('seminars_workshops_2016-17_title');?>">
                  <img src="<?php the_sub_field('seminars_workshops_2016-17_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <?php the_field('seminarsworkshops_2015-16_table');?>
        </div>

        <div>
          <span>2014-2015</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('seminarsworkshops_2014-2015') ): ?>
            <?php while( have_rows('seminarsworkshops_2014-2015') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('seminarsworkshops_2014-2015_image');?>" data-fancybox-group="SWC1" title="<?php the_sub_field('seminarsworkshops_2014-2015_title');?>">
                  <img src="<?php the_sub_field('seminarsworkshops_2014-2015_image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <?php the_field('seminarsworkshops_2014-2015_table');?>
        </div>

        <div>
          <span>2013-2014</span>
          <div> <?php the_field('seminarsworkshops_2013-2014_table');?></div>
        </div>

        <div>
          <span>2012-2013</span>
          <div> <?php the_field('seminarsworkshops_2012-2013_table');?></div>
        </div>

        <div>
          <h4><strong>MoUs</strong></h4>
          <span>LIST OF MOUS SIGNED</span>
          <div>
            <?php the_field('list_of_mous_signed_table');?>
          </div>
        </div>

        <div>
          <h4>Placements &amp; Higher Studies</h4>
          <h3>LIST OF STUDENTS PLACED THROUGH CAMPUS PLACEMENT</h3>
          <span>ACADEMIC YEAR 2017 – 18</span>
          <?php if( have_rows('campus_placement__year_2017_18') ): ?>
          <?php while( have_rows('campus_placement__year_2017_18') ): the_row(); ?>
          <p><?php the_sub_field('company_name');?></p>
          <div class="ach_sec clearfix">
            <?php if( have_rows('list_of_students_images') ): ?>
            <?php while( have_rows('list_of_students_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('image');?>" data-fancybox-group="teaching1" title="<?php the_sub_field('image_title');?>">
                  <img src="<?php the_sub_field('image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <span>Academic Year 2016 – 17</span>
          <?php if( have_rows('campus_placement__year_2016_17') ): ?>
          <?php while( have_rows('campus_placement__year_2016_17') ): the_row(); ?>
          <p><?php the_sub_field('company_name');?></p>
          <div class="ach_sec clearfix">
            <?php if( have_rows('list_of_students_images') ): ?>
            <?php while( have_rows('list_of_students_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('image');?>" data-fancybox-group="teaching3" title="<?php the_sub_field('image_title');?>">
                  <img src="<?php the_sub_field('image');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <h3>Consolidation of Placed, Higher Studies, Entrepreneur Count</h3>
          <div> <?php the_field('entrepreneur_count_table');?></div>
        </div>

        <div>
          <h3>ENTREPRENEURS</h3>
          <div> <?php the_field('entrepreneurs_table');?></div>
        </div>

        <div>
          <h3>PLACEMENT DETAILS</h3>
          <?php if( have_rows('placement_details') ): ?>
          <?php while( have_rows('placement_details') ): the_row(); ?>
          <p><?php the_sub_field('placement_details_years');?> </p>
          <div>
            <p><?php the_sub_field('placement_details_table');?> </p>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <h3>HIGHER STUDIES</h3>
          <div>
            <table width="592">
              <tbody>
                <tr>
                  <td colspan="4" width="592"><strong>Year&nbsp; – 2009-2013</strong></td>
                </tr>
                <tr>
                  <td width="46"><strong>Sl.no</strong></td>
                  <td width="188"><strong>Name of the Student</strong></td>
                  <td width="108"><strong>Course Taken</strong></td>
                  <td width="251"><strong>University/ College Name</strong></td>
                </tr>
                <tr>
                  <td width="46">1.</td>
                  <td width="188">BAGYALAKSHMI.M</td>
                  <td width="108">M.E</td>
                  <td width="251">Francis Xavier Engineering College, Tirunelveli</td>
                </tr>
                <tr>
                  <td width="46">2.</td>
                  <td width="188">KAVITHA.V</td>
                  <td width="108">M.E</td>
                  <td width="251">S.A.Engineering College, Chennai</td>
                </tr>
                <tr>
                  <td width="46">3.</td>
                  <td width="188">BASHETHA.A</td>
                  <td width="108">M.E</td>
                  <td width="251">S.A.Engineering College, Chennai</td>
                </tr>
                <tr>
                  <td width="46">4.</td>
                  <td width="188">SATHISH KUMAR.S</td>
                  <td width="108">M.B.A</td>
                  <td width="251">University of Madras, Chennai</td>
                </tr>
                <tr>
                  <td width="46">5.</td>
                  <td width="188">RAJIV</td>
                  <td width="108">M.B.A</td>
                  <td width="251">Velammal Institute of Technology, Chennai</td>
                </tr>
                <tr>
                  <td width="46">6.</td>
                  <td width="188">RAJARAM</td>
                  <td width="108">M.B.A</td>
                  <td width="251">Symbiosis Centre of Distance Learning, Chennai</td>
                </tr>
                <tr>
                  <td width="46">7.</td>
                  <td width="188">RANJITH</td>
                  <td width="108">M.B.A</td>
                  <td width="251">Velammal Institute of Technology, Chennai</td>
                </tr>
                <tr>
                  <td width="46">8.</td>
                  <td width="188">VISHAL</td>
                  <td width="108">M.B.A</td>
                  <td width="251">Valliammal Engineering College, Chennai</td>
                </tr>
                <tr>
                  <td width="46">9.</td>
                  <td width="188">JOHN FERNANDO.R</td>
                  <td width="108">M.E</td>
                  <td width="251">Meenakshi College of Engineering, Chennai</td>
                </tr>
                <tr>
                  <td width="46">10.</td>
                  <td width="188">PRASATH.S</td>
                  <td width="108">M.E</td>
                  <td width="251">Meenakshi College of Engineering, Chennai</td>
                </tr>
                <tr>
                  <td width="46">11.</td>
                  <td width="188">MEGALA.P.S</td>
                  <td width="108">M.TECH</td>
                  <td width="251">VIT University, Chennai</td>
                </tr>
                <tr>
                  <td width="46">12.</td>
                  <td width="188">DIVYA.R</td>
                  <td width="108">M.E</td>
                  <td width="251">JEPPIAR Engineering College, Chennai</td>
                </tr>
                <tr>
                  <td width="46">13.</td>
                  <td width="188">DEEPAK SINGH</td>
                  <td width="108">M.E</td>
                  <td width="251">St.Peter’s University, Chennai</td>
                </tr>
                <tr>
                  <td width="46">14.</td>
                  <td width="188">AAMIL MOHAMMED.F</td>
                  <td width="108">M.B.A</td>
                  <td width="251">Loyola College, Chennai</td>
                </tr>
              </tbody>
            </table>
            <?php if( have_rows('higher_studies') ): ?>
            <?php while( have_rows('higher_studies') ): the_row(); ?>
            <div>
              <p><?php the_sub_field('higher_studies_table');?> </p>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu6">
        <div class="container">
          <h2 class="title_line"><?php the_field('fdp_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('fdp') ): ?>
              <?php while( have_rows('fdp') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('fdp_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('fdp_slide_image_title');?>">
                    <img src="<?php the_sub_field('fdp_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('fdp_slide_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('guest_lecture_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('guest_lecture') ): ?>
              <?php while( have_rows('guest_lecture') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('guest_lecture_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('guest_lecture_slide_image_title');?>">
                    <img src="<?php the_sub_field('guest_lecture_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('guest_lecture_slide_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>


          <h2 class="title_line"><?php the_field('industrial_visit_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">

              <?php if( have_rows('industrial_visit') ): ?>
              <?php while( have_rows('industrial_visit') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('industrial_visit_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('industrial_visit_slide__title');?>">
                    <img src="<?php the_sub_field('industrial_visit_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('industrial_visit_slide__content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>

            </div>
          </div>

          <h2 class="title_line"><?php the_field('seminar');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('seminar') ): ?>
              <?php while( have_rows('seminar') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('seminar_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('seminar_slide__title');?>">
                    <img src="<?php the_sub_field('seminar_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('seminar_slide__content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>


          <h2 class="title_line"><?php the_field('sponsored_events_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('sponsored_events') ): ?>
              <?php while( have_rows('sponsored_events') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('sponsored_events_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('sponsored_events_slide__title');?>">
                    <img src="<?php the_sub_field('sponsored_events_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('sponsored_events_slide__content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>


          <h2 class="title_line"><?php the_field('students_clubs_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('students_clubs') ): ?>
              <?php while( have_rows('students_clubs') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('students_clubs_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('students_clubs_slide__title');?>">
                    <img src="<?php the_sub_field('students_clubs_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('students_clubs_slide__content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('value_added_cours_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('value_added_cours') ): ?>
              <?php while( have_rows('value_added_cours') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('value_added_cours_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('value_added_cours_title');?>">
                    <img src="<?php the_sub_field('value_added_cours_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('value_added_cours_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('workshop_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">

              <?php if( have_rows('workshop_cours') ): ?>
              <?php while( have_rows('workshop_cours') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('workshop_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('workshop_title');?>">
                    <img src="<?php the_sub_field('workshop_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('workshop_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('conference_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('conference') ): ?>
              <?php while( have_rows('conference') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('conference_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('conference_title');?>">
                    <img src="<?php the_sub_field('conference_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('conference_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('convention_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('convention') ): ?>
              <?php while( have_rows('convention') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('convention_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('convention_title');?>">
                    <img src="<?php the_sub_field('convention_image');?>" class="img-responsive">
                  </a>
                </div>
                <div class="text"><?php the_sub_field('convention_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>

            </div>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu7">
        <div>
          <div class="row">
            <?php if( have_rows('infrastructure') ): ?>
            <?php while( have_rows('infrastructure') ): the_row(); ?>
            <div class="col-sm-4">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('infrastructure_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('infrastructure_image');?>" class="img-responsive">
                </a>
              </div>
              <span><?php the_sub_field('infrastructure_lab');?></span>
              <div>
                <table class="infra_table">
                  <tbody>
                    <tr>
                      <td>Area in Sq.mts:</td>
                      <td><?php the_sub_field('area_in_sqmts');?></td>
                    </tr>
                    <tr>
                      <td>Major Cost of Equipment:</td>
                      <td><?php the_sub_field('infrastructure_cost');?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu8">

        <div class="box-holder">
          <?php the_field('our_vision');?>
        </div>

        <div class="box-holder">
          <?php the_field('our_mission');?>
        </div>

        <div class="box-holder">
          <?php the_field('programme_educational_objectives_peos');?>
        </div>

        <div class="box-holder">
          <?php the_field('programme_specific_outcomes_psos');?>
        </div>

        <div class="box-holder">
          <?php the_field('programme_educational_objectives');?>
        </div>
        <div class="box-holder">
          <?php the_field('programme_outcomes_pos');?>
        </div>



      </div>
    </div>
    <div class="clearfix"></div>
    <div class="external_links course_ext-links">
      <label>Also check: </label>
      <ul class="share_icons clearfix">

        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</div>

<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
