<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"> <?php _e('Vision & Mission') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"> <?php _e('Faculty') ?></a></li>
        <li><a data-toggle="tab" href="#menu4"> <?php _e('Infrastructure') ?> </a></li>
        <li><a data-toggle="tab" id="listmenu5" href="#menu5"> <?php _e('Department Activities') ?></a></li>
        <li><a href="http://saec.ac.in/eee-temp/placement/" target="_blank">
            <?php _e('Student Activities') ?> </a></li>
        <li><a href="http://saec.ac.in/eee-temp/student-performance/" target="_blank">
            <?php _e('Student’s Performance') ?></a></li>
        <li><a href="http://saec.ac.in/eee-temp/research-and-development/" target="_blank">
            <?php _e('Research and Development') ?></a></li>
      </ul>
    </div>

    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <div>
          <p><?php the_field('about_course_electrical');?></p>
        </div>
        <div class="row">
          <?php if( have_rows('department_award_1') ): ?>
          <?php while( have_rows('department_award_1') ): the_row(); ?>
          <div class="col-sm-4">
            <div class="ach_sec clearfix">
              <a class="fancybox" href="<?php the_sub_field('department1_award_images'); ?>" data-fancybox-group="gallery20" title="<?php the_sub_field('department1_award_images_title'); ?>">
                <img src="<?php the_sub_field('department1_award_images'); ?>" class="img-responsive">
              </a>
            </div>
            <span><?php the_sub_field('department1_award_label'); ?></span>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <p><?php the_field('department1_award_details');?></p>
        </div>
        <div class="row">
          <?php if( have_rows('department_award_2') ): ?>
          <?php while( have_rows('department_award_2') ): the_row(); ?>
          <div class="col-sm-4">
            <div class="ach_sec clearfix">
              <a class="fancybox" href="<?php the_sub_field('department2_award_images'); ?>" data-fancybox-group="gallery20" title="<?php the_sub_field('department2_award_images_title'); ?>">
                <img src="<?php the_sub_field('department2_award_images'); ?>" class="img-responsive">
              </a>
            </div>
            <span><?php the_sub_field('department2_award_label'); ?></span>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="course_feature electrical-course-feature">
          <div class="course_feature">
            <?php if( have_rows('course_feature') ): ?>
            <?php while( have_rows('course_feature') ): the_row(); ?>
            <div class="feat-box">
              <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
              <i>
                <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
              </i>
              <div>
                <label><?php the_sub_field('feature_label'); ?></label>
                <p><?php the_sub_field('feature_sub_text'); ?></p>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('innovation_using_arm_processor');?>
        </div>

        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('innovation_using_arm_processor_images') ): ?>
            <?php while( have_rows('innovation_using_arm_processor_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('innovation_arm_processor_images')?>" data-fancybox-group="gallery20" title="<?php the_sub_field('innovation_images_title')?>">
                  <img src="<?php the_sub_field('innovation_arm_processor_images')?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('collaboration');?>
        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div class="box-holder">
          <?php the_field('our_vision');?>
        </div>

        <div class="box-holder">
          <?php the_field('our_mission');?>
        </div>

        <div class="box-holder">
          <?php the_field('programme_educational_objectives_peos');?>
        </div>

        <div class="box-holder">
          <?php the_field('programme_specific_outcomes_psos');?>
        </div>

        <div class="box-holder">
          <?php the_field('programme_educational_objectives');?>
        </div>
        <div class="box-holder">
          <?php the_field('programme_outcomes_pos');?>
        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div>
          <h3>Head of Department </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>

          <div>
            <?php the_field('head_of_department_details');?>
          </div <div>
          <h3>PROFESSOR</h3>
          <div>
            <?php the_field('professor');?>
          </div>
        </div>

        <div>
          <h3>ASSOCIATE PROFESSOR</h3>
          <div>
            <?php the_field('associate_professor');?>
          </div>
        </div>

        <div>
          <h3>ASSISTANT PROFESSOR</h3>
          <div>
            <?php the_field('assistant_professor');?>
          </div>
        </div>

      </div>
      <div class="tab-content fade" id="menu4">
        <div>
          <div class="row">
            <?php if( have_rows('infrastructure') ): ?>
            <?php while( have_rows('infrastructure') ): the_row(); ?>
            <div class="col-sm-4">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('infrastructure_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('infrastructure_image');?>" class="img-responsive">
                </a>
              </div>
              <span><?php the_sub_field('infrastructure_title');?></span>
              <div>
                <table class="infra_table">
                  <tbody>
                    <tr>
                      <?php 
                        if (get_sub_field ('area_in_sqmts')) {
                        ?>
                      <td>Area in Sq.mts:</td>
                      <td><?php the_sub_field('area_in_sqmts');?></td>
                      <?php 
                      }
                    ?>
                    </tr>
                    <tr>
                      <?php 
                        if (get_sub_field('infrastructure_cost')) {
                        ?>
                      <td>Major Cost of Equipment:</td>
                      <td><?php the_sub_field('infrastructure_cost');?></td>
                      <?php 
                      }
                    ?>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="tab-content fade" id="menu5">
        <div class="container">
          <h2 class="title_line"><?php the_field('fdp_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('fdp') ): ?>
              <?php while( have_rows('fdp') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('fdp_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('fdp_slide_image_title');?>">
                    <img src="<?php the_sub_field('fdp_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('fdp_slide_image_head');?> </span>
                <div class="text"><?php the_sub_field('fdp_slide_content');?> </div>

              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('guest_lecture_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('guest_lecture') ): ?>
              <?php while( have_rows('guest_lecture') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('guest_lecture_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('guest_lecture_slide_image_title');?>">
                    <img src="<?php the_sub_field('guest_lecture_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('guest_lecture_slide_image_head');?> </span>
                <div class="text"><?php the_sub_field('guest_lecture_slide_content');?> </div>

              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('industrial_visit_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('industrial_visit') ): ?>
              <?php while( have_rows('industrial_visit') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('industrial_visit_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('industrial_visit_slide__title');?>">
                    <img src="<?php the_sub_field('industrial_visit_slide_image');?>" class="img-responsive">
                  </a>
                </div>

                <span class="text1"><?php the_sub_field('industrial_visit_slide__head');?> </span>
                <div class="text"><?php the_sub_field('industrial_visit_slide__content');?> </div>

              </div>
              <?php endwhile; ?>
              <?php endif; ?>

            </div>
          </div>

          <h2 class="title_line"><?php the_field('seminar_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('seminar') ): ?>
              <?php while( have_rows('seminar') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('seminar_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('seminar_slide__title');?>">
                    <img src="<?php the_sub_field('seminar_slide_image');?>" class="img-responsive">
                  </a>
                </div>

                <span class="text1"><?php the_sub_field('seminar_slide_head');?> </span>
                <div class="text"><?php the_sub_field('seminar_slide__content');?> </div>

              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('value_added_cours_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('value_added_cours') ): ?>
              <?php while( have_rows('value_added_cours') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('value_added_cours_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('value_added_cours_title');?>">
                    <img src="<?php the_sub_field('value_added_cours_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('value_added_cours_head');?> </span>
                <div class="text"><?php the_sub_field('value_added_cours_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <h2 class="title_line"><?php the_field('workshop_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">

              <?php if( have_rows('workshop_cours') ): ?>
              <?php while( have_rows('workshop_cours') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('workshop_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('workshop_title');?>">
                    <img src="<?php the_sub_field('workshop_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('workshop_title');?> </span>
                <div class="text"><?php the_sub_field('workshop_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu6">
        <div>
          <h3>Research and Development</h3>
          <p>Research at S.A. Engineering College, aims to develop innovative solutions to the world&rsquo;s most alarming problems. From addressing the energy needs of tomorrow to improving power quality, our research efforts are enhanced through creative collaborations with leading research institutes and organization around the world. Compiled here are just some of the R&amp;D labs, centers and programs where cutting-edge research is taking place. Our department has been recognized as a research centre by Centre of Research, Anna University, Chennai 600 025. (Approval No: Lr.No.114/IR/EEE/AR1 Dated: 07/01/2013).</p>
        </div>
        <div>
          <h3>R&amp;D Centre Excellence</p>
            <p>National Institute for Wind Energy: Inaugurated on 29-3-2016.</p>
            <p>The main objective is to train the students with keen practical knowledge on basics of Power and energy systems. Initiate the students and faculties in research and developments in recent scenario on power systems.</p>
            <p>Trainings were given to students regarding to the high end placements in reputed companies.</p>
        </div>
        <div>
          <h3>Centre of Excellence in Industrial Automation and Robotics.</h3>
          <p>Centre for Industrial Automation and Robotics is based in the department of Electrical and Electronics Engineering at S.A. Engineering College. This centre provides academic and industrial research in the key areas including industrial automation training, industrial drives, industrial SCADA/HMI, industrial robotics training and solution. The centre has two labs</p>

          <ul class="tic_list">
            <li>1. Six Axis Robotics Lab</li>
            <li>2. Spark Automation Centre</li>
          </ul>
        </div>
        <div>
          <h3>Supervisors</h3>
          <table class="c13">
            <tbody>
              <tr class="c11">
                <td class="c87" colspan="1" rowspan="1">
                  <p>Dr. Priya S., M.E., Ph.D</p>
                  <p>HoD/EEE</p>
                  <p class="c0"><img alt="electrical and electronics engineering college " src="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/s-priya.png"></p>
                </td>
                <td class="c55" colspan="1" rowspan="1">
                  <p>Dr. SathiyaSekar&nbsp; K., M.Tech., Ph.D</p>
                  <p>Professor</p>
                  <p class="c123"><img alt="best electrical and electronics engineering colleges in chennai " src="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/sathiyasekar.png" style="width: 133.62px; height: 144.95px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
                </td>
                <td class="c124" colspan="1" rowspan="1">
                  <p>Dr. Suresh A.,&nbsp; M.E., Ph.D</p>
                  <p>Professor</p>
                  <p class="c0"><img alt="electrical and electronics engineering colleges in chennai  " src="http://saec.ac.in/eee-temp/wp-content/uploads/2018/02/suresh-234x300.png"></p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="external_links course_ext-links">
      <label>Also check: </label>
      <ul class="share_icons clearfix">
        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</div>

<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
