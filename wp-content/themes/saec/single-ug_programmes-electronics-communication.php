<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner test" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"> <?php _e('About Course') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"> <?php _e('Department') ?> </a></li>
        <li><a data-toggle="tab" href="#menu3"> <?php _e('Faculty') ?> </a></li>
        <li><a data-toggle="tab" id="listmenu5" href="#menu5"> <?php _e('Activities') ?> </a></li>
        <li><a data-toggle="tab" href="#menu6"> <?php _e('Research & Development') ?> </a></li>
        <li><a data-toggle="tab" href="#menu7"> <?php _e('Vision & Mission') ?></a></li>
        <li><a data-toggle="tab" href="#menu8"> <?php _e('Infrastructure') ?> </a></li>
        <li><a data-toggle="tab" href="#menu9"> <?php _e('Student Activities ') ?> </a></li>
        <li><a data-toggle="tab" href="#menu10"><?php _e('Student’s Performance') ?> </a></li>
      </ul>
    </div>

    <div class="tab-content-wrap clearfix">
      <div class="tab-content lh-custom fade in active" id="menu1">
        <div>
          <p><?php the_field('about_course_electrical');?></p>
        </div>
        <div>
          <div class="row">
            <?php if( have_rows('department_award_1') ): ?>
            <?php while( have_rows('department_award_1') ): the_row(); ?>
            <div class="col-sm-4">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('department1_award_images'); ?>" data-fancybox-group="gallery20" title="<?php the_sub_field('department1_award_images_title'); ?>">
                  <img src="<?php the_sub_field('department1_award_images'); ?>" class="img-responsive">
                </a>
              </div>
              <span class="equal-height-wrap"><?php the_sub_field('department1_award_label'); ?></span>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>

        <div>
          <p><?php the_field('department1_award_details');?></p>
        </div>

        <div class="row">
          <?php if( have_rows('department_award_2') ): ?>
          <?php while( have_rows('department_award_2') ): the_row(); ?>
          <div class="col-sm-4">
            <div class="ach_sec clearfix">
              <a class="fancybox" href="<?php the_sub_field('department2_award_images'); ?>" data-fancybox-group="gallery20" title="<?php the_sub_field('department2_award_images_title'); ?>">
                <img src="<?php the_sub_field('department2_award_images'); ?>" class="img-responsive">
              </a>
            </div>
            <span class="equal-height-wrap"><?php the_sub_field('department2_award_label'); ?></span>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="course_feature electrical-course-feature">
          <div class="course_feature">
            <?php if( have_rows('course_feature') ): ?>
            <?php while( have_rows('course_feature') ): the_row(); ?>
            <div class="feat-box">
              <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
              <i>
                <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
              </i>
              <div>
                <label><?php the_sub_field('feature_label'); ?></label>
                <p><?php the_sub_field('feature_sub_text'); ?></p>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h3><?php the_field('newsletter_title');?></h3>
          <?php if( have_rows('newsletter') ): ?>
          <?php while( have_rows('newsletter') ): the_row(); ?>
          <p>
            <a href="<?php the_sub_field('pdf'); ?>" target="_blank"><?php the_sub_field('newsletter_content'); ?></a>
          </p>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <h3 class="title_line"><strong><?php the_field('e_magazine_title');?>:</strong></h3>
          <?php if( have_rows('e_magazine') ): ?>
          <?php while( have_rows('e_magazine') ): the_row(); ?>
          <p>
            <a href="<?php the_sub_field('pdf'); ?>" target="_blank"><?php the_sub_field('e_magazine_content'); ?></a>
          </p>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div>
          <p><?php the_field('department_detail');?></p>
        </div>

        <div>
          <h3>Programmes</h3>
          <ul class="tic_list">
            <?php if( have_rows('programmes') ): ?>
            <?php while( have_rows('programmes') ): the_row(); ?>
            <li><?php the_sub_field('programmes_list'); ?></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3>Students Achievements</h3>
          <table>
            <tbody>
              <?php if( have_rows('students_achievements') ): ?>
              <?php while( have_rows('students_achievements') ): the_row(); ?>
              <tr>
                <td>
                  <?php the_sub_field('students_achievements_content'); ?>
                </td>
                <td>
                  <?php if( have_rows('students_achievements_image') ): ?>
                  <?php while( have_rows('students_achievements_image') ): the_row(); ?>
                  <img src="<?php the_sub_field('images'); ?>" alt="" width="360" height="268">
                  <?php endwhile; ?>
                  <?php endif; ?>
                </td>
              </tr>
              <?php endwhile; ?>
              <?php endif; ?>

            </tbody>
          </table>
        </div>
        <div>
          <h4>INTRA / INTER COLLEGE COMPETITION (CO-CURRICULAR ACTIVITIES)</h4>
          <p>WINNERS LIST ACADEMIC YEAR: 2016-2017</p>
          <div><?php the_field('intra_inter_college_competition_table1'); ?></div>
        </div>

        <div>
          <h4>INTRA / INTER COLLEGE COMPETITION (CO-CURRICULAR ACTIVITIES)</h4>
          <span>WINNERS LIST</span>
          <div><?php the_field('intra_inter_college_competition_table2'); ?></div>
        </div>

        <div>
          <h3><?php the_field('title'); ?></h3>
          <div><?php the_field('research_and_development'); ?></div>
        </div>

        <div>
          <h4>Department of ECE is recognized Research Center under Anna University since 2011.</h4>
          <span>SUPERVISOR LIST :</span>
          <div><?php the_field('supervisor_list'); ?></div>
        </div>
        <div>
          <span>FACULTY – PURSUING PH.D</span>
          <div>
            <?php the_field('faculty_pursuing_phd'); ?>
          </div>
        </div>
        <div>
          <span>FUNDED PROJECTS</span>

          <div>
            <?php the_field('funded_projects'); ?></div>
        </div>
        <div> <?php the_field('funded_projects_2'); ?></div>

        <div>
          <h4>CONSULTANCY BROCHURE – Click Here CONSULTANCY PHOTOS<a href="pdf/CONSULTANCY-Photos.pdf"> </a>– Click Here</h4>
          <span>PATENTS</span>
          <div> <?php the_field('patents'); ?></div>
        </div>

        <div>
          <span>PUBLICATIONS</span>
          <div> <?php the_field('publications'); ?></div>
        </div>

        <div> <span>SEMINAR GRANTS</span>
          <ul class="tic_list">
            <?php if( have_rows('seminar_grants') ): ?>
            <?php while( have_rows('seminar_grants') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('seminar_grants_content'); ?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <span> LIST OF PAPERS PUBLISHED IN INDEXED JOURNAL</span>
          <ul class="tic_list">
            <ol>
              <?php if( have_rows('list_of_papers_published') ): ?>
              <?php while( have_rows('list_of_papers_published') ): the_row(); ?>
              <li><?php the_sub_field('list_of_papers'); ?></li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ol>
          </ul>
        </div>

        <div>
          <h3>Academic Performance</h3>
          <div>
            <?php the_field('academic_performance'); ?>
          </div>
        </div>

        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('academic_performance_img') ): ?>
            <?php while( have_rows('academic_performance_img') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('performance_img'); ?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('performance_img'); ?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div>
          <h3>Head of Department </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>
          <div>
            <?php the_field('head_of_department_details');?>
          </div>

        </div>
        <div>
          <h3>PROFESSOR</h3>
          <div>
            <?php the_field('professor');?>
          </div>
        </div>

        <div>
          <h3>ASSOCIATE PROFESSOR</h3>
          <div>
            <?php the_field('associate_professor');?>
          </div>
        </div>

        <div>
          <h3>ASSISTANT PROFESSOR</h3>
          <div>
            <?php the_field('assistant_professor');?>
          </div>
        </div>


      </div>
      <div class="tab-content fade" id="menu5">
        <div class="container">
          <h2 class="title_line"><?php the_field('guest_lecture_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1">
              <?php if( have_rows('guest_lecture') ): ?>
              <?php while( have_rows('guest_lecture') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('guest_lecture_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('guest_lecture_slide_image_title');?>">
                    <img src="<?php the_sub_field('guest_lecture_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('guest_lecture_slide_image_head');?></span>
                <div class="text"><?php the_sub_field('guest_lecture_slide_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>


          <h2 class="title_line"><?php the_field('cca_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1 ">
              <?php if( have_rows('cca') ): ?>
              <?php while( have_rows('cca') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('guest_lecture_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('guest_lecture_slide_image_title');?>">
                    <img src="<?php the_sub_field('guest_lecture_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('guest_lecture_slide_image_head');?></span>
                <div class="text"></div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>


          <h2 class="title_line"><?php the_field('fdp_title');?> </h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1 ">
              <?php if( have_rows('fdp') ): ?>
              <?php while( have_rows('fdp') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('fdp_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('fdp_slide_image_title');?>">
                    <img src="<?php the_sub_field('fdp_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('fdp_slide_image_head');?></span>
                <div class="text"><?php the_sub_field('fdp_slide_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>

          </div>

          <h2 class="title_line"><?php the_field('industrial_visit_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1 ">
              <?php if( have_rows('industrial_visit') ): ?>
              <?php while( have_rows('industrial_visit') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('industrial_visit_slide_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('industrial_visit_slide__title');?>">
                    <img src="<?php the_sub_field('industrial_visit_slide_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('industrial_visit_slide__head');?> </span>
                <div class="text"></div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>


          <h2 class="title_line"><?php the_field('workshop_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1 ">
              <?php if( have_rows('workshop_cours') ): ?>
              <?php while( have_rows('workshop_cours') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('workshop_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('workshop_title');?>">
                    <img src="<?php the_sub_field('workshop_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('workshop_title');?> </span>
                <div class="text"><?php the_sub_field('workshop_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>

            </div>
          </div>
          <h2 class="title_line"><?php the_field('value_added_cours_title');?></h2>
          <div class="board_navslide"></div>
          <div class="board_trustes_inner">
            <div class="board_trustes_slder_hh1 ">

              <?php if( have_rows('value_added_cours') ): ?>
              <?php while( have_rows('value_added_cours') ): the_row(); ?>
              <div class="board_trustes_box">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('value_added_cours_image');?>" data-fancybox-group="gallery20" title="<?php the_sub_field('value_added_cours_title');?>">
                    <img src="<?php the_sub_field('value_added_cours_image');?>" class="img-responsive">
                  </a>
                </div>
                <span class="text1"><?php the_sub_field('value_added_cours_head');?> </span>
                <div class="text"><?php the_sub_field('value_added_cours_content');?> </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu6">
        <div>
          <?php the_field('research_development');?>
        </div>
      </div>
      <div class="tab-content fade" id="menu7">
        <div> <?php the_field('vision_&_mission');?></div>
      </div>
      <div class="tab-content fade" id="menu8">
        <div>
          <div class="row">
            <?php if( have_rows('infrastructure') ): ?>
            <?php while( have_rows('infrastructure') ): the_row(); ?>
            <div class="col-sm-4">
              <div class="ach_sec clearfix">
                <a class="fancybox" href="<?php the_sub_field('infrastructure_image');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('infrastructure_image');?>" class="img-responsive">
                </a>
              </div>
              <span><?php the_sub_field('infrastructure_lab');?></span>
              <div>
                <table class="infra_table">
                  <tbody>
                    <tr>
                      <td>Area in Sq.mts:</td>
                      <td><?php the_sub_field('area_in_sqmts');?></td>
                    </tr>
                    <tr>
                      <td>Major Cost of Equipment:</td>
                      <td><?php the_sub_field('infrastructure_cost');?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <div>
            <a href="http://164.100.247.30/data" target="_blank">DELNET FOR ONLINE JOURNAL </a>
          </div>
          <div>
            <h3 class="title_line">CIRCUIT OF THE WEEK</h3><br><br>
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/pdf/winners list 2018-19 odd.pdf" target="_blank"> WINNERS LIST 2018-19 </a>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="tab-content fade" id="menu9">
        <div>
          <div class="best_projects-wrap">
            <h4><?php the_field('best_projects_title');?></h4>
            <ul class="tic_list">
              <?php if( have_rows('best_projects') ): ?>
              <?php while( have_rows('best_projects') ): the_row(); ?>
              <li><a href="<?php the_sub_field('best_projects_pdf_image');?>" target="_blank">
                  <?php the_sub_field('best_projects_list');?></a></li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ul>
          </div>


          <div class="best_projects-wrap">
            <h4><?php the_field('sponsorships_title');?></h4>
            <ul class="tic_list">
              <?php if( have_rows('sponsorships') ): ?>
              <?php while( have_rows('sponsorships') ): the_row(); ?>
              <li><a href="<?php the_sub_field('professional_body_pdf_image');?>" target="_blank">
                  <?php the_sub_field('professional_body_list');?></a></li>
              <?php endwhile; ?>
              <?php endif; ?>
            </ul>
          </div>

          <a href="<?php the_field('professional_body_affiliations');?>">
            <h3>PROFESSIONAL BODY AFFILIATIONS</h3>
          </a>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="tab-content fade" id="menu10">
        <div>
          <h4>DEPARTMENT OF ECE</h4>
          <h4>GRADUATE COUNT</h4>
          <div>
            <?php the_field('graduate_count');?>
          </div>

          <img src="<?php the_field('graduate_count_graph');?>" class="img-responsive" style="max-width:700px;width:100%;">

          <h4>DEPARTMENT OF ECE</h4>
          <h4>ANNA UNIVERSITY RANK HOLDERS</h4>
          <table style="margin:30px 0;">
            <tbody>
              <tr>
                <td>
                  <p><strong>YEAR</strong></p>
                </td>
                <td>
                  <p><strong>NAME</strong></p>
                </td>
                <td>
                  <p><strong>RANK</strong></p>
                </td>
              </tr>
              <tr>
                <td rowspan="5">
                  <p><span style="font-weight: 400;">M.E -CS (2014-2016)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">D.VARALAKSHMI</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">28</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">G.DIVYA</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">36</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">R.S.MAHATHI</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">37</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">A.ASHWINI</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">39</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">B.GAJALAKSHMI</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">40</span></p>
                </td>
              </tr>
              <tr>
                <td rowspan="3">
                  <p><span style="font-weight: 400;">B.E ECE(2011-15)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">STEFFI KERAN RANI.J </span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">9</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">ABINAYA. S</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">36</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">CHARUMATHI.S</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">49</span></p>
                </td>
              </tr>
              <tr>
                <td rowspan="7">
                  <p><span style="font-weight: 400;">M.E -CS (2013-2015)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">GAJALAKSHMI.S</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">22</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">POORANI.C</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">25</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">JANANI.<strong>B</strong></span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">29</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">RISHMA PODRIGO.R</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">34</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">BERYL J.VICTOR</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">36</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">SANDHYA.P</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">46</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">RAJPREETHA.C</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">46</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">B.E ECE(2010-14)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">S.JAYASREE ARCHANA</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">42</span></p>
                </td>
              </tr>
              <tr>
                <td rowspan="10">
                  <p><span style="font-weight: 400;">M.E -CS (2012-2014)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">NITHYA.A</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">5</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">GNANAJENI.G.S</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">17</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">REVATHI.B</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">21</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">JEBARAJ.V</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">26</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">SATHIYALAKSHMI.S</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">31</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">SUBRAM.S.N</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">37</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">MURALIKRISHNA.K</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">39</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">PUSHPA.T</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">41</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">PARAMASIVAM.M</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">41</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">GAYATHRI.T</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">50&nbsp;</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">B.E ECE (2009-13 Batch)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">S.VINODH</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">41</span></p>
                </td>
              </tr>
              <tr>
                <td rowspan="2">
                  <p><span style="font-weight: 400;">M.E -CS (2011-2013)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">JAYASHREE.R</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">4</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">JARINA YASMIN.J</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">41</span></p>
                </td>
              </tr>
              <tr>
                <td rowspan="2">
                  <p><span style="font-weight: 400;">B.E ECE (2008-12 Batch)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">MOSES DINAKARAN</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">33</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">NIKITHA.S.V</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">47</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">B.E ECE (2003-07 Batch)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">SETHURAMAN</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">15</span></p>
                </td>
              </tr>
              <tr>
                <td>
                  <p><span style="font-weight: 400;">B.E ECE (2002-06 Batch)</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">M.PRATHAP</span></p>
                </td>
                <td>
                  <p><span style="font-weight: 400;">3</span></p>
                </td>
              </tr>
            </tbody>
          </table>
          <img src=" <?php the_field('anna_university_rank_holders_graph');?>" class="img-responsive" style="max-width:700px;width:100%;">
          <div>
            <h4>ANNA UNIVERSITY RANK HOLDERS</h4>
            <?php the_field('anna_university_rank_holders_list');?>

          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>

    <div class="external_links course_ext-links">

      <label>Also check: </label>
      <ul class="share_icons clearfix">
        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
    
    </div> <?php include('virtual-tour-strip.php');?> <section class="clg_range">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 clg_range_box">
          <div>
            <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
            <span class="plus-symbol">+</span>
            <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
          </div>
        </div>

        <div class="col-sm-4 clg_range_box">
          <div>
            <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
            <span><?php the_field('university_rank_label_gallery', 680);?></span>
          </div>
        </div>

        <div class="col-sm-4 clg_range_box">
          <div>
            <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
            <span><?php the_field('square_feet_label_gallery', 680);?></span>
          </div>
        </div>
      </div>
    </div>
    </section>

    <?php
get_footer();
