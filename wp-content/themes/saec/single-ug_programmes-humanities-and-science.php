<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About Course') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"><?php _e('Department') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"><?php _e('Faculty') ?> </a></li>
        <li><a data-toggle="tab" href="#menu4"><?php _e(' Infrastructure') ?> </a></li>
        <li><a data-toggle="tab" href="#menu5"><?php _e(' Activities') ?>  </a></li>
        <li><a data-toggle="tab" href="#menu6"><?php _e(' Research & Development') ?> </a></li>
      </ul>
    </div>
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <p><?php the_field('about_course_detail'); ?> </p>
        <div class="row">
          <?php if( have_rows('about_course_img_block') ): ?>
          <?php while( have_rows('about_course_img_block') ): the_row(); ?>
          <div class="col-sm-4 custom-h">
            <div class="ach_sec clearfix">
              <a class="fancybox" href="<?php the_sub_field('images'); ?>" data-fancybox-group="gallery20" title="<?php the_sub_field('image_title'); ?>">
                <img src="<?php the_sub_field('images'); ?>" class="img-responsive">
              </a>
            </div>
            <span><?php the_sub_field('block_title'); ?></span>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <?php the_field('faculty_section'); ?>
        </div>
        <div>
          <?php the_field('activities_and_achievements'); ?>
        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div>
          <?php the_field('dev_achievements');?>
        </div>


        <div>
          <?php the_field('confernce__fdp_conducted');?>
        </div>

        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('confernce_img_list') ): ?>
            <?php while( have_rows('confernce_img_list') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('confernce_img');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('confernce_img');?>" class="img-responsive" /></a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <?php the_field('national_conference');?>
        </div>
        <div>


          <div class="ach_sec clearfix">
            <?php if( have_rows('national_conference_img') ): ?>
            <?php while( have_rows('national_conference_img') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('conference_img');?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('conference_img');?>" class="img-responsive" /></a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div>
          <h3>Head of Department </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>
          <div>
            <?php the_field('head_of_department_details');?>
          </div>
        </div>
        <div>
          <h3>PROFESSOR</h3>
          <div>
            <?php the_field('professor');?>
          </div>
        </div>

        <div>
          <h3>ASSOCIATE PROFESSOR</h3>
          <div>
            <?php the_field('associate_professor');?>
          </div>
        </div>

        <div>
          <h3>ASSISTANT PROFESSOR</h3>
          <div>
            <?php the_field('assistant_professor');?>
          </div>
        </div>

        <div class="clearfix"></div>


      </div>
      <div class="tab-content fade" id="menu4">
        <div>
          <?php the_field('infrastructure_contents');?>
        </div>

        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('infrastructure_images') ): ?>
            <?php while( have_rows('infrastructure_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('infrastructure_img');?>" title="<?php the_sub_field('infrastructure_title');?>">
                  <img src="<?php the_sub_field('infrastructure_img');?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

            <h4><a href="<?php the_field('infrastructure_document');?>" target="_blank">- Click here for INFRASTRUCTURE document</a></h4>
          </div>
        </div>



      </div>
      <div class="tab-content fade" id="menu5">
        <div>
          <?php the_field('activities_content');?>
        </div>

        <div>
          <?php the_field('association_&_activities');?>
        </div>

        <div>
          <?php the_field('sports_activities');?>
        </div>

        <div>
          <?php the_field('iste-_activities');?>
        </div>

        <div>
          <h3>ELANSA’17- Gallery</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('elansa-gallery') ): ?>
            <?php while( have_rows('elansa-gallery') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href=" <?php the_sub_field('elansa_gallery_image');?>" data-fancybox-group="gallery15" title="">
                  <img src="<?php the_sub_field('elansa_gallery_image');?>" class="img-responsive" /></a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="tab-content fade in" id="menu6">
       <div>
           <?php the_field('research_&_development');?>
       </div>

      </div>
    </div>

    <div class="external_links course_ext-links">
      <label>Also check: </label>
      <ul class="share_icons clearfix">

        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>

</div>


<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
