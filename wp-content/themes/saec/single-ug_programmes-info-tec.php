<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"> <?php _e('About Course') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"><?php _e('Department') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"><?php _e('Faculty') ?></a></li>
        <li><a data-toggle="tab" href="#menu4"><?php _e('Infrastructure') ?></a></li>
        <li><a data-toggle="tab" href="#menu5"><?php _e('Activities') ?></a></li>
        <li><a data-toggle="tab" href="#menu6"><?php _e('Research and development') ?></a></li>
        <li><a data-toggle="tab" href="#menu7"><?php _e('Gallery') ?></a></li>
      </ul>
    </div>


    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <div>
          <h3><?php the_field('our_vision_title');?></h3>
          <p> <?php the_field('our_vision_content');?></p>
        </div>
        <div>

          <h3><?php the_field('our_mission_title');?></h3>
          <ul class="tic_list">
            <?php if( have_rows('our_mission') ): ?>
            <?php while( have_rows('our_mission') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('mission_list'); ?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="">
          <h3><?php the_field('peos_title');?></h3>
          <ul class="tic_list">
            <?php if( have_rows('peos') ): ?>
            <?php while( have_rows('peos') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('peos_mission_list'); ?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3><?php the_field(' programme_outcomes_title');?></h3>
          <ul class="tic_list">
            <?php if( have_rows('programme_outcomes') ): ?>
            <?php while( have_rows('programme_outcomes') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('peos_mission_list'); ?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div class="">
          <h3><?php the_field('programme_specific_outcomes_title');?></h3>
          <table style="height: 224px;" width="826">
            <tbody>
              <tr>
                <td style="text-align: center;" colspan="2" width="677"><?php the_field('programme_specific_outcomes_sub_title');?>:</td>
              </tr>

              <ul class="tic_list">
                <?php if( have_rows('programme_specific_outcomes') ): ?>
                <?php while( have_rows('programme_specific_outcomes') ): the_row(); ?>
                <tr>
                  <td width="59">
                    <p style="text-align: justify;"><strong><?php the_sub_field('pos_number'); ?></strong></p>
                  </td>
                  <td width="616">
                    <p style="text-align: justify;"><?php the_sub_field('programme_specific_outcomes_data'); ?></p>
                  </td>
                </tr>
                <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </tbody>
          </table>
        </div>

        <div>
          <h3><?php the_field('newsletter_title');?></h3>
          <?php if( have_rows('newsletter') ): ?>
          <?php while( have_rows('newsletter') ): the_row(); ?>
          <p>
            <a href="<?php the_sub_field('pdf'); ?>" target="_blank"><?php the_sub_field('newsletter_content'); ?></a>
          </p>
          <?php endwhile; ?>
          <?php endif; ?>

        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div>
          <p> <?php the_field('department_content');?></p>
        </div>

        <div>
          <h4><?php the_field('academic_year');?></h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('academic_images') ): ?>
            <?php while( have_rows('academic_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('images')?>" data-fancybox-group="gallery1" title="<?php the_sub_field('academic_images_title')?>">
                  <img src="<?php the_sub_field('images')?>" class="img-responsive" alt="">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <span><?php the_field('rank_holders_title');?></span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('rank_holders_image') ): ?>
            <?php while( have_rows('rank_holders_image') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('holders_image')?>" data-fancybox-group="gallery2" title="<?php the_sub_field('holders_image_title')?>">
                  <img src="<?php the_sub_field('holders_image')?>" class="img-responsive" alt="<?php the_sub_field('holders_image_title')?>">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>


        <div>
          <h4><?php the_field('academic_year_16-17');?></h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('academic_year_16-17_images') ): ?>
            <?php while( have_rows('academic_year_16-17_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('images')?>" data-fancybox-group="gallery3" title="<?php the_sub_field('academic_images_title')?>">
                  <img src="<?php the_sub_field('images')?>" class="img-responsive" alt="">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>

        <div>
          <ul class="tic_list">
            <h3><?php the_field('achievements_title');?></h3>
            <?php if( have_rows('achievements_list') ): ?>
            <?php while( have_rows('achievements_list') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('achievement_content');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <ul class="tic_list">
            <h3><?php the_field('awards_title');?></h3>
            <?php if( have_rows('awards') ): ?>
            <?php while( have_rows('awards') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('awards_details');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3><?php the_field('advisory_committee_title');?></h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('advisory_committee') ): ?>
            <?php while( have_rows('advisory_committee') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('advisory_committee_image')?>" data-fancybox-group="gallery4" title="<?php the_sub_field('advisory_committee_title')?>">
                  <img src="<?php the_sub_field('advisory_committee_image')?>" class="img-responsive" alt="<?php the_sub_field('advisory_committee_title')?>">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>

        <div>
          <h3> <?php the_field('grants_received_title');?></h3>
          <div style="overflow-x: auto;">
            <table width="936">
              <tbody>
                <tr>
                  <td width="297"><?php the_field('title_proposal');?></td>
                  <td width="639"><?php the_field('proposal');?></td>
                </tr>


                <?php if( have_rows('grants_received') ): ?>
                <?php while( have_rows('grants_received') ): the_row(); ?>
                <tr>
                  <td width="297">
                    <p style="text-align: justify;"><strong><?php the_sub_field('proposal_title'); ?></strong></p>
                  </td>
                  <td width="639">
                    <p style="text-align: justify;"><?php the_sub_field('proposal_content'); ?></p>
                  </td>
                </tr>
                <?php endwhile; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>

        <div class="">
          <h3><?php the_field('journals_title');?> </h3>
          <ul class="tic_list">
            <?php if( have_rows('journals') ): ?>
            <?php while( have_rows('journals') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('journals_items');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3><?php the_field('registered_scholars_title');?> </h3>
          <p><?php the_field('registered_scholars_sub_title');?></p>
          <div style="overflow-x: auto;">
            <table style="height: 321px;" width="726">
              <tbody>
                <tr>
                  <td width="132"><strong><?php the_field('scholars_name');?></strong></td>
                  <td width="528"><strong><?php the_field('candidates_titles');?></strong></td>
                </tr>

                <?php if( have_rows('registered_scholars') ): ?>
                <?php while( have_rows('registered_scholars') ): the_row(); ?>
                <tr>
                  <td width="132">
                    <p><?php the_sub_field('scholars__name'); ?></p>
                  </td>
                  <td width="528">
                    <p><?php the_sub_field('scholars__name'); ?></p>
                  </td>
                </tr>
                <?php endwhile; ?>
                <?php endif; ?>

              </tbody>
            </table>
          </div>
        </div>

        <div>
          <h3><?php the_field('modules_developed_title');?></h3>
          <div>
            <?php the_field('course_modules_developed');?>
          </div>

        </div>

        <div>
          <h3><?php the_field('university_rank_holders_title');?></h3>
          <span><?php the_field('year_2008-2012');?></span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('rank_holders_images_2008-2012') ): ?>
            <?php while( have_rows('rank_holders_images_2008-2012') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('holders_images')?>" data-fancybox-group="gallery5" title="<?php the_sub_field('holders_images_title')?>">
                  <img src="<?php the_sub_field('holders_images')?>" class="img-responsive" alt="<?php the_sub_field('holders_images_title')?>">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <span><?php the_field('year_2009-2013');?></span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('rank_holders_images_2009-2013') ): ?>
            <?php while( have_rows('rank_holders_images_2009-2013') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('images_2008-2012')?>" data-fancybox-group="gallery6" title="<?php the_sub_field('images_2008-2012_title')?>">
                  <img src="<?php the_sub_field('images_2008-2012')?>" class="img-responsive" alt="<?php the_sub_field('images_2008-2012_title')?>">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <span><?php the_field('year_2011-2015');?></span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('rank_holders_images_2011-2015') ): ?>
            <?php while( have_rows('rank_holders_images_2011-2015') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('holders_images')?>" data-fancybox-group="gallery7" title="<?php the_sub_field('holders_images_title')?>">
                  <img src="<?php the_sub_field('holders_images')?>" class="img-responsive" alt="<?php the_sub_field('holders_images_title')?>">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('rank_table');?>
        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div class="head_department">
          <h3>Head of Department </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>
          <div>
            <?php the_field('head_of_department_details');?>
          </div>
        </div>

        <div>
          <h3>ASSOCIATE PROFESSOR</h3>

         <div style="overflow-x: auto;">
            <?php the_field('associate_professor_details');?>
          </div>
        </div>
       <div style="overflow-x: auto;">
          <h3>ASSISTANT PROFESSOR</h3>
          <div>
            <?php the_field('assistant_professor_details');?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu4">

        <div>
          <h3>CLASS ROOMS</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('class_rooms_images') ): ?>
            <?php while( have_rows('class_rooms_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('rooms_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('class_rooms_image_title');?>">
                  <img src="<?php the_sub_field('rooms_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('class_rooms_details');?>
        </div>

        <div>
          <h3>TUTORIAL ROOMS</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('tutorial_rooms_images') ): ?>
            <?php while( have_rows('tutorial_rooms_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('tutorial_rooms_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('tutorial_rooms_image_title');?>">
                  <img src="<?php the_sub_field('tutorial_rooms_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('tutorial_rooms_details');?>
        </div>

        <div>
          <h3>SEMINAR HALL</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('seminar_hall_images') ): ?>
            <?php while( have_rows('seminar_hall_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('seminar_hall__images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('seminar_hall_image_title');?>">
                  <img src="<?php the_sub_field('seminar_hall__images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('seminar_hall_details');?>
        </div>

        <div>
          <h3>HOD ROOM</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('hod_room_images') ): ?>
            <?php while( have_rows('hod_room_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('hod_rooms_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('hod_room_images_title');?>">
                  <img src="<?php the_sub_field('hod_rooms_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('hod_room_details');?>
        </div>

        <div>
          <h3>STAFF ROOMS</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('staff_rooms') ): ?>
            <?php while( have_rows('staff_rooms') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('staff_rooms_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('staff_rooms_images_title');?>">
                  <img src="<?php the_sub_field('staff_rooms_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('staff_rooms_details');?>
        </div>

        <div>
          <h3>DEPARTMENT OFFICE ROOM</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('department_office_room') ): ?>
            <?php while( have_rows('department_office_room') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('department_office_room_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('department_office_room_images_title');?>">
                  <img src="<?php the_sub_field('department_office_room_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('department_office_room_details');?>
        </div>

        <div>
          <h3>COUNSELING ROOM</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('counseling_room') ): ?>
            <?php while( have_rows('counseling_room') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('counseling_room_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('counseling_room_images_title');?>">
                  <img src="<?php the_sub_field('counseling_room_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('counseling_room_details');?>
        </div>

        <div>
          <h3>AUDITORIUM</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('auditorium') ): ?>
            <?php while( have_rows('auditorium') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('auditorium_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('auditorium_images_title');?>">
                  <img src="<?php the_sub_field('auditorium_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('auditorium_details');?>
        </div>

        <div>
          <h3>DEPARTMENT LIBRARY</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('department_library') ): ?>
            <?php while( have_rows('department_library') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('department_library_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('department_library_images_title');?>">
                  <img src="<?php the_sub_field('department_library_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('department_library_details');?>
        </div>

        <div>
          <h3>ADA LOVELACE LABORATORIE (Lab 11)</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('ada_lovelace_laboratorie') ): ?>
            <?php while( have_rows('ada_lovelace_laboratorie') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('ada_lovelace_laboratorie_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('ada_lovelace_laboratorie_images_title');?>">
                  <img src="<?php the_sub_field('ada_lovelace_laboratorie_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <div>
            <?php the_field(' ada_lovelace_laboratorie_details');?>
          </div>
        </div>
        <div>
          <h3>RONALD RIVEST LABORATORIE (Lab 12)</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('ronald_rivest_laboratorie') ): ?>
            <?php while( have_rows('ronald_rivest_laboratorie') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('rivest_laboratorie_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('rivest_laboratorie_images_title');?>">
                  <img src="<?php the_sub_field('rivest_laboratorie_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('ronald_rivest_laboratorie_details');?>
        </div>

        <div>
          <h3>ALAN TURING LABORATORIE (Lab 13)</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('alan_turing_laboratorie') ): ?>
            <?php while( have_rows('alan_turing_laboratorie') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('turing_laboratorie_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('turing_laboratorie_images_title');?>">
                  <img src="<?php the_sub_field('turing_laboratorie_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('alan_turing_laboratorie_details');?>
        </div>

        <div>
          <h3>EDGAR CODD LABORATORIE (Lab 13)</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('edgar_codd_laboratorie') ): ?>
            <?php while( have_rows('edgar_codd_laboratorie') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('edgar_codd_laboratorie_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('edgar_codd_laboratorie_images_title');?>">
                  <img src="<?php the_sub_field('edgar_codd_laboratorie_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <?php the_field('edgar_codd_laboratorie_details');?>
        </div>

        <div>
          <h3>Dr.A.P.J.ABDUL KALAM DST-FIST R&D LABORATORIE </h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('abdhul_kalam_laboratorie') ): ?>
            <?php while( have_rows('abdhul_kalam_laboratorie') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('kalam_laboratorie_images');?>" data-fancybox-group="gallery8" title="<?php the_sub_field('kalam_laboratorie_images_title');?>">
                  <img src="<?php the_sub_field('kalam_laboratorie_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>
        <div>
          <?php the_field('abdhul_kalam_laboratorie_details');?>
        </div>

        <div>
          <h3>Infrastructure and Facilities </h3>

          <div>
            <?php the_field('infrastructure_and_facilities');?>
          </div>
        </div>
      </div>

      <div class="tab-content fade" id="menu5">
        <div class="industrial_visits">
          <h3><?php the_field('industrial_visits_title');?></h3>
          <ul class="tic_list">
            <?php if( have_rows('industrial_visits') ): ?>
            <?php while( have_rows('industrial_visits') ): the_row(); ?>
            <li>
              <?php the_sub_field('industrial_visits_lists');?>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div><span>For more details about GL <a href="<?php the_field('more_details_pdf');?>" target="_blank"> click here</a></span></div>
        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('gl_images') ): ?>
            <?php while( have_rows('gl_images') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('gl_more_images');?>" data-fancybox-group="gallery10" title="<?php the_sub_field('gl_more_image_title');?>">
                  <img src="<?php the_sub_field('gl_more_images');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h3><?php the_field('conferences_title');?></h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('conferences') ): ?>
            <?php while( have_rows('conferences') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('conferences_image');?>" data-fancybox-group="gallery11" title="<?php the_sub_field('conferences_image_title');?>">
                  <img src="<?php the_sub_field('conferences_image');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h3><?php the_field('technical_symposium_title');?></h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('technical_symposium') ): ?>
            <?php while( have_rows('technical_symposium') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('technical_symposium_image');?>" data-fancybox-group="gallery11" title="<?php the_sub_field('technical_symposium_title');?>">
                  <img src="<?php the_sub_field('technical_symposium_image');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <div>
            <?php the_field('technical_symposium_table');?>
          </div>

        </div>

        <div>
          <h3><?php the_field('symposiums_title');?> :</h3>
          <div>
            <?php the_field('technical_symposium_table');?>
          </div>

        </div>

        <div>
          <h3><?php the_field('internship_title');?></h3>
          <div><?php the_field('symposium_table');?></div>
        </div>

        <div>
          <span><strong>INTERNSHIP TRAINING ATTENDED BY STUDENTS</strong> &nbsp; <strong>ACADEMIC YEAR 2015-2016</strong> &nbsp;</span>

          <div>
            <?php the_field('inplant_training_table_2');?>
          </div>
        </div>

        <div><span><strong>ACADEMIC YEAR 2014-2015</strong> &nbsp;</span>
          <div>
            <?php the_field('academic_year_2014-2015_table_1');?>
          </div>
        </div>

        <div>
          <div>
            <?php the_field('academic_year_2024-2015_table_2');?>
          </div>
        </div>

        <div>
          <div>
            <?php the_field('academic_year_2024-2015_table_3');?>
          </div>
        </div>

        <div>
          <h3><?php the_field('events_organised_title');?></h3>
          <ul class="tic_list">
            <?php if( have_rows('events_organised') ): ?>
            <?php while( have_rows('events_organised') ): the_row(); ?>
            <li><?php the_sub_field('events_organised_text');?><a style="color: #0000ff;" href="<?php the_sub_field('events__pdf');?>" target="_blank"> -click here</a></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3>MoUs</h3>
          <div><?php the_field('mous');?></div>
        </div>

        <div>
          <h4><?php the_field('committee_members_title');?></h4>
          <ul class="tic_list">
            <?php if( have_rows('committee_members') ): ?>
            <?php while( have_rows('committee_members') ): the_row(); ?>
            <li><?php the_sub_field('committee_members_list');?></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h4><?php the_field('advisory_committee_members_title');?></h4>
          <ul class="tic_list">
            <?php if( have_rows('advisory_committee_members') ): ?>
            <?php while( have_rows('advisory_committee_members') ): the_row(); ?>
            <li><?php the_sub_field('advisory_committee_members_list');?></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3>Placement Details</h3>
          <div>
            <?php the_field('placement_details');?>
          </div>

        </div>

        <div>
          <h4>MAJOR RECRUITERS</h4>
          <div>
            <div class="ach_sec clearfix">
              <?php if( have_rows('major_recruiters') ): ?>
              <?php while( have_rows('major_recruiters') ): the_row(); ?>
              <div class="col-sm-3">
                <div>
                  <a class="fancybox" href="<?php the_sub_field('major_recruiters_image');?>" data-fancybox-group="gallery1">
                    <img src="<?php the_sub_field('major_recruiters_image');?>" class="img-responsive" />
                  </a>
                </div>
              </div>

              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div>
          <h3>e-Newsletter</h3>
          <?php if( have_rows('e-newsletter') ): ?>
          <?php while( have_rows('e-newsletter') ): the_row(); ?>
          <span><a target="_blank" href="<?php the_sub_field('e-newsletter_pdf');?>"><?php the_sub_field('e-newsletter_label');?></a></span>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

      </div>
      <div class="tab-content fade" id="menu6">
        <div>
          <h3><?php the_field('grants_received_title');?> </h3>

          <table width="100%">
            <tbody>
              <tr>
                <td width="330">Title of the Proposal</td>
                <td width="330">An Entrepreneurship Awareness Camp (EAC)</td>
              </tr>
              <?php if( have_rows('grants_received_block_one') ): ?>
              <?php while( have_rows('grants_received_block_one') ): the_row(); ?>
              <tr>
                <td width="330"><?php the_sub_field('title_of_proposal');?></td>
                <td width="330"><?php the_sub_field('proposal_data');?></td>
              </tr>
              <?php endwhile; ?>
              <?php endif; ?>


            </tbody>
          </table>
          <br><br><br>
          <table width="100%">
            <tbody>
              <tr>
                <td width="330">Title of the Proposal</td>
                <td width="330">FDP on Artificial Intelligence & Advanced Machine Learning Using Data Science</td>
              </tr>
              <?php if( have_rows('grants_received_block_two') ): ?>
              <?php while( have_rows('grants_received_block_two') ): the_row(); ?>
              <tr>
                <td width="330"><?php the_sub_field('title_of_proposal_one');?></td>
                <td width="330"><?php the_sub_field('proposal_data_two');?></td>
              </tr>
              <?php endwhile; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>

        <div>
          <h2 class="title_line">R&D publications :</h2><br><br><br>

          <ul class="tic_list">
            <h3><?php the_field('year_title_17-18');?></h3>
            <?php if( have_rows('2017-2018_publications') ): ?>
            <?php while( have_rows('2017-2018_publications') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('publications_list_17-18');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <br><br>
          <ul class="tic_list">
            <h3><?php the_field('year_title_16-17');?></h3>
            <?php if( have_rows('2016-2017_publications') ): ?>
            <?php while( have_rows('2016-2017_publications') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('publications_list_16-17');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <br><br>
          <ul class="tic_list">
            <h3><?php the_field('year_title_15-16');?></h3>
            <?php if( have_rows('2015-2016_publications') ): ?>
            <?php while( have_rows('2015-2016_publications') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('publications_list_15-16');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <br><br>
          <ul class="tic_list">
            <h3><?php the_field('year_title_14-15');?></h3>
            <?php if( have_rows('2014-2015_publications') ): ?>
            <?php while( have_rows('2014-2015_publications') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('publications_list_14-15');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <br><br>
          <ul class="tic_list">
            <h3><?php the_field('year_title_13-14');?></h3>
            <?php if( have_rows('2013-2014_publications') ): ?>
            <?php while( have_rows('2013-2014_publications') ): the_row(); ?>
            <li>
              <p><?php the_sub_field('publications_list_13-14');?></p>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

      </div>
      <div class="tab-content fade" id="menu7">
        <div>
          <h3>Gallery </h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('info_tech_gallery') ): ?>
            <?php while( have_rows('info_tech_gallery') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('gallery_image');?>" data-fancybox-group="gallery11" title="<?php the_sub_field('gallery_title');?>">
                  <img src="<?php the_sub_field('gallery_image');?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="external_links course_ext-links">
      <label>Also check: </label>
      <ul class="share_icons clearfix">

        <?php if( have_rows('external_links') ): ?>
        <?php while( have_rows('external_links') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</div>

<?php include('virtual-tour-strip.php');?>
<section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>

      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>

      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
