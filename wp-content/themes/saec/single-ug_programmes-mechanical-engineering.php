<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="courses_banner" style="background-image: url('<?php echo $image[0]; ?>')">
  <div class="container">
    <div class="course_title">
      <div>
        <span>
          <img src="<?php the_field('programmes_icon'); ?>" alt="">
        </span>
        <h1><?php the_title()?></h1>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="course-tab-contain">
  <div class="container">
    <div class="tab-menu">
      <ul>
        <li class="active"><a data-toggle="tab" href="#menu1"><?php _e('About') ?></a></li>
        <li><a data-toggle="tab" href="#menu2"><?php _e('Department') ?></a></li>
        <li><a data-toggle="tab" href="#menu3"><?php _e('Faculty') ?> </a></li>
        <li><a data-toggle="tab" href="#menu4"><?php _e('Infrastructure') ?> </a></li>
        <li><a data-toggle="tab" href="#menu5"><?php _e('Activities') ?></a></li>
        <li><a data-toggle="tab" href="#menu6"><?php _e('Research & Development') ?> </a></li>
        <li><a data-toggle="tab" href="#menu7"><?php _e('Gallery') ?> </a></li>
      </ul>
    </div>
    <div class="tab-content-wrap clearfix">
      <div class="tab-content fade in active" id="menu1">
        <div>
          <?php the_field('about');?>
        </div>
        <div class="course_feature">
          <?php if( have_rows('course_feature') ): ?>
          <?php while( have_rows('course_feature') ): the_row(); ?>
          <div class="feat-box">
            <a class="full_cont-link" href="<?php the_field('download_brochure')?>" target="_blank"></a>
            <i>
              <img src="<?php the_sub_field('feature_icon'); ?>" alt="" class="img-responsive">
            </i>
            <div>
              <label><?php the_sub_field('feature_label'); ?></label>
              <p><?php the_sub_field('feature_sub_text'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="tab-content fade" id="menu2">
        <div>
          <?php the_field('department'); ?>
        </div>
        <div>
          <?php the_field('achievements'); ?>
        </div>
        <div>
          <h3>Awards</h3>
          <h4>ISIE AWARDS</h4>
          <?php if( have_rows('awards') ): ?>
          <?php while( have_rows('awards') ): the_row(); ?>
          <span> <?php the_sub_field('awards_title'); ?></span>
          <p> <?php the_sub_field('awards_sub_title'); ?></p>
          <div class="ach_sec clearfix">
            <?php if( have_rows('awards_image') ): ?>
            <?php while( have_rows('awards_image') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('img'); ?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('img'); ?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div>
          <?php the_field('teaching_awards_list'); ?>
        </div>
        <div>
          <h3>Research and Development</h3>
          <span>FUNDED PROJECTS</span>
          <div class="ach_sec clearfix">
            <?php if( have_rows('funded_projects') ): ?>
            <?php while( have_rows('funded_projects') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('funded_projects_images'); ?>" data-fancybox-group="gallery1" title="<?php the_sub_field('funded_projects_images_title'); ?>">
                  <img src="<?php the_sub_field('funded_projects_images'); ?>" class="img-responsive">
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        <div>
          <span>SCOPUS</span>
          <?php the_field('scopus'); ?>
        </div>
        <div>
          <span>GOOGLE SCHOLAR</span>
          <?php the_field('google_scholar'); ?>
        </div>

        <div>
          <span>RESEARCHGATE</span>
          <?php the_field('researchgate'); ?>
        </div>
        <div>
          <?php the_field('academic_research'); ?>
        </div>
      </div>
      <div class="tab-content fade" id="menu3">
        <div>
          <h3>Head of Department </h3>
          <div>
            <img src="<?php the_field('head_of_department');?>" class="img-responsive" />
          </div>

          <div>
            <?php the_field('head_of_department_details');?>
          </div>
        </div>
        <div>
          <h3></h3>
          <h3>PROFESSOR</h3>
          <div>
            <?php the_field('professor');?>
          </div>
        </div>
        <div>
          <h3>ASSOCIATE PROFESSOR</h3>
          <div>
            <?php the_field('associate_professor');?>
          </div>
        </div>
        <div>
          <h3>ASSISTANT PROFESSOR</h3>
          <div>
            <?php the_field('assistant_professor');?>
          </div>
        </div>
      </div>
      <div class="tab-content fade" id="menu4">
        <div>
          <?php if( have_rows('infrastructure') ): ?>
          <?php while( have_rows('infrastructure') ): the_row(); ?>
          <h3><?php the_sub_field('manufacturing_technology_title'); ?></h3>
          <div class="ach_sec clearfix">
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('manufacturing_technology_image'); ?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('manufacturing_technology_image'); ?>" class="img-responsive" />
                </a>
              </div>
            </div>
          </div>
          <div>
            <?php the_sub_field('manufacturing_technology_table'); ?>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>


      </div>
      <div class="tab-content fade" id="menu5">
        <div>
          <h3><?php the_field('activities_head'); ?></h3>
          <?php if( have_rows('activities') ): ?>
          <?php while( have_rows('activities') ): the_row(); ?>
          <p><?php the_sub_field('activities_sub_title'); ?></p>
          <div class="ach_sec clearfix">
            <?php if( have_rows('activities_image') ): ?>
            <?php while( have_rows('activities_image') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('activities_img'); ?>" data-fancybox-group="gallery1" title="">
                  <img src="<?php the_sub_field('activities_img'); ?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="news-updates">
          <h3>News and Updates</h3>
          <ul class="tic_list">
            <?php if( have_rows('news_and_updates') ): ?>
            <?php while( have_rows('news_and_updates') ): the_row(); ?>
            <li>
              <a href="<?php the_sub_field('news_and_updates_pdf'); ?>">
                <?php the_sub_field('news_and_updates_list'); ?></a>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div>
          <h3>Internship / In-Plant Training</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('internship__in-plant_training') ): ?>
            <?php while( have_rows('internship__in-plant_training') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('internship_images'); ?>" data-fancybox-group="gallery1" title="<?php the_sub_field('internship_images_title'); ?>">
                  <img src="<?php the_sub_field('internship_images'); ?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h3>Projects</h3>
          <div class="ach_sec clearfix">
            <?php if( have_rows('projects') ): ?>
            <?php while( have_rows('projects') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('projects_images'); ?>" data-fancybox-group="gallery2" title="<?php the_sub_field('projects_images_title'); ?>">
                  <img src="<?php the_sub_field('projects_images'); ?>" class="img-responsive" />
                </a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div>
          <h3>Press Release</h3>
          <h4>Indian Express News Paper</h4>
          <div class="ach_sec clearfix">
            <?php if( have_rows('indian_express_news_paper') ): ?>
            <?php while( have_rows('indian_express_news_paper') ): the_row(); ?>
            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="<?php the_sub_field('press_release_images'); ?>" data-fancybox-group="gallery3" title="<?php the_sub_field('press_release_images_title'); ?>">
                  <img src="<?php the_sub_field('press_release_images'); ?>" class="img-responsive" /></a>
              </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>







      </div>
      <div class="tab-content fade" id="menu6">
        <div>
          <p> <?php the_field('research_&_development_content');?></p>
        </div>
        <div class="space-updates">
          <h3>ACADEMIC RESEARCH</h3>
          <?php if( have_rows('academic_research_field') ): ?>
          <?php while( have_rows('academic_research_field') ): the_row(); ?>
          <span><?php the_sub_field('academic_research_title'); ?></span>
          <ul class="tic_list add-spacing">
            <?php if( have_rows('academic_research_list') ): ?>
            <?php while( have_rows('academic_research_list') ): the_row(); ?>
            <li><a href="<?php the_sub_field('academic_research_list_pdf'); ?>" target="_blank">
                <?php the_sub_field('research_lists'); ?></a></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>


        <div class="space-updates">
          <h3>CENTRE FOR EXCELLENCE</h3>
          <?php if( have_rows('centre_for_excellence') ): ?>
          <?php while( have_rows('centre_for_excellence') ): the_row(); ?>
          <span><?php the_sub_field('centre_excellence_title'); ?> </span>
          <ul class="tic_list add-spacing">
            <?php if( have_rows('centre_excellence_list') ): ?>
            <?php while( have_rows('centre_excellence_list') ): the_row(); ?>
            <li><a href="<?php the_sub_field('centre_excellence_list_pdf'); ?>" target="_blank"><?php the_sub_field('centre__excellence_lists'); ?></a></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="space-updates">
          <h3>CENTRE FOR EXCELLENCE IN SMART AND ADVANCED MATERIALS (C-SAM)</h3>
          <ul class="tic_list add-spacing">
            <?php if( have_rows('centre_for_excellence_in_smart') ): ?>
            <?php while( have_rows('centre_for_excellence_in_smart') ): the_row(); ?>
            <li><a href="<?php the_sub_field('centre_for_excellence_in_smart_pdf'); ?>">
                <?php the_sub_field('centre_for_excellence_in_smart_fields'); ?></a></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>


        <div>
          <span>CERIA SPARK AUTOMATION CENTRE </span>
        </div>

        <div>
          <?php if( have_rows('ceria_spark_automation_centre') ): ?>
          <?php while( have_rows('ceria_spark_automation_centre') ): the_row(); ?>
          <span><?php the_sub_field('ceria_spark_automation_centre_title'); ?></span>
          <ul class="tic_list">
            <?php if( have_rows('ceria_spark_automation_centre_list') ): ?>
            <?php while( have_rows('ceria_spark_automation_centre_list') ): the_row(); ?>
            <li><a href="<?php the_sub_field('automation_centre_pdf'); ?>" target="_blank">
                <?php the_sub_field('automation_centre_lists'); ?>
              </a></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div>
          <h3>PEER REVIEWS / WEBSITE LINKS </h3>
          <ul class="tic_list">
            <?php if( have_rows('reviews__website_links') ): ?>
            <?php while( have_rows('reviews__website_links') ): the_row(); ?>
            <li><a href="<?php the_sub_field('reviews_website_links_pdf'); ?>" target="_blank">
                <?php the_sub_field('reviews_website_links_fields '); ?></a></li>
            <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

      </div>
      <div class="tab-content fade" id="menu7">
        <div>
          <div class="ach_sec clearfix">
            <?php if( have_rows('mechanical_gallery') ): ?>
            <?php while( have_rows('mechanical_gallery') ): the_row(); ?>

            <div class="col-sm-3">
              <div>
                <a class="fancybox" href="  <?php the_sub_field('mechanical_gallery_images'); ?>" data-fancybox-group="gallery1">
                  <img src="  <?php the_sub_field('mechanical_gallery_images'); ?>" class="img-responsive">
                </a>
              </div>
            </div>

            <?php endwhile; ?>
            <?php endif; ?>

            <!--
            <?php
            for($i=1; $i<33; $i++)
            echo '<div class="col-sm-3"><div><a class="fancybox" href="img/mech-gallery'.$i.'.jpg" data-fancybox-group="gallery1" ><img src="img/mech-gallery'.$i.'.jpg" class="img-responsive"></a></div></div>';
            ?>
-->


          </div>
        </div>
      </div>
      <div class="external_links course_ext-links">
        <label>Also check: </label>
        <ul class="share_icons clearfix">

          <?php if( have_rows('external_links') ): ?>
          <?php while( have_rows('external_links') ): the_row(); ?>
          <li>
            <a href="<?php the_sub_field('external_link');?>" target="_blank"><?php the_sub_field('external_links_text');?></a>
          </li>
          <?php endwhile; ?>
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </div>
</div>

<?php include('virtual-tour-strip.php');?>
  <section class="clg_range">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2 class="counter" data-count="<?php the_field('student_placed_count_gallery', 680);?>">0</h2>
          <span class="plus-symbol">+</span>
          <span><?php the_field('student_placed_count_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('university_rank_gallery', 680);?></label></h2>
          <span><?php the_field('university_rank_label_gallery', 680);?></span>
        </div>
      </div>
      
      <div class="col-sm-4 clg_range_box">
        <div>
          <h2><label class="counter"><?php the_field('square_feet_count_gallery', 680);?></label>L</h2>
          <span><?php the_field('square_feet_label_gallery', 680);?></span>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
