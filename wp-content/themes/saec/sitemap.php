<?php

/**
  Template Name: sitemap
*/

get_header();
?>


<section class="inner">
  <div class="container content-only sitemappage">
    <h2 class="title_line">Sitemap</h2>
    <div class="grid-layouts">

      <div class="grid-items main-clm">
        <div>
          <?php echo do_shortcode( '[wp_sitemap_page only="page"]' ); ?>
        </div>
      </div>
      <div class="row">
        <div class="grid-items single-clm col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div>
            <?php echo do_shortcode( '[wp_sitemap_page only="ug_programmes"]' ); ?>
          </div>
        </div>
        <div class="grid-items single-clm col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div>
            <?php echo do_shortcode( '[wp_sitemap_page only="pg_programmes"]' ); ?>
          </div>
        </div>

        <div class="grid-items single-clm col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div>
            <?php echo do_shortcode( '[wp_sitemap_page only="resource"]' ); ?>
          </div>
        </div>

        <div class="grid-items single-clm col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div>
            <?php echo do_shortcode( '[wp_sitemap_page only="campus_activities"]' ); ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<?php
get_footer();
