<?php

/**
  Template Name: thank-you
*/


get_header();
?>

<section class="thankyou-page">
  <div class="container">
    <div class="thankyou">
     
     <div class="thanku-contents">
     <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tick-icon.png" alt="">
      <h1><?php echo get_the_title(); ?></h1>
      <p>Thank You for subscribing to our exclusive Newsletter</p>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
