<?php

/**
  Template Name: virtual-campus-tour
*/

get_header();
?>


<div class="container virtualtourwrap">
  <div style="height:400px" class="videowrap">
    <noscript>
      <p class="warning">Please enable Javascript!</p>
    </noscript>
  </div>
  
  <section class="inner vtour-wrap">
    <h3>Please click the options below to view our 360 Degree Virtual Reality Tours</h3>
    <div class="row thumbwrap">
      <?php if( have_rows('virtual_reality_tours') ): ?>
      <?php while( have_rows('virtual_reality_tours') ): the_row(); ?>
      <div class="col-lg-3 col-sm-4 col-xs-6">
        <div align="center">
          <a href="#">
            <img src="<?php the_sub_field('tours_images');?>" />
          </a>
        </div>
        <h4><?php the_sub_field('tours_title');?></h4>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </section>

<!--
  <div class="container">
    <audio controls loop autoplay style="display:none;">
      <source src="<?php echo $siteurl ?>/virtual-tour/Music.ogg" type="audio/ogg">
      <source src="<?php echo $siteurl ?>/virtual-tour/Music.mp3" type="audio/mpeg">
      Your browser does not support the audio tag.
    </audio>
  </div>
-->
</div>

<?php
get_footer();
