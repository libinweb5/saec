<section class="dept_common">
  <div class="container">
    <div class="common_box_wrap">
      <div class="row">
        <div class="col-xs-6">
          <div class="common_lifeat">
            <a href="campus-life" class="full_cont-link"></a>
            <div>
              <h3>A Campus To <br>Fall In Love With!</h3>
              <label class="title_line">Life @ SAEC</label>
            </div>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="common_virtual">
            <a href="<?php echo get_page_link( get_page_by_path( 'virtual-campus-tour' ) ); ?>" class="full_cont-link"></a>
            <div class="ply-btn">
              <svg width="80" height="80" class="play-icon" viewBox="0 0 100 100">
                <rect x="4" y="4" width="92" height="92" class="box" rx="50" ry="50"></rect>
                <polygon points="44,42 62,52 44,62" class="play"></polygon>
              </svg>
            </div>
            <h3>
              <div class="virtual_title">360<sup>°</sup><br>Virtual Tour</div>
            </h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
